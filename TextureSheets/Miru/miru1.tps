<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Miru/miru1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Miru/miru1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru1/idle_79.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>102,97,204,194</rect>
                <key>scale9Paddings</key>
                <rect>102,97,204,194</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2/laughter_013.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,106,194,212</rect>
                <key>scale9Paddings</key>
                <rect>97,106,194,212</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru1/idle_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2/laughter_013.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
