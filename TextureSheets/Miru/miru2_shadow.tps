<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Miru/miru2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Miru/miru2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_109.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>178,67,356,134</rect>
                <key>scale9Paddings</key>
                <rect>178,67,356,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>213,77,426,154</rect>
                <key>scale9Paddings</key>
                <rect>213,77,426,154</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru2_shadow/laughter_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3_shadow/idle2_03.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
