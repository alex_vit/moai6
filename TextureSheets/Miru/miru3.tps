<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Miru/miru3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Miru/miru3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/miru3/idle2_94.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>119,117,238,234</rect>
                <key>scale9Paddings</key>
                <rect>119,117,238,234</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/miru3/idle2_89.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
