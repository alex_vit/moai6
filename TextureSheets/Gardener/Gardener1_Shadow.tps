<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Gardener/Gardener1_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Gardener/Gardener1_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/final_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>91,33,182,66</rect>
                <key>scale9Paddings</key>
                <rect>91,33,182,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_left_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_look_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_right_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/action_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/ask_help_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/final_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gardener1_shadow/idle_head_up_3.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
