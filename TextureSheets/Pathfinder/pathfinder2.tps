<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Pathfinder/pathfinder2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Pathfinder/pathfinder2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1/run_up22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,78,102,156</rect>
                <key>scale9Paddings</key>
                <rect>51,78,102,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/action_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder2/run_right22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,76,148,152</rect>
                <key>scale9Paddings</key>
                <rect>74,76,148,152</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1/run_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/run_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder2/action_left16.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
