<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Pathfinder/pathfinder1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Pathfinder/pathfinder1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right73.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>98,41,196,82</rect>
                <key>scale9Paddings</key>
                <rect>98,41,196,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/action_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_down73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_left73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/pathfinder1_shadow/idle_right72.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
