<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/MayanShaman/mayanshaman1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanShaman/mayanshaman1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>180</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk200.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>124,48,248,96</rect>
                <key>scale9Paddings</key>
                <rect>124,48,248,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1_shadow/talk024.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
