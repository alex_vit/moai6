<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanShaman/mayanshaman2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanShaman/mayanshaman2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_109.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,78,122,156</rect>
                <key>scale9Paddings</key>
                <rect>61,78,122,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_100.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,78,142,156</rect>
                <key>scale9Paddings</key>
                <rect>71,78,142,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle2_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman2/idle1_011.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
