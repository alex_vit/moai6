<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanShaman/mayanshaman1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanShaman/mayanshaman1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_shaman1/talk200.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,79,126,158</rect>
                <key>scale9Paddings</key>
                <rect>63,79,126,158</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_shaman1/talk008.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
