<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Lake.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Lake.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_01.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,34,22,68</rect>
                <key>scale9Paddings</key>
                <rect>11,34,22,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_19.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_20.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_21.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_22.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_23.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_1_24.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,34,44,68</rect>
                <key>scale9Paddings</key>
                <rect>22,34,44,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_00.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,36,31</rect>
                <key>scale9Paddings</key>
                <rect>18,16,36,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_19.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_20.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_2_21.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,31,36,62</rect>
                <key>scale9Paddings</key>
                <rect>18,31,36,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish1_3_19.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_2_14.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,28,30,56</rect>
                <key>scale9Paddings</key>
                <rect>15,28,30,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_03.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,21,60,42</rect>
                <key>scale9Paddings</key>
                <rect>30,21,60,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_1_17.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,21,30,42</rect>
                <key>scale9Paddings</key>
                <rect>15,21,30,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_04.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,23,66,46</rect>
                <key>scale9Paddings</key>
                <rect>33,23,66,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_2_19.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,23,33,46</rect>
                <key>scale9Paddings</key>
                <rect>17,23,33,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_01.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,20,56,40</rect>
                <key>scale9Paddings</key>
                <rect>28,20,56,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish2_3_14.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,20,28,40</rect>
                <key>scale9Paddings</key>
                <rect>14,20,28,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_1_14.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,26,22,52</rect>
                <key>scale9Paddings</key>
                <rect>11,26,22,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/fish3_3_15.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,31,38,62</rect>
                <key>scale9Paddings</key>
                <rect>19,31,38,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/lake1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/lake1_overlay.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>103,81,206,162</rect>
                <key>scale9Paddings</key>
                <rect>103,81,206,162</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/lake2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/lake2_overlay.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,86,274,172</rect>
                <key>scale9Paddings</key>
                <rect>137,86,274,172</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/lake3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/lake3_overlay.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>135,88,270,176</rect>
                <key>scale9Paddings</key>
                <rect>135,88,270,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_19.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_20.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_21.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_22.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_23.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_24.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_25.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_26.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_27.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_28.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_29.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_30.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_31.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_32.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_33.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_34.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_35.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_36.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_37.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_38.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_39.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_40.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_41.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_42.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings1_43.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,30,106,60</rect>
                <key>scale9Paddings</key>
                <rect>53,30,106,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_19.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_20.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_21.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_22.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_23.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_24.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_25.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_26.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_27.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_28.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_29.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_30.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_31.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_32.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_33.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_34.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_35.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_36.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_37.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_38.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_39.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_40.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_41.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_42.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings2_43.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,23,84,46</rect>
                <key>scale9Paddings</key>
                <rect>42,23,84,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_00.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_01.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_02.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_03.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_04.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_05.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_06.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_07.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_08.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_09.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_10.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_11.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_12.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_13.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_14.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_15.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_16.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_17.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_18.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_19.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_20.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_21.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_22.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_23.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_24.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_25.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_26.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_27.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_28.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_29.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_30.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_31.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_32.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_33.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_34.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_35.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_36.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_37.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_38.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_39.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_40.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_41.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_42.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_rings3_43.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,17,60,34</rect>
                <key>scale9Paddings</key>
                <rect>30,17,60,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_0.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_1.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_2.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_3.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_4.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_5.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash1_6.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,15,54,30</rect>
                <key>scale9Paddings</key>
                <rect>27,15,54,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_0.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_1.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_2.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_3.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_4.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_5.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,16,40,32</rect>
                <key>scale9Paddings</key>
                <rect>20,16,40,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash2_6.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,8,40,16</rect>
                <key>scale9Paddings</key>
                <rect>20,8,40,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash3_0.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash3_1.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash3_2.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash3_3.tga</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lake/water_splash3_4.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,26,26</rect>
                <key>scale9Paddings</key>
                <rect>13,13,26,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_20.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_21.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_22.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_23.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_1_24.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_20.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_2_21.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish1_3_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_1_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_2_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish2_3_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_1_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_2_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/fish3_3_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/lake1_overlay.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/lake1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/lake2_overlay.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/lake2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/lake3_overlay.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/lake3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_20.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_21.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_22.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_23.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_24.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_25.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_26.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_27.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_28.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_29.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_30.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_31.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_32.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_33.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_34.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_35.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_36.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_37.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_38.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_39.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_40.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_41.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_42.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings1_43.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_20.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_21.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_22.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_23.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_24.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_25.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_26.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_27.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_28.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_29.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_30.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_31.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_32.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_33.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_34.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_35.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_36.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_37.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_38.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_39.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_40.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_41.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_42.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings2_43.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_00.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_01.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_02.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_03.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_04.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_05.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_06.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_07.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_08.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_09.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_10.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_11.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_12.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_13.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_14.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_15.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_16.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_17.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_18.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_19.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_20.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_21.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_22.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_23.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_24.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_25.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_26.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_27.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_28.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_29.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_30.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_31.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_32.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_33.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_34.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_35.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_36.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_37.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_38.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_39.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_40.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_41.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_42.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_rings3_43.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_0.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_1.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_2.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_3.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_4.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_5.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash1_6.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_0.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_1.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_2.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_3.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_4.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_5.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash2_6.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash3_0.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash3_1.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash3_2.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash3_3.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/lake/water_splash3_4.tga</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
