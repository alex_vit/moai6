<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Bandit/BanditDown1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Bandit/BanditDown1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit1/talk_down242.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,84,98,168</rect>
                <key>scale9Paddings</key>
                <rect>49,84,98,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit1/talk_down222.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
