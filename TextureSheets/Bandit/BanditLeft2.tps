<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Bandit/BanditLeft2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Bandit/BanditLeft2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit2/idle_left70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,64,64,128</rect>
                <key>scale9Paddings</key>
                <rect>32,64,64,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle2_left120.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,68,74,136</rect>
                <key>scale9Paddings</key>
                <rect>37,68,74,136</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/bandit4/idle3_left179.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,72,232,144</rect>
                <key>scale9Paddings</key>
                <rect>116,72,232,144</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit2/idle_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle2_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/bandit4/idle3_left172.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
