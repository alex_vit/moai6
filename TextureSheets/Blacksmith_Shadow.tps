<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Blacksmith_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Blacksmith_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_1_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_2_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_2_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_3_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_3_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_4_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever_0_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>240,158,480,316</rect>
                <key>scale9Paddings</key>
                <rect>240,158,480,316</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows77.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows79.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows81.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows84.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows86.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows87.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows89.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows_0_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>111,104,222,208</rect>
                <key>scale9Paddings</key>
                <rect>111,104,222,208</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_1_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_2_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_2_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_3_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_3_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_4_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/blacksmith_shadow_d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows77.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows79.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows81.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows86.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows87.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/blacksmith_bellows89.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5_shadow/smithy_lever24.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
