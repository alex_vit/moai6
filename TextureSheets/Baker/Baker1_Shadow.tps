<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Baker/Baker1_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Baker/Baker1_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_42.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>115,75,230,150</rect>
                <key>scale9Paddings</key>
                <rect>115,75,230,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_up_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/idle_up70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/take_bread60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_down_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1_shadow/walk_right_22.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
