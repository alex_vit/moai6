<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Baker/Baker2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Baker/Baker2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/ask_help115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/finish_work46.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,60,98,120</rect>
                <key>scale9Paddings</key>
                <rect>49,60,98,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/finish_work45.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
