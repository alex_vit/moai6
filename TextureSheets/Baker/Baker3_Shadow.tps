<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Baker/Baker3_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Baker/Baker3_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>91,30,182,60</rect>
                <key>scale9Paddings</key>
                <rect>91,30,182,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_head_up8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker2_shadow/idle_down35.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
