<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Baker/Baker1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Baker/Baker1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/idle_up70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/take_bread60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/thanks106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_down_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/baker1/walk_up_48.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,60,98,120</rect>
                <key>scale9Paddings</key>
                <rect>49,60,98,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/idle_up70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/take_bread60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_down_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/baker1/walk_up_28.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
