<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Dialogs.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Dialogs.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/bandit_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/empty.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/giant_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lampman_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/wizard_lips_o_100.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>179,197,358,394</rect>
                <key>scale9Paddings</key>
                <rect>179,197,358,394</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/dialog.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>340,115,680,230</rect>
                <key>scale9Paddings</key>
                <rect>340,115,680,230</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_o_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/indus_lips_o_100.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>179,99,358,197</rect>
                <key>scale9Paddings</key>
                <rect>179,99,358,197</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hero_eyebrow_evil.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>90,197,179,394</rect>
                <key>scale9Paddings</key>
                <rect>90,197,179,394</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hika-ri.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>179,198,358,396</rect>
                <key>scale9Paddings</key>
                <rect>179,198,358,396</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyebrow_evil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyebrow_wonder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyes_close_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyes_close_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_a_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_a_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_e_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_e_100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_o_050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_o_100.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>191,197,382,394</rect>
                <key>scale9Paddings</key>
                <rect>191,197,382,394</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/dialogs/make-make.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>179,206,358,412</rect>
                <key>scale9Paddings</key>
                <rect>179,206,358,412</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/bandit.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/charo-tei.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/dialog.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/empty.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/fimi-am.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ghost-leader.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/giant.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hero.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hika-ri.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/hoku-lea.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/ika-ika.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/indus.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kao-ri.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/kihe-wahine.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lampman.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/lazu-rita.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/luche-zara.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/make-make.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/pangolin.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/posei-don.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/sebastyan.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_eyebrow_evil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_eyebrow_wonder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_eyes_close_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_eyes_close_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_lips_a_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_lips_a_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_lips_e_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_lips_e_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_lips_o_050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard_lips_o_100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/dialogs/wizard.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
