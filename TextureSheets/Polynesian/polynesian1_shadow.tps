<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Polynesian/polynesian1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Polynesian/polynesian1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right104.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,39,228,78</rect>
                <key>scale9Paddings</key>
                <rect>114,39,228,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle1_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/idle2_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right102.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
