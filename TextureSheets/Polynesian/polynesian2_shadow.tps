<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Polynesian/polynesian2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Polynesian/polynesian2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right133.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,39,228,78</rect>
                <key>scale9Paddings</key>
                <rect>114,39,228,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down079.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>111,39,222,78</rect>
                <key>scale9Paddings</key>
                <rect>111,39,222,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian1_shadow/talk_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle2_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/idle3_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/polynesian2_shadow/talk_down078.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
