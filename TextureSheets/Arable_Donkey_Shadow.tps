<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Arable_Donkey_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Arable_Donkey_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action78.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action80.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action84.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle000.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle002.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle004.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle006.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle008.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle010.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle012.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle014.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle016.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle018.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle020.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle022.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle024.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle026.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle028.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle030.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle032.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle034.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle036.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle038.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle040.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle042.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle044.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle046.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle048.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle052.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle054.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle056.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle058.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle060.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle062.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle064.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle066.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle068.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle070.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle072.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle074.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle076.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle078.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle080.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle082.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle084.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle086.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle088.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle090.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle092.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle094.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle096.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle098.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle102.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle104.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle106.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle108.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle110.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle112.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle114.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle116.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle118.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle120.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle122.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle124.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle126.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle128.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle130.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle132.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle134.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle136.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle138.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle140.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>191,93,382,186</rect>
                <key>scale9Paddings</key>
                <rect>191,93,382,186</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action78.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action80.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/action84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle000.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle002.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle004.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle006.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle008.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle010.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle012.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle014.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle016.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle018.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle020.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle022.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle024.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle026.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle028.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle030.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle032.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle034.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle036.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle038.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle040.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle042.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle044.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle046.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle048.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle052.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle054.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle056.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle058.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle060.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle062.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle064.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle066.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle068.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle070.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle072.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle074.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle076.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle078.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle080.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle082.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle084.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle086.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle088.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle090.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle092.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle094.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle096.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle098.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle102.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle104.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle106.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle108.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle110.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle112.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle114.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle116.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle118.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle120.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle122.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle124.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle126.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle128.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle130.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle132.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle134.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle136.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle138.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey1_shadow/idle140.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
