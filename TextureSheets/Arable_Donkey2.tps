<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Arable_Donkey2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Arable_Donkey2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle000.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle001.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle002.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle003.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle004.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle006.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle007.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle008.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle009.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle010.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle011.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle012.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle013.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle014.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle016.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle017.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle018.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle019.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle020.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle021.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle022.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle023.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle024.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle026.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle027.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle028.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle029.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle030.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle031.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle032.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle033.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle034.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle036.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle037.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle038.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle039.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle040.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle041.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle042.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle043.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle044.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle046.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle047.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle048.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle049.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle050.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle051.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle052.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle053.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle054.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle056.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle057.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle058.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle059.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle060.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle061.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle062.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle063.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle064.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle066.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle067.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle068.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle069.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle070.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle071.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle072.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle073.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle074.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle075.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle076.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle077.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle078.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle079.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle080.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle081.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle082.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle083.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle084.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle086.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle087.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle088.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle089.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle090.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle091.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle092.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle093.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle094.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle096.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle097.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle098.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle099.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle100.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle101.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle102.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle103.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle104.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle106.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle107.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle108.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle109.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle110.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle111.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle112.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle113.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle114.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle116.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle117.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle118.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle119.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle120.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle121.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle122.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle123.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle124.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle126.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle127.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle128.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle129.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle130.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle131.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle132.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle133.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle134.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle136.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle137.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle138.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle139.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arabledonkey2/idle140.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>82,75,164,150</rect>
                <key>scale9Paddings</key>
                <rect>82,75,164,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle000.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle001.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle002.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle003.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle004.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle006.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle007.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle008.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle009.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle010.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle011.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle012.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle013.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle014.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle016.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle017.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle018.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle019.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle020.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle021.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle022.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle023.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle024.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle026.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle027.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle028.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle029.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle030.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle031.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle032.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle033.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle034.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle036.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle037.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle038.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle039.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle040.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle041.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle042.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle043.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle044.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle046.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle047.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle048.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle049.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle050.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle051.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle052.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle053.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle054.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle056.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle057.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle058.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle059.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle060.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle061.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle062.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle063.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle064.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle066.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle067.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle068.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle069.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle070.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle071.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle072.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle073.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle074.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle075.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle076.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle077.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle078.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle079.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle080.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle081.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle082.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle083.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle084.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle086.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle087.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle088.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle089.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle090.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle091.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle092.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle093.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle094.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle096.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle097.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle098.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle099.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle100.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle101.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle102.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle103.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle104.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle106.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle107.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle108.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle109.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle110.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle111.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle112.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle113.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle114.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle116.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle117.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle118.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle119.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle120.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle121.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle122.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle123.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle124.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle126.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle127.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle128.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle129.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle130.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle131.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle132.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle133.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle134.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle136.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle137.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle138.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle139.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arabledonkey2/idle140.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
