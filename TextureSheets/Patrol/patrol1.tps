<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Patrol/patrol1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Patrol/patrol1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/ask_help_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/head_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/patrol1/idle_look_70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,59,98,118</rect>
                <key>scale9Paddings</key>
                <rect>49,59,98,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/head_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/idle_look_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/patrol1/ask_help_000.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
