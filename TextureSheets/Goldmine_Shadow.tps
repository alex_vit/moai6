<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Goldmine_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Goldmine_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_2_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_3_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_4_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_1_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_2_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_2_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_3_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_3_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_4_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_destroy.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_destroy.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart_destroy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>201,118,402,236</rect>
                <key>scale9Paddings</key>
                <rect>201,118,402,236</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart73.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart77.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart78.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart79.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart80.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart81.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart83.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart84.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>102,97,204,194</rect>
                <key>scale9Paddings</key>
                <rect>102,97,204,194</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart_destroy.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart73.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart77.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart78.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart79.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart80.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart81.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart83.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects4_shadow/goldmine_cart84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_2_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_3_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_canopy_shadow_4_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_1_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_2_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_2_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_3_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_3_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_4_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_shadow_destroy.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects3_shadow/goldmine_stithy_shadow_destroy.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
