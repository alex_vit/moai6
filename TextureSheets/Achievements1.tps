<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Achievements1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/achievements1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/68.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,57,113,113</rect>
                <key>scale9Paddings</key>
                <rect>57,57,113,113</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/00d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/01d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/02d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/03d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/04d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/05d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/06d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/07d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/08d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/09d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/10d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/11d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/12d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/13d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/14d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/15d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/16d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/17d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/18d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/19d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/20d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/21d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/22d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/23d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/24d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/25d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/26d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/27d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/28d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/29d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/30d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/31d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/32d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/33d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/34d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/35d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/36d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/37d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/38d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/39d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/40d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/41d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/42d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/43d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/44d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/45d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/46d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/47d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/48d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/49d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/50d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/51d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/52d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/53d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/54d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/55d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/56d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/57d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/58d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/59d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/60d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/61d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/62d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/63d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/64d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/65d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/66d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/67d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/achievements/68d.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,75,75</rect>
                <key>scale9Paddings</key>
                <rect>38,38,75,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/achievements/00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/00d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/01d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/02d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/03d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/04d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/05d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/06d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/07d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/08d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/09d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/10d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/11d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/12d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/13d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/14d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/15d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/16d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/17d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/18d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/19d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/20d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/21d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/22d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/23d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/24d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/25d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/26d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/27d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/28d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/29d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/30d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/31d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/32d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/33d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/34d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/35d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/36d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/37d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/38d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/39d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/40d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/41d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/42d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/43d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/44d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/45d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/46d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/47d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/48d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/49d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/50d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/51d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/52d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/53d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/54d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/55d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/56d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/57d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/58d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/59d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/60d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/61d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/62d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/63d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/64d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/65d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/66d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/67d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/achievements/68d.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
