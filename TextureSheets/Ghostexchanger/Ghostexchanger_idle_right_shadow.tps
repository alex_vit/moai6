<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Ghostexchanger/Ghostexchanger_idle_left_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghostexchanger/Ghostexchanger_idle_right_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right229.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,28,186,56</rect>
                <key>scale9Paddings</key>
                <rect>93,28,186,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle1_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/idle2_right228.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
