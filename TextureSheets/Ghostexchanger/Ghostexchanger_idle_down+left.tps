<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Ghostexchanger/Ghostexchanger_idle_down+left.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghostexchanger/Ghostexchanger_idle_down+left.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down229.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,57,76,114</rect>
                <key>scale9Paddings</key>
                <rect>38,57,76,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left69.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,53,88,106</rect>
                <key>scale9Paddings</key>
                <rect>44,53,88,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle2_down229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3/idle1_down06.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
