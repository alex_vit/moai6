<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Ghostexchanger/Ghostexchanger_talk_left.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghostexchanger/Ghostexchanger_talk_left.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left261.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left263.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left264.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left265.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left267.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left269.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left271.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left273.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left274.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left275.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left277.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left279.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left281.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left283.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left284.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left285.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left286.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left287.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left288.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left289.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,58,98,116</rect>
                <key>scale9Paddings</key>
                <rect>49,58,98,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left288.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left289.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left261.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left263.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left264.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left265.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left267.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left269.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left271.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left273.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left274.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left275.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left277.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left279.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left281.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left283.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left284.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left285.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger1/talk_left287.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
