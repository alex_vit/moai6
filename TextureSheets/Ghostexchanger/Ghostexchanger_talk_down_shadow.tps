<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Ghostexchanger/Ghostexchanger_talk_down_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghostexchanger/Ghostexchanger_talk_down_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down263.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down265.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down269.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down273.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down275.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down279.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down283.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down285.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down286.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down288.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down289.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>90,32,180,64</rect>
                <key>scale9Paddings</key>
                <rect>90,32,180,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down279.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down283.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down285.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down288.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down289.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down263.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down265.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down269.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down273.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down275.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger2_shadow/talk_down278.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
