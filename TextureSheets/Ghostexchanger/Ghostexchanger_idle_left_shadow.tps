<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Ghostexchanger/Ghostexchanger_idle_left_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghostexchanger/Ghostexchanger_idle_left_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left229.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,30,194,60</rect>
                <key>scale9Paddings</key>
                <rect>97,30,194,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghostexchanger3_shadow/idle2_left210.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
