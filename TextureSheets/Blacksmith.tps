<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Blacksmith.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Blacksmith.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows73.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows77.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows78.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows79.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows80.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows81.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows83.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows84.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows85.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows86.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows87.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows88.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows89.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows90.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows_0_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,71,188,142</rect>
                <key>scale9Paddings</key>
                <rect>94,71,188,142</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_1_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_2_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_2_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_3_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_3_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_4_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_2_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_3_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_4_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_1_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_lever_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_leverbase_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/smithy_leverbase_0_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>177,181,354,362</rect>
                <key>scale9Paddings</key>
                <rect>177,181,354,362</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_3_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_4_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_d.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>177,98,354,196</rect>
                <key>scale9Paddings</key>
                <rect>177,98,354,196</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows73.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows77.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows78.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows79.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows80.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows81.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows83.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows85.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows86.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows87.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows88.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows89.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_bellows90.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_1_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_2_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_2_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_3_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_3_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_4_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/blacksmith_object_d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_3_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_4_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_ground_d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_2_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_3_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_heat_4_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_1_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/blacksmith_smith_d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_leverbase_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_leverbase_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/smithy_lever20.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
