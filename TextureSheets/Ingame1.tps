<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Ingame1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Ingame1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/action_progress_frame.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,5,70,9</rect>
                <key>scale9Paddings</key>
                <rect>35,5,70,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/action_progress_green.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/action_progress_red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,3,65,5</rect>
                <key>scale9Paddings</key>
                <rect>33,3,65,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/additionalbar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,44,145,87</rect>
                <key>scale9Paddings</key>
                <rect>73,44,145,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/btn_disabled.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/btn_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/btn_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/btn_pushed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,19,147,38</rect>
                <key>scale9Paddings</key>
                <rect>74,19,147,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/check_check.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/check_frame.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/check_full.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,9,18,18</rect>
                <key>scale9Paddings</key>
                <rect>9,9,18,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/context_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>187,187,374,374</rect>
                <key>scale9Paddings</key>
                <rect>187,187,374,374</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/context_line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>177,177,354,354</rect>
                <key>scale9Paddings</key>
                <rect>177,177,354,354</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/context_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,71,126,142</rect>
                <key>scale9Paddings</key>
                <rect>63,71,126,142</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/grade_flag50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,28</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_arable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_arable_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_backery.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_backery_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_blacksmith.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_blacksmith_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_churn.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_churn_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_goldmine.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_goldmine_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_grove.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_grove_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_hut.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_hut_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_lighthouse.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_lighthouse_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_moai.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_moai_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_quarry.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_quarry_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_sawmill.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_sawmill_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_scarecrow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_scarecrow_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_windturbine.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_build_windturbine_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_ceremonialarea_action.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_ceremonialarea_action_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_crash.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_crash_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_destroy.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_destroy_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_drought.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_flood.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_food.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_food_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_freezing.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_gold.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_gold_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_heal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_heal_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_mana.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_metal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_metal_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_oil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_oil_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_pray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_pray_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_puttingoutfire_action.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_puttingoutfire_action_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_repair.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_repair_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_coconaut_oil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_coconaut_oil_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_food.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_gold.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_metal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_unit.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_res_wood.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_rocks.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_rocks_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_sandal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_sandal_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_upgrade.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_upgrade_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_wood.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_wood_disable.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,53,106,106</rect>
                <key>scale9Paddings</key>
                <rect>53,53,106,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_cup.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_cup_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_gloves.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_gloves_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_horn.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_horn_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_round.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_sandals.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_sandals_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_scales.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_scales_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_shield.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_shield_inactive.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,34,68,68</rect>
                <key>scale9Paddings</key>
                <rect>34,34,68,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_round_small.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,35,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/ingame_menu_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>187,222,374,444</rect>
                <key>scale9Paddings</key>
                <rect>187,222,374,444</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/mainbar_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>421,42,841,84</rect>
                <key>scale9Paddings</key>
                <rect>421,42,841,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/mainbar_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,109,53,217</rect>
                <key>scale9Paddings</key>
                <rect>27,109,53,217</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/mark.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,38,76,76</rect>
                <key>scale9Paddings</key>
                <rect>38,38,76,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area4.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area6.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area7.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/moai_area8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,49,176,98</rect>
                <key>scale9Paddings</key>
                <rect>88,49,176,98</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/not_attended.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,42,84,84</rect>
                <key>scale9Paddings</key>
                <rect>42,42,84,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_cloud.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>79,47,158,94</rect>
                <key>scale9Paddings</key>
                <rect>79,47,158,94</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_cloud_drip.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,11,16,22</rect>
                <key>scale9Paddings</key>
                <rect>8,11,16,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/problem_waterpump07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,46,63,92</rect>
                <key>scale9Paddings</key>
                <rect>32,46,63,92</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/round.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,62,124,124</rect>
                <key>scale9Paddings</key>
                <rect>62,62,124,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/selector1.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,31,106,61</rect>
                <key>scale9Paddings</key>
                <rect>53,31,106,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/selector2.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>102,57,203,114</rect>
                <key>scale9Paddings</key>
                <rect>102,57,203,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/selector3.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>146,82,291,164</rect>
                <key>scale9Paddings</key>
                <rect>146,82,291,164</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_11.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,81,160,162</rect>
                <key>scale9Paddings</key>
                <rect>80,81,160,162</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/sun_heat.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>83,85,166,170</rect>
                <key>scale9Paddings</key>
                <rect>83,85,166,170</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/taskbar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>206,87,412,173</rect>
                <key>scale9Paddings</key>
                <rect>206,87,412,173</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/timebar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,56,20,111</rect>
                <key>scale9Paddings</key>
                <rect>10,56,20,111</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/timecoin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,21,42,42</rect>
                <key>scale9Paddings</key>
                <rect>21,21,42,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/timemark.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,25,39,50</rect>
                <key>scale9Paddings</key>
                <rect>20,25,39,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/toolback.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>127,53,254,105</rect>
                <key>scale9Paddings</key>
                <rect>127,53,254,105</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/tutorial_arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,51,69,101</rect>
                <key>scale9Paddings</key>
                <rect>35,51,69,101</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/unit_cloud.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,62,91,124</rect>
                <key>scale9Paddings</key>
                <rect>46,62,91,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/unit_cloud_start.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,32,41,64</rect>
                <key>scale9Paddings</key>
                <rect>21,32,41,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/ingame1/action_progress_frame.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/action_progress_green.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/action_progress_red.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/additionalbar.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/btn_disabled.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/btn_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/btn_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/btn_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/check_check.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/check_frame.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/context_big.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/context_line.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/context_small.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/grade_flag50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_arable_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_arable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_backery_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_backery.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_blacksmith_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_blacksmith.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_churn_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_churn.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_goldmine_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_goldmine.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_grove_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_grove.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_hut_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_hut.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_lighthouse_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_lighthouse.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_moai_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_moai.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_quarry_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_quarry.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_sawmill_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_sawmill.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_scarecrow_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_scarecrow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_windturbine_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_build_windturbine.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_ceremonialarea_action_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_ceremonialarea_action.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_crash_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_crash.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_destroy_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_destroy.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_drought.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_flood.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_food_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_food.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_freezing.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_gold_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_gold.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_heal_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_heal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_mana.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_metal_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_metal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_pray_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_pray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_puttingoutfire_action_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_puttingoutfire_action.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_repair_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_repair.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_coconaut_oil_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_coconaut_oil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_food.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_gold.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_metal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_unit.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_res_wood.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_rocks_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_rocks.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_sandal_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_sandal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_upgrade_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_upgrade.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_wood_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_wood.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_cup_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_cup.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_gloves_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_gloves.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_horn_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_horn.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_round_small.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_round.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_sandals_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_sandals.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_scales_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_scales.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_shield_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_shield.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/ingame_menu_back.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/mainbar_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/mainbar_2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/mark.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area4.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area6.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area7.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/moai_area8.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/not_attended.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_cloud_drip.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_cloud.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/problem_waterpump07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/round.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/selector1.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/selector2.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/selector3.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/sun_heat.tga</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/taskbar.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/timebar.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/timecoin.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/timemark.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/tutorial_arrow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/unit_cloud_start.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/unit_cloud.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/toolback.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_oil_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_oil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/check_full.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
