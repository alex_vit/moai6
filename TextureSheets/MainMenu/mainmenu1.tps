<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MainMenu/mainmenu1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/MainMenu/mainmenu1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_left_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_left_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_left_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,83,225,166</rect>
                <key>scale9Paddings</key>
                <rect>112,83,225,166</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_right_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_right_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_right_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>104,80,208,159</rect>
                <key>scale9Paddings</key>
                <rect>104,80,208,159</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/Golem_torso.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>171,232,342,463</rect>
                <key>scale9Paddings</key>
                <rect>171,232,342,463</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/Man_closed.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/Man_semi-closed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>175,370,349,739</rect>
                <key>scale9Paddings</key>
                <rect>175,370,349,739</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/Woman_big_ball.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/Woman_closed.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/Woman_semi-closed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>185,334,370,669</rect>
                <key>scale9Paddings</key>
                <rect>185,334,370,669</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_games_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_games_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_games_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,37,70,74</rect>
                <key>scale9Paddings</key>
                <rect>35,37,70,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_info_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_info_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_info_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>39,37,77,74</rect>
                <key>scale9Paddings</key>
                <rect>39,37,77,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_options_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_options_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_options_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,56,100,111</rect>
                <key>scale9Paddings</key>
                <rect>50,56,100,111</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_play_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_play_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_play_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>58,63,116,126</rect>
                <key>scale9Paddings</key>
                <rect>58,63,116,126</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_vaza_hover.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_vaza_norm.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_vaza_press.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,57,98,113</rect>
                <key>scale9Paddings</key>
                <rect>49,57,98,113</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/grass_glow1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/grass_glow2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/grass_glow3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>287,65,574,130</rect>
                <key>scale9Paddings</key>
                <rect>287,65,574,130</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/main_menu_moai6/name_stones.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>279,59,558,118</rect>
                <key>scale9Paddings</key>
                <rect>279,59,558,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/Woman_big_ball.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/Woman_closed.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/Woman_semi-closed.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_games_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_games_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_games_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_info_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_info_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_info_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_options_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_options_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_options_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_play_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_play_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_play_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_vaza_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_vaza_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/btn_vaza_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_left_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_left_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_left_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_right_hover.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_right_norm.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/CE_stone_right_press.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/Golem_torso.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/grass_glow1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/grass_glow2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/grass_glow3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/Man_closed.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/Man_semi-closed.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/main_menu_moai6/name_stones.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
