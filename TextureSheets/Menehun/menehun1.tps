<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Menehun/menehun1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Menehun/menehun1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/action/menehun_action99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,81,210,162</rect>
                <key>scale9Paddings</key>
                <rect>105,81,210,162</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/action/menehun_action1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun/idle/menehun_idle1.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
