<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Lampman/lampman2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lampman/lampman2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right49.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>136,41,272,82</rect>
                <key>scale9Paddings</key>
                <rect>136,41,272,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right094.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>118,50,236,100</rect>
                <key>scale9Paddings</key>
                <rect>118,50,236,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman1_shadow/idle2_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle1_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/idle2_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2_shadow/talk_right093.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
