<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Lampman/lampman4.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lampman/lampman4.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left184.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,80,134,160</rect>
                <key>scale9Paddings</key>
                <rect>67,80,134,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/idle2_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down117.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,57,124,114</rect>
                <key>scale9Paddings</key>
                <rect>62,57,124,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/idle2_down21.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
