<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Lampman/lampman_shine.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lampman/lampman_shine.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_down184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_left184.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,70,128,140</rect>
                <key>scale9Paddings</key>
                <rect>64,70,128,140</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/action_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman_shine/talk_right184.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,70,64,140</rect>
                <key>scale9Paddings</key>
                <rect>32,70,64,140</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle1_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/idle2_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_down184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_left184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/talk_right184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman_shine/action_down009.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
