<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Lampman/lampman3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lampman/lampman3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman2/talk_right184.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,89,120,178</rect>
                <key>scale9Paddings</key>
                <rect>60,89,120,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/action_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman3/talk_left077.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,80,134,160</rect>
                <key>scale9Paddings</key>
                <rect>67,80,134,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman2/talk_right164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/action_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman3/talk_left077.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
