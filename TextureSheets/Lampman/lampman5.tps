<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Lampman/lampman5.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lampman/lampman5.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman4/talk_down184.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,57,124,114</rect>
                <key>scale9Paddings</key>
                <rect>62,57,124,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman5/action_right074.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,74,134,148</rect>
                <key>scale9Paddings</key>
                <rect>67,74,134,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman4/talk_down175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman5/action_down013.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
