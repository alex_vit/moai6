<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Lampman/lampman7.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lampman/lampman7.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right49.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>83,78,166,156</rect>
                <key>scale9Paddings</key>
                <rect>83,78,166,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left139.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,84,110,168</rect>
                <key>scale9Paddings</key>
                <rect>55,84,110,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle1_light_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman6/idle2_light_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lampman7/idle1_light_left138.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
