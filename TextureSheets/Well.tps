<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Well.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Well.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_bucket1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_bucket2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_1_0d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_lever_1_0d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_object_water67.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,134,226,268</rect>
                <key>scale9Paddings</key>
                <rect>113,134,226,268</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_ground_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/objects5/well_ground_1_0d.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>167,101,334,202</rect>
                <key>scale9Paddings</key>
                <rect>167,101,334,202</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_bucket1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_bucket2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_ground_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_ground_1_0d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_1_0d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever_1_0d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_lever55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/objects5/well_object_water67.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
