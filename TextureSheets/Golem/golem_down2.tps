<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Golem/golem_down2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Golem/golem_down2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,75,200,150</rect>
                <key>scale9Paddings</key>
                <rect>100,75,200,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_action/golem_down_action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Down/golem_front_dead/golem_down_dead52.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
