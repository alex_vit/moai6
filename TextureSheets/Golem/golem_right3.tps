<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Golem/golem_right3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Golem/golem_right3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,75,200,150</rect>
                <key>scale9Paddings</key>
                <rect>100,75,200,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_dead/golem_right_dead91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Right/golem_right_idle/golem_right_idle61.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
