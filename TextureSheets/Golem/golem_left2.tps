<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Golem/golem_left2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Golem/golem_left2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,75,200,150</rect>
                <key>scale9Paddings</key>
                <rect>100,75,200,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_action/golem_left_action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Golem/Left/golem_left_dead/golem_left_dead78.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
