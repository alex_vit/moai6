<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Hungrybird/hungrybird1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Hungrybird/hungrybird1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/attack_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hungrybird1/fly_right19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,117,228,234</rect>
                <key>scale9Paddings</key>
                <rect>114,117,228,234</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/fly_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hungrybird1/attack_right07.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
