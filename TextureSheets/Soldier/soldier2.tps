<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Soldier/soldier2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Soldier/soldier2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,72,92,144</rect>
                <key>scale9Paddings</key>
                <rect>46,72,92,144</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/dig_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/run_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,61,160,122</rect>
                <key>scale9Paddings</key>
                <rect>80,61,160,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier1/walk_with_bomb_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/run_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/walk_with_bomb_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/soldier2/dig_down24.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
