<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Smith/smith2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Smith/smith2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith1_shadow/thanks106.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>92,34,184,68</rect>
                <key>scale9Paddings</key>
                <rect>92,34,184,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/smith2_shadow/action251.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>141,97,282,194</rect>
                <key>scale9Paddings</key>
                <rect>141,97,282,194</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith1_shadow/thanks096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/smith2_shadow/action086.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
