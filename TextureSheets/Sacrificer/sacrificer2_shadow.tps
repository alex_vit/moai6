<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Sacrificer/sacrificer2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Sacrificer/sacrificer2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right43.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,38,256,76</rect>
                <key>scale9Paddings</key>
                <rect>128,38,256,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left35.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>138,42,276,84</rect>
                <key>scale9Paddings</key>
                <rect>138,42,276,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer1_shadow/idle3_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/idle3_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/sacrificer/sacrificer2_shadow/talk_left35.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
