<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Jeweller/jeweller1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Jeweller/jeweller1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>80</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/action184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,56,214,112</rect>
                <key>scale9Paddings</key>
                <rect>107,56,214,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1_shadow/object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,28,214,56</rect>
                <key>scale9Paddings</key>
                <rect>107,28,214,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/head_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle_look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/idle70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1_shadow/action024.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
