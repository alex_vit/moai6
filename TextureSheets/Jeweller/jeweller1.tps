<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Jeweller/jeweller1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Jeweller/jeweller1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/action184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/head_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/idle_look70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>81,85,162,170</rect>
                <key>scale9Paddings</key>
                <rect>81,85,162,170</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/jeweller1/object_over.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,85,81,170</rect>
                <key>scale9Paddings</key>
                <rect>41,85,81,170</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/head_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle_look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/idle70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/object_over.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/jeweller1/action026.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
