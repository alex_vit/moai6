<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Ghosts/GhostPurple4.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghosts/GhostPurple4.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_00.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_01.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_02.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_03.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_04.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_05.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_06.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_07.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_08.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_09.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_10.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_11.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_12.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_13.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_14.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_15.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_16.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_17.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_18.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_19.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_20.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_21.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_22.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_23.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_24.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_25.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_26.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_27.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_28.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_29.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_30.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_31.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_32.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_33.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_34.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_35.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_36.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_37.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_38.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_39.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_40.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_41.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_42.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_43.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_44.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_45.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_46.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_47.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_48.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_49.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_50.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_51.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_52.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_53.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_54.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_55.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_00.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_01.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_02.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_03.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_04.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_05.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_06.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_07.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_08.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_09.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_10.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_11.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_12.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_13.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_14.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_15.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_16.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_17.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_18.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_19.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_20.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_21.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_22.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_23.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_24.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_25.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_26.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_27.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_28.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_29.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_30.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_31.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_32.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_33.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_34.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_35.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_36.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_37.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_38.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_39.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_40.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_41.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_42.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_43.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_44.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_45.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_46.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_47.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_48.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_49.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_50.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_51.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_52.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_53.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_54.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_55.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,89,168,178</rect>
                <key>scale9Paddings</key>
                <rect>84,89,168,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_00.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_01.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_02.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_03.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_04.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_05.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_06.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_07.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_08.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_09.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_10.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_11.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_12.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_13.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_14.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_15.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_16.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_17.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_18.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_19.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_20.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_21.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_22.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_23.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_24.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_25.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_26.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_27.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_28.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_29.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_30.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_31.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_32.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_33.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_34.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_35.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_36.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_37.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_38.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_39.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_40.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_41.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_42.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_43.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_44.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_45.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_46.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_47.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_48.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_49.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_50.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_51.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_52.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_53.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_54.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_down_55.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_00.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_01.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_02.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_03.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_04.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_05.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_06.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_07.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_08.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_09.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_10.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_11.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_12.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_13.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_14.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_15.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_16.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_17.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_18.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_19.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_20.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_21.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_22.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_23.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_24.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_25.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_26.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_27.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_28.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_29.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_30.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_31.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_32.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_33.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_34.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_35.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_36.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_37.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_38.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_39.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_40.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_41.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_42.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_43.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_44.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_45.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_46.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_47.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_48.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_49.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_50.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_51.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_52.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_53.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_54.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost4_purple/death_left_55.tga</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
