<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Ghosts/GhostRed1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Ghosts/GhostRed1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_00.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_01.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_02.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_03.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_04.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_05.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_06.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_07.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_08.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_09.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_10.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_11.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_12.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_13.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_14.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_15.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_16.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_17.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_18.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_19.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_20.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_21.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_22.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_23.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_24.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_25.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_26.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_27.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_28.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_29.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_30.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_31.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_32.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_33.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_34.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_35.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_36.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_37.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_38.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_39.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_40.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_41.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_42.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_43.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_44.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_45.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_46.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_47.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_48.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_49.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_50.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_51.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_52.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_53.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_54.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_55.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_56.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_57.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_58.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_59.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_60.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_61.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_62.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_63.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_64.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_65.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_66.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_67.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_68.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_69.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_70.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_71.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_72.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_73.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_74.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_75.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_76.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_77.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_78.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_79.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_80.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_00.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_01.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_02.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_03.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_04.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_05.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_06.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_07.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_08.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_09.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_10.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_11.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_12.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_13.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_14.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_15.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_16.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_17.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_18.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_19.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_20.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_21.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_22.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_23.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_24.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_25.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_26.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_27.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_28.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_29.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_30.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_31.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_32.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_33.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_34.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_35.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_36.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_37.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_38.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_39.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_40.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_41.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_42.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_43.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_44.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_45.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_46.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_47.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_48.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_49.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_50.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_51.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_52.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_53.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_54.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_55.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_56.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_57.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_58.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_59.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_60.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_61.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_62.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_63.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_64.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_65.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_66.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_67.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_68.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_69.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_70.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_71.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_72.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_73.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_74.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_75.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_76.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_77.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_78.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_79.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_80.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_00.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_01.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_02.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_03.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_04.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_05.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_06.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_07.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_08.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_09.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_10.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_11.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_12.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_13.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_14.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_15.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_16.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_17.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_18.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_19.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_20.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_21.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_22.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_23.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_24.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_25.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_26.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_27.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_28.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_29.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_30.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_31.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_32.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_33.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_34.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_35.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_36.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_37.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_38.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_39.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_40.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_41.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_42.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_43.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_44.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_45.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_46.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_47.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_48.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_49.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_50.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_51.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_52.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_53.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_54.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_55.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_56.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_57.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_58.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_59.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_60.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_61.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_62.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_63.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_64.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_65.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_66.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_67.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_68.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_69.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_70.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_71.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_72.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_73.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_74.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_75.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_76.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_77.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_78.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_79.tga</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_80.tga</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,84,104,168</rect>
                <key>scale9Paddings</key>
                <rect>52,84,104,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_00.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_01.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_02.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_03.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_04.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_05.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_06.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_07.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_08.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_09.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_10.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_11.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_12.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_13.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_14.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_15.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_16.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_17.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_18.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_19.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_20.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_21.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_22.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_23.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_24.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_25.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_26.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_27.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_28.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_29.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_30.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_31.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_32.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_33.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_34.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_35.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_36.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_37.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_38.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_39.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_40.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_41.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_42.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_43.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_44.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_45.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_46.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_47.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_48.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_49.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_50.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_51.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_52.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_53.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_54.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_55.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_56.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_57.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_58.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_59.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_60.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_61.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_62.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_63.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_64.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_65.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_66.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_67.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_68.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_69.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_70.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_71.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_72.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_73.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_74.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_75.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_76.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_77.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_78.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_79.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_down_80.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_00.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_01.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_02.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_03.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_04.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_05.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_06.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_07.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_08.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_09.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_10.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_11.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_12.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_13.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_14.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_15.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_16.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_17.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_18.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_19.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_20.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_21.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_22.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_23.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_24.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_25.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_26.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_27.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_28.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_29.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_30.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_31.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_32.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_33.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_34.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_35.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_36.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_37.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_38.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_39.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_40.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_41.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_42.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_43.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_44.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_45.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_46.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_47.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_48.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_49.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_50.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_51.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_52.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_53.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_54.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_55.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_56.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_57.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_58.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_59.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_60.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_61.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_62.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_63.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_64.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_65.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_66.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_67.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_68.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_69.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_70.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_71.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_72.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_73.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_74.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_75.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_76.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_77.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_78.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_79.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_right_80.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_00.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_01.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_02.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_03.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_04.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_05.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_06.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_07.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_08.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_09.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_10.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_11.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_12.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_13.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_14.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_15.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_16.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_17.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_18.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_19.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_20.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_21.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_22.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_23.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_24.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_25.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_26.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_27.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_28.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_29.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_30.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_31.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_32.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_33.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_34.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_35.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_36.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_37.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_38.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_39.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_40.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_41.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_42.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_43.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_44.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_45.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_46.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_47.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_48.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_49.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_50.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_51.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_52.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_53.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_54.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_55.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_56.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_57.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_58.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_59.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_60.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_61.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_62.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_63.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_64.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_65.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_66.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_67.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_68.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_69.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_70.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_71.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_72.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_73.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_74.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_75.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_76.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_77.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_78.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_79.tga</filename>
            <filename>../../../Moai6_tmp/old_art_all/ghost1_red/fly_up_80.tga</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
