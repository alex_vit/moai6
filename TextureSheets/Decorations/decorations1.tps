<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Decorations/decorations1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Decorations/decorations1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/alhagi01_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/alhagi02_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/alhagi03_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/alhagi04_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus01_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus02_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus03_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus04_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus05_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus06_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus07_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/cactus08_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>127,98,254,196</rect>
                <key>scale9Paddings</key>
                <rect>127,98,254,196</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/beam01h.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/beam01v.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/beam02h.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/beam02v.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>131,74,262,148</rect>
                <key>scale9Paddings</key>
                <rect>131,74,262,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_3_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush01_3_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>109,74,218,148</rect>
                <key>scale9Paddings</key>
                <rect>109,74,218,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_3_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush02_3_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>79,58,158,116</rect>
                <key>scale9Paddings</key>
                <rect>79,58,158,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_3_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush03_3_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>89,59,178,118</rect>
                <key>scale9Paddings</key>
                <rect>89,59,178,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush04_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush04_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush04_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush05_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush05_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush05_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush05_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush05_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush05_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush06_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush06_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush06_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush06_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush06_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/blush06_2_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,79,256,158</rect>
                <key>scale9Paddings</key>
                <rect>128,79,256,158</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/bonfire_ground.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/bonfire_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,44,146,88</rect>
                <key>scale9Paddings</key>
                <rect>73,44,146,88</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/dinosaur.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/dinosaur_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>241,332,482,664</rect>
                <key>scale9Paddings</key>
                <rect>241,332,482,664</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/flower8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,90,132,180</rect>
                <key>scale9Paddings</key>
                <rect>66,90,132,180</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/hovel_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>194,143,388,286</rect>
                <key>scale9Paddings</key>
                <rect>194,143,388,286</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/nymphaea8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/reed1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/reed2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/reed3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/reed4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>285,166,570,332</rect>
                <key>scale9Paddings</key>
                <rect>285,166,570,332</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/pier1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/pier2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/pier3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>298,174,596,348</rect>
                <key>scale9Paddings</key>
                <rect>298,174,596,348</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_3_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant01_3_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,87,274,174</rect>
                <key>scale9Paddings</key>
                <rect>137,87,274,174</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_2_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_3_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant02_3_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>180,89,360,178</rect>
                <key>scale9Paddings</key>
                <rect>180,89,360,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant03_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant03_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant03_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant03_2_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant03_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/plant03_3_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>111,69,222,138</rect>
                <key>scale9Paddings</key>
                <rect>111,69,222,138</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/table_bandit.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/table_indus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>33,74,66,148</rect>
                <key>scale9Paddings</key>
                <rect>33,74,66,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_3_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_3_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_3_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush03_3_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush04_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush04_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush04_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush05_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush05_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush05_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush05_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush05_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush05_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush06_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush06_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush06_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush06_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush06_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush06_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/bonfire_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/bonfire_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus01_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus02_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus03_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus04_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus05_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus06_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus07_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/cactus08_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/dinosaur.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/dinosaur_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/flower8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/hovel_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/nymphaea8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/pier1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/pier2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/pier3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_3_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant01_3_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_3_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant02_3_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant03_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant03_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant03_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant03_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant03_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/plant03_3_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/reed1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/reed2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/reed3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/reed4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/table_bandit.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/table_indus.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/alhagi01_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/alhagi02_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/alhagi03_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/alhagi04_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/beam01h.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/beam01v.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/beam02h.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/beam02v.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_2_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_2_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_3_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush01_3_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/blush02_1_2.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
