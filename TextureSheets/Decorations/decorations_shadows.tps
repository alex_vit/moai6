<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Decorations/decorations_shadows.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Decorations/decorations_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/blush24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/tree16.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>196,94,392,188</rect>
                <key>scale9Paddings</key>
                <rect>196,94,392,188</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/bonfire_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,44,146,88</rect>
                <key>scale9Paddings</key>
                <rect>73,44,146,88</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/decoration_stone.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,44,140,88</rect>
                <key>scale9Paddings</key>
                <rect>70,44,140,88</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/dinosaur.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>241,332,482,664</rect>
                <key>scale9Paddings</key>
                <rect>241,332,482,664</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/flower8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>131,53,262,106</rect>
                <key>scale9Paddings</key>
                <rect>131,53,262,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/moai4_stockade_horizontal.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/moai4_stockade_vertical.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,48,210,96</rect>
                <key>scale9Paddings</key>
                <rect>105,48,210,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,41,200,82</rect>
                <key>scale9Paddings</key>
                <rect>100,41,200,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/table_bandit_indus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,34,132,68</rect>
                <key>scale9Paddings</key>
                <rect>66,34,132,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant02_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant02_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant07_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant07_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant08_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant08_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant09_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant09_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant09_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant10_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant10_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,65,250,130</rect>
                <key>scale9Paddings</key>
                <rect>125,65,250,130</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations_shadow/warior_house_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>277,120,554,240</rect>
                <key>scale9Paddings</key>
                <rect>277,120,554,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant07_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant07_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant08_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant08_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant09_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant09_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant09_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant10_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant10_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/warior_house_shadow.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/blush24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/bonfire_shadow.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/decoration_stone.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/dinosaur.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/flower8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/moai4_stockade_horizontal.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/moai4_stockade_vertical.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/mushroom7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/table_bandit_indus.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/tree16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant01_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant02_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant02_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant04_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations_shadow/uw_plant05.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
