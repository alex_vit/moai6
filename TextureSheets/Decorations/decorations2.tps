<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Decorations/decorations2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Decorations/decorations2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/ship_p1_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>456,347,912,694</rect>
                <key>scale9Paddings</key>
                <rect>456,347,912,694</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/ship_p2_ice_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/ship_p2_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>339,330,678,660</rect>
                <key>scale9Paddings</key>
                <rect>339,330,678,660</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/ship_p3_ice_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations/ship_p3_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>266,207,532,414</rect>
                <key>scale9Paddings</key>
                <rect>266,207,532,414</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/blush25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/tree17.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,117,150,234</rect>
                <key>scale9Paddings</key>
                <rect>75,117,150,234</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/decoration_stone.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,48,148,96</rect>
                <key>scale9Paddings</key>
                <rect>74,48,148,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/moai4_stockade_horizontal.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/moai4_stockade_vertical.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,78,144,156</rect>
                <key>scale9Paddings</key>
                <rect>72,78,144,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/mushroom7.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,74,142,148</rect>
                <key>scale9Paddings</key>
                <rect>71,74,142,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/square.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>294,154,588,308</rect>
                <key>scale9Paddings</key>
                <rect>294,154,588,308</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant02_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant02_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant07_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant07_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant08_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant08_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant09_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant09_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant09_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant10_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/uw_plant10_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,133,194,266</rect>
                <key>scale9Paddings</key>
                <rect>97,133,194,266</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/decorations2/warior_house_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>143,184,286,368</rect>
                <key>scale9Paddings</key>
                <rect>143,184,286,368</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/decorations/ship_p2_ice_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/ship_p2_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/ship_p3_ice_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/ship_p3_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations/ship_p1_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/decoration_stone.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/moai4_stockade_horizontal.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/moai4_stockade_vertical.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/mushroom7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/square.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/tree17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant01_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant02_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant02_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant04_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant07_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant07_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant08_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant08_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant09_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant09_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant09_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant10_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/uw_plant10_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/warior_house_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/decorations2/blush12.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
