<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Worker/worker1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Worker/worker1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/ask_help114.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>111,40,222,80</rect>
                <key>scale9Paddings</key>
                <rect>111,40,222,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_74.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>108,41,216,82</rect>
                <key>scale9Paddings</key>
                <rect>108,41,216,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/ask_help114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_left_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker_shadow/blow_right_53.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
