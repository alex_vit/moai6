<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Worker/worker1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Worker/worker1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/ask_help115.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,64,132,128</rect>
                <key>scale9Paddings</key>
                <rect>66,64,132,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_left_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_right_75.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,72,128,144</rect>
                <key>scale9Paddings</key>
                <rect>64,72,128,144</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_quest_75.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,55,82,110</rect>
                <key>scale9Paddings</key>
                <rect>41,55,82,110</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_left_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_quest_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_right_71.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
