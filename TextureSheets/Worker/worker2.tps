<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Worker/worker2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Worker/worker2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/blow_up_75.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,72,128,144</rect>
                <key>scale9Paddings</key>
                <rect>64,72,128,144</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/canoeing_right58.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,73,160,146</rect>
                <key>scale9Paddings</key>
                <rect>80,73,160,146</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/chopping_down32.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,88,176,176</rect>
                <key>scale9Paddings</key>
                <rect>88,88,176,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/blow_up_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/canoeing_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/chopping_down26.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
