<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Worker/worker12.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Worker/worker12.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_left_60.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,64,132,128</rect>
                <key>scale9Paddings</key>
                <rect>66,64,132,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_pain_up60.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,63,96,126</rect>
                <key>scale9Paddings</key>
                <rect>48,63,96,126</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/walk_up_48.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,88,176,176</rect>
                <key>scale9Paddings</key>
                <rect>88,88,176,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/waterpump_right78.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,66,122,132</rect>
                <key>scale9Paddings</key>
                <rect>61,66,122,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/well99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,67,104,134</rect>
                <key>scale9Paddings</key>
                <rect>52,67,104,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_left_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_pain_up60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/walk_up_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/waterpump_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/well85.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
