<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Worker/worker7.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Worker/worker7.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_231.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,64,132,128</rect>
                <key>scale9Paddings</key>
                <rect>66,64,132,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_sit_right_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/idle_up70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,61,86,122</rect>
                <key>scale9Paddings</key>
                <rect>43,61,86,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/kirka_left_32.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,88,176,176</rect>
                <key>scale9Paddings</key>
                <rect>88,88,176,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_sit_right_231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/idle_up68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/kirka_left_30.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
