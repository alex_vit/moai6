<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Worker/worker9.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Worker/worker9.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down53.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,66,114,132</rect>
                <key>scale9Paddings</key>
                <rect>57,66,114,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/put_on_fire_down21.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,66,57,132</rect>
                <key>scale9Paddings</key>
                <rect>29,66,57,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_up18.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,88,176,176</rect>
                <key>scale9Paddings</key>
                <rect>88,88,176,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>52,67,104,134</rect>
                <key>scale9Paddings</key>
                <rect>52,67,104,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right68.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,63,142,126</rect>
                <key>scale9Paddings</key>
                <rect>71,63,142,126</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/worker/shaketree_right56.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,63,71,126</rect>
                <key>scale9Paddings</key>
                <rect>36,63,71,126</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/put_on_fire_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/run_with_bucket_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/worker/shaketree_right64.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
