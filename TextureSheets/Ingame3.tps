<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Ingame3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Ingame3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/context_big_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>177,177,354,354</rect>
                <key>scale9Paddings</key>
                <rect>177,177,354,354</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_sign_vai.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_sign_vai_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icon_whirl.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_clover_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_cup_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_gloves_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_horn_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_sandals_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_scales_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_shaman_drum_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_shield_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/icondrop_scroll_of_the_builder_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/icondrop_stick_disable.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,53,106,106</rect>
                <key>scale9Paddings</key>
                <rect>53,53,106,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_clover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_clover_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_shaman_drum.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame1/icondrop_shaman_drum_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/icondrop_scroll_of_the_builder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/icondrop_scroll_of_the_builder_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/icondrop_stick.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/icondrop_stick_inactive.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>68,68,136,136</rect>
                <key>scale9Paddings</key>
                <rect>68,68,136,136</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/artefact_frame.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_00d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_01d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_02d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_03d.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/sign_04d.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,31,62,61</rect>
                <key>scale9Paddings</key>
                <rect>31,31,62,61</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/bells15.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,25,70,49</rect>
                <key>scale9Paddings</key>
                <rect>35,25,70,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/fence10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,126,192,252</rect>
                <key>scale9Paddings</key>
                <rect>96,126,192,252</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/soundwave10.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,69,96,138</rect>
                <key>scale9Paddings</key>
                <rect>48,69,96,138</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/ingame3/underwater_constructions_bubble.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,128,256,256</rect>
                <key>scale9Paddings</key>
                <rect>128,128,256,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/artefact_frame.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/bells15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/soundwave10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/fence10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_00d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_01d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_02d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_03d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/sign_04d.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/context_big_background.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_sign_vai_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_sign_vai.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_clover_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_clover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icon_whirl.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_shaman_drum_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_shaman_drum.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_clover_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_cup_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_gloves_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_horn_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_sandals_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_scales_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_shield_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame1/icondrop_shaman_drum_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/underwater_constructions_bubble.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/icondrop_scroll_of_the_builder_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/icondrop_scroll_of_the_builder_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/icondrop_scroll_of_the_builder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/icondrop_stick_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/icondrop_stick_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/ingame3/icondrop_stick.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
