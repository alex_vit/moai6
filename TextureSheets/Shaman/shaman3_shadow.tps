<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Shaman/shaman3_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Shaman/shaman3_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_052.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,41,210,82</rect>
                <key>scale9Paddings</key>
                <rect>105,41,210,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/spasibo_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/pered_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman3_shadow/mashet_rukoy_016.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
