<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Shaman/shaman1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Shaman/shaman1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>150</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_261.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_263.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_264.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_265.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_267.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_269.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_271.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_273.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_274.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_275.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_277.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_279.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_281.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_283.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_284.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_285.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_286.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,43,228,86</rect>
                <key>scale9Paddings</key>
                <rect>114,43,228,86</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_261.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_263.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_264.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_265.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_267.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_269.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_271.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_273.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_274.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_275.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_277.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_279.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_281.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_283.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_284.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_285.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1_shadow/pray_024.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
