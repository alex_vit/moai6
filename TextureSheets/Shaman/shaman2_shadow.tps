<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Shaman/shaman2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Shaman/shaman2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/final_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_114.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,41,210,82</rect>
                <key>scale9Paddings</key>
                <rect>105,41,210,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/final_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman2_shadow/pomoshy_113.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
