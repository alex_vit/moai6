<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Shaman/shaman1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Shaman/shaman1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_261.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_263.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_264.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_265.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_267.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_269.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_271.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_273.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_274.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_275.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_277.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_279.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_281.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_283.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_284.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_285.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/shaman1/pray_286.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,74,110,148</rect>
                <key>scale9Paddings</key>
                <rect>55,74,110,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_261.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_263.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_264.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_265.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_267.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_269.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_271.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_273.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_274.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_275.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_277.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_279.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_281.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_283.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_284.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_285.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/shaman1/pray_028.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
