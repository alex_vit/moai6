<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant11_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant11_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_148.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>195,67,390,134</rect>
                <key>scale9Paddings</key>
                <rect>195,67,390,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10_shadow/sleep_left_138.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
