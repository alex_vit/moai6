<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant7.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant7.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant6/idle2_left79.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,104,146,208</rect>
                <key>scale9Paddings</key>
                <rect>73,104,146,208</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/bridge_down.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/bridge_right.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/make_bridge_left82.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>188,176,376,352</rect>
                <key>scale9Paddings</key>
                <rect>188,176,376,352</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant7/bridge_left.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,189,202,378</rect>
                <key>scale9Paddings</key>
                <rect>101,189,202,378</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant6/idle2_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/bridge_down.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/bridge_left.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/bridge_right.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant7/make_bridge_left70.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
