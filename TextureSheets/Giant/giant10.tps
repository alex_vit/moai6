<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant10.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant10.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant10/sleep_left_062.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,105,140,210</rect>
                <key>scale9Paddings</key>
                <rect>70,105,140,210</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9/sleep_down_148.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>86,121,172,242</rect>
                <key>scale9Paddings</key>
                <rect>86,121,172,242</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9/sleep_down_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant10/sleep_left_033.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
