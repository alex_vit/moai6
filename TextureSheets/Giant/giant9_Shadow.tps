<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant9_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant9_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_147.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>163,66,326,132</rect>
                <key>scale9Paddings</key>
                <rect>163,66,326,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_080.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>184,64,368,128</rect>
                <key>scale9Paddings</key>
                <rect>184,64,368,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant8_shadow/sleep_right_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant9_shadow/sleep_down_078.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
