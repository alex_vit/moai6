<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant2_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant2_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right49.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>176,66,352,132</rect>
                <key>scale9Paddings</key>
                <rect>176,66,352,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down117.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>189,64,378,128</rect>
                <key>scale9Paddings</key>
                <rect>189,64,378,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant1_shadow/idle1_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2_shadow/talk_down115.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
