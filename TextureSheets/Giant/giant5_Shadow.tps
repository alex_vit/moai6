<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant5_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant5_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right145.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>185,68,370,136</rect>
                <key>scale9Paddings</key>
                <rect>185,68,370,136</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right21.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>186,68,372,136</rect>
                <key>scale9Paddings</key>
                <rect>186,68,372,136</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant4_shadow/talk_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant5_shadow/idle2_right20.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
