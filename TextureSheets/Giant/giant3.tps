<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Giant/giant3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Giant/giant3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant2/talk_down130.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>95,120,190,240</rect>
                <key>scale9Paddings</key>
                <rect>95,120,190,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/giant3/talk_left132.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,104,150,208</rect>
                <key>scale9Paddings</key>
                <rect>75,104,150,208</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant2/talk_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/giant3/talk_left132.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
