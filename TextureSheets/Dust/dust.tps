<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Dust/dust.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Dust.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust1_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust2_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/dust/dust3_41.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,29,72,57</rect>
                <key>scale9Paddings</key>
                <rect>36,29,72,57</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust2_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust3_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/dust/dust1_06.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
