<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Anglerfish/anglerfish_left1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Anglerfish/anglerfish_left1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left79.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,75,232,150</rect>
                <key>scale9Paddings</key>
                <rect>116,75,232,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2/action_left108.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>122,84,244,168</rect>
                <key>scale9Paddings</key>
                <rect>122,84,244,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1/idle1_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2/action_left104.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
