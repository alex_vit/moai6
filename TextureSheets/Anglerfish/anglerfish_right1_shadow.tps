<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Anglerfish/anglerfish_right1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Anglerfish/anglerfish_right1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right78.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>157,62,314,124</rect>
                <key>scale9Paddings</key>
                <rect>157,62,314,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right108.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>162,69,324,138</rect>
                <key>scale9Paddings</key>
                <rect>162,69,324,138</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right72.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>168,66,336,132</rect>
                <key>scale9Paddings</key>
                <rect>168,66,336,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish1_shadow/idle1_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish2_shadow/action_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/anglerfish3_shadow/talk_right54.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
