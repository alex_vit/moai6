<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Obstacles_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Obstacles_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_quest_objects1_shadows/queststoneboulder_shadow_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_quest_objects1_shadows/queststoneboulder_shadow_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>118,62,236,124</rect>
                <key>scale9Paddings</key>
                <rect>118,62,236,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/basket.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,49,188,98</rect>
                <key>scale9Paddings</key>
                <rect>94,49,188,98</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/breadfruit0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/breadfruit1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/breadfruit2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>235,84,470,168</rect>
                <key>scale9Paddings</key>
                <rect>235,84,470,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crab.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,37,106,74</rect>
                <key>scale9Paddings</key>
                <rect>53,37,106,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crown1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crown2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crown3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>77,40,154,80</rect>
                <key>scale9Paddings</key>
                <rect>77,40,154,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_blue.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_blue_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_green.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_green_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_purple.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_purple_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_red.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_red_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_yellow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_yellow_f.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,61,274,122</rect>
                <key>scale9Paddings</key>
                <rect>137,61,274,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_turquoise.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_turquoise_f.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,67,274,134</rect>
                <key>scale9Paddings</key>
                <rect>137,67,274,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein1_shadow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein2_shadow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein3_shadow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein4_shadow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein5_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>102,54,204,108</rect>
                <key>scale9Paddings</key>
                <rect>102,54,204,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/haystack.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>126,78,252,156</rect>
                <key>scale9Paddings</key>
                <rect>126,78,252,156</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/leavespile.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>136,76,272,152</rect>
                <key>scale9Paddings</key>
                <rect>136,76,272,152</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/magic_flower.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,39,146,78</rect>
                <key>scale9Paddings</key>
                <rect>73,39,146,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_lavas_road_hole01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_lavas_road_hole02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>86,46,172,92</rect>
                <key>scale9Paddings</key>
                <rect>86,46,172,92</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_seashell0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_seashell1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_seashell2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>76,39,152,78</rect>
                <key>scale9Paddings</key>
                <rect>76,39,152,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_1_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_1_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_2_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_2_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_3_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_3_v.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>153,74,306,148</rect>
                <key>scale9Paddings</key>
                <rect>153,74,306,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>110,50,220,100</rect>
                <key>scale9Paddings</key>
                <rect>110,50,220,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/questrocksdam0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/questrocksdam1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,87,72,174</rect>
                <key>scale9Paddings</key>
                <rect>36,87,72,174</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/red_coral_tree_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/red_coral_tree_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/white_coral_tree_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/white_coral_tree_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/yellow_coral_tree_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/yellow_coral_tree_v.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>176,75,352,150</rect>
                <key>scale9Paddings</key>
                <rect>176,75,352,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle0_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle0_50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle0_75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle1_00.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,47,144,94</rect>
                <key>scale9Paddings</key>
                <rect>72,47,144,94</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/sapphirevein1_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>77,48,154,96</rect>
                <key>scale9Paddings</key>
                <rect>77,48,154,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/seaplant_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>99,49,198,98</rect>
                <key>scale9Paddings</key>
                <rect>99,49,198,98</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/seashell1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/seashell2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/seashell3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,41,144,82</rect>
                <key>scale9Paddings</key>
                <rect>72,41,144,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/signpost_0_shadow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/signpostshadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,46,97,91</rect>
                <key>scale9Paddings</key>
                <rect>48,46,97,91</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/snowdrift_shadow_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/snowdrift_shadow_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>139,61,278,122</rect>
                <key>scale9Paddings</key>
                <rect>139,61,278,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/snowdriftsmall_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,19,43,38</rect>
                <key>scale9Paddings</key>
                <rect>22,19,43,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/stalagmit_shadow_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/stalagmit_shadow_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/stalagmit_shadow_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,45,194,90</rect>
                <key>scale9Paddings</key>
                <rect>97,45,194,90</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/starfish1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/starfish2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/starfish3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,43,118,86</rect>
                <key>scale9Paddings</key>
                <rect>59,43,118,86</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/thicket_shadow_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/thicket_shadow_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/thicket_shadow_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,37,93,75</rect>
                <key>scale9Paddings</key>
                <rect>46,37,93,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_0_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_0_50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_0_75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_1_00.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,48,210,96</rect>
                <key>scale9Paddings</key>
                <rect>105,48,210,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/basket.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/breadfruit0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/breadfruit1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/breadfruit2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crab.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crown1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crown2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crown3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_blue_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_blue.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_green_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_green.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_purple_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_purple.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_red_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_red.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_turquoise_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_turquoise.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_yellow_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/crystals_yellow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein1_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein2_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein3_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein4_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/goldvein5_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/haystack.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/magic_flower.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_lavas_road_hole01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_lavas_road_hole02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_seashell0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_seashell1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/moai4_seashell2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_1_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_1_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_2_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_2_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_3_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/palmX_3_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/questrocksdam0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/questrocksdam1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/red_coral_tree_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/red_coral_tree_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle0_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle0_50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle0_75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/rocksobstacle1_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/sapphirevein1_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/seaplant_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/seashell1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/seashell2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/seashell3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/snowdrift_shadow_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/snowdrift_shadow_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/stalagmit_shadow_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/stalagmit_shadow_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/stalagmit_shadow_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/starfish1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/starfish2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/starfish3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/white_coral_tree_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/white_coral_tree_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_0_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_0_50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_0_75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/woods01_1_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/yellow_coral_tree_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/yellow_coral_tree_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_quest_objects1_shadows/queststoneboulder_shadow_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_quest_objects1_shadows/queststoneboulder_shadow_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/leavespile.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/paporotnik3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/snowdriftsmall_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/signpost_0_shadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/signpostshadow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/thicket_shadow_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/thicket_shadow_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles_shadow/thicket_shadow_03.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
