<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Lizardman/Lizardman2_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Lizardman/Lizardman2_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>100</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right79.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>157,50,314,100</rect>
                <key>scale9Paddings</key>
                <rect>157,50,314,100</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_down79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/lizardman2_shadow/idle2_right79.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
