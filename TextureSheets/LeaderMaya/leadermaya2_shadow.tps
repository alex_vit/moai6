<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/LeaderMaya/leadermaya2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/LeaderMaya/leadermaya2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>100</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left069.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,36,226,72</rect>
                <key>scale9Paddings</key>
                <rect>113,36,226,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left168.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>115,38,230,76</rect>
                <key>scale9Paddings</key>
                <rect>115,38,230,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya1_shadow/talk_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle1_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya2_shadow/idle2_left167.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
