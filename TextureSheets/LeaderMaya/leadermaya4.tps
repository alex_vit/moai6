<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/LeaderMaya/leadermaya4.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/LeaderMaya/leadermaya4.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya3/talk_left097.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,73,96,146</rect>
                <key>scale9Paddings</key>
                <rect>48,73,96,146</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right024.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>68,73,136,146</rect>
                <key>scale9Paddings</key>
                <rect>68,73,136,146</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya3/talk_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle3_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/leader_maya4/idle4_right024.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
