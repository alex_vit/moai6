<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Farmer/Farmer1_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Farmer/Farmer1_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>89,32,178,64</rect>
                <key>scale9Paddings</key>
                <rect>89,32,178,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_head_up8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/idle68.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
