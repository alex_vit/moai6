<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Farmer/Farmer3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Farmer/Farmer3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/idle_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/mow_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/sows_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer2/walk_down50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,63,140,126</rect>
                <key>scale9Paddings</key>
                <rect>70,63,140,126</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/mow_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/sows_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/walk_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer2/idle_right20.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
