<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Farmer/Farmer1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Farmer/Farmer1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/ask_help115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_head_up8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/idle_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1/look70.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,60,98,120</rect>
                <key>scale9Paddings</key>
                <rect>49,60,98,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_head_up8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/idle70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1/look54.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
