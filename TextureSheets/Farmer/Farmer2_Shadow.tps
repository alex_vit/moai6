<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Farmer/Farmer2_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Farmer/Farmer2_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/look70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>89,32,178,64</rect>
                <key>scale9Paddings</key>
                <rect>89,32,178,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/farmer1_shadow/walk_up40.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
