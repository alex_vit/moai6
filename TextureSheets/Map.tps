<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Map.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Map.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,73,110,145</rect>
                <key>scale9Paddings</key>
                <rect>55,73,110,145</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_4.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_6.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_7.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_8.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1250,1250,2500,2500</rect>
                <key>scale9Paddings</key>
                <rect>1250,1250,2500,2500</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/btn_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/btn_inactive.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/btn_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/btn_pushed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>104,24,207,47</rect>
                <key>scale9Paddings</key>
                <rect>104,24,207,47</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_object_19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,54,58,108</rect>
                <key>scale9Paddings</key>
                <rect>29,54,58,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/flag_shadow_19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>82,21,164,42</rect>
                <key>scale9Paddings</key>
                <rect>82,21,164,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_base1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,58,200,116</rect>
                <key>scale9Paddings</key>
                <rect>100,58,200,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_base2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>78,58,156,116</rect>
                <key>scale9Paddings</key>
                <rect>78,58,156,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base4.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_pushed.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_pushed.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_pushed.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_base3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_base4.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_pushed.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_pushed.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_pushed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,62,168,124</rect>
                <key>scale9Paddings</key>
                <rect>84,62,168,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/leftbottom.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>247,103,493,206</rect>
                <key>scale9Paddings</key>
                <rect>247,103,493,206</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/lefttop.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>233,184,466,368</rect>
                <key>scale9Paddings</key>
                <rect>233,184,466,368</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/rightbottom.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>248,105,496,210</rect>
                <key>scale9Paddings</key>
                <rect>248,105,496,210</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/map_interface/righttop.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>235,104,469,208</rect>
                <key>scale9Paddings</key>
                <rect>235,104,469,208</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/map_interface/00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/btn_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/btn_inactive.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/btn_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/btn_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_object_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/flag_shadow_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_base4.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_gold_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_green_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_base1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_base2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_base3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_base4.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_green_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_red_pushed.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/leftbottom.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/lefttop.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/rightbottom.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/righttop.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_4.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_6.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_7.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_8.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_9.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/bonus way/bonus_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_ball_red_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/map_interface/indicator_star_gold_disable.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
