<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Skyforge/sf02_2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Buildings/Skyforge/sf02_2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,80,175,160</rect>
                <key>scale9Paddings</key>
                <rect>88,80,175,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_02/sf2_crystal_02_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_03/sf2_crystal_03_165.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
