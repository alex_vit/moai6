<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Skyforge/sf02_1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Buildings/Skyforge/sf02_1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,80,175,160</rect>
                <key>scale9Paddings</key>
                <rect>88,80,175,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_hole/sf2_center_hole_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/center_light/sf2_center_light_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf2/crystal_01/sf2_crystal_01_158.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
