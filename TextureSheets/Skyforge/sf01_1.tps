<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Skyforge/sf01_1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Buildings/Skyforge/sf01_1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,80,175,160</rect>
                <key>scale9Paddings</key>
                <rect>88,80,175,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_hole/sf1_center_hole219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/center_light/sf1_center_light1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_01/sf1_crystal01_2.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
