<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Skyforge/sf01_3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Buildings/Skyforge/sf01_3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,80,175,160</rect>
                <key>scale9Paddings</key>
                <rect>88,80,175,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/crystal_04/sf1_crystal04_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/skyforge/sf1/rune_light/sf1_rune_light_219.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
