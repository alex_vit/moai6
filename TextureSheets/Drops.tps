<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Drops.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Drops.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_book.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_butterfly.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_cloth.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_key.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_mask.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_nettle.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_pollen.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_pumpkin_vessel.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_quartz.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_rope.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_round.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_sea_urchin.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/drops_knife.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,45,73,89</rect>
                <key>scale9Paddings</key>
                <rect>37,45,73,89</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/artefact19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/bomb.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/crown.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/crystals.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/green_crystall.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_clover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_gloves.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_horn_of_plenty.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_sandals.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_scales.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_scroll_of_the_builder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_shaman_drum.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_shield.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_stick.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai2_quest2a.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai2_quest2b.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai2_quest2c.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_ancient_key.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_ancient_relic.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_beading.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_bozhok.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_fern.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_flower.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_gold_necklace.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_gold_nugget.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_mabdrake_root.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_magic_bottle.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_magic_flower.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_magic_sands.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_map_piece1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_map_piece2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_map_piece3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_maya_amulet.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_mirror.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_puma.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_purple_fire.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_sapphire.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai3_sapphire_uncut.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_blueprint.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_compass.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_fishing_net.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ikaika_helmet.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_feather.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_gems.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_glass.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_grass_tee.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_leather.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_mushroom_kai_kai.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_orichalcum.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_rope.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_ingredient_sea_grapes.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_necklace.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_orb.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_pipkin.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_rum.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_seashell.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_spyglass.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_wand.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai4_waterplant.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_bead.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_clothing.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_dome.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_fire_tree_berries.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_guitar.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_honey.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_hot_watter.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_bean.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_comb.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_mushroom.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_pillar.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_posidonia_fruit.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_rune11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_scroll_with_letters.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_sea_tree_berries.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_sleepy_potion.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_snow_tree_berries.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_snowball.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_stone_of_dreams.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_swamp_tree_berries.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_undeground_tree_berries.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/moai5_wooden_tablet_with_letters.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/pearl.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_coconaut_oil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_food.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_gold.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_mana.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_metal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_oil.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_rocks.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_sandal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_unit.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/res_wood.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/ritual_stone.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/star_fish.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/statue_piece.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/water.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/wizard_statue_piece.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,89,146,178</rect>
                <key>scale9Paddings</key>
                <rect>73,89,146,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/drops/miracle_cup_of_fertility.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>74,89,148,178</rect>
                <key>scale9Paddings</key>
                <rect>74,89,148,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/artefact19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/bomb.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/crown.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/crystals.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/green_crystall.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_clover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_cup_of_fertility.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_gloves.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_horn_of_plenty.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_sandals.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_scales.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_shaman_drum.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_shield.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai1_amulet_piece04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai2_quest2a.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai2_quest2b.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai2_quest2c.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_ancient_key.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_ancient_relic.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_beading.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_bozhok.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_fern.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_flower.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_gold_necklace.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_gold_nugget.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_mabdrake_root.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_magic_bottle.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_magic_flower.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_magic_sands.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_map_piece1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_map_piece2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_map_piece3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_maya_amulet.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_mirror.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_puma.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_purple_fire.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_sapphire_uncut.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai3_sapphire.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_blueprint.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_compass.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_fishing_net.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ikaika_helmet.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_feather.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_gems.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_glass.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_grass_tee.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_leather.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_mushroom_kai_kai.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_orichalcum.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_rope.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_ingredient_sea_grapes.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_necklace.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_orb.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_pipkin.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_rum.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_seashell.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_spyglass.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_wand.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai4_waterplant.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_bead.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_clothing.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_dome.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_fire_tree_berries.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_guitar.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_honey.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_hot_watter.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_bean.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_comb.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_magic_symbol04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_mushroom.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_pillar.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_posidonia_fruit.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_rune11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_scroll_with_letters.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_sea_tree_berries.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_sleepy_potion.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_snow_tree_berries.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_snowball.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_stone_of_dreams.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_swamp_tree_berries.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_undeground_tree_berries.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/moai5_wooden_tablet_with_letters.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/pearl.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_coconaut_oil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_food.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_gold.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_mana.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_metal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_oil.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_rocks.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_sandal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_unit.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/res_wood.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/ritual_stone.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/star_fish.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/statue_piece.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/water.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/wizard_statue_piece.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_rope.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_round.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_sea_urchin.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_book.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_butterfly.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_cloth.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_key.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_mask.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_nettle.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_pollen.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_pumpkin_vessel.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_quartz.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/drops_knife.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_scroll_of_the_builder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/drops/miracle_stick.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
