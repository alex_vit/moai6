<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Woodcutter/woodcutter1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Woodcutter/woodcutter1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks106.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>91,31,182,62</rect>
                <key>scale9Paddings</key>
                <rect>91,31,182,62</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_headup8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/idle_look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1_shadow/thanks103.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
