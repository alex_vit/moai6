<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Woodcutter/woodcutter2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Woodcutter/woodcutter2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter1/walk_right50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>49,60,98,120</rect>
                <key>scale9Paddings</key>
                <rect>49,60,98,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/action77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/dig_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>72,79,144,158</rect>
                <key>scale9Paddings</key>
                <rect>72,79,144,158</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter1/walk_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_with_wood_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/action77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/dig_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/woodcutter2/walk_left23.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
