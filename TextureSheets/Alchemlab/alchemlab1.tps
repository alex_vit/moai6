<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Alchemlab/alchemlab1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Buildings/Alchemlab/alchemlab1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/al_static.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00000 - 00060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/ground.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/shadow.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00060.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,75,150,150</rect>
                <key>scale9Paddings</key>
                <rect>75,75,150,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/broken_final.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/alchemlab/broken_shadow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>90,90,180,181</rect>
                <key>scale9Paddings</key>
                <rect>90,90,180,181</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/bubbles/bubbles_00173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/fire/fire_00180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_01/gear_01_00000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00000 - 00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/gear_02/gear_02_00180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/mexa/mexa_00120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/tent/tent_00005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/al_static.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/shadow.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/broken_final.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/alchemlab/broken_shadow.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
