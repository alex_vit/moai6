<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Trader/trader3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Trader/trader3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_final0126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0379.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0380.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0382.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0383.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0385.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0386.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0388.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0389.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0391.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0392.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0394.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0395.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0397.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0398.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0400.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0401.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0403.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0404.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0406.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0407.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0409.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0410.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0412.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0413.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0415.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0416.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0418.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0419.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0421.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0422.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0424.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0425.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0427.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0428.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0430.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0431.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0433.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0434.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0436.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0437.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0439.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0440.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0442.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0443.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0445.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0446.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0448.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0449.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0451.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0452.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0454.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0455.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0457.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0458.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0460.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0461.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0463.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0464.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0466.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0467.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0469.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0470.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0472.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0473.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0475.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0476.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0478.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0479.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0481.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0482.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0484.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0485.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0487.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0488.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0490.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0491.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0493.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0494.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0496.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0497.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0499.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0500.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0502.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0503.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0505.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0506.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0508.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0509.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0511.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0512.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0514.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0515.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0517.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0518.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0520.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0521.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0523.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0524.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0526.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0527.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0529.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0530.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0532.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0533.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0535.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0536.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0538.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0539.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0541.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0542.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0544.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0545.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0547.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0548.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0550.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0551.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0553.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0554.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0556.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0557.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0559.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0560.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0562.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0563.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0565.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0566.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0568.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0569.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0571.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0572.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0574.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0575.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0577.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0578.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0580.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0581.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0583.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0584.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0586.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0587.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0589.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0590.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0592.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0593.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0595.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0596.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0598.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0599.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0601.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0602.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0604.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0605.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0607.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0608.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0610.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0611.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0613.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0614.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0616.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0617.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0619.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0620.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0622.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0623.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0625.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0626.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0628.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0629.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0631.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0632.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0634.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0635.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0637.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0638.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0640.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0641.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0643.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0644.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0646.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0647.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0649.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0650.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0652.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0653.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0655.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0656.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0658.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0659.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0661.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0662.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0664.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2/torgash_telo0665.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,62,112,124</rect>
                <key>scale9Paddings</key>
                <rect>56,62,112,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_final0126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0379.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0380.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0382.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0383.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0385.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0386.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0388.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0389.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0391.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0392.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0394.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0395.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0397.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0398.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0400.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0401.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0403.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0404.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0406.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0407.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0409.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0410.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0412.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0413.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0415.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0416.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0418.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0419.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0421.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0422.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0424.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0425.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0427.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0428.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0430.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0431.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0433.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0434.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0436.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0437.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0439.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0440.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0442.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0443.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0445.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0446.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0448.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0449.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0451.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0452.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0454.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0455.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0457.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0458.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0460.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0461.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0463.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0464.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0466.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0467.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0469.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0470.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0472.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0473.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0475.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0476.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0478.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0479.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0481.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0482.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0484.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0485.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0487.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0488.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0490.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0491.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0493.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0494.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0496.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0497.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0499.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0500.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0502.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0503.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0505.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0506.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0508.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0509.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0511.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0512.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0514.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0515.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0517.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0518.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0520.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0521.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0523.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0524.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0526.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0527.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0529.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0530.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0532.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0533.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0535.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0536.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0538.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0539.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0541.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0542.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0544.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0545.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0547.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0548.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0550.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0551.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0553.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0554.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0556.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0557.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0559.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0560.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0562.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0563.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0565.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0566.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0568.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0569.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0571.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0572.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0574.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0575.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0577.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0578.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0580.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0581.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0583.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0584.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0586.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0587.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0589.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0590.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0592.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0593.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0595.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0596.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0598.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0599.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0601.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0602.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0604.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0605.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0607.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0608.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0610.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0611.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0613.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0614.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0616.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0617.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0619.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0620.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0622.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0623.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0625.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0626.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0628.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0629.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0631.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0632.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0634.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0635.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0637.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0638.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0640.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0641.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0643.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0644.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0646.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0647.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0649.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0650.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0652.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0653.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0655.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0656.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0658.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0659.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0661.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0662.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0664.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_telo0665.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2/torgash_ask_help0041.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
