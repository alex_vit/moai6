<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Trader/trader1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Trader/trader1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0261.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0264.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0265.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0267.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0271.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0274.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0275.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0277.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0281.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0284.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0285.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0287.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0288.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0290.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0291.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0292.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0294.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0295.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0297.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0298.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0300.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0301.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0302.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0304.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0305.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0307.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0308.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0310.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0311.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0312.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0314.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0315.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0317.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0318.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0320.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0321.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0322.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0324.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0325.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0327.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0328.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0330.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0331.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0332.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0334.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0335.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0337.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0338.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0340.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0341.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0342.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0344.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0345.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0347.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0348.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0350.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0351.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0352.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0354.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0355.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0357.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0358.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0360.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0361.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0362.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0364.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0365.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0367.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0368.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0370.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0371.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0372.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0374.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0375.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0377.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0378.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0667.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0668.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0670.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0671.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0672.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0674.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0675.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0677.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0678.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0680.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0681.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0682.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0684.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0685.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0687.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0688.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0690.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0691.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0692.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0694.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0695.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0697.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0698.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0700.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0701.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0702.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0704.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0705.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0707.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0708.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0710.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0711.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0712.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0714.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0715.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0717.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0718.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,33,186,66</rect>
                <key>scale9Paddings</key>
                <rect>93,33,186,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0717.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0718.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0261.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0264.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0265.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0267.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0271.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0274.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0275.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0277.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0281.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0284.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0285.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0287.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0288.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0290.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0291.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0292.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0294.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0295.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0297.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0298.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0300.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0301.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0302.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0304.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0305.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0307.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0308.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0310.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0311.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0312.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0314.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0315.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0317.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0318.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0320.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0321.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0322.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0324.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0325.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0327.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0328.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0330.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0331.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0332.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0334.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0335.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0337.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0338.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0340.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0341.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0342.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0344.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0345.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0347.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0348.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0350.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0351.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0352.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0354.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0355.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0357.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0358.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0360.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0361.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0362.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0364.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0365.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0367.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0368.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0370.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0371.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0372.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0374.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0375.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0377.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0378.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0667.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0668.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0670.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0671.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0672.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0674.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0675.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0677.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0678.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0680.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0681.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0682.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0684.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0685.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0687.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0688.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0690.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0691.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0692.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0694.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0695.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0697.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0698.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0700.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0701.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0702.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0704.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0705.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0707.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0708.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0710.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0711.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0712.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0714.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader1_shadow/torgash_telo0715.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
