<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Trader/trader3_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Trader/trader3_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0379.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0380.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0382.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0383.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0385.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0386.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0388.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0389.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0391.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0392.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0394.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0395.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0398.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0400.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0401.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0403.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0404.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0406.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0409.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0410.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0412.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0413.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0415.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0416.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0418.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0419.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0421.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0422.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0424.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0425.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0428.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0430.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0431.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0433.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0434.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0436.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0439.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0440.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0442.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0443.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0445.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0446.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0448.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0449.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0451.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0452.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0454.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0455.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0458.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0460.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0461.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0463.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0464.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0466.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0469.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0470.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0472.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0473.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0475.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0476.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0478.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0479.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0481.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0482.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0484.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0485.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0488.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0490.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0491.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0493.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0494.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0496.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0499.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0500.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0502.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0503.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0505.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0506.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0508.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0509.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0511.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0512.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0514.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0515.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0518.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0520.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0521.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0523.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0524.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0526.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0529.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0530.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0532.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0533.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0535.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0536.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0538.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0539.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0541.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0542.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0544.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0545.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0548.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0550.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0551.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0553.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0554.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0556.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0559.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0560.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0562.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0563.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0565.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0566.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0568.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0569.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0571.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0572.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0574.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0575.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0578.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0580.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0581.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0583.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0584.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0586.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0589.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0590.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0592.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0593.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0595.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0596.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0598.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0599.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0601.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0602.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0604.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0605.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0608.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0610.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0611.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0613.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0614.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0616.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0619.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0620.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0622.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0623.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0625.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0626.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0628.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0629.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0631.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0632.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0634.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0635.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0638.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0640.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0641.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0643.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0644.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0646.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0649.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0650.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0652.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0653.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0655.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0656.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0658.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0659.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0661.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0662.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0664.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0665.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,34,186,68</rect>
                <key>scale9Paddings</key>
                <rect>93,34,186,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_final0126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0379.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0380.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0382.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0383.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0385.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0386.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0388.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0389.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0391.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0392.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0394.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0395.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0398.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0400.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0401.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0403.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0404.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0406.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0409.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0410.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0412.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0413.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0415.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0416.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0418.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0419.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0421.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0422.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0424.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0425.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0428.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0430.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0431.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0433.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0434.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0436.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0439.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0440.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0442.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0443.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0445.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0446.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0448.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0449.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0451.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0452.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0454.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0455.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0458.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0460.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0461.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0463.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0464.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0466.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0469.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0470.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0472.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0473.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0475.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0476.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0478.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0479.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0481.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0482.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0484.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0485.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0488.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0490.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0491.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0493.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0494.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0496.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0499.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0500.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0502.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0503.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0505.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0506.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0508.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0509.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0511.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0512.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0514.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0515.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0518.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0520.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0521.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0523.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0524.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0526.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0529.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0530.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0532.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0533.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0535.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0536.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0538.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0539.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0541.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0542.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0544.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0545.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0548.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0550.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0551.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0553.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0554.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0556.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0559.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0560.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0562.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0563.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0565.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0566.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0568.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0569.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0571.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0572.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0574.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0575.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0578.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0580.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0581.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0583.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0584.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0586.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0589.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0590.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0592.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0593.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0595.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0596.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0598.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0599.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0601.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0602.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0604.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0605.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0608.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0610.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0611.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0613.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0614.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0616.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0619.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0620.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0622.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0623.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0625.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0626.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0628.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0629.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0631.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0632.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0634.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0635.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0638.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0640.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0641.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0643.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0644.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0646.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0649.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0650.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0652.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0653.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0655.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0656.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0658.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0659.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0661.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0662.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0664.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_telo_teny0665.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/trader2_shadow/torgash_ask_help0049.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
