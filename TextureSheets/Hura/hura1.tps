<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Hura/hura1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Hura/hura1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action261.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action263.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action264.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action265.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action267.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action269.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action271.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action273.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action274.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action275.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action277.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action279.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action281.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action283.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action284.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action285.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action286.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action287.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1/action288.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,91,118,182</rect>
                <key>scale9Paddings</key>
                <rect>59,91,118,182</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action261.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action263.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action264.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action265.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action267.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action269.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action271.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action273.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action274.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action275.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action277.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action279.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action281.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action283.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action284.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action285.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action287.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action288.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1/action020.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
