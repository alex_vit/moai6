<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Hura/hura1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Hura/hura1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>80</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action261.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action263.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action264.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action267.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action269.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action271.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action273.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action274.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action277.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action279.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action281.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action283.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action284.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action286.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action287.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura1_shadow/action288.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>118,64,236,128</rect>
                <key>scale9Paddings</key>
                <rect>118,64,236,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action261.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action263.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action264.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action267.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action269.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action271.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action273.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action274.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action277.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action279.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action281.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action283.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action284.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action287.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action288.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura1_shadow/action017.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
