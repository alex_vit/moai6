<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Hura/hura2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Hura/hura2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action289.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action290.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action291.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action292.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action293.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action294.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action295.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action296.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action297.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action298.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action299.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action300.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action301.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action302.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action303.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action304.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action305.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action306.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action307.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action308.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action309.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action310.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action311.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action312.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action313.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action314.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action315.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action316.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action317.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action318.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action319.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/hura2/action320.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,91,118,182</rect>
                <key>scale9Paddings</key>
                <rect>59,91,118,182</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action311.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action312.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action313.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action314.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action315.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action316.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action317.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action318.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action319.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action320.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action289.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action290.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action291.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action292.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action293.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action294.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action295.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action296.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action297.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action298.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action299.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action300.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action301.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action302.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action303.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action304.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action305.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action306.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action307.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action308.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action309.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/hura2/action310.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
