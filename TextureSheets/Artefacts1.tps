<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Artefacts1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Artefacts1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/00_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/01_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/02_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/03_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/04_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/05_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/06_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/07_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/08_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/09_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/10_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/11_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/12_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/13_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/14_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/15_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/16_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/17_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_00_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_01_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_02_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_03_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_04_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_05_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_06_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_07_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_08_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_09_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_10_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/rune_11_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/sign_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/sign_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/sign_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/sign_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/sign_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,54,107,107</rect>
                <key>scale9Paddings</key>
                <rect>54,54,107,107</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/ability00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/ability01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/ability02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,50,99,99</rect>
                <key>scale9Paddings</key>
                <rect>50,50,99,99</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/btn_make_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/btn_make_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/btn_make_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/btn_upgrade_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/btn_upgrade_hover.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/btn_upgrade_normal.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,21,42,42</rect>
                <key>scale9Paddings</key>
                <rect>21,21,42,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,14,69,28</rect>
                <key>scale9Paddings</key>
                <rect>35,14,69,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_feather.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_feather_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_gems.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_gems_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_glass.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_glass_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_grass_tee.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_grass_tee_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_leather.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_leather_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_mushroom_kai_kai.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_mushroom_kai_kai_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_orichalcum.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_orichalcum_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_rope.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_rope_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_sea_grapes.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_sea_grapes_gray.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingridient_counter.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/square_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/square_normal.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,35,70,70</rect>
                <key>scale9Paddings</key>
                <rect>35,35,70,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/img_small_btn_connector.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,11,14</rect>
                <key>scale9Paddings</key>
                <rect>6,7,11,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/progress_arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,14,1,27</rect>
                <key>scale9Paddings</key>
                <rect>1,14,1,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_07_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_08_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_09_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_10_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_11_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/sign_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/sign_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/sign_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/sign_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/sign_04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/square_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/square_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/00_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/01_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/02_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/03_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/04_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/05_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/06_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/07_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/08_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/09_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/10_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/11_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/12_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/13_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/14_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/15_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/16_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/17_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/ability00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/ability01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/ability02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/btn_make_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/btn_make_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/btn_make_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/btn_upgrade_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/btn_upgrade_hover.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/btn_upgrade_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_artefact_grade_3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_feather.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_feather_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_gems.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_gems_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_glass.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_glass_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_grass_tee.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_grass_tee_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_leather.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_leather_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_mushroom_kai_kai.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_mushroom_kai_kai_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_orichalcum.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_orichalcum_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_rope.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_rope_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_sea_grapes.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingredient_sea_grapes_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_ingridient_counter.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/img_small_btn_connector.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/progress_arrow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_00_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_01_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_02_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_03_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_04_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_05_gray.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/rune_06_gray.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
