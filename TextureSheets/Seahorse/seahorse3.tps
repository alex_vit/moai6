<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Seahorse/seahorse3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Seahorse/seahorse3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right98.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,74,100,148</rect>
                <key>scale9Paddings</key>
                <rect>50,74,100,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_left98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/seahorse/seahorse2/talk_right88.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
