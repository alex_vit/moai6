<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Indus/IndusLeft1_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Indus/IndusLeft1_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_200.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,28,188,56</rect>
                <key>scale9Paddings</key>
                <rect>94,28,188,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_232.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>95,27,190,54</rect>
                <key>scale9Paddings</key>
                <rect>95,27,190,54</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus3_shadow/idle1_left_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus5_shadow/idle2_left_232.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
