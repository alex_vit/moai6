<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Indus/IndusRight1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Indus/IndusRight1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus1/idle1_right_200.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>58,66,116,132</rect>
                <key>scale9Paddings</key>
                <rect>58,66,116,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4/idle2_right_232.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,66,62,132</rect>
                <key>scale9Paddings</key>
                <rect>31,66,62,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus1/idle1_right_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4/idle2_right_232.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
