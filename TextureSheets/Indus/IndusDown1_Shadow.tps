<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Indus/IndusDown1_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Indus/IndusDown1_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>100</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_200.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>111,32,222,64</rect>
                <key>scale9Paddings</key>
                <rect>111,32,222,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_232.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>96,30,192,60</rect>
                <key>scale9Paddings</key>
                <rect>96,30,192,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus2_shadow/idle1_down_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/indus4_shadow/idle2_down_232.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
