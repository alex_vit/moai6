<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Obstacles.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Obstacles.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/lockers_thicket/thicket_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lockers_thicket/thicket_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/lockers_thicket/thicket_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,37,93,75</rect>
                <key>scale9Paddings</key>
                <rect>46,37,93,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_quest_objects1/queststoneboulder_object_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_quest_objects1/queststoneboulder_object_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>82,92,164,184</rect>
                <key>scale9Paddings</key>
                <rect>82,92,164,184</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/basket.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,74,128,148</rect>
                <key>scale9Paddings</key>
                <rect>64,74,128,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/breadfruit0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/breadfruit0_1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/breadfruit1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/breadfruit1_1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/breadfruit2_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/breadfruit2_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,145,202,290</rect>
                <key>scale9Paddings</key>
                <rect>101,145,202,290</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crab_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crab_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crab_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crab_y_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crab_y_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crab_y_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,60,122,120</rect>
                <key>scale9Paddings</key>
                <rect>61,60,122,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crown1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crown2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crown3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,59,82,118</rect>
                <key>scale9Paddings</key>
                <rect>41,59,82,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_blue.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_blue_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_green.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_green_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_purple.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_purple_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_red.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_red_f.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_yellow.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_yellow_f.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>79,98,158,196</rect>
                <key>scale9Paddings</key>
                <rect>79,98,158,196</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_turquoise.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/crystals_turquoise_f.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,98,160,196</rect>
                <key>scale9Paddings</key>
                <rect>80,98,160,196</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/goldvein1_object.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/goldvein2_object.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/goldvein3_object.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/goldvein4_object.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/goldvein5_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>77,65,154,130</rect>
                <key>scale9Paddings</key>
                <rect>77,65,154,130</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/haystack.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,83,210,166</rect>
                <key>scale9Paddings</key>
                <rect>105,83,210,166</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/leavespile.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>133,82,266,164</rect>
                <key>scale9Paddings</key>
                <rect>133,82,266,164</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/magic_flower.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,62,70,124</rect>
                <key>scale9Paddings</key>
                <rect>35,62,70,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole01_over.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole02_over.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_seashell0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_seashell1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/moai4_seashell2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,46,124,92</rect>
                <key>scale9Paddings</key>
                <rect>62,46,124,92</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm1_1_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm1_1_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm1_2_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm1_2_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm1_3_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm1_3_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm2_1_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm2_1_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm2_2_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm2_2_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm2_3_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm2_3_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm3_1_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm3_1_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm3_2_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm3_2_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm3_3_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/palm3_3_v.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>120,90,240,180</rect>
                <key>scale9Paddings</key>
                <rect>120,90,240,180</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/paporotnik0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/paporotnik1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/paporotnik2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/paporotnik3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>86,79,172,158</rect>
                <key>scale9Paddings</key>
                <rect>86,79,172,158</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle1_horizontal_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle1_horizontal_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle1_horizontal_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle1_vertical_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle1_vertical_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle1_vertical_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle2_horizontal_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle2_horizontal_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle2_horizontal_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle2_vertical_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle2_vertical_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/puddle2_vertical_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>188,89,376,178</rect>
                <key>scale9Paddings</key>
                <rect>188,89,376,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/red_coral_tree_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/red_coral_tree_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/white_coral_tree_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/white_coral_tree_v.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/yellow_coral_tree_h.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/yellow_coral_tree_v.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>132,102,264,204</rect>
                <key>scale9Paddings</key>
                <rect>132,102,264,204</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/roadhole1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/roadhole2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/roadhole3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,45,128,90</rect>
                <key>scale9Paddings</key>
                <rect>64,45,128,90</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/rocksobstacle0_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/rocksobstacle0_50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/rocksobstacle0_75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/rocksobstacle1_00.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,59,124,118</rect>
                <key>scale9Paddings</key>
                <rect>62,59,124,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/sapphirevein1_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,60,126,120</rect>
                <key>scale9Paddings</key>
                <rect>63,60,126,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/seaplant_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>69,46,138,92</rect>
                <key>scale9Paddings</key>
                <rect>69,46,138,92</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/seaplant_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,130,122,260</rect>
                <key>scale9Paddings</key>
                <rect>61,130,122,260</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/seashell1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/seashell2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/seashell3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>43,51,86,102</rect>
                <key>scale9Paddings</key>
                <rect>43,51,86,102</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/snowdrift_object_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/snowdrift_object_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,82,228,164</rect>
                <key>scale9Paddings</key>
                <rect>114,82,228,164</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/snowdriftsmall_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,48,90,96</rect>
                <key>scale9Paddings</key>
                <rect>45,48,90,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_0_object_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_0_object_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_0_object_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_1_object_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_1_object_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_1_object_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_2_object_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_2_object_0_5.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/stalagmit_2_object_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,75,134,150</rect>
                <key>scale9Paddings</key>
                <rect>67,75,134,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/starfish1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/starfish2.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/starfish3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,43,110,86</rect>
                <key>scale9Paddings</key>
                <rect>55,43,110,86</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/woods01_0_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/woods01_0_50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/woods01_0_75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/obstacles/woods01_1_00.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>89,62,178,124</rect>
                <key>scale9Paddings</key>
                <rect>89,62,178,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/signpost/signpost_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/signpost/signpost_1.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/signpost/signpost_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,46,97,91</rect>
                <key>scale9Paddings</key>
                <rect>48,46,97,91</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/obstacles/basket.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/breadfruit0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/breadfruit0_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/breadfruit1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/breadfruit1_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/breadfruit2_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/breadfruit2_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crab_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crab_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crab_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crab_y_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crab_y_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crab_y_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crown1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crown2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crown3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_blue_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_blue.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_green_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_green.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_purple_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_purple.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_red_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_red.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_turquoise_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_turquoise.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_yellow_f.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/crystals_yellow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/goldvein1_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/goldvein2_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/goldvein3_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/goldvein4_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/goldvein5_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/haystack.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/magic_flower.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole01_over.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole02_over.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_lavas_road_hole02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_seashell0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_seashell1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/moai4_seashell2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm1_1_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm1_1_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm1_2_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm1_2_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm1_3_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm1_3_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm2_1_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm2_1_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm2_2_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm2_2_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm2_3_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm2_3_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm3_1_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm3_1_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm3_2_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm3_2_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm3_3_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/palm3_3_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle1_horizontal_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle1_horizontal_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle1_horizontal_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle1_vertical_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle1_vertical_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle1_vertical_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle2_horizontal_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle2_horizontal_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle2_horizontal_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle2_vertical_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle2_vertical_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/puddle2_vertical_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/red_coral_tree_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/red_coral_tree_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/roadhole1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/roadhole2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/roadhole3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/rocksobstacle0_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/rocksobstacle0_50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/rocksobstacle0_75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/rocksobstacle1_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/sapphirevein1_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/seaplant_ground.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/seaplant_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/seashell1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/seashell2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/seashell3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/snowdrift_object_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/snowdrift_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_0_object_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_0_object_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_0_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_1_object_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_1_object_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_1_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/starfish1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/starfish2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/starfish3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/white_coral_tree_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/white_coral_tree_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/woods01_0_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/woods01_0_50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/woods01_0_75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/woods01_1_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/yellow_coral_tree_h.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/yellow_coral_tree_v.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_quest_objects1/queststoneboulder_object_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_quest_objects1/queststoneboulder_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/leavespile.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/paporotnik0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/paporotnik1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/paporotnik2.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/paporotnik3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/snowdriftsmall_object.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_2_object_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_2_object_0_5.png</filename>
            <filename>../../Moai6_tmp/old_art_all/obstacles/stalagmit_2_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lockers_thicket/thicket_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lockers_thicket/thicket_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/lockers_thicket/thicket_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/signpost/signpost_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/signpost/signpost_1.png</filename>
            <filename>../../Moai6_tmp/old_art_all/signpost/signpost_2.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
