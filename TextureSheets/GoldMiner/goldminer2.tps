<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Gold Miner/goldminer2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/GoldMiner/goldminer2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_look70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/thanks106.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,59,96,118</rect>
                <key>scale9Paddings</key>
                <rect>48,59,96,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/thanks096.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
