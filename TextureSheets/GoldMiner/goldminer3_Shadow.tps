<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Gold Miner/goldminer3_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/GoldMiner/goldminer3_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action260.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>123,89,246,178</rect>
                <key>scale9Paddings</key>
                <rect>123,89,246,178</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2_shadow/action060.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
