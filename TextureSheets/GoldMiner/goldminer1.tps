<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Gold Miner/goldminer1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/GoldMiner/goldminer1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/ask_help115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,59,96,118</rect>
                <key>scale9Paddings</key>
                <rect>48,59,96,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner1/idle_headup7.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
