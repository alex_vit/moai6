<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Gold Miner/goldminer3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/GoldMiner/goldminer3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action259.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gold_miner2/action260.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,57,186,114</rect>
                <key>scale9Paddings</key>
                <rect>93,57,186,114</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action259.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gold_miner2/action028.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
