<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Artefacts2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Artefacts2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_gloves_of_the_merchant.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_gloves_of_the_merchant_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_light_boots.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_light_boots_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_health.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_health_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_loving_heaven.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_loving_heaven_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_restraint.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_restraint_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_scroll_of_the_builder.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_scroll_of_the_builder_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_stick.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/artefacts_stick_01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_naturalist_hat.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts/drops_naturalist_hat_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,45,73,89</rect>
                <key>scale9Paddings</key>
                <rect>37,45,73,89</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/artefacts_boy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,287,228,573</rect>
                <key>scale9Paddings</key>
                <rect>114,287,228,573</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/artefacts_girl.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,268,200,536</rect>
                <key>scale9Paddings</key>
                <rect>100,268,200,536</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/frame.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>288,84,575,168</rect>
                <key>scale9Paddings</key>
                <rect>288,84,575,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/progress_arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,14,1,27</rect>
                <key>scale9Paddings</key>
                <rect>1,14,1,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/progress_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>527,39,1054,78</rect>
                <key>scale9Paddings</key>
                <rect>527,39,1054,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/progress_full.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>208,14,416,27</rect>
                <key>scale9Paddings</key>
                <rect>208,14,416,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/round_disable.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/round_disable_white.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/round_highlight.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/round_highlight_white.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/round_normal.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/round_normal_white.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,54,107,107</rect>
                <key>scale9Paddings</key>
                <rect>54,54,107,107</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/artefacts_menu1/tab.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>170,51,340,101</rect>
                <key>scale9Paddings</key>
                <rect>170,51,340,101</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/tab.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/artefacts_boy.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/artefacts_girl.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/frame.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/round_disable_white.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/round_highlight.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/round_highlight_white.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/round_normal.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/round_normal_white.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/progress_background.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/progress_full.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/round_disable.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts_menu1/progress_arrow.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_loving_heaven.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_loving_heaven_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_restraint.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_restraint_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_scroll_of_the_builder.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_scroll_of_the_builder_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_stick.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_stick_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_naturalist_hat.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/drops_naturalist_hat_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_gloves_of_the_merchant.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_gloves_of_the_merchant_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_light_boots.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_light_boots_01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_health.png</filename>
            <filename>../../Moai6_tmp/old_art_all/artefacts/artefacts_rune_of_health_01.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
