<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Gatherer/Gatherer.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Gatherer/Gatherer.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_down98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_left98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/action_right98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle1_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_down98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_left98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/idle2_right98.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,60,124,120</rect>
                <key>scale9Paddings</key>
                <rect>62,60,124,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_down98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_left98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/action_right98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle1_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_down98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_left98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/idle2_right98.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
