<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Gatherer/Gatherer2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Gatherer/Gatherer2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer1/run_up22.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,60,124,120</rect>
                <key>scale9Paddings</key>
                <rect>62,60,124,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/run_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_down88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_left88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gatherer2/talk_right88.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,60,94,120</rect>
                <key>scale9Paddings</key>
                <rect>47,60,94,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer1/run_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/run_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_down88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_left88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gatherer2/talk_right88.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
