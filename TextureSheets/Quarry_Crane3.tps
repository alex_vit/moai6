<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Quarry_Crane3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Quarry_Crane3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action73.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action77.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action78.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action79.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action80.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action81.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action83.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action84.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action85.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action86.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>140,160,280,320</rect>
                <key>scale9Paddings</key>
                <rect>140,160,280,320</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action73.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action77.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action78.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action79.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action80.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action81.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action83.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action85.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane3/crane3_action86.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
