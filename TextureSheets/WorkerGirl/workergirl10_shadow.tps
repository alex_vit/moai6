<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl10_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl10_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up68.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,47,228,94</rect>
                <key>scale9Paddings</key>
                <rect>114,47,228,94</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_67.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,64,214,128</rect>
                <key>scale9Paddings</key>
                <rect>107,64,214,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_69.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,34,200,68</rect>
                <key>scale9Paddings</key>
                <rect>100,34,200,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/shaketree_up68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/take_water_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_down_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/talk_left_66.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
