<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl12_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl12_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_58.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>112,41,224,82</rect>
                <key>scale9Paddings</key>
                <rect>112,41,224,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up60.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>100,34,200,68</rect>
                <key>scale9Paddings</key>
                <rect>100,34,200,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_48.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>129,53,258,106</rect>
                <key>scale9Paddings</key>
                <rect>129,53,258,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right78.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,64,214,128</rect>
                <key>scale9Paddings</key>
                <rect>107,64,214,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/well98.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>121,66,242,132</rect>
                <key>scale9Paddings</key>
                <rect>121,66,242,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_pain_up60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/walk_up_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/waterpump_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/well84.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
