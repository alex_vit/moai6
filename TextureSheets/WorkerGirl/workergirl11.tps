<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl11.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl11.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_right_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/thanks106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down60.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,62,94,124</rect>
                <key>scale9Paddings</key>
                <rect>47,62,94,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/talk_up_69.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,61,90,122</rect>
                <key>scale9Paddings</key>
                <rect>45,61,90,122</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_down_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/walk_left_48.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,84,168,168</rect>
                <key>scale9Paddings</key>
                <rect>84,84,168,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_right_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/talk_up_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_down_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/walk_pain_down56.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
