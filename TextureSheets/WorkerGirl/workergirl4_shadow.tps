<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl4_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl4_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_154.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>192,59,384,118</rect>
                <key>scale9Paddings</key>
                <rect>192,59,384,118</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_left_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/fishing_right_153.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
