<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/chopping_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/construct_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_down_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_up_33.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,84,168,168</rect>
                <key>scale9Paddings</key>
                <rect>84,84,168,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/constructd_left_33.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>176,100,352,200</rect>
                <key>scale9Paddings</key>
                <rect>176,100,352,200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/dig_up19.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,66,128,132</rect>
                <key>scale9Paddings</key>
                <rect>64,66,128,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/final63.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,62,94,124</rect>
                <key>scale9Paddings</key>
                <rect>47,62,94,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/chopping_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/construct_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_down_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/constructd_up_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/dig_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/final48.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
