<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl8_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl8_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_32.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>129,53,258,106</rect>
                <key>scale9Paddings</key>
                <rect>129,53,258,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_53.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,64,214,128</rect>
                <key>scale9Paddings</key>
                <rect>107,64,214,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/kirka_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/praying_257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl_shadow/put_in_fire_50.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
