<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/WorkerGirl/workergirl8.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/WorkerGirl/workergirl8.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/kirka_up_32.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,84,168,168</rect>
                <key>scale9Paddings</key>
                <rect>84,84,168,168</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_231.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_233.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_234.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_235.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_237.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_239.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_241.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_243.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_244.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_245.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_247.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_249.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_251.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_253.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_254.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_255.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/praying_257.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_53.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,66,128,132</rect>
                <key>scale9Paddings</key>
                <rect>64,66,128,132</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/kirka_up_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_231.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_233.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_234.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_235.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_237.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_239.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_241.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_243.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_244.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_245.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_247.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_249.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_251.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_253.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_254.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_255.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/praying_257.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/workergirl/put_in_fire_52.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
