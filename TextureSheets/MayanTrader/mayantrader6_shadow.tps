<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanTrader/mayantrader6_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanTrader/mayantrader6_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down229.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down136.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>108,40,216,80</rect>
                <key>scale9Paddings</key>
                <rect>108,40,216,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/talk_down134.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
