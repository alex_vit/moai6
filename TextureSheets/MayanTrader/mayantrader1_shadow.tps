<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanTrader/mayantrader1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanTrader/mayantrader1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right217.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right219.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right221.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right223.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right224.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right225.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right227.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right229.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,38,214,76</rect>
                <key>scale9Paddings</key>
                <rect>107,38,214,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right229.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right217.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right219.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right221.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right223.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right224.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right225.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right227.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle1_right228.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
