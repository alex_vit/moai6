<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanTrader/mayantrader2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanTrader/mayantrader2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down059.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,38,214,76</rect>
                <key>scale9Paddings</key>
                <rect>107,38,214,76</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right059.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>114,36,228,72</rect>
                <key>scale9Paddings</key>
                <rect>114,36,228,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader1_shadow/idle2_down054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader2_shadow/idle2_right058.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
