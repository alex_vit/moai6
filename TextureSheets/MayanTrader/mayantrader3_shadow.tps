<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanTrader/mayantrader3_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanTrader/mayantrader3_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left100.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,39,232,78</rect>
                <key>scale9Paddings</key>
                <rect>116,39,232,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left010.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
