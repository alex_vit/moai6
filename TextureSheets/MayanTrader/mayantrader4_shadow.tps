<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanTrader/mayantrader4_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanTrader/mayantrader4_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right146.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>116,39,232,78</rect>
                <key>scale9Paddings</key>
                <rect>116,39,232,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down083.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,39,226,78</rect>
                <key>scale9Paddings</key>
                <rect>113,39,226,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_right146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader3_shadow/talk_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down078.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
