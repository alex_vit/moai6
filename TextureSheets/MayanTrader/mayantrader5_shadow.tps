<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MayanTrader/mayantrader5_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MayanTrader/mayantrader5_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left173.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,39,226,78</rect>
                <key>scale9Paddings</key>
                <rect>113,39,226,78</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down034.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>108,40,216,80</rect>
                <key>scale9Paddings</key>
                <rect>108,40,216,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader4_shadow/idle3_down092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/mayan_trader5_shadow/idle1_down030.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
