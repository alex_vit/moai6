<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Guardian/guardian2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Guardian/guardian2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,40,105,80</rect>
                <key>scale9Paddings</key>
                <rect>53,40,105,80</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,47,118,95</rect>
                <key>scale9Paddings</key>
                <rect>59,47,118,95</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_down/strag_front_ischez/guardian_down_dissappear5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_left/strag_left_cheshet/guardian_left_action1.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
