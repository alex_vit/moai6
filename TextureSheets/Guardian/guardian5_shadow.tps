<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Guardian/guardian5_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Guardian/guardian5_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>59,47,118,95</rect>
                <key>scale9Paddings</key>
                <rect>59,47,118,95</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/Guardian/guardian_right/strag_shadow_right_ischez/guardian_right_shadow_dissappear5.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
