<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Gangster/Gangster_Walk_Shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Gangster/Gangster_Walk_Shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up50.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>120,58,240,116</rect>
                <key>scale9Paddings</key>
                <rect>120,58,240,116</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/gangster1_shadow/walk_up38.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
