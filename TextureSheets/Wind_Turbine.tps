<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Wind_Turbine.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Wind_Turbine.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_0_3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_0_6.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>89,52,178,104</rect>
                <key>scale9Paddings</key>
                <rect>89,52,178,104</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_0_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_0_3.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_0_6.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_1_0.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation72.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,170,214,340</rect>
                <key>scale9Paddings</key>
                <rect>107,170,214,340</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_0_3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_0_6.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_ground_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_0_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_0_3.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_0_6.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects1/wind_turbine_object_1_0.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/moai4_objects2/wind_turbine_animation72.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
