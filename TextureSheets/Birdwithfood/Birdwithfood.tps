<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Birdwithfood/Birdwithfood.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/BIrdwithfood/Birdwithfood.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/death_up50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/birdwithfood/fly_up20.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>76,76,152,152</rect>
                <key>scale9Paddings</key>
                <rect>76,76,152,152</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/death_up50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/birdwithfood/fly_up20.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
