<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Canoneer/Canoneer1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Canoneer/Canoneer1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/ask_help115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/final63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/idle_look70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/cannoneer1/thanks106.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,88,122,176</rect>
                <key>scale9Paddings</key>
                <rect>61,88,122,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/ask_help115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/final63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_head_up8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/idle_look70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/cannoneer1/thanks105.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
