<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Arable_Wheat.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Polygon</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Arable_Wheat.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat0_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_1_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_2_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat1_46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat2_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_1_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_1_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_final.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/arable_wheat/wheat3_3_final.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>166,122,332,244</rect>
                <key>scale9Paddings</key>
                <rect>166,122,332,244</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat0_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_1_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_2_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat1_46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat2_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_1_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_1_46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_1_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_2_final.png</filename>
            <filename>../../Moai6_tmp/old_art_all/arable_wheat/wheat3_3_final.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
