<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Wizard/wizard7_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Wizard/wizard7_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_215.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,37,188,74</rect>
                <key>scale9Paddings</key>
                <rect>94,37,188,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard6_shadow/talk_left_205.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
