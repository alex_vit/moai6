<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Wizard/wizard2_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Wizard/wizard2_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_190.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>130,45,260,90</rect>
                <key>scale9Paddings</key>
                <rect>130,45,260,90</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_43.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>117,48,234,96</rect>
                <key>scale9Paddings</key>
                <rect>117,48,234,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard1_shadow/action_right_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/action_down_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_down_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_41.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
