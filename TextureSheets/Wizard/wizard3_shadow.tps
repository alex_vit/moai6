<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Wizard/wizard3_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Wizard/wizard3_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_58.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>117,48,234,96</rect>
                <key>scale9Paddings</key>
                <rect>117,48,234,96</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_082.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,41,186,82</rect>
                <key>scale9Paddings</key>
                <rect>93,41,186,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard2_shadow/idle1_right_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_left_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/idle2_right_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_080.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
