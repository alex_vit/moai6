<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Wizard/wizard5.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Wizard/wizard5.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_183.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_189.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_193.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_199.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_203.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_209.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_213.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_215.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4/talk_down_216.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,56,110,112</rect>
                <key>scale9Paddings</key>
                <rect>55,56,110,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle2_down_99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard5/idle3_left_086.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,56,108,112</rect>
                <key>scale9Paddings</key>
                <rect>54,56,108,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_213.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_183.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_189.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_193.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_199.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_203.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_209.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4/talk_down_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle2_down_96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard5/idle3_left_086.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
