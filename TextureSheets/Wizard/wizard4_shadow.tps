<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Wizard/wizard4_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Wizard/wizard4_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_184.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_185.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_187.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_191.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_194.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_195.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_197.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_201.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_204.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_205.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_207.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_211.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_214.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_215.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,41,186,82</rect>
                <key>scale9Paddings</key>
                <rect>93,41,186,82</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_053.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,36,188,72</rect>
                <key>scale9Paddings</key>
                <rect>94,36,188,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_201.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_204.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_205.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_207.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_211.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_214.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_215.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_184.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_185.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_187.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_191.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_194.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_195.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard3_shadow/talk_right_197.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/idle3_down_123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/wizard4_shadow/talk_down_052.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
