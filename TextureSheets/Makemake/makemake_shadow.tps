<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Makemake/makemake_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Makemake/makemake_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1_shadows/idle59.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>115,36,230,72</rect>
                <key>scale9Paddings</key>
                <rect>115,36,230,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2_shadows/action119.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>117,42,234,84</rect>
                <key>scale9Paddings</key>
                <rect>117,42,234,84</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1_shadows/idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2_shadows/action036.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
