<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Makemake/makemake.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Makemake/makemake.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle00.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle06.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle07.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle08.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle09.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake1/idle59.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,83,76,166</rect>
                <key>scale9Paddings</key>
                <rect>38,83,76,166</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action030.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action031.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action032.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action033.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action034.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action035.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action036.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action037.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action038.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action039.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action087.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action088.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action089.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action090.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action091.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/makemake2/action120.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>47,83,94,166</rect>
                <key>scale9Paddings</key>
                <rect>47,83,94,166</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle00.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle06.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle07.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle08.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle09.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake1/idle40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action030.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action031.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action032.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action033.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action034.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action035.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action036.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action037.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action038.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action039.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action086.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action088.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action089.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action090.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action091.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/makemake2/action017.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
