<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/QuestObjects/questobjects1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/QuestObjects/questobjects1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/moai4_quest_objects1/earth_crack_ground.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai4_quest_objects1/earth_crack_ground_over.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>233,147,466,294</rect>
                <key>scale9Paddings</key>
                <rect>233,147,466,294</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.409091,0.320122</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>77,82,154,164</rect>
                <key>scale9Paddings</key>
                <rect>77,82,154,164</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.509804,0.662338</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,39,77,77</rect>
                <key>scale9Paddings</key>
                <rect>38,39,77,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_object_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_object_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,76,128,152</rect>
                <key>scale9Paddings</key>
                <rect>64,76,128,152</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>145,165,290,330</rect>
                <key>scale9Paddings</key>
                <rect>145,165,290,330</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/stonewithpetroglyphs_object_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects1/stonewithpetroglyphs_object_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,128,170,256</rect>
                <key>scale9Paddings</key>
                <rect>85,128,170,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_over_0_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>213,201,426,402</rect>
                <key>scale9Paddings</key>
                <rect>213,201,426,402</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/qst_fireplace_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,45,132,90</rect>
                <key>scale9Paddings</key>
                <rect>66,45,132,90</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_b.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_g.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_over.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_p.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_r.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,162,274,324</rect>
                <key>scale9Paddings</key>
                <rect>137,162,274,324</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_firecup_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,53,170,106</rect>
                <key>scale9Paddings</key>
                <rect>85,53,170,106</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_firecup_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_firecup_over.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,68,108,136</rect>
                <key>scale9Paddings</key>
                <rect>54,68,108,136</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_ground_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>178,99,356,198</rect>
                <key>scale9Paddings</key>
                <rect>178,99,356,198</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_object_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_food.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_gold.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_heat2_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_heat_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_mana.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>147,163,294,326</rect>
                <key>scale9Paddings</key>
                <rect>147,163,294,326</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_moai3_boiler_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>66,46,132,92</rect>
                <key>scale9Paddings</key>
                <rect>66,46,132,92</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_moai3_boiler_object.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects2/qst_moai4_pot_object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,67,114,134</rect>
                <key>scale9Paddings</key>
                <rect>57,67,114,134</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>259,148,518,296</rect>
                <key>scale9Paddings</key>
                <rect>259,148,518,296</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_object_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_object_0_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_object_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>370,286,740,572</rect>
                <key>scale9Paddings</key>
                <rect>370,286,740,572</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_0_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_1_0_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_1_0_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_1_0_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_ground_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/stockade/stockade_ground_1_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>67,81,134,162</rect>
                <key>scale9Paddings</key>
                <rect>67,81,134,162</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/totem/coal.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/totem/coal_hot.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/totem/object.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>179,135,358,270</rect>
                <key>scale9Paddings</key>
                <rect>179,135,358,270</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_object_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_food.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_gold.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_heat_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_heat2_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_over_mana.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_lavas_god_ground_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_moai3_boiler_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_moai4_pot_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_moai3_boiler_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_firecup_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_firecup_over.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects2/qst_firecup_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_r.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_b.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_g.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_over.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/quest_column_ball_1_0_p.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai4_quest_objects1/earth_crack_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai4_quest_objects1/earth_crack_ground_over.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_0_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_1_0_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_1_0_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_1_0_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_ground_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_ground_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/stockade/stockade_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/totem/coal_hot.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/totem/object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/totem/coal.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_object_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_object_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects3/qst_teleport_object_0_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_object_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_object_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_hive4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/moai5_honey_tree_object.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_over_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_0_75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects2/moai5_quest_megalit_object_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects1/qst_fireplace_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/stonewithpetroglyphs_object_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_quest_objects1/stonewithpetroglyphs_object_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/teleport_ground_1_5.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
