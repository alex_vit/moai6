<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/QuestObjects/questobjects3.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/QuestObjects/questobjects3.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/bowl_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/bowl_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/bowl_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/ore_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/ore_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,41,107,83</rect>
                <key>scale9Paddings</key>
                <rect>53,41,107,83</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/column_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/column_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/column_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowering_shrubs_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowering_shrubs_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowering_shrubs_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/island_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/island_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/island_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/moss_covered_slabs_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/moss_covered_slabs_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/moss_covered_slabs_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>63,51,125,102</rect>
                <key>scale9Paddings</key>
                <rect>63,51,125,102</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,75,150,150</rect>
                <key>scale9Paddings</key>
                <rect>75,75,150,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>93,75,185,150</rect>
                <key>scale9Paddings</key>
                <rect>93,75,185,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,75,210,150</rect>
                <key>scale9Paddings</key>
                <rect>105,75,210,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,60,150,120</rect>
                <key>scale9Paddings</key>
                <rect>75,60,150,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_04.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_05.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_destroy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,113,249,225</rect>
                <key>scale9Paddings</key>
                <rect>125,113,249,225</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_destroyed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>151,112,302,224</rect>
                <key>scale9Paddings</key>
                <rect>151,112,302,224</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>22,21,45,42</rect>
                <key>scale9Paddings</key>
                <rect>22,21,45,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/hoist_object/pic_1.gif</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>62,54,125,108</rect>
                <key>scale9Paddings</key>
                <rect>62,54,125,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/lake_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/lake_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>133,85,266,171</rect>
                <key>scale9Paddings</key>
                <rect>133,85,266,171</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,67,193,133</rect>
                <key>scale9Paddings</key>
                <rect>97,67,193,133</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_03.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,75,175,150</rect>
                <key>scale9Paddings</key>
                <rect>88,75,175,150</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_lever_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_lever_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,31,63,63</rect>
                <key>scale9Paddings</key>
                <rect>32,31,63,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/jar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>77,42,153,83</rect>
                <key>scale9Paddings</key>
                <rect>77,42,153,83</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,35,68,71</rect>
                <key>scale9Paddings</key>
                <rect>34,35,68,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_01_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_01_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_01_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_02_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_02_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_02_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,33,100,66</rect>
                <key>scale9Paddings</key>
                <rect>50,33,100,66</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/thicket_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/thicket_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/thicket_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,37,93,75</rect>
                <key>scale9Paddings</key>
                <rect>46,37,93,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/vase_01.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/vase_02.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects5/lockers/vase_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,44,109,88</rect>
                <key>scale9Paddings</key>
                <rect>54,44,109,88</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/bowl_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/bowl_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/ore_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/ore_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Building_CE/bowl_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/column_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/column_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/column_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowering_shrubs_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowering_shrubs_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowering_shrubs_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/flowers_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/island_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/island_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/island_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/moss_covered_slabs_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/moss_covered_slabs_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/moss_covered_slabs_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/patterned_plates_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/tree_8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Decorations/wall_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/vase_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_lever_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/block_lever_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/jar.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/mushroom_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_01_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_01_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_01_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_02_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_02_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/quartz_02_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/spiderweb_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/thicket_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/thicket_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/thicket_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/vase_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/lockers/vase_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_destroyed.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/lake_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/lake_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_05.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/drilling_rig_destroy.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_01.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_02.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_03.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/glade_butterfly_04.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/hoist_object/pic_1.gif</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects5/Objects/pumpkin_4.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
