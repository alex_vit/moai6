<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/QuestObjects/questobjects2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/QuestObjects/questobjects2.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_ground_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_ground_d.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,49,168,98</rect>
                <key>scale9Paddings</key>
                <rect>84,49,168,98</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_coconaut_oil.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_d.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_food.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_gold.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_mana.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_metal.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_rocks.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_wood.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>81,74,162,148</rect>
                <key>scale9Paddings</key>
                <rect>81,74,162,148</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_0_over.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_1_0.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_1_0_over.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>166,178,332,356</rect>
                <key>scale9Paddings</key>
                <rect>166,178,332,356</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>178,101,356,202</rect>
                <key>scale9Paddings</key>
                <rect>178,101,356,202</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_base.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_base_fire.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_bellows.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_broken.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_rocks.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassbowl.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>97,68,194,137</rect>
                <key>scale9Paddings</key>
                <rect>97,68,194,137</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>152,107,303,214</rect>
                <key>scale9Paddings</key>
                <rect>152,107,303,214</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bathyscaphe/bathyscaphe_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bathyscaphe/bathyscaphe_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bathyscaphe/bathyscaphe_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>90,72,180,143</rect>
                <key>scale9Paddings</key>
                <rect>90,72,180,143</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bunker/Bunker_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bunker/Bunker_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bunker/Bunker_3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/bunker/bunker_door.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>140,91,279,181</rect>
                <key>scale9Paddings</key>
                <rect>140,91,279,181</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/column.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/column_light.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/sphere_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/sphere_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/sphere_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>58,56,115,112</rect>
                <key>scale9Paddings</key>
                <rect>58,56,115,112</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_base.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_base_light.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_stairs.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_stairs_crystal_off.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_stairs_crystal_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>148,149,296,298</rect>
                <key>scale9Paddings</key>
                <rect>148,149,296,298</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_fly_base.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_fly_feather_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_fly_tail.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_rope.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_stand.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_stick.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>179,181,358,362</rect>
                <key>scale9Paddings</key>
                <rect>179,181,358,362</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/observatory/observatory_1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/observatory/observatory_2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/observatory/observatory_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>102,85,205,171</rect>
                <key>scale9Paddings</key>
                <rect>102,85,205,171</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../Moai6_tmp/old_art_all/quest_objects4/sea_urchin/sea_urchin.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>46,34,92,69</rect>
                <key>scale9Paddings</key>
                <rect>46,34,92,69</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_coconaut_oil.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_d.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_food.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_gold.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_mana.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_metal.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_rocks.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_wood.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_ground_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_ground_d.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_altarakhu_object_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_ground.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_0_over.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_0_66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_1_0.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/moai5_objects1/moai5_templeofelemental_1_0_over.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/airship/airship_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bathyscaphe/bathyscaphe_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bathyscaphe/bathyscaphe_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bathyscaphe/bathyscaphe_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bunker/Bunker_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bunker/Bunker_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bunker/Bunker_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/bunker/bunker_door.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/sphere_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/sphere_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/column.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/column_light.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_base.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_base_light.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_stairs.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_stairs_crystal_off.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/magic-generator_stairs_crystal_on.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/generator/sphere_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_base.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_base_fire.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_bellows.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_broken.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassblower_rocks.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/Glassblower/Glassbowl.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_fly_base.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_fly_feather_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_fly_tail.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_rope.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_stand.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/kite/kite_stick.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/observatory/observatory_3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/observatory/observatory_1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/observatory/observatory_2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/quest_objects4/sea_urchin/sea_urchin.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
