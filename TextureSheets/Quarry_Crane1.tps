<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>/Volumes/Data/work/moai6/TextureSheets/Quarry_Crane1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../Assets/Atlases/Buildings/Quarry_Crane1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action25.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action65.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action73.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action77.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action78.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action79.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action80.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action81.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action83.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action84.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action85.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action86.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action00.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action01.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action02.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action03.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action04.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action05.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action06.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action07.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action08.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action09.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action10.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action11.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action12.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action13.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action14.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action15.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action16.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action17.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action18.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action19.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action20.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action21.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action22.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action23.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action24.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action26.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action27.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action28.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action29.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action30.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action31.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action32.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action33.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action34.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action35.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action36.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action37.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action38.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action39.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action40.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action41.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action42.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action43.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action44.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action45.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action46.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action47.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action48.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action49.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action50.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action51.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action52.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action53.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action54.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action55.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action56.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action57.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action58.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action59.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action60.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action61.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action62.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action63.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action64.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action66.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action67.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action68.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action69.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action70.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action71.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action72.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action73.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action74.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action75.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action76.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action77.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action78.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action79.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action80.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action81.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action82.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action83.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action84.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action85.png</key>
            <key type="filename">../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action86.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>140,160,280,320</rect>
                <key>scale9Paddings</key>
                <rect>140,160,280,320</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action25.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action65.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action73.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action77.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action78.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action79.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action80.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action81.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action83.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action85.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane0_action86.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action00.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action01.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action02.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action03.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action04.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action05.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action06.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action07.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action08.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action09.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action10.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action11.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action12.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action13.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action14.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action15.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action16.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action17.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action18.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action19.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action20.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action21.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action22.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action23.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action24.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action26.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action27.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action28.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action29.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action30.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action31.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action32.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action33.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action34.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action35.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action36.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action37.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action38.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action39.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action40.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action41.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action42.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action43.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action44.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action45.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action46.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action47.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action48.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action49.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action50.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action51.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action52.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action53.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action54.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action55.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action56.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action57.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action58.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action59.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action60.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action61.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action62.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action63.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action64.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action66.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action67.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action68.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action69.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action70.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action71.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action72.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action73.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action74.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action75.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action76.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action77.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action78.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action79.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action80.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action81.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action82.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action83.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action84.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action85.png</filename>
            <filename>../../Moai6_tmp/old_art_all/quarry_crane1/crane1_action86.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
