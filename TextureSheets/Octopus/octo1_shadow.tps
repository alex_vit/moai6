<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Octopus/octo1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Octopus/octo1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_000.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_001.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_002.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_003.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_004.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_005.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_006.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_007.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_008.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_009.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_010.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_011.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_012.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_013.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_014.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_015.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_016.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_017.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_018.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_019.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_020.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_021.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_022.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_023.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_024.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_025.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_026.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_027.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_028.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_029.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_040.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_041.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_042.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_043.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_044.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_045.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_046.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_047.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_048.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_049.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_050.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_051.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_052.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_053.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_054.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_055.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_056.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_057.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_058.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_059.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_060.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_061.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_062.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_063.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_064.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_065.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_066.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_067.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_068.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_069.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_070.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_071.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_072.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_073.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_074.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_075.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_076.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_077.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_078.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_079.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_080.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_081.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_082.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_083.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_084.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_085.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_086.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_087.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>168,60,336,120</rect>
                <key>scale9Paddings</key>
                <rect>168,60,336,120</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_087.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_000.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_001.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_002.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_003.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_004.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_005.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_006.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_007.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_008.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_009.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_010.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_011.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_012.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_013.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_014.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_015.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_016.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_017.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_018.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_019.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_020.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_021.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_022.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_023.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_024.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_025.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_026.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_027.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_028.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_029.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_040.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_041.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_042.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_043.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_044.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_045.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_046.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_047.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_048.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_049.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_050.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_051.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_052.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_053.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_054.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_055.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_056.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_057.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_058.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_059.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_060.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_061.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_062.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_063.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_064.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_065.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_066.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_067.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_068.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_069.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_070.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_071.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_072.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_073.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_074.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_075.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_076.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_077.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_078.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_079.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_080.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_081.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_082.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_083.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_084.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_085.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus1_shadow/octopus_right_086.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
