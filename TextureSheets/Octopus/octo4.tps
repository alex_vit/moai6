<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Octopus/octo4.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Octopus/octo4.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_092.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_093.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus3/octopus_down_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_282.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_286.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_288.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_290.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_292.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_296.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_298.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_300.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_302.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_306.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_308.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_310.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_312.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_316.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_318.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_320.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus4/octopus_down_322.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>106,102,212,204</rect>
                <key>scale9Paddings</key>
                <rect>106,102,212,204</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_092.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_093.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus3/octopus_down_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_286.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_288.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_290.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_292.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_296.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_298.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_300.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_302.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_306.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_308.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_310.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_312.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_316.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_318.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_320.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus4/octopus_down_322.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
