<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/Octopus/octo6.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/Octopus/octo6.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus5/octopus_left_094.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus5/octopus_left_095.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus5/octopus_left_096.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus5/octopus_left_097.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus5/octopus_left_098.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus5/octopus_left_099.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_182.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_186.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_188.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_190.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_192.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_196.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_198.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_200.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_202.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_206.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_208.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_210.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_212.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_216.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_218.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_220.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_222.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_226.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_228.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_230.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_232.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_236.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_238.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_240.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_242.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_246.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_248.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_250.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_252.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_256.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_258.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_260.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_262.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_266.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_268.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_270.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_272.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_276.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_278.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_280.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/octopus6/octopus_left_282.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,102,214,204</rect>
                <key>scale9Paddings</key>
                <rect>107,102,214,204</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/octopus5/octopus_left_098.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus5/octopus_left_099.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus5/octopus_left_094.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus5/octopus_left_095.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus5/octopus_left_096.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus5/octopus_left_097.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_182.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_186.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_188.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_190.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_192.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_196.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_198.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_200.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_202.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_206.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_208.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_210.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_212.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_216.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_218.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_220.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_222.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_226.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_228.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_230.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_232.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_236.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_238.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_240.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_242.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_246.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_248.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_250.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_252.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_256.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_258.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_260.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_262.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_266.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_268.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_270.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_272.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_276.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_278.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_280.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_282.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/octopus6/octopus_left_142.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
