<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MenehunTrader/menehuntrader_down_1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MenehunTrader/menehuntrader_down_1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down99.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,35,90,70</rect>
                <key>scale9Paddings</key>
                <rect>45,35,90,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/apple/apple_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_down/dissapear/dissapear_down120.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
