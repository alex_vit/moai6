<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MenehunTrader/menehuntrader_right_1_shadow.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MenehunTrader/menehuntrader_right_1_shadow.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,35,90,70</rect>
                <key>scale9Paddings</key>
                <rect>45,35,90,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/apple/apple_right_shadow181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/dissapear/dissapear_right_shadow120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/shadow/idle/idle_right_shadow1.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
