<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MenehunTrader/menehuntrader_right_1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MenehunTrader/menehuntrader_right_1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,35,90,70</rect>
                <key>scale9Paddings</key>
                <rect>45,35,90,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/apple/apple_right2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/dissapear/dissapear_right120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_right/idle/idle_right61.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
