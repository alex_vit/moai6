<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.4.0</string>
        <key>fileName</key>
        <string>D:/GITLAB MOAI 6/TextureSheets/MenehunTrader/menehuntrader_left_1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity-texture2d</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">WordAligned</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>datafile</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Assets/Atlases/Units/MenehunTrader/menehuntrader_left_1.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Polygon</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left121.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left122.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left123.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left124.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left125.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left126.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left127.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left128.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left129.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left130.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left131.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left132.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left133.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left134.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left135.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left136.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left137.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left138.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left139.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left140.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left141.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left142.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left143.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left144.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left145.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left146.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left147.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left148.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left149.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left150.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left151.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left152.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left153.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left154.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left155.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left156.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left157.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left158.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left159.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left160.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left161.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left162.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left163.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left164.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left165.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left166.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left167.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left168.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left169.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left170.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left171.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left172.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left173.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left174.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left175.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left176.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left177.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left178.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left179.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left180.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left181.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left100.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left101.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left102.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left103.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left104.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left105.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left106.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left107.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left108.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left109.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left110.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left111.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left112.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left113.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left114.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left115.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left116.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left117.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left118.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left119.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left120.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left62.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left63.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left64.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left65.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left66.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left67.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left68.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left69.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left70.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left71.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left72.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left73.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left74.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left75.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left76.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left77.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left78.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left79.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left80.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left81.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left82.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left83.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left84.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left85.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left86.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left87.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left88.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left89.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left9.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left90.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left91.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left92.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left93.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left94.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left95.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left96.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left97.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left98.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left99.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left1.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left10.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left11.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left12.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left13.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left14.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left15.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left16.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left17.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left18.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left19.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left2.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left20.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left21.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left22.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left23.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left24.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left25.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left26.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left27.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left28.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left29.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left3.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left30.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left31.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left32.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left33.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left34.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left35.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left36.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left37.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left38.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left39.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left4.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left40.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left41.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left42.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left43.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left44.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left45.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left46.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left47.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left48.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left49.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left5.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left50.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left51.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left52.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left53.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left54.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left55.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left56.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left57.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left58.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left59.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left6.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left60.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left61.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left7.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left8.png</key>
            <key type="filename">../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>45,35,90,70</rect>
                <key>scale9Paddings</key>
                <rect>45,35,90,70</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left121.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left122.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left123.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left124.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left125.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left126.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left127.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left128.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left129.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left130.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left131.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left132.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left133.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left134.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left135.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left136.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left137.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left138.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left139.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left140.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left141.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left142.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left143.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left144.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left145.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left146.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left147.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left148.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left149.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left150.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left151.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left152.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left153.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left154.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left155.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left156.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left157.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left158.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left159.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left160.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left161.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left162.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left163.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left164.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left165.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left166.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left167.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left168.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left169.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left170.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left171.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left172.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left173.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left174.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left175.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left176.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left177.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left178.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left179.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left180.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/apple/apple_left181.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left62.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left63.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left64.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left65.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left66.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left67.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left68.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left69.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left70.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left71.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left72.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left73.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left74.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left75.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left76.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left77.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left78.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left79.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left80.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left81.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left82.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left83.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left84.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left85.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left86.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left87.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left88.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left89.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left90.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left91.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left92.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left93.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left94.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left95.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left96.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left97.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left98.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left99.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left100.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left101.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left102.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left103.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left104.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left105.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left106.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left107.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left108.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left109.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left110.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left111.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left112.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left113.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left114.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left115.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left116.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left117.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left118.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left119.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left120.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/dissapear/dissapear_left1.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left2.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left3.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left4.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left5.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left6.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left7.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left8.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left9.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left10.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left11.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left12.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left13.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left14.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left15.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left16.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left17.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left18.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left19.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left20.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left21.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left22.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left23.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left24.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left25.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left26.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left27.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left28.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left29.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left30.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left31.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left32.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left33.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left34.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left35.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left36.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left37.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left38.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left39.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left40.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left41.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left42.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left43.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left44.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left45.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left46.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left47.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left48.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left49.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left50.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left51.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left52.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left53.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left54.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left55.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left56.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left57.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left58.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left59.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left60.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left61.png</filename>
            <filename>../../../Moai6_tmp/old_art_all/menehun_trader/mh_left/idle/idle_left1.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
