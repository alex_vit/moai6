using System.Collections.Generic;
using UnityEngine;

public class AnimatorStringsHashesCache {
	private static Dictionary<string, int> HashesCache;

	public static int GetStringHash(string animatorString) {
		if (HashesCache == null) {
			HashesCache = new Dictionary<string, int>();
		}
		if (!HashesCache.ContainsKey(animatorString)) {
			HashesCache.Add(animatorString, Animator.StringToHash(animatorString));
		}
		return HashesCache[animatorString];
	}
}