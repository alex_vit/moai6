using System;
using UnityEngine;

public class GenericSingleton<T>  : MonoBehaviour where T : UnityEngine.MonoBehaviour {
	public static T Instance { get { return SingletonFactory.GetInstance<T>(); } }

	public virtual void Awake()
	{
		T instance = Instance;
		if (instance != null && instance != this) {
			Destroy(gameObject);
		} else {
//#if !UNITY_EDITOR
			MakeUndeletable();
//#endif
		}
	}

	private bool _Initialized;
	public bool Initialized {get {return _Initialized;}}

	public virtual void OnApplicationQuit() {
		SingletonFactory.DestroyInstance<T>();
	}

	public virtual void Init() {
		_Initialized = true;
	}

	private void MakeUndeletable()
	{
		try
		{
			DontDestroyOnLoad(gameObject);
		}
		catch (InvalidOperationException ex)
		{

		}
	}

}