﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
//using System.Web;

public class BonusContentManager
{
	static readonly string audioDirectoryName = "MOAI 6 CE OST";
	static readonly string imageDirectoryName = "MOAI 6 CE Wallpapers";	

	public static void SaveClip(string name)
	{
		name += ".ogg";
		string _filePath = Application.streamingAssetsPath + "\\" + "Sounds/" + name;
		byte[] _audioData = File.ReadAllBytes(_filePath);
		bool res = WriteFile(GetPathForSave(audioDirectoryName),name, _audioData);
		Debug.Log(res);
	}

	public static void SaveImage(int width, int height, string name)
	{
		name += ".jpg";
		string _filePath = Application.streamingAssetsPath + "\\" + "Wallpapers/" + width.ToString() + "x" + height.ToString() + "/" + name;
		byte[] _texData = File.ReadAllBytes(_filePath);
		WriteFile(GetPathForSave(imageDirectoryName),name, _texData);
	}

	static string GetPathForSave(string dirName)
	{
		return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), dirName);
	}

	static bool WriteFile(string directory, string filename, byte[] data)
	{
		bool exists = Directory.Exists(directory);
		if (!exists)
			Directory.CreateDirectory(directory);

		string _filePath = directory + "/" + filename;
		try
		{
			using (var _fs = new FileStream(_filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
			{
				_fs.Write(data, 0, data.Length);
				return true;
			}
		}
		catch (Exception ex)
		{	
			Debug.Log(ex.Message);
			return false;
		}
	}
}
