using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsCache : GenericSingleton<PrefabsCache>{

	private class CachedLevelGameObject {
		public bool IsFree;
		public GameObject GameObject;
	}

	private Queue<GameObject> AsapQueue;
	private Queue<GameObject> SlowlyQueue;
	private Dictionary<string, List<CachedLevelGameObject>> CachedInstances;
	private Coroutine CacheCoroutine;

	override public void Init() {
		if (!Initialized) {
			base.Init();
			CachedInstances = new Dictionary<string, List<CachedLevelGameObject>>();
			AsapQueue = new Queue<GameObject>();
			SlowlyQueue = new Queue<GameObject>();
		}
	}

	public void ClearCache() {
		if (CacheCoroutine != null) {
			StopCoroutine(CacheCoroutine);
			CacheCoroutine = null;
		}
		AsapQueue.Clear();
		SlowlyQueue.Clear();
		CachedInstances.Clear();
	}

	public GameObject GetCachedInstanceForPrefab(GameObject instancePrefab, Transform originalTransform = null) {
		List<CachedLevelGameObject> cachedInstances = GetCachedInstances(instancePrefab);
		CachedLevelGameObject cachedLevelGameObject = null;
		for (int i = 0; i < cachedInstances.Count; i++) {
			CachedLevelGameObject cachedGameObject = cachedInstances[i];
			if (cachedGameObject.IsFree && cachedGameObject.GameObject != null) {
				cachedLevelGameObject = cachedGameObject;
				break;
			}
		}
		if (cachedLevelGameObject == null) {
			CacheInstancePrefab(instancePrefab, PrefabsCachePriority.Immidiate);
			return GetCachedInstanceForPrefab(instancePrefab, originalTransform);
		}
		if (originalTransform != null) {
			cachedLevelGameObject.GameObject.transform.SetParent(originalTransform.parent, false);
			cachedLevelGameObject.GameObject.transform.position = originalTransform.position;
		}
		cachedLevelGameObject.GameObject.SetActive(true);
		cachedLevelGameObject.IsFree = false;
		CheckPrefabForCache(instancePrefab, PrefabsCachePriority.ASAP);
		return cachedLevelGameObject.GameObject;
	}

	public void CheckPrefabForCache(GameObject instancePrefab, PrefabsCachePriority priority) {
		if (instancePrefab != null) {
			List<CachedLevelGameObject> cachedInstances = GetCachedInstances(instancePrefab);
			bool hasFreeInstances = false;
			for (int i = 0; i < cachedInstances.Count; i++) {
				CachedLevelGameObject cachedGameObject = cachedInstances[i];
				if (cachedGameObject.IsFree) {
					hasFreeInstances = true;
					break;
				}
			}
			if (!hasFreeInstances && !AsapQueue.Contains(instancePrefab) && !SlowlyQueue.Contains(instancePrefab)) {
				CacheInstancePrefab(instancePrefab, priority);
			}
		}
	}

	public void FreeCachedInstance(GameObject cachedInstance) {
		cachedInstance.SetActive(false);
		List<CachedLevelGameObject> cachedInstances = GetCachedInstances(cachedInstance);
		if (cachedInstances != null) {
			for (int i = 0; i < cachedInstances.Count; i++) {
				CachedLevelGameObject cachedGameObject = cachedInstances[i];
				if (cachedGameObject.GameObject == cachedInstance) {
					cachedGameObject.IsFree = true;
					return;
				}
			}
			CachedLevelGameObject cachedLevelGameObject = new CachedLevelGameObject();
			cachedLevelGameObject.IsFree = true;
			cachedLevelGameObject.GameObject = cachedInstance;
			cachedInstances.Add(cachedLevelGameObject);
		}
	}

	private void CacheInstancePrefab(GameObject instancePrefab, PrefabsCachePriority priority) {
		switch (priority) {
			case PrefabsCachePriority.Immidiate:
				CreateInstance(instancePrefab);
				break;
			case PrefabsCachePriority.ASAP:
				AsapQueue.Enqueue(instancePrefab);
				break;
			case PrefabsCachePriority.Slowly:
				SlowlyQueue.Enqueue(instancePrefab);
				break;
		}
		if (CacheCoroutine == null && (AsapQueue.Count > 0 || SlowlyQueue.Count > 0)) {
			CacheCoroutine = StartCoroutine(CheckQueues());
		}
	}

	private IEnumerator CheckQueues() {
		while (AsapQueue.Count > 0 || SlowlyQueue.Count > 0) {
			yield return new WaitForEndOfFrame();
			if (AsapQueue.Count > 0) {
				CreateInstance(AsapQueue.Dequeue());
			} else if (SlowlyQueue.Count > 0) {
				yield return new WaitForSeconds(1.0f);
				CreateInstance(SlowlyQueue.Dequeue());
			}
		}
		StopCoroutine(CacheCoroutine);
		CacheCoroutine = null;
	}

	private void CreateInstance(GameObject instancePrefab) {
		CachedLevelGameObject cachedLevelGameObject = new CachedLevelGameObject();
		cachedLevelGameObject.IsFree = true;
		cachedLevelGameObject.GameObject = GameObject.Instantiate (instancePrefab);
		cachedLevelGameObject.GameObject.name = cachedLevelGameObject.GameObject.name.Replace("(Clone)", "");
		cachedLevelGameObject.GameObject.SetActive(false);
		GetCachedInstances(instancePrefab).Add(cachedLevelGameObject);
	}

	private List<CachedLevelGameObject> GetCachedInstances(GameObject instancePrefab) {
		if (!CachedInstances.ContainsKey(instancePrefab.name)) {
			CachedInstances.Add(instancePrefab.name, new List<CachedLevelGameObject>());
		}
		return CachedInstances[instancePrefab.name];
	}

}