using System.Collections.Generic;
using UnityEngine;

public class CachedCollider : MonoBehaviour {
	[SerializeField]
	private ColliderPathsBySprite _PathsBySprites;
	[SerializeField]
	private SpriteRenderer _SpriteRenderer;
	[SerializeField]
	private PolygonCollider2D _Collider;

	#if UNITY_EDITOR
	public ColliderPathsBySprite GetPathsBySprite() {
		return _PathsBySprites;
	}
	public void SetPathsBySprite(ColliderPathsBySprite value) {
		_PathsBySprites = value;
	}
	public void SetSpriteRenderer(SpriteRenderer value) {
		_SpriteRenderer = value;
	}
	public void SetCollider(PolygonCollider2D value) {
		_Collider = value;
	}
	#endif

	public void OnEnabled() {
	}

	public void UpdatePaths() {
		_Collider.pathCount = 0;
		if (_SpriteRenderer.sprite != null) {
			ColliderPath colliderPath = null;
			if (_PathsBySprites.HasSprite(_SpriteRenderer.sprite.name)) {
				colliderPath = _PathsBySprites.GetPathForSprite(_SpriteRenderer.sprite.name);
			} else {
				colliderPath = _PathsBySprites.GetAnyPath();
			}

			List<ColliderPathPoints> colliderPathPoints = colliderPath.GetPathPoints();
			foreach (ColliderPathPoints pathPoints in colliderPathPoints) {
				_Collider.pathCount = _Collider.pathCount + 1;
				_Collider.SetPath(_Collider.pathCount - 1, pathPoints.GetPathPoints());
			}
		}
	}
}