using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColliderPathsBySprite
{
	[SerializeField]
	private List<string> _SpriteNames;
	[SerializeField]
	private List<ColliderPath> _Paths;

	public ColliderPathsBySprite() {
		_SpriteNames = new List<string>();
		_Paths = new List<ColliderPath>();
	}

#if UNITY_EDITOR
	public void Clear() {
		_SpriteNames = new List<string>();
		_Paths = new List<ColliderPath>();
	}

	public void SetPathForSprite(string spriteName, ColliderPath colliderPath) {
		if (!_SpriteNames.Contains(spriteName)) {
			_SpriteNames.Add(spriteName);
			_Paths.Add(colliderPath);
		} else {
			_Paths[_SpriteNames.IndexOf(spriteName)] = colliderPath;
		}
	}
#endif

	public bool HasSprite(string spriteName) {
		return _SpriteNames.Contains(spriteName);
	}

	public ColliderPath GetPathForSprite(string spriteName) {
		return _Paths[_SpriteNames.IndexOf(spriteName)];
	}

	public ColliderPath GetAnyPath() {
		return _Paths[0];
	}
}