using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColliderPath {
	[SerializeField]
	private List<ColliderPathPoints> _PathPoints;

	public ColliderPath() {
		_PathPoints = new List<ColliderPathPoints>();
	}

#if UNITY_EDITOR
	public void AddPathPoints(ColliderPathPoints pathPoints) {
		_PathPoints.Add(pathPoints);
	}
#endif

	public List<ColliderPathPoints> GetPathPoints() {
		return _PathPoints;
	}
}