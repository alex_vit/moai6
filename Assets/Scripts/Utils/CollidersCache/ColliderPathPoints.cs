using UnityEngine;

[System.Serializable]
public class ColliderPathPoints
{
	[SerializeField]
	private Vector2[] _Points;

	public ColliderPathPoints() {
		_Points = new Vector2[]{};
	}

	public ColliderPathPoints(Vector2[] points) {
		_Points = points;
	}

	public Vector2[] GetPathPoints() {
		return _Points;
	}
}