using System.Collections.Generic;
using UnityEngine;

public class SingletonFactory {

	static private Dictionary<System.Type, UnityEngine.MonoBehaviour> Instances;

	static public T GetInstance<T> () where T : UnityEngine.MonoBehaviour {
		if (Instances == null) {
			UnityEngine.Debug.Log("create new pull");
			Instances = new Dictionary<System.Type, UnityEngine.MonoBehaviour>(){};
		}
		T instance = null;
		
		if (Instances.ContainsKey(typeof(T)))
		{
			instance = (T) Instances[typeof(T)];
		}
		else
		{
			Instances.Add(typeof(T), instance);
		}
		if (instance == null)
		{
			instance = (T) Object.FindObjectOfType(typeof(T));
			Instances[typeof(T)] = instance;
		}
		if (instance == null)
		{
			GameObject gameObject = new GameObject(typeof(T).ToString());
			instance = gameObject.AddComponent(typeof(T)) as T;
			Instances[typeof(T)] = instance;
		}
		
		if (instance is GenericSingleton<T> && !(instance as GenericSingleton<T>).Initialized)
		{
			(instance as GenericSingleton<T>).Init();
			Instances[typeof(T)] = instance;
		}
		return instance;
	}

	static public void DestroyInstance<T> () where T : UnityEngine.Object {
		if (Instances.ContainsKey(typeof(T))) {
			Instances.Remove(typeof(T));
		}
	}
}