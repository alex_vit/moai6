using System.Collections.Generic;
using UnityEngine;

public class MultipleAnimationWithDirection : AnimationWithDirection {
	public const int ANIMATION_PARAMETER_ANIMATION_INDEX = 1140694833;

	[SerializeField]
	private List<string> AnimationNames;

	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		int angle = animator.GetInteger (ANIMATION_PARAMETER_ANGLE);
		int index = animator.GetInteger (ANIMATION_PARAMETER_ANIMATION_INDEX);
		animator.Play (GetDirectionHash(AnimationNames[index], angle));
	}
}