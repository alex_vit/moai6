﻿using UnityEngine;

[SharedBetweenAnimators]
public class ComplexAnimation : StateMachineBehaviour {

	private static int EMPTY_ANIMATION_HASH = -1459219776;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		if (layerIndex == 0) {
			for (int i = 1; i < animator.layerCount; i++) {
				if (animator.HasState(i, stateInfo.shortNameHash)) {
					animator.Play(stateInfo.shortNameHash, i);
				} else
				{
					animator.Play(EMPTY_ANIMATION_HASH, i);
				}
			}
		}
	}
}
