﻿using UnityEngine;

public class AnimationForProcess : StateMachineBehaviour {
    public const int ANIMATION_PARAMETER_PROCESS_TIME = 949206549;
    private const int ANIMATION_PARAMETER_SPEED_MULTIPLIER = -1620466655;
	private const int ANIMATION_PARAMETER_PROCESS_COMPLETE = 266647363;
	[SerializeField]
	private bool CycledProcess;

    private float AnimationLength;
	private float TimePassed;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		TimePassed = 0.0f;
		if (!CycledProcess) {
			AnimationLength = stateInfo.length * stateInfo.speedMultiplier;
			animator.SetFloat(ANIMATION_PARAMETER_SPEED_MULTIPLIER, 0.0f);
		} else {
			animator.SetBool(ANIMATION_PARAMETER_PROCESS_COMPLETE, false);
		}
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		TimePassed += Time.deltaTime;
       	float processTime = animator.GetFloat (ANIMATION_PARAMETER_PROCESS_TIME);
		if (!CycledProcess) {
			if (processTime > 0) {
				animator.SetFloat(ANIMATION_PARAMETER_SPEED_MULTIPLIER, AnimationLength / processTime);
			}
		} else {
			if (TimePassed >= processTime) {
				animator.SetBool(ANIMATION_PARAMETER_PROCESS_COMPLETE, true);
			}
		}
	}

	override public void OnStateExit(UnityEngine.Animator animator, UnityEngine.AnimatorStateInfo stateInfo, int layerIndex) {
		if (!CycledProcess) {
			animator.SetFloat(ANIMATION_PARAMETER_SPEED_MULTIPLIER, 1.0f);
		}
	}
}
