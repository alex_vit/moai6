﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
#endif

[SharedBetweenAnimators]
public class RandomTransitionBehaviour : StateMachineBehaviour {

#if UNITY_EDITOR
	[MenuItem("TimeManager/Generate random transitions %#t")]
	public static void GenerateTransition() {
		foreach (AnimatorState animatorState in Selection.objects) {
			bool hasRandomTransitionBehaviour = false;
			foreach (StateMachineBehaviour behaviour in animatorState.behaviours) {
				if (behaviour is RandomTransitionBehaviour) {
					hasRandomTransitionBehaviour = true;
					break;
				}
			}
			if (!hasRandomTransitionBehaviour) {
				animatorState.AddStateMachineBehaviour<RandomTransitionBehaviour>();
			}
			for (int i = 0; i < Selection.objects.Length; i++) {
				AnimatorStateTransition transition = new AnimatorStateTransition();
				transition.duration = 0;
				transition.exitTime = 1.0f;
				transition.hasExitTime = true;
				transition.hasFixedDuration = true;
				transition.destinationState = Selection.objects[i] as AnimatorState;
				transition.AddCondition(AnimatorConditionMode.Equals, i, "RandomTransition");
				animatorState.AddTransition(transition);
				AssetDatabase.AddObjectToAsset(transition, AssetDatabase.GetAssetPath(animatorState));
			}
			AssetDatabase.SaveAssets();
		}
	}

	[MenuItem("TimeManager/Remove all animation behaviours %#r")]
	public static void RemoveBehaviours() {
		foreach (AnimatorState animatorState in Selection.objects) {
			while (animatorState.behaviours.Length > 0) {
				List<StateMachineBehaviour> behaviours = new List<StateMachineBehaviour>(animatorState.behaviours);
				behaviours.RemoveAt(0);
				animatorState.behaviours = behaviours.ToArray();
			}
		}
		AssetDatabase.SaveAssets();
	}
#endif

	private static int RANDOM_TRANSITION_HASH = 1271903874;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		animator.SetInteger(RANDOM_TRANSITION_HASH, Random.Range(0, animator.GetBehaviours<RandomTransitionBehaviour>().Length));
	}
}
