﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AnimationWithDirection : StateMachineBehaviour {
	protected const int ANIMATION_PARAMETER_ANGLE = -1052743155;

	public static void SetAnimatorAngle(Animator animator, Vector3 animatorPosition, Vector3 targetPosition) {
		AnimationDirections direction;
		if (animatorPosition.x < targetPosition.x) {
			if (animatorPosition.y < targetPosition.y) {
				direction = AnimationDirections.Up;
			} else {
				direction = AnimationDirections.Right;
			}
		} else {
			if (animatorPosition.y < targetPosition.y) {
				direction = AnimationDirections.Left;
			} else {
				direction = AnimationDirections.Down;
			}
		}
		SetAnimatorAngle(animator, direction);
	}

	public static void SetAnimatorAngle(Animator animator, AnimationDirections direction) {
		animator.SetInteger(ANIMATION_PARAMETER_ANGLE, Convert.ToInt32(direction));
	}

	[SerializeField]
	protected string AnimationName;

	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		int angle = animator.GetInteger (ANIMATION_PARAMETER_ANGLE);
		animator.Play (GetDirectionHash(AnimationName, angle));
	}

	protected static int GetDirectionHash(string animationName, int angle) {
		if (angle == Convert.ToInt32(AnimationDirections.Up)) {
			animationName += "Up";
		} else if (angle == Convert.ToInt32(AnimationDirections.Right)) {
			animationName += "Right";
		} else if (angle == Convert.ToInt32(AnimationDirections.Down)) {
			animationName += "Down";
		} else {
			animationName += "Left";
		}
		return AnimatorStringsHashesCache.GetStringHash(animationName);
	}
}
