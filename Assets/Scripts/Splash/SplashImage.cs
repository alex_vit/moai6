using UnityEngine;

public class SplashImage {
	public string ImageName;
	public float Duration;
	public Sprite Image;
	public bool Stretch;
}