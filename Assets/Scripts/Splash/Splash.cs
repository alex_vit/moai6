using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Splash : MonoBehaviour {
	[SerializeField]
	private SceneField NextScene;
	[SerializeField]
	private Image Logo;
	private List<SplashImage> Splashes;
	private Animator Animator;
	private Coroutine Coroutine;

	public void Awake() {
		ScreensizeManager.CheckScreenSize();
		LocalizationManager.Instance.Init();
		Animator = GetComponent<Animator>();
		Splashes = new List<SplashImage>();
		string configPath = System.IO.Path.Combine(System.IO.Path.Combine(Application.streamingAssetsPath, "Splash"),"config.csv");
		string[] lines = File.ReadAllLines(configPath);
		foreach (string line in lines) {
			if (line[0] != '#') {
				string[] lineSplit = line.Split(';');
				SplashImage splash = new SplashImage();
				splash.ImageName = lineSplit[0];
				splash.Duration = float.Parse(lineSplit[1]);
				splash.Stretch = bool.Parse(lineSplit[2]);
				Splashes.Add(splash);
			}
		}
		StartCoroutine(LoadSplashes(Splashes));
	}

	public void MouseDown() {
		if (Coroutine != null) {
			StopCoroutine(Coroutine);
			Animator.Play("Disappear");
		}
	}

	private IEnumerator LoadSplashes(List<SplashImage> splashes)
	{
		foreach (SplashImage splash in splashes) {
			WWW localFile = new WWW("file://" + System.IO.Path.Combine(Application.streamingAssetsPath, "Splash/" + splash.ImageName));
			yield return localFile;
			Texture texture = localFile.texture;
			splash.Image = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
		}
		Coroutine = StartCoroutine(ShowNextSplash());
	}

	private IEnumerator ShowNextSplash() {
		if (Splashes.Count > 0) {
			SplashImage splash = Splashes[0];
			Splashes.RemoveAt(0);
			Logo.sprite = splash.Image;
			if (splash.Stretch) {
				Logo.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (transform as RectTransform).rect.width);
				Logo.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (transform as RectTransform).rect.height);
			} else {
				Logo.SetNativeSize();
			}
			Animator.Play("Appear");
			yield return new WaitForSeconds(splash.Duration);
			Animator.Play("Disappear");
		} else {
			SceneSwitcher.Instance.SwitchTo(NextScene);
		}
	}

	private void OnDisappear() {
		Coroutine = StartCoroutine(ShowNextSplash());
	}
}
