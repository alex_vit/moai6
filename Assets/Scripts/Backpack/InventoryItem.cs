using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/InventoryItem", fileName="InventoryItem")]
public class InventoryItem : ScriptableObject {
	[SerializeField]
	private bool Consumable;
	[SerializeField]
	private List<InventoryItemCraftCost> CraftCosts;
	[SerializeField]
	private Sprite ActiveSprite;
	[SerializeField]
	private Sprite InActiveSprite;

	public string LocaleName;
	public string LocaleInfo;

	public Sprite GetActiveSprite() {
		return ActiveSprite;
	}

	public Sprite GetInActiveSprite() {
		return InActiveSprite;
	}

	public string GetItemType() {
		return name;
	}

	public List<InventoryItemCraftCost> GetCraftCosts() {
		return new List<InventoryItemCraftCost>(CraftCosts);
	}

	public bool IsConsumable() {
		return Consumable;
	}
}