using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipableItemSlot : ItemSlot {
	[SerializeField]
	private List<HeroSlot> PossibleSlots;
	[SerializeField]
	private Button Button;

	override protected void UpdateSlot() {
		base.UpdateSlot();
		Button.interactable = ItemCount > 0 ? !ProfileManager.GetCurrentProfile().IsItemEquipped(Item.GetItemType()) : false;
		if (ItemCount > 0) {
			Icon.sprite = ProfileManager.GetCurrentProfile().IsItemEquipped(Item.GetItemType()) ? Item.GetInActiveSprite() : Item.GetActiveSprite();
		}
	}

	public void OnEquip() {
		if (!Item.IsConsumable()) {
			foreach (HeroSlot heroSlot in PossibleSlots) {
				if (heroSlot.IsFree()) {
					ProfileManager.GetCurrentProfile().EquipItem(Item.GetItemType(), heroSlot.GetFullId());
					GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, Item.GetItemType()));
					break;
				}
			}
		} else {
			ProfileManager.GetCurrentProfile().EquipItem(Item.GetItemType(), "");
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, Item.GetItemType()));
		}
		ProfileManager.Instance.Save();
	}
}