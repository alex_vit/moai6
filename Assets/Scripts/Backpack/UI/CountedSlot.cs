using UnityEngine;
using UnityEngine.UI;

public class CountedSlot : ItemSlot {
	[SerializeField]
	private Image Background;
	[SerializeField]
	private Image CountFrame;
	[SerializeField]
	private Text Counter;

	[SerializeField]
	private Sprite ActiveFrame;
	[SerializeField]
	private Sprite InactiveFrame;

	override protected void UpdateSlot() {
		base.UpdateSlot();
		Background.sprite = ItemCount > 0 ? ActiveFrame : InactiveFrame;
		CountFrame.gameObject.SetActive(ItemCount > 0);
		Counter.text = ItemCount.ToString();
	}
}