using UnityEngine;
using UnityEngine.UI;

public class Backpack : MonoBehaviour {

	[SerializeField]
	private Toggle MaleToggle;
	[SerializeField]
	private Toggle FemaleToggle;
	[SerializeField]
	private CanvasRenderer MaleTab;
	[SerializeField]
	private CanvasRenderer FemaleTab;

	public void OnEnable() {
		MaleToggle.isOn = true;
		MaleTab.gameObject.SetActive(true);
		FemaleTab.gameObject.SetActive(false);
	}

	public void MaleToggleChanged(bool value) {
		MaleTab.gameObject.SetActive(value);
	}

	public void FemaleToggleChanged(bool value) {
		FemaleTab.gameObject.SetActive(value);
	}
}