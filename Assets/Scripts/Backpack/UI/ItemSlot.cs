using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour {
	[SerializeField]
	protected InventoryItem Item;
	[SerializeField]
	protected Image Icon;

	protected int ItemCount;

	public void OnEnable() {
		GameEventDispatcher.Instance.AddEventListener(GameEventType.InventoryItemUpdate, OnItemUpdateEvent);
		UpdateSlot();
	}

	public void OnDisable() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.InventoryItemUpdate, OnItemUpdateEvent);
	}

	private void OnItemUpdateEvent(GameEvent gameEvent) {
		if ((gameEvent.Target as string) == Item.GetItemType()) {
			UpdateSlot();
		}
	}

	protected virtual void UpdateSlot() {
		ItemCount = ProfileManager.GetCurrentProfile().GetInventoryItemCount(Item.GetItemType());
		if (ItemCount == 0) {
			Icon.sprite = Item.GetInActiveSprite();
		} else {
			Icon.sprite = Item.GetActiveSprite();
		}
	}
}