using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradableItemSlot : CraftableItemSlot {
	[SerializeField]
	private Image Star;
	[SerializeField]
	private Button Upgrade;

	[SerializeField]
	private List<Sprite> GradeIcons;

	override protected void UpdateSlot() {
		base.UpdateSlot();

	}

	override protected void UpdateCraftButton() {
		Star.gameObject.SetActive(ItemCount == 0 ? false : true);
		Craft.gameObject.SetActive(ItemCount == 0 ? true : false);
		Upgrade.gameObject.SetActive(ItemCount == 0 ? false : true);
		foreach (InventoryItemCraftCost craftCost in Item.GetCraftCosts()) {
			if (ProfileManager.GetCurrentProfile().GetInventoryItemCount(craftCost.Ingredient.GetItemType()) < craftCost.Count) {
				Craft.interactable = false;
				Upgrade.interactable = false;
				break;
			}
		}
		int itemGrade = ProfileManager.GetCurrentProfile().GetInventoryItemGrade(Item.GetItemType());
		if (ItemCount > 0) {
			Star.sprite = GradeIcons[itemGrade - 1];
			Star.SetNativeSize();
			if (itemGrade == GradeIcons.Count) {
				Upgrade.interactable = false;
			}
		}
	}

	public void OnUpgrade() {
		if (ProfileManager.GetCurrentProfile().GetInventoryItemGrade(Item.GetItemType()) <= GradeIcons.Count) {
			foreach (InventoryItemCraftCost craftCost in Item.GetCraftCosts()) {
				ProfileManager.GetCurrentProfile().ConsumeInventoryItem(craftCost.Ingredient.GetItemType(), craftCost.Count);
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, craftCost.Ingredient.GetItemType()));
			}
			ProfileManager.GetCurrentProfile().UpgradeInventoryItem(Item.GetItemType());
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, Item.GetItemType()));
			ProfileManager.Instance.Save();
		}
	}
}