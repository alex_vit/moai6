using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroSlot : MonoBehaviour {
	[SerializeField]
	private List<InventoryItem> PossibleItems;
	[SerializeField]
	private Button Button;
	[SerializeField]
	private Image Icon;
	private string EquippedItem;

	public void OnEnable() {
		GameEventDispatcher.Instance.AddEventListener(GameEventType.InventoryItemUpdate, OnItemUpdateEvent);
		UpdateSlot();
	}

	public void OnDisable() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.InventoryItemUpdate, OnItemUpdateEvent);
	}

	public string GetFullId() {
		return transform.parent.parent.name + transform.parent.name + name;
	}

	private void OnItemUpdateEvent(GameEvent gameEvent) {
		UpdateSlot();
	}

	private void UpdateSlot() {
		EquippedItem = ProfileManager.GetCurrentProfile().GetEquippedItemType(GetFullId());
		Button.interactable = EquippedItem != null;
		Icon.gameObject.SetActive(EquippedItem != null);
		if (EquippedItem != null) {
			foreach (InventoryItem inventoryItem in PossibleItems) {
				if (inventoryItem.GetItemType() == EquippedItem) {
					Icon.sprite = inventoryItem.GetActiveSprite();
					Icon.SetNativeSize();
					break;;
				}
			}
		}
	}

	public bool IsFree() {
		return EquippedItem == null;
	}

	public void OnClick() {
		if (!IsFree()) {
			ProfileManager.GetCurrentProfile().UnequipItem(EquippedItem);
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, EquippedItem));
			ProfileManager.Instance.Save();
		}
	}
}