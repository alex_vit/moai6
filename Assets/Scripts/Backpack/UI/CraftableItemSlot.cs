using UnityEngine;
using UnityEngine.UI;

public class CraftableItemSlot : EquipableItemSlot {
	[SerializeField]
	protected Button Craft;

	override protected void UpdateSlot() {
		base.UpdateSlot();
		Craft.interactable = ItemCount == 0 ? true : false;
		UpdateCraftButton();
	}

	protected virtual void UpdateCraftButton() {
		foreach (InventoryItemCraftCost craftCost in Item.GetCraftCosts()) {
			if (ProfileManager.GetCurrentProfile().GetInventoryItemCount(craftCost.Ingredient.GetItemType()) < craftCost.Count) {
				Craft.interactable = false;
				break;
			}
		}
	}

	public void OnCraft() {
		foreach (InventoryItemCraftCost craftCost in Item.GetCraftCosts()) {
			ProfileManager.GetCurrentProfile().ConsumeInventoryItem(craftCost.Ingredient.GetItemType(), craftCost.Count);
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, craftCost.Ingredient.GetItemType()));
		}
		ProfileManager.GetCurrentProfile().AddInventoryItem(Item.GetItemType(), Item.IsConsumable());
		ProfileManager.Instance.Save();
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.InventoryItemUpdate, Item.GetItemType()));
	}
}