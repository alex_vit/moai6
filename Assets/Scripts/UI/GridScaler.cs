﻿using System;
using System.Collections; 
using System.Collections.Generic; 
using UnityEngine; 
using System.Linq;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI; 

[RequireComponent(typeof(GridLayoutGroup))] 
public class GridScaler : MonoBehaviour 
{
	[Header("Use resolution or aspect ratio")]
	public Vector2 baseResolution; 
	public Vector2 baseRatio; 

	private float _initialRatio, _currentRatio; 
	private RectOffset _rectOffset; 
	private GridLayoutGroup _gridLayout; 

	private List<RectTransform> _childrensRects;
	private List<Text> _childrenTexts;
	private RectTransform _rootTransform; 
	private Vector2 _rectSize; 

	void Awake() 
	{ 
		_gridLayout = GetComponent<GridLayoutGroup>(); 
		_childrensRects = _gridLayout.GetComponentsInChildren<RectTransform>().ToList(); 
		_childrenTexts = _gridLayout.GetComponentsInChildren<Text>().ToList(); 
		
		if (baseRatio.x > 0 && baseRatio.y > 0) 
			_initialRatio = (float)Math.Round((baseRatio.x / baseRatio.y), 1); 
		else 
			_initialRatio = (float)Math.Round(baseResolution.x / baseResolution.y, 1);
		
		_currentRatio = (float) Math.Round((float)Screen.width / Screen.height, 1); 
		Debug.Log("_initialRatio " + _initialRatio + "_currentRatio " + _currentRatio);
		_rootTransform = _gridLayout.GetComponent<RectTransform>(); 
		_rectOffset = new RectOffset(); 
		
		
	} 

	void Start() 
	{ 
		if(_currentRatio != _initialRatio) 
			Scale(); 
	} 

	void Scale() 
	{ 
		_currentRatio /= _initialRatio; 

		for (int i = 0; i < _childrensRects.Count(); i++) 
		{ 
			_rectSize.x = _childrensRects[i].rect.width * _currentRatio; 
			_rectSize.y = _childrensRects[i].rect.height * _currentRatio; 
			_childrensRects[i].sizeDelta = _rectSize;

			_childrensRects[i].anchoredPosition *= _currentRatio;
		} 
		_gridLayout.cellSize = new Vector2(Mathf.Round(_gridLayout.cellSize.x * _currentRatio), Mathf.Round(_gridLayout.cellSize.y * _currentRatio));
			
		_rectOffset.bottom = (int)(_gridLayout.padding.bottom * _currentRatio); 
		_rectOffset.top = (int)(_gridLayout.padding.top * _currentRatio); 
		_rectOffset.right = (int)(_gridLayout.padding.right * _currentRatio); 
		_rectOffset.left = (int)(_gridLayout.padding.left * _currentRatio); 

		_gridLayout.padding = _rectOffset; 
		_gridLayout.spacing = new Vector2(_gridLayout.spacing.x * _currentRatio, _gridLayout.spacing.y * _currentRatio);
		
		for (int i = 0; i < _childrenTexts.Count(); i++)
		{
			_childrenTexts[i].fontSize = Mathf.RoundToInt((float)_childrenTexts[i].fontSize * _currentRatio);
		} 

		LayoutRebuilder.ForceRebuildLayoutImmediate(_rootTransform); 
	} 
}
