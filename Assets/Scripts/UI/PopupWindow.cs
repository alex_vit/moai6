using UnityEngine;

public class PopupWindow : MonoBehaviour {
	public delegate void WindowDelegate(PopupWindow window);

	public event WindowDelegate OnWindowShowed;
	public event WindowDelegate OnWindowClosed;

	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float Volume = 1;
	[SerializeField]
	private AudioClip AppearSound;
	[SerializeField]
	private AudioClip DisappearSound;
	private Animator WindowAnimator;

	public virtual void Awake() {
		WindowAnimator = GetComponent<Animator>();
	}

	public virtual void Show()
	{
		gameObject.SetActive(true);
		SetChildrenActive(true);
		if (OnWindowShowed != null) OnWindowShowed(this);
		if (WindowAnimator) {
			WindowAnimator.Play("Appear");
		}
		if (AppearSound != null) {
			AudioManager.Instance.PlaySound(AppearSound, Volume);
		}
	}

	public virtual void Close() {
		SetChildrenActive(false);
		if (OnWindowClosed != null) OnWindowClosed(this);
		if (WindowAnimator) {
			WindowAnimator.Play("Disappear");
		}
		if (DisappearSound != null) {
			AudioManager.Instance.PlaySound(DisappearSound, Volume);
		}
	}

	public void OnHidden() {
		gameObject.SetActive(false);
	}

	private void SetChildrenActive(bool active) {
		foreach (Transform child in transform) {
			child.gameObject.SetActive(active);
		}
	}
}