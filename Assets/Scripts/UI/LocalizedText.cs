using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[RequireComponent(typeof(Text))]
public class LocalizedText : MonoBehaviour {
	[SerializeField]
	[HideInInspector]
	private string _StringKey;
	public string StringKey { get {return _StringKey;} set {_StringKey = value;}}

	protected Text TextField;

	public void Awake() {
		TextField = gameObject.GetComponent<Text>();
	}

	public void SetKey(string key) {
		string newText = LocalizationManager.Instance.GetString(key);
		if (newText != "") {
			if (TextField == null) {
				TextField = gameObject.GetComponent<Text>();
			}
			TextField.text = newText;
			if (TextField.text != null) {
				TextField.text = TextField.text.Replace ("\\n", "\n");
			}
		} else {
			TextField.text = "";
		}
	}

	public virtual void OnEnable() {
		if (_StringKey != null) {
			SetKey(_StringKey);
		}
	}
}