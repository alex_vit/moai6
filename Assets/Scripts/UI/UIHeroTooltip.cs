﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI {
	public class UIHeroTooltip : BaseTooltip {
		private static UIHeroTooltip _Instance;
		public static UIHeroTooltip Instance { get {return _Instance;} }

		[SerializeField] private Text _title;
		[SerializeField] private Text _info;
		
		public override void Awake() {
			if (_Instance == null) {
				_Instance = this;
				gameObject.SetActive(false);
			} else if (_Instance != this) {
				Destroy (this);
			}
		}

		public void Show(string templateName, string nameKey, string templateTitle, string titleKey)
		{
			string _heroNameTemplate = LocalizationManager.Instance.GetString(templateName);
			string _heroNameTitle = LocalizationManager.Instance.GetString(nameKey);
			string _heroTitleTemplate = LocalizationManager.Instance.GetString(templateTitle);
			string _heroTitleTitle = LocalizationManager.Instance.GetString(titleKey);
			
			
			_title.text = _heroNameTemplate.Replace("%HERO_NAME%", _heroNameTitle);
			_info.text = _heroTitleTemplate.Replace("%STATUS_NAME%", _heroTitleTitle);
			Show();
		}
	}
}
