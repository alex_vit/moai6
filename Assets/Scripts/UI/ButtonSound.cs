using UnityEngine;
using UnityEngine.UI;

public class ButtonSound : MonoBehaviour{
	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float Volume = 1;
	[SerializeField]
	private AudioClip ClickSound;

	public virtual void Awake ()
	{
		if (ClickSound != null) {
			GetComponent<Button> ().onClick.AddListener (() => AudioManager.Instance.PlaySound(ClickSound, Volume));
		}
	}
}