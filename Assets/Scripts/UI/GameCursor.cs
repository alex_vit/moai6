using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Cursor/GameCursor", fileName="GameCursor")]
public class GameCursor : ScriptableObject {
	[SerializeField]
	private List<GameCursorFrame> _NormalFrames;
	[SerializeField]
	private List<GameCursorFrame> _PressedFrames;
	[SerializeField]
	private AudioData _PressedAudio;

	public List<GameCursorFrame> NormalFrames {get{return _NormalFrames;}}

	public List<GameCursorFrame> PressedFrames {
		get {
			if (_PressedFrames == null || _PressedFrames.Count == 0) {
				return _NormalFrames;
			}
			return _PressedFrames;
		}
	}
	public AudioData PressedAudio {get{return _PressedAudio;}}
}