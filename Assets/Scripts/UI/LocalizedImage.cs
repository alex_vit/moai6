using UnityEngine;
using UnityEngine.UI;

public class LocalizedImage : MonoBehaviour {
	[SerializeField]
	[HideInInspector]
	private string _ImageName;

	public void OnEnable() {
		ShowImage();
	}

	private void ShowImage(GameEvent gameEvent = null) {
		if (gameEvent == null || (gameEvent.Type == GameEventType.LoadedLocalizedImage && (gameEvent.Target as string) == _ImageName)) {
			SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
			Sprite sprite = LocalizationManager.Instance.GetImage(_ImageName);
			if (sprite != null) {
				if (spriteRenderer != null) {
					spriteRenderer.sprite = sprite;
				} else {
					Image image = GetComponent<Image>();
					if (image != null) {
						image.sprite = sprite;
						image.SetNativeSize();
					}
				}
				GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LoadedLocalizedImage, ShowImage);
			} else {
				GameEventDispatcher.Instance.AddEventListener(GameEventType.LoadedLocalizedImage, ShowImage);
			}
		}
	}
}