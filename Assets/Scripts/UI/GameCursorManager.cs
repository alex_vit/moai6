using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCursorManager : GenericSingleton<GameCursorManager>{
	private GameCursor _MainCursor;
	private GameCursor _CurrentCursor;
	private List<GameCursorFrame> _CurrentCursorFrames;
	private bool _MousePressed;
	private bool _MouseReleased;
	private int _CurrentFrameNum;
	private Coroutine _UpdateCoroutine;

	override public void Init() {
		base.Init();
		_MouseReleased = true;
		_MainCursor = Resources.Load<GameCursor>("MainCursor");
		_CurrentCursor = _MainCursor;
	}

	public void ShowCursor(GameCursor cursor) {
		if (cursor != _CurrentCursor) {
			if (cursor != null) {
				_CurrentCursor = cursor;
			} else if (_CurrentCursor != _MainCursor) {
				_CurrentCursor = _MainCursor;
			}
			_CurrentCursorFrames = null;
		}
	}

	public void RefreshCursor() {
		if (_UpdateCoroutine != null) {
			StopCoroutine(_UpdateCoroutine);
		}
		if (ProfileManager.GetCurrentProfile().SystemCursor) {
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
		} else {
			_CurrentCursorFrames = null;
			_UpdateCoroutine = StartCoroutine(UpdateGameCursor());
		}
	}

	private IEnumerator UpdateGameCursor() {
		while (true) {
			if (Input.GetMouseButton(0) && !_MousePressed && _MouseReleased) {
				_MouseReleased = false;
				_MousePressed = true;
				_CurrentFrameNum = 0;
				_CurrentCursorFrames = _CurrentCursor.PressedFrames;
				if (_CurrentCursor.PressedAudio != null) {
					AudioManager.Instance.PlaySound(_CurrentCursor.PressedAudio);
				}
			}
			if (!Input.GetMouseButton(0) && !_MouseReleased) {
				_MouseReleased = true;
			}
			if (_CurrentCursorFrames == null) {
				_CurrentFrameNum = 0;
				_CurrentCursorFrames = _CurrentCursor.NormalFrames;
			}
			Cursor.SetCursor(_CurrentCursorFrames[_CurrentFrameNum].Texture, _CurrentCursorFrames[_CurrentFrameNum].HotSpot, CursorMode.ForceSoftware);
			_CurrentFrameNum++;
			if (_CurrentFrameNum >= _CurrentCursorFrames.Count) {
				_CurrentFrameNum = 0;
				if (_MousePressed) {
					_MousePressed = false;
					_CurrentCursorFrames = _CurrentCursor.NormalFrames;
				}
			}
			yield return new WaitForEndOfFrame();
		}
	}
}