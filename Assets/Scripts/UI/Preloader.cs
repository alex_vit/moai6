using UnityEngine;
using UnityEngine.UI;

public class Preloader : MonoBehaviour {
	[SerializeField]
	private Text _LevelLabel;
	[SerializeField]
	private Text _BonusLevelLabel;

	public void OnEnable() {
		_LevelLabel.gameObject.SetActive(!StorylineManager.Instance.GetCurrentLevel().BonusLevel);
		_BonusLevelLabel.gameObject.SetActive(StorylineManager.Instance.GetCurrentLevel().BonusLevel);
	}
}