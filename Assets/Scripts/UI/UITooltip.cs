﻿using UnityEngine;
using UnityEngine.UI;

namespace UI {
	public class UITooltip : BaseTooltip {
		private static UITooltip _Instance;
		public static UITooltip Instance { get {return _Instance;} }

		[SerializeField] private LocalizedText _title;
		[SerializeField] private LocalizedText _info;
		
		public override void Awake() {
			if (_Instance == null) {
				_Instance = this;
			} else if (_Instance != this) {
				Destroy (this);
			}
		}

		public void Show(string titleText, string infoText) {
			_title.StringKey = titleText;
			_info.StringKey = infoText;
			Show();
		}
	}
}