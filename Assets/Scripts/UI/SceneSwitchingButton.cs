using UnityEngine;
using UnityEngine.UI;

public class SceneSwitchingButton : MonoBehaviour
{
	[Header("Is special scene or last loaded")]
	public bool IsSpecialScene = true;
	[SerializeField]
	private SceneField NextScene;

	public void Start() {
		GetComponent<Button>().onClick.AddListener(OnClick);
	}

	public void OnClick()
	{
		SceneSwitcher.Instance.SwitchTo(IsSpecialScene ? NextScene : SceneSwitcher.Instance.LastSceneName);
	}
}