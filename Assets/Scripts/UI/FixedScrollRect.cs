﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FixedScrollRect : ScrollRect
{
	private float _scrollBarSizeX, _scrollBarSizeY;

	protected override void LateUpdate()
	{
		if (this.horizontalScrollbar)
			_scrollBarSizeX = this.horizontalScrollbar.size;
		if (this.verticalScrollbar)
			_scrollBarSizeY = this.verticalScrollbar.size;
		base.LateUpdate();
		if (this.horizontalScrollbar)
			this.horizontalScrollbar.size = _scrollBarSizeX;
		if (this.verticalScrollbar)
			this.verticalScrollbar.size = _scrollBarSizeY;		
		
	}

	public override void Rebuild(CanvasUpdate executing)
	{
		if (this.horizontalScrollbar)
			_scrollBarSizeX = this.horizontalScrollbar.size;
		if (this.verticalScrollbar)
			_scrollBarSizeY = this.verticalScrollbar.size;
		base.Rebuild(executing);
		if (this.horizontalScrollbar)
			this.horizontalScrollbar.size = _scrollBarSizeX;
		if (this.verticalScrollbar)
			this.verticalScrollbar.size = _scrollBarSizeY;		
	}

	public override void OnBeginDrag(PointerEventData data)
	{
		
	}
	
	public override void OnDrag(PointerEventData data)
	{
		
	}
	
	public override void OnEndDrag(PointerEventData data)
	{
		
	}
}
