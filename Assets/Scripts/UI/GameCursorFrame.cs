using UnityEngine;
[System.Serializable]
public class GameCursorFrame {
	[SerializeField]
	private Texture2D _Texture;
	[SerializeField]
	private Vector2 _HotSpot = Vector2.zero;

	public Texture2D Texture {get {return _Texture;}}
	public Vector2 HotSpot {get {return _HotSpot;}}
}