﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class HighlightTextButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Color baseColor;
	public Color lightColor;

	private Text _selfText;
	
	void Awake()
	{
		_selfText = GetComponentInChildren<Text>();
		_selfText.color = baseColor;
	}

	public void OnPointerEnter(PointerEventData data)
	{
		if(_selfText)
			_selfText.color = lightColor;
	}
	
	public void OnPointerExit(PointerEventData data)
	{
		if(_selfText)
			_selfText.color = baseColor;
	}
}
