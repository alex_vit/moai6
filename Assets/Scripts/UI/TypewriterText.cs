using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TypewriterText : MonoBehaviour {

	public delegate void TypewriterComplete();

	[SerializeField]
	private float TypeTime;
	private Text TextField;
	private string FinalText;

	private TypewriterComplete CompleteDelegate;

	public void OnEnable() {
		TextField = GetComponent<Text>();
		FinalText = TextField.text;
		StartCoroutine(WriteNextLetter());
	}

	public void SetDelegate(TypewriterComplete completeDelegate) {
		CompleteDelegate = completeDelegate;
	}

	IEnumerator WriteNextLetter(){
		for (int i = 0; i < FinalText.Length + 1; i++)
		{
			TextField.text = FinalText.Substring(0, i);
			yield return new WaitForSeconds(.03f);
		}
		CompleteDelegate();
	}
}