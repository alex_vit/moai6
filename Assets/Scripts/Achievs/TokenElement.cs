﻿using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class TokenElement : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler
{
	public int ID;
	[SerializeField] private string _TokenTitle;
	[SerializeField] private string _TokenText;
	
	public void OnPointerEnter(PointerEventData eventData) {
		UITooltip.Instance.Show(_TokenTitle,_TokenText);
	}

	public void OnPointerExit(PointerEventData eventData) {
		UITooltip.Instance.Hide();
	}
}
