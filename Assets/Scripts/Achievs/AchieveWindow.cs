﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AchieveWindow : MonoBehaviour
{
	public Transform AchievesContent;
	public Transform TokenContent;
	
	private List<AchieveElement> achieves;
	private List<TokenElement> tokens;
	// Use this for initialization
	void Awake()
	{
		tokens = TokenContent.GetComponentsInChildren<TokenElement>().ToList();
		achieves = AchievesContent.GetComponentsInChildren<AchieveElement>().ToList();
	}

	void Start () {
		FillInfo();	
	}

	void FillInfo()
	{
		if (achieves != null)
		{
			//stub
			for (int i = 0; i < achieves.Count; i++)
			{
				achieves[i].FillTextInfo();
				//stub
				achieves[i].Enable();
				
			}
		}

		if (tokens != null)
		{
			//stub
			foreach (var token in tokens)
			{
				token.gameObject.SetActive(true);
			}
		}
	}
}
