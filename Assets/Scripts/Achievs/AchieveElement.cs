﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;
using UnityEngine.UI;

public class AchieveElement : MonoBehaviour
{
	public AchieveType type;
	public int id;
	public GameObject ActiveBackground;
	public GameObject DisabledBackground;
	public GameObject ActiveIcon;
	public GameObject DisabledIcon;
	public Text Title;
	public Text Text;

	private readonly string achieveKey = "achievement";
	private readonly string titleKey = "_title";
	private readonly string textKey = "_text";

	public void FillTextInfo()
	{
		string name = achieveKey + (id < 10 ? "0" + id.ToString() : id.ToString());
		Title.text = LocalizationManager.Instance.GetString(name + titleKey);
		Text.text = LocalizationManager.Instance.GetString(name + textKey);
	}

	public void Enable(bool active = false)
	{
		ActiveIcon.SetActive(active);
		ActiveBackground.SetActive(active);
		DisabledIcon.SetActive(!active);
		DisabledBackground.SetActive(!active);
	}
}

public enum AchieveType
{
	COMMON,
	CE
}
