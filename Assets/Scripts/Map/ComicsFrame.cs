using UnityEngine;

[System.Serializable]
public class ComicsFrame {
	[SerializeField]
	private Sprite _Image;
	[HideInInspector]
	[SerializeField]
	private string _Text;
	[SerializeField]
	private float _Duration;

	public Sprite Image {get{return _Image;}}
	public string Text {get{return _Text;}}
	public float Duration {get{return _Duration;}}
}