using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Storyline/Level", fileName="Level")]
public class StorylineLevel : ScriptableObject
{
	[SerializeField] private int id;
	[SerializeField]
	private SceneField _LevelScene;
	[HideInInspector]
	[SerializeField]
	private string _LevelName;
	[SerializeField]
	private bool _BonusLevel;
	
	public int LevelId {get { return id; }}
	public SceneField LevelScene {get { return _LevelScene;}}
	public string LevelName {get { return _LevelName;}}
	public bool BonusLevel {get { return _BonusLevel;}}
	public HeroType Herotype;
}