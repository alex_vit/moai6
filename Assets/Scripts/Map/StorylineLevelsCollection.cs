using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Storyline/LevelsCollection", fileName="LevelsCollection")]
public class StorylineLevelsCollection : ScriptableObject {
	[SerializeField]
	private List<StorylineLevel> _LevelsOrder;
	public List<StorylineLevel> LevelsOrder {get {return _LevelsOrder;}}
}