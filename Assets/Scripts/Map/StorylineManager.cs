using System.Collections.Generic;
using UnityEngine;

public class StorylineManager : GenericSingleton<StorylineManager> {

	public StorylineLevel NewUnlockedLevel;
	private StorylineLevelsCollection _LevelsCollection;
	private List<LevelResult> _LevelResults;

	override public void Init() {
		if (!Initialized) {
			base.Init();
			//Debug.Log();
			_LevelsCollection = Resources.Load<StorylineLevelsCollection>("Storyline");
			_LevelResults = ProfileManager.GetCurrentProfile().GetLevelResults();
			if (_LevelResults == null) {
				ProfileManager.GetCurrentProfile().InitLevelResults(_LevelsCollection.LevelsOrder.Count);
				_LevelResults = ProfileManager.GetCurrentProfile().GetLevelResults();
				_LevelResults[0].Unlocked = true;
				ProfileManager.Instance.Save();
			}
		}
	}

	public void CompleteCurrentLevel(bool goldTime, int scores) {
		int currentLevelIndex = ProfileManager.GetCurrentProfile().GetCurrentLevel();
		LevelResult result = _LevelResults[currentLevelIndex];
		result.GoldTime = goldTime;
		result.Scores = scores;
		if (!_LevelsCollection.LevelsOrder[currentLevelIndex].BonusLevel) {
			bool nextLevelUnlocked = false;
			currentLevelIndex++;
			while (currentLevelIndex < _LevelResults.Count && !nextLevelUnlocked) {
				if (!_LevelsCollection.LevelsOrder[currentLevelIndex].BonusLevel) {
					nextLevelUnlocked = true;
					NewUnlockedLevel = _LevelsCollection.LevelsOrder[currentLevelIndex]; 
				}
				_LevelResults[currentLevelIndex].Unlocked = true;
				currentLevelIndex++;
			}
		}
		ProfileManager.Instance.Save();
	}

	public void SelectLevel(StorylineLevel selectedLevel) {
		ProfileManager.GetCurrentProfile().SetCurrentLevel(_LevelsCollection.LevelsOrder.IndexOf(selectedLevel));
		ProfileManager.Instance.Save();
	}

	public LevelResult GetLevelResult(StorylineLevel level) {
		return _LevelResults[_LevelsCollection.LevelsOrder.IndexOf(level)];
	}

	public StorylineLevel GetCurrentLevel() {
		return _LevelsCollection.LevelsOrder[ProfileManager.GetCurrentProfile().GetCurrentLevel()];
	}

	public int GetCurrentLevelNumber() {
		int result = 1;
		for (int i = 0; i < ProfileManager.GetCurrentProfile().GetCurrentLevel(); i++) {
			if (_LevelsCollection.LevelsOrder[i].BonusLevel == _LevelsCollection.LevelsOrder[ProfileManager.GetCurrentProfile().GetCurrentLevel()].BonusLevel) {
				result++;
			}
		}
		return result;
	}
}