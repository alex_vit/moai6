using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComicsContainer : MonoBehaviour {
	[SerializeField]
	private Image Image;
	[SerializeField]
	private LocalizedText Text;
	[SerializeField]
	private Animator ImageAnimator;
	[SerializeField]
	private Animator TextAnimator;
	private Coroutine Coroutine;

	private List<ComicsFrame> Frames;

	public bool IsShowing = false;

	public void Awake() {
		SetChildrenActive(false);
	}

	public void ShowFrames(List<ComicsFrame> frames)
	{
		IsShowing = true;
		SetChildrenActive(true);
		Frames = frames;
		Coroutine = StartCoroutine(ShowNextFrame());
	}

	public void OnSkip() {
		Frames.Clear();
		StopCoroutine(Coroutine);
		TextAnimator.Play("Disappear");
		IsShowing = false;
	}

	public void MouseDown() {
		StopCoroutine(Coroutine);
		TextAnimator.Play("Disappear");
	}

	private IEnumerator ShowNextFrame() {
		if (Frames.Count > 0) {
			ComicsFrame frame = Frames[0];
			Frames.RemoveAt(0);
			if (Image.sprite != frame.Image) {
				Image.sprite = frame.Image;
				ImageAnimator.Play("Appear");
			}
			Text.StringKey = frame.Text;
			Text.OnEnable();
			TextAnimator.Play("Appear");
			yield return new WaitForSeconds(frame.Duration);
			if ((Frames.Count > 0 && frame.Image != Frames[0].Image) || Frames.Count == 0) {
				ImageAnimator.Play("Disappear");
			}
			if (Frames.Count == 0) {
				TextAnimator.Play("FullDisappear");
			} else {
				TextAnimator.Play("Disappear");
			}
		} else {
			SetChildrenActive(false);
			IsShowing = false;
		}
	}

	private void SetChildrenActive(bool active) {
		foreach (Transform child in transform) {
			child.gameObject.SetActive(active);
		}
	}

	private void OnDisappear() {
		Coroutine = StartCoroutine(ShowNextFrame());
	}
}