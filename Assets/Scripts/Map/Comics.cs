using System.Collections.Generic;
using UnityEngine;

public class Comics : TriggeredBehaviour {
	[SerializeField]
	private List<ComicsFrame> Frames;
	private ComicsContainer Container;

	override public void Awake() {
		base.Awake();
		Container = GetComponent<ComicsContainer>();
	}

	override protected void TriggersComplete() {
		Container.ShowFrames(Frames);
	}
}