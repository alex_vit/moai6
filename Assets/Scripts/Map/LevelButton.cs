using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour {
	[SerializeField]
	private StorylineLevel _StorylineLevel;
	[SerializeField]
	private Sprite Hero;
	[SerializeField]
	private LevelButtonBall NewLevelBall;
	[SerializeField]
	private LevelButtonBall FinishedLevelBall;
	[SerializeField]
	private LevelButtonBall GoldLevelBall;
	[SerializeField]
	private Button _BallButton;
	private LevelButtonBall ActualBall;

	public HeroType HeroType;
	public Button BallButton {get {return _BallButton;}}

	public virtual void OnEnable() {
		LevelResult levelResult = StorylineManager.Instance.GetLevelResult(_StorylineLevel);
		//if (Debug.isDebugBuild)
			//levelResult.Unlocked = true;
		
		_BallButton.interactable = levelResult.Unlocked;
		if (_StorylineLevel.BonusLevel && !levelResult.Unlocked)
		{
			gameObject.SetActive(levelResult.Unlocked);
		}

		if (levelResult.Scores > 0) {
			if (levelResult.GoldTime) {
				SetActualBall(GoldLevelBall);
			} else {
				SetActualBall(FinishedLevelBall);
			}
			if (!levelResult.CompleteProcessed) {
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelComplete, _StorylineLevel));
				levelResult.CompleteProcessed = true;
				ProfileManager.Instance.Save();
			}
		} else {
			SetActualBall(NewLevelBall);
			if (levelResult.Unlocked && !levelResult.UnlockProcessed) {
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelUnlocked, _StorylineLevel));
				Debug.Log("send unlock msg");
				levelResult.UnlockProcessed = true;
				ProfileManager.Instance.Save();
			}
		}
	}

	public StorylineLevel GetStorylineLevel() {
		return _StorylineLevel;
	}

	public Sprite GetHeroSprite() {
		return Hero;
	}

	private void SetActualBall(LevelButtonBall levelButtonBall) {
		ActualBall = levelButtonBall;
		(_BallButton.targetGraphic as Image).sprite = ActualBall.GetNormalSprite();
		(_BallButton.targetGraphic as Image).SetNativeSize();
		_BallButton.spriteState = ActualBall.GetSpriteState();
	}
}

public enum HeroType
{
	NONE,
	MALE,
	FEMALE
}