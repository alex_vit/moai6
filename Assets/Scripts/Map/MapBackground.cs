using UnityEngine;
using UnityEngine.UI;

public class MapBackground : MonoBehaviour{

	[SerializeField]
	private float _TileIndex;
	private Material MapMaterial;

	public float TileIndex {
		set {
			_TileIndex = value;
		}
	}

	public void Awake() {
		MapMaterial = this.GetComponent<Image>().material;
		_TileIndex = 0;
		MapMaterial.SetFloat("_TileIndex", _TileIndex);
	}

	public void Update() {
		MapMaterial.SetFloat("_TileIndex", _TileIndex);
	}
}