using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName="LevelButtonBall", menuName="TimeManager/Level Button Ball")]
public class LevelButtonBall : ScriptableObject {
	[SerializeField]
	private Sprite _Normal;
	[SerializeField]
	private Sprite _Highlight;
	[SerializeField]
	private Sprite _Pressed;
	[SerializeField]
	private Sprite _Disabled;

	public Sprite GetNormalSprite() {
		return _Normal;
	}

	public SpriteState GetSpriteState() {
		SpriteState spriteState = new SpriteState();
		spriteState.disabledSprite = _Disabled;
		spriteState.highlightedSprite = _Highlight;
		spriteState.pressedSprite = _Pressed;
		return spriteState;
	}
}