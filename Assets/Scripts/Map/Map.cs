using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour {
	[SerializeField]
	private RectTransform Flag;
	[SerializeField]
	private SpriteRenderer HeroIcon;
	[SerializeField]
	private ScrollRect Scroll;
	[SerializeField]
	private Canvas Preloader;
	[SerializeField]
	private Image LevelLoadingBar;
	private LevelButton SelectedLevel;

	static private Map _Instance;
	static public Map Instance { get {return _Instance;} }

	public void Awake() {
		if (_Instance != this) {
			Destroy (_Instance);
		}
		_Instance = this;
	}

	public void OnDestroy() {
		_Instance = null;
	}

	public void OnPlay() {
		Preloader.gameObject.SetActive(true);
		SceneSwitcher.Instance.SwitchTo(SelectedLevel.GetStorylineLevel().LevelScene, LevelLoadingBar);
	}

	public void OnEnable() {
		if (StorylineManager.Instance.NewUnlockedLevel != null) {
			StorylineManager.Instance.SelectLevel(StorylineManager.Instance.NewUnlockedLevel);
			StorylineManager.Instance.NewUnlockedLevel = null;
		}
		List<LevelButton> levelButtons = new List<LevelButton>(GetComponentsInChildren<LevelButton>());
		for (int i = 0; i < levelButtons.Count; i++) {
			if (levelButtons[i].GetStorylineLevel() == StorylineManager.Instance.GetCurrentLevel()) {
				SelectLevel(levelButtons[i], false);
			}
		}
	}

	public void SelectLevel(LevelButton levelButton) {
		if (SelectedLevel != levelButton) {
			SelectLevel(levelButton, true);
		} else {
			OnPlay();
		}
	}

	public void SelectLevel(LevelButton levelButton, bool animated) {
		if (!animated) {
			Flag.position = (levelButton.transform as RectTransform).position;
			Scroll.horizontalNormalizedPosition = Mathf.Clamp(((levelButton.transform as RectTransform).anchoredPosition.x + Scroll.content.rect.width / 2 - (transform as RectTransform).rect.width / 2) / Scroll.content.rect.width, 0.0f, 1.0f);
			Scroll.verticalNormalizedPosition = Mathf.Clamp(((levelButton.transform as RectTransform).anchoredPosition.y + Scroll.content.rect.height / 2 - (transform as RectTransform).rect.height / 2) / Scroll.content.rect.height, 0.0f, 1.0f);
		} else {
			Vector2 position = new Vector2(Flag.anchoredPosition.x, Flag.anchoredPosition.y);
			Vector2 destination = new Vector2((levelButton.transform as RectTransform).anchoredPosition.x, (levelButton.transform as RectTransform).anchoredPosition.y);
			Animation anim = Flag.GetComponent<Animation>();
			AnimationClip flyClip = new AnimationClip();
			flyClip.legacy = true;
			Keyframe[] keys = new Keyframe[2];
			keys[0] = new Keyframe(0.0f, position.x);
			keys[1] = new Keyframe(0.5f, destination.x);
			flyClip.SetCurve("", typeof(Transform), "m_AnchoredPosition.x", new AnimationCurve(keys));
			keys = new Keyframe[2];
			keys[0] = new Keyframe(0.0f, position.y);
			keys[1] = new Keyframe(0.5f, destination.y);
			flyClip.SetCurve("", typeof(Transform), "m_AnchoredPosition.y", new AnimationCurve(keys));

			anim.AddClip(flyClip, flyClip.name);
			anim.Play(flyClip.name);
			StorylineManager.Instance.SelectLevel(levelButton.GetStorylineLevel());

			if (StorylineManager.Instance.GetCurrentLevel().BonusLevel)
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelButtonClicked, null));
		}
		SelectedLevel = levelButton;
		HeroIcon.sprite = SelectedLevel.GetHeroSprite();
	}
}

