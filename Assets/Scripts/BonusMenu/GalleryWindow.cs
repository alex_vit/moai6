﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryWindow : MonoBehaviour {
	public Image ImageView;
	public LocalizedText TextView;
	
	public List<Sprite> Images;
	private int _currSpriteIndex;

	private readonly string baseKey = "gallery_";
	// Use this for initialization
	void Start()
	{
		_currSpriteIndex = 0;
		UpdateInfo(_currSpriteIndex);
	}

	public void GoToNextImage()
	{
		if (++_currSpriteIndex > Images.Count - 1)
			_currSpriteIndex = 0;
		UpdateInfo(_currSpriteIndex);
	}

	public void GoToPrevImage()
	{
		if (--_currSpriteIndex < 0)
			_currSpriteIndex = Images.Count - 1;
		UpdateInfo(_currSpriteIndex);
	}

	private void UpdateInfo(int index)
	{
		if(Images.Count <= 0) return;
		ImageView.sprite = Images[index];
		int _titleIndex = index + 1;
		string _key = baseKey + (_titleIndex < 10 ? "0" + _titleIndex : _titleIndex.ToString());
		TextView.SetKey(_key);
	}
}
