﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicWindow : MonoBehaviour
{
	public AudioClip[] Clips;
	
	public void SaveClip(int id)
	{
		if (Clips != null && Clips.Length >= id)
		{
			if (Clips[id] != null)
				BonusContentManager.SaveClip(Clips[id].name);
			else
				Debug.LogError("Clip is null");
		}
		else
		{
			#if UNITY_EDITOR
			Debug.LogError("Clips empty or clips doesn't contains element with this id");
			#endif
		}
	}
}
