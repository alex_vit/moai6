﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class WallpaperWindow : MonoBehaviour
{
	public Image ImageView;
	
	public List<Sprite> ImagesEn;
	public List<Sprite> ImagesRus;


	private List<Sprite> _currImageSprites;
	private int _currSpriteIndex;
	// Use this for initialization
	void Start()
	{
		_currSpriteIndex = 0;
		if(ImagesEn.Count != ImagesRus.Count)
			Debug.LogError("Images count are not identical");
		
		_currImageSprites = Application.systemLanguage == SystemLanguage.Russian ? ImagesRus : ImagesEn;
		UpdateInfo(_currSpriteIndex);
	}

	public void GoToNextImage()
	{
		if (++_currSpriteIndex > _currImageSprites.Count - 1)
			_currSpriteIndex = 0;
		//ImageView.sprite = Images[_currSpriteIndex >= Images.Count-1 ? 0 : ++_currSpriteIndex];
		UpdateInfo(_currSpriteIndex);
	}

	public void GoToPrevImage()
	{
		if (--_currSpriteIndex < 0)
			_currSpriteIndex = _currImageSprites.Count - 1;
		UpdateInfo(_currSpriteIndex);
	}
	
	private void UpdateInfo(int index)
	{
		if(_currImageSprites.Count <= 0) return;
		ImageView.sprite = _currImageSprites[index];
	}

	public void SaveImage(string size)
	{
		switch (size)
		{
			case "1280x1024":
				BonusContentManager.SaveImage(1280, 1024, ImageView.sprite.name);
				break;
			case "1366x768":
				BonusContentManager.SaveImage(1366, 768, ImageView.sprite.name);
				break;
			case "1600x900":
				BonusContentManager.SaveImage(1600, 900, ImageView.sprite.name);
				break;
			case "1920x1080":
				BonusContentManager.SaveImage(1920, 1080, ImageView.sprite.name);
				break;
		}
	}
}
