using UnityEngine;

public class LevelGoal : TriggeredBehaviour {

	[SerializeField]
	private string _GoalDescription;
	[SerializeField]
	private string _GoalCounter;

	private GoalDescription _GoalUI;

	public GoalDescription GoalUI {
		get {
			return _GoalUI;
		}
		set {
			_GoalUI = value;
			UpdateText();
		}
	}

	override protected void OnTriggerUpdate() {
		UpdateText();
		if (Complete) {
			foreach (Trigger trigger in Triggers) {
				if (!trigger.Complete) {
					GoalUI.ResetComplete();
					Complete = false;
					return;
				}
			}	
		}
	}

	override protected void OnTriggerComplete() {
		base.OnTriggerComplete();
		UpdateText();
	}

	override protected void TriggersComplete() {
		Complete = true;
		GoalUI.SetComplete();
		Level.Instance.OnGoalComplete();
	}

	public bool IsComplete() {
		return Complete;
	}

	private int GetCountToComplete() {
		int result = 0;
		foreach (Trigger trigger in Triggers) {
			result += trigger.GetCountToComplete();
		}
		return result;
	}

	private int GetCurrentCount() {
		int result = 0;
		foreach (Trigger trigger in Triggers) {
			result += trigger.GetCurrentCount();
		}
		return result;
	}

	private void UpdateText() {
		int currentCount = GetCurrentCount();
		int totalCount = GetCountToComplete();
		string counterText = LocalizationManager.Instance.GetString(_GoalCounter);
		counterText = counterText.Replace("%LEFT_COUNT%", (totalCount - currentCount).ToString());
		counterText = counterText.Replace("%COUNT%", currentCount.ToString());
		counterText = counterText.Replace("%TOTAL_COUNT%", totalCount.ToString());
		GoalUI.SetText(LocalizationManager.Instance.GetString(_GoalDescription) + " " + counterText);
	}
}