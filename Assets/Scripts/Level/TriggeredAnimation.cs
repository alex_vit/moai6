using UnityEngine;

public class TriggeredAnimation : TriggeredBehaviour {
	[SerializeField]
	private Animator _TargetAnimator;
	[SerializeField]
	private string _AnimationName;

	override protected void TriggersComplete() {
		base.TriggersComplete();
		_TargetAnimator.Play(AnimatorStringsHashesCache.GetStringHash(_AnimationName));
	}
}