using UnityEngine;

[System.Serializable]
public class LevelResource {
	[SerializeField]
	private GameObject ResourcePrefab;
	[SerializeField]
	public int ResourceCount;
	private string _ResourceType;

	public void SetResourcePrefab(GameObject resourcePrefab) {
		ResourcePrefab = resourcePrefab;
	}

	public void SetResourceType(string resourceType) {
		_ResourceType = resourceType;
	}

	public string GetResourceType() {
		if (ResourcePrefab != null) {
			return ResourcePrefab.name;
		}
		return _ResourceType;
	}

	public int GetScoresModifier() {
		if (ResourcePrefab != null) {
			IResource resource = ResourcePrefab.GetComponent<IResource>();
			if (resource != null) {
				return resource.ScoresModifier;
			}
		}
		return 0;
	}

	public Sprite GetResourceIcon() {
		if (ResourcePrefab != null) {
			return ResourcePrefab.GetComponent<IResource>().ResourceIcon;
		}
		return null;
	}

	public LevelBonus GetBonus() {
		if (ResourcePrefab != null) {
			return ResourcePrefab.GetComponent<BonusDrop>().Bonus;
		}
		return null;
	}
}