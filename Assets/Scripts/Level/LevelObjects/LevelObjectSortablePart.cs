using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LevelObjectSortablePart : MonoBehaviour, IComparable<LevelObjectSortablePart> {
	[SerializeField]
	private List<Vector3> _Corners;
	private bool Runable;
	private int _Order;

#if UNITY_EDITOR
	public List<Vector3> Corners {
		get {return _Corners;}
		set {_Corners = value;}
	}
#endif

	private Vector3 LeftCorner {
		get {return transform.TransformPoint(_Corners[0]);}
	}

	private Vector3 DownCorner {
		get {return transform.TransformPoint(_Corners[2]);}
	}

	private Vector3 RightCorner {
		get {return transform.TransformPoint(_Corners[3]);}
	}

	private Vector3 UpCorner {
		get {return transform.TransformPoint(_Corners[1]);}
	}

	private List<Renderer> _AffectedRenderers;

	public List<Renderer> AffectedRenderers {
		get {
			return _AffectedRenderers;
		}
	}

	public int Order {
		get {
			return _Order;
		}
		set {
			_Order = value;
		}
	}

	public int CompareTo(LevelObjectSortablePart other) {
		if (this == other) {
			return 0;
		} else if (UpCorner.y < other.DownCorner.y) {
			return 1;
		} else if (DownCorner.y > other.UpCorner.y) {
			return -1;
		} else if (UpCorner.y < other.DownCorner.y) {
			return 1;
		} else if (DownCorner.y > other.UpCorner.y) {
			return -1;
		} else {
			Vector3 intersectionPoint = Vector3.zero;
			bool linesIntersect;
			LevelObjectSortablePart first = this;
			LevelObjectSortablePart second = other;
			int orderModifier = 1;
			if (first.UpCorner.y < second.UpCorner.y) {
				first = other;
				second = this;
				orderModifier = -1;
			}
			if (first.DownCorner.x < second.DownCorner.x) {
				linesIntersect = GeometryUtils.LineLineIntersection(out intersectionPoint, second.DownCorner, first.UpCorner - second.DownCorner, first.DownCorner, first.RightCorner - first.DownCorner);
				if (linesIntersect) {
					if (intersectionPoint.y < second.DownCorner.y) {
						return 1 * orderModifier;
					} else {
						return -1 * orderModifier;
					}
				}
			} else {
				linesIntersect = GeometryUtils.LineLineIntersection(out intersectionPoint, second.DownCorner, first.UpCorner - second.DownCorner, first.LeftCorner, first.DownCorner - first.LeftCorner);
				if (linesIntersect) {
					if (intersectionPoint.y < second.DownCorner.y) {
						return 1 * orderModifier;
					} else {
						return -1 * orderModifier;
					}
				}
			}
			return 0;
		}
	}

	public bool IsRunable() {
		return Runable;
	}

	public void OnEnable() {
		Runable = GetComponent<WorkerRunningState>() != null;
		_AffectedRenderers = new List<Renderer>(){};
		Renderer[] allRenderers = GetComponentsInChildren<Renderer>(true);
		foreach (Renderer renderer in allRenderers) {
			if (renderer.sortingLayerName == "LevelObjects") {
				LevelObjectSortablePart sortablePart = renderer.gameObject.GetComponentInParent<LevelObjectSortablePart>();
				if (sortablePart == this) {
					_AffectedRenderers.Add(renderer);
				}
			}
		}
		if (Application.isPlaying) {
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.SortablePartEnable, this));
		}
	}

	public void OnDisable() {
		if (Application.isPlaying) {
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.SortablePartDisable, this));
		}
	}
}