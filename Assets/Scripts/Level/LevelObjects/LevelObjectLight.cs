using System.Collections.Generic;
using UnityEngine;

public class LevelObjectLight : MonoBehaviour, IPassableBetweenObjects {

	[SerializeField]
	private HidingArea _Light;
	private HidingArea _ActualLight;

	#if UNITY_EDITOR
	public HidingArea Light {
		get {
			return _Light;
		}
	}
	#endif

	public void OnEnable() {
		Level.Instance.Fog.AddLight(_Light);
	}

	private void PassLight(HidingArea light) {
		_Light.Position = light.Position;
	}

	public void PassToSuccessor(MonoBehaviour successor) {
		(successor as LevelObjectLight).PassLight(_Light);
	}
}