using System.Collections.Generic;
using UnityEngine;

public class LevelObjectForeignTransition : MonoBehaviour {
	[SerializeField]
	private List<PossibleStatesTransitionDescription> _Transitions;

	public void SetTransitions(List<PossibleStatesTransitionDescription> transitions) {
		_Transitions = transitions;
	}

	public void OnEnable() {
		foreach (PossibleStatesTransitionDescription transitionDescription in _Transitions) {
			if (transitionDescription.IsLevelObjectAffected(transitionDescription.LevelObject)) {
				transitionDescription.LevelObject.CurrentStateName = transitionDescription.TargetState;
			}
		}
	}
}