using System.Collections;
using UnityEngine;

public class LevelObjectHealth : MonoBehaviour {
	[SerializeField]
	private BaseStateDescription _DamagedState;
	[SerializeField]
	private int _TotalHealth;
	[SerializeField]
	private float _HealthRestore;
	[SerializeField]
	private float _ClickDamage;
	private float _CurrentHealth;
	private LevelObject _LevelObject;
	[SerializeField]
	private AdditionalLevelObjectElement _HealthBar;

	private Coroutine RestoreHealthCoroutine;

	public void OnEnable() {
		_CurrentHealth = _TotalHealth;
		_LevelObject = gameObject.GetComponent<LevelObject>();
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectStateChanged, OnStateChanged);
		UpdateHealthbar();
		if (_HealthBar != null) {
			_HealthBar.Init(transform);
		}
		if (_ClickDamage > float.Epsilon) {
			GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectMousePressed, OnClick);
		}
	}

	public void OnDisable() {
		if (RestoreHealthCoroutine != null) {
			StopCoroutine(RestoreHealthCoroutine);
			RestoreHealthCoroutine = null;
		}
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectMousePressed, OnClick);
	}

	public void OnDestroy() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectStateChanged, OnStateChanged);
	}

	public void Damage(float damageValue) {
		if (_CurrentHealth > float.Epsilon) {
			_CurrentHealth -= damageValue;
			if (_CurrentHealth <= float.Epsilon) {
				_CurrentHealth = 0.0f;
				_LevelObject.CurrentStateName = _DamagedState.GetStateName();
			}
			if (RestoreHealthCoroutine == null && _HealthRestore > float.Epsilon) {
				RestoreHealthCoroutine = StartCoroutine(HealthRestoration());
			}
			UpdateHealthbar();
		}
	}

	public IEnumerator HealthRestoration() {
		while (true) {
			yield return new WaitForSeconds(1.0f);
			if (_CurrentHealth < _TotalHealth) {
				_CurrentHealth += _HealthRestore;
				UpdateHealthbar();
				if (_CurrentHealth >= _TotalHealth) {
					_CurrentHealth = _TotalHealth;
					StopCoroutine(RestoreHealthCoroutine);
					RestoreHealthCoroutine = null;
				}
			}
		}
	}

	public float GetCurrentHealth() {
		return _CurrentHealth;
	}

	private void OnClick(GameEvent gameEvent) {
		if ((gameEvent.Target as LevelObject) == _LevelObject) {
			Damage(_ClickDamage);
		}
	}

	private void OnStateChanged(GameEvent gameEvent) {
		if (gameEvent.Type == GameEventType.LevelObjectStateChanged && (gameEvent.Target as LevelObject) == _LevelObject) {
			UpdateHealthbar();
		}
	}

	private void UpdateHealthbar() {
		if (_HealthBar != null) {
			if (_CurrentHealth >= _TotalHealth || _LevelObject.CurrentStateName == _DamagedState.GetStateName()) {
				_HealthBar.HideElementObject();
			} else {
				_HealthBar.ShowElementObject();
				if (_HealthBar.GetComponent<ProgressBar>() != null) {
					_HealthBar.GetComponent<ProgressBar>().Progress = _CurrentHealth / _TotalHealth;
				}
			}
		}
	}
}