using System.Collections.Generic;
using UnityEngine;

public class LevelObjectRandomAnimator : MonoBehaviour, IPassableBetweenObjects{
	[SerializeField]
	private List<RuntimeAnimatorController> PossibleAnimators;
	private int _SelectedAnimatorIndex = -1;

	public int SelectedAnimatorIndex {
		set {
			_SelectedAnimatorIndex = value;
			gameObject.GetComponent<Animator>().runtimeAnimatorController = PossibleAnimators[_SelectedAnimatorIndex];
		}
	}

	public void OnEnable() {
		if (_SelectedAnimatorIndex == -1) {
			_SelectedAnimatorIndex = Random.Range(0, PossibleAnimators.Count);
		}
		gameObject.GetComponent<Animator>().runtimeAnimatorController = PossibleAnimators[_SelectedAnimatorIndex];
	}

	public void OnDisable() {
		_SelectedAnimatorIndex = -1;
	}

	public void PassToSuccessor(MonoBehaviour successor) {
		successor.GetComponent<LevelObjectRandomAnimator>().SelectedAnimatorIndex = _SelectedAnimatorIndex;
	}
}