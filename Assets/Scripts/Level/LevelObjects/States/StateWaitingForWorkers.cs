﻿using System.Collections.Generic;
using UnityEngine;

public class StateWaitingForWorkers : LevelObjectState {
	protected TaskInQueue CurrentTask;

	override public void OnEnable() {
		base.OnEnable();
		_LevelObject.ShowTaskMark(true);
	}

	override public void OnDisable() {
		base.OnDisable();
		_LevelObject.ShowTaskMark(false);
	}

	public virtual void SetWorkersTask(TaskInQueue taskInQueue) {
		CurrentTask = taskInQueue;
	}

	public virtual void OnWorkerRunningComplete(TaskWorker worker) {
		GotoNextState();
	}

	override protected void PrepareForDestroyBeforeNextPrefab(LevelObject nextLevelObject) {
		base.PrepareForDestroyBeforeNextPrefab(nextLevelObject);
		CurrentTask.LevelObject = nextLevelObject;
		nextLevelObject.InheritWorkingPoints(_LevelObject);
	}

	override protected void PrepareNextPrefab(LevelObject nextLevelObject) {
		if (nextLevelObject.CurrentState is StateWaitingForWorkers) {
			(nextLevelObject.CurrentState as StateWaitingForWorkers).SetWorkersTask(CurrentTask);
		}
	}

	override public void OnCollidersMouseDown() {
		base.OnCollidersMouseDown();
		Level.Instance.TaskManager.CancelTask(CurrentTask);
	}
}
