using System.Collections.Generic;
using UnityEngine;

public class StateWithTransitionBasedOnCounter : LevelObjectState {
	[SerializeField]
	private List<int> Counters;
	private int CurrentCounter = 0;

	override public void OnEnable() {
		if (Counters.Count > 0) {
			if (CurrentCounter >= Counters[0]) {
				Counters.RemoveAt(0);
				_NextStateDescriptions.RemoveAt(0);
				CurrentCounter = 0;
			} else {
				CurrentCounter++;
			}
		}
		GotoNextState();
	}

	override public ExtendedStateDescription GetNextState(int index = -1) {
		if (_NextStateDescriptions.Count > 0) {
			return _NextStateDescriptions[0];
		}
		return null;
	}
}