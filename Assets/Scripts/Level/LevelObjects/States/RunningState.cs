using System.Collections;
using UnityEngine;

public class RunningState : WorkerRunningState {
	[SerializeField]
	private Waypoint TargetPosition;

	override public void OnEnable() {
		base.OnEnable();
		StartCoroutine(PerformStartRunning());
	}

	protected virtual void StartRunning() {
		OnRunningCompleteHandler += OnArrival;
		if (TargetPosition != null) {
			RunTo(TargetPosition.GetParameters());
		}
	}

	private IEnumerator PerformStartRunning() {
		yield return new WaitForEndOfFrame();
		StartRunning();
	}

	private void OnArrival() {
		TargetPosition = null;
		GotoNextState();
	}
}