﻿using UnityEngine;

public class StateWaitingDropsGathering : StateWithDrops {

	override public void OnEnable() {
		base.OnEnable();
		Drops.OnAllDropsGathered += AllDropsGathered;
	}

	override public void OnDisable() {
		base.OnEnable();
		Drops.OnAllDropsGathered -= AllDropsGathered;
	}

	public void AllDropsGathered() {
		if (this.enabled) {
			GotoNextState();
		}
	}
}
