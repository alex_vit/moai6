using UnityEngine;

public class StateReleasingObject : LevelObjectState {
	override public void OnEnable() {
		GotoNextState();
		PrefabsCache.Instance.FreeCachedInstance(gameObject);
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectReleased, _LevelObject));
	}
}