using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPointRunningState : RunningState {

	override protected void StartRunning() {
		base.StartRunning();
		if (!_LevelObject.IsPathBlocked()) {
			while (!RunToRandomPoint()) {
				RunToRandomPoint();
			}
		} else {
			GotoNextState();
		}
	}
}