﻿using System.Collections.Generic;
using System.Net.Configuration;
using UnityEngine;

public class WorkerRunningState : LevelObjectState {

	public delegate void RunningCompleteDelegate();

	public event RunningCompleteDelegate OnRunningCompleteHandler;

	[SerializeField]
	private float BaseSpeed;
	private List<WaypointParameters> Way;
	private float SpeedModifier = 1.0f;
	private int CurrentAnimationIndex;

	override public void Awake() {
		base.Awake();
		if (_StateName == null) {
			this.enabled = false;
		}
	}

	override public void OnDisable() {
		base.OnDisable();
		StopRunning();
	}

	public void SetStateName(string newStateName) {
		_StateName = newStateName;
	}

	public bool RunToRandomPoint() {
		Way = Level.Instance.Road.GetRandomPath(_LevelObject.GetWorkingPointInWorldSpace(0), _LevelObject);
		if (Way != null && Way.Count > 1) {
			SpeedModifier = Way[0].GetSpeedModifier();
			return true;
		}
		return false;
	}

	public void RunTo(WaypointParameters targetWaypoint, bool ignoreObstacles = false) {
		Way = Level.Instance.Road.GetPath(_LevelObject.GetWorkingPointInWorldSpace(0), targetWaypoint, false,null,ignoreObstacles);
		if (Way != null && Way.Count > 0) {
			SpeedModifier = Way[0].GetSpeedModifier();
		}
	}

	public void RunTo(LevelObject levelObject, string workerState) {
		RunTo(levelObject.GetNearestWorkingPointForOccupying(LevelObject, workerState));
	}

	protected virtual void WayComplete() {
		if (OnRunningCompleteHandler != null) {
			OnRunningCompleteHandler ();
		}
		this.StopRunning();
	}

	private void StopRunning() {
		Way = null;
		if (OnRunningCompleteHandler != null) {
			foreach(RunningCompleteDelegate runningCompleteDelegate in OnRunningCompleteHandler.GetInvocationList())
			{
				OnRunningCompleteHandler -= runningCompleteDelegate;
			}
		}
		_LevelObject.UpdatePathBlocked();
	}
	
	public virtual void Update() {
		if (Way != null && Way.Count > 0) {
			if (CurrentAnimationIndex != Way[0].MultipleAnimationsIndex) {
				CurrentAnimationIndex = Way[0].MultipleAnimationsIndex;
				LevelObject.LevelObjectAnimator.SetInteger(MultipleAnimationWithDirection.ANIMATION_PARAMETER_ANIMATION_INDEX, CurrentAnimationIndex);
			}
			float distance = Vector3.Distance (transform.position, Way[0].Position);
			if (distance < 0.1f) {
				SpeedModifier = Way[0].GetSpeedModifier();
				CurrentAnimationIndex = Way[0].MultipleAnimationsIndex;
				if (Way.Count == 1 && Way[0] is WorkingPoint) {
					AnimationWithDirection.SetAnimatorAngle(LevelObject.LevelObjectAnimator, (Way[0] as WorkingPoint).WorkerDirection);
				}
				Way.RemoveAt(0);
				if (Way.Count == 0) {
					WayComplete();
					return;
				}
			}
			float step = BaseSpeed * Time.deltaTime * (SpeedModifier == Way[0].GetSpeedModifier() ? SpeedModifier : 1.0f);
			List<SpeedUpWorkersBonus> speedUpWorkersBonuses = Level.Instance.GetActiveBonuses<SpeedUpWorkersBonus>(LevelObject);
			foreach (SpeedUpWorkersBonus speedUpWorkersBonus in speedUpWorkersBonuses) {
				step *= speedUpWorkersBonus.SpeedModifier;
			}
			AnimationWithDirection.SetAnimatorAngle(LevelObject.LevelObjectAnimator, transform.position, Way[0].Position);
			Vector3 newPosition = Vector3.MoveTowards (transform.position, Way[0].Position, step);
			transform.position = newPosition;
		}
	}
}
