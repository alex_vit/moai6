using System.Collections;
using UnityEngine;

public class StateChangingAfterAnimation : LevelObjectState {

	private Coroutine AnimationCoroutine;

	override public void OnEnable() {
		base.OnEnable();
		AnimationCoroutine = StartCoroutine(WaitForAnimation());
	}

	override public void OnDisable() {
		base.OnDisable();
		StopCoroutine(AnimationCoroutine);
	}

	private IEnumerator WaitForAnimation() {
		yield return new WaitForEndOfFrame();
		while (true) {
			yield return new WaitForEndOfFrame();
			if (_LevelObject.LevelObjectAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1) {
				GotoNextState();
			}
		}
	}
}