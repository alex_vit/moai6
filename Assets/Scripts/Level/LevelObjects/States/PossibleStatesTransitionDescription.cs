using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PossibleStatesTransitionDescription : PossibleStatesDescription {
	[SerializeField]
	private string _TargetState;
	[SerializeField]
	private float _MinimumNotAffectedCount;

	public string TargetState {get {return _TargetState;}}
	public float MinimumNotAffectedCount {get {return _MinimumNotAffectedCount;}}

	public PossibleStatesTransitionDescription(PossibleStatesTransitionDescription original, LevelObject targetLevelObject) {
		_TargetState = original.TargetState;
		_MinimumNotAffectedCount = original.MinimumNotAffectedCount;
		_LevelObject = targetLevelObject;
		PossibleStates = new List<string>(original.PossibleStates);
	}

	public void CheckForCache() {
		List<LevelObjectState> states = new List<LevelObjectState>(LevelObject.GetComponents<LevelObjectState>());
		foreach (LevelObjectState state in states) {
			if (state.StateName == TargetState) {
				ExtendedStateDescription targetStateDescription = state.GetNextState();
				if (targetStateDescription != null && !targetStateDescription.IsOwnState()) {
					PrefabsCache.Instance.CheckPrefabForCache(targetStateDescription.GetLevelObject(), PrefabsCachePriority.Slowly);
				}
				break;
			}
		}
	}
}