﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LevelObjectTimer))]
public class StateWithWorkers : StateWaitingForWorkers {
	[SerializeField]
	private float TimeToComplete;
	[SerializeField]
	private bool ShowDust;

	private LevelObjectTimer Timer;
	private int WorkersArrived;

	override public void SetWorkersTask(TaskInQueue taskInQueue) {
		base.SetWorkersTask(taskInQueue);
		RefreshTimer();
	}

	override public void OnWorkerRunningComplete(TaskWorker worker) {
		WorkersArrived++;
		worker.LevelObject.CurrentStateName = CurrentTask.GetWorkerState(worker);
		RefreshTimer();
	}

	override public void Awake() {
		base.Awake();
		Timer = this.GetComponent<LevelObjectTimer> ();
	}

	override public void OnEnable() {
		base.OnEnable();
		WorkersArrived = 1;
		_LevelObject.ShowTaskMark(false);
		foreach (FadeEffect fadeEffect in _LevelObject.FadeEffects) {
			fadeEffect.enabled = true;
		}
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusActivated, OnBonus);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusDeactivated, OnBonus);
	}

	override public void OnDisable() {
		base.OnDisable();
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusActivated, OnBonus);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusDeactivated, OnBonus);
	}

	override public void OnCollidersMouseDown() {
	}

	private void OnBonus(GameEvent gameEvent) {
		if (gameEvent.Target is SpeedUpWorkersBonus) {
			RefreshTimer();
		}
	}

	private void RefreshTimer() {
		float totalSpeed = 0.0f;
		int totalWorkers = 0;
		foreach (TaskInQueueWorker taskWorker in CurrentTask.Workers) {
			if (taskWorker.Worker != null) {
				float taskWorkerSpeedModifier = 1.0f;
				List<SpeedUpWorkersBonus> speedUpWorkersBonuses = Level.Instance.GetActiveBonuses<SpeedUpWorkersBonus>(taskWorker.Worker.LevelObject);
				foreach (SpeedUpWorkersBonus speedUpWorkersBonus in speedUpWorkersBonuses) {
					taskWorkerSpeedModifier *= speedUpWorkersBonus.SpeedModifier;
				}
				totalSpeed += taskWorkerSpeedModifier;
				totalWorkers++;
			}
		}
		float speedModifier = totalSpeed / totalWorkers;
		float totalTimeToComplete = TimeToComplete;
		List<StateWithWorkersTimeBonus> stateBonuses = Level.Instance.GetActiveBonuses<StateWithWorkersTimeBonus>(LevelObject);
		foreach (StateWithWorkersTimeBonus stateBonuse in stateBonuses) {
			totalTimeToComplete *= stateBonuse.TimeModifier;
		}
		float timeToCompleteFromWorkersCount = totalTimeToComplete * (1 + (CurrentTask.Workers.Count + CurrentTask.GetRemainWorkersCount() - WorkersArrived)) / speedModifier;
		timeToCompleteFromWorkersCount = timeToCompleteFromWorkersCount - timeToCompleteFromWorkersCount * Timer.PercentPassed;
		LevelObject.LevelObjectAnimator.SetFloat(AnimationForProcess.ANIMATION_PARAMETER_PROCESS_TIME, timeToCompleteFromWorkersCount);
		Timer.StartTimer (timeToCompleteFromWorkersCount, Timer.PercentPassed, OnTimerComplete);
		if (ShowDust) {
			_LevelObject.ShowDust(timeToCompleteFromWorkersCount);
		}
	}

	private void OnTimerComplete() {
		_LevelObject.HideDust();
		foreach (FadeEffect fadeEffect in _LevelObject.FadeEffects) {
			fadeEffect.enabled = false;
		}
		Level.Instance.TaskManager.CompleteTask(CurrentTask);
		GotoNextState();
	}
}
