using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateSeekengEnemy : WorkerRunningState{
	[SerializeField]
	private float LineOfSightRadius;
	[SerializeField]
	private List<PossibleStatesTransitionDescription> PossibleTargets;
	private LevelObject _TargetObject;
	private WorkingPoint _TargetWorkingPoint;
	private Coroutine _SeekingCoroutine;
	private LevelObjectState _NextState;

	override public void Awake() {
		base.Awake();
		_NextState = _LevelObject.GetStateComponentByName(GetNextState().GetStateName());
	}

	override public void OnEnable() {
		base.OnEnable();
		_TargetObject = null;
		_TargetWorkingPoint = null;
		_SeekingCoroutine = StartCoroutine(StartSeeking());
	}

	override public void OnDisable() {
		base.OnDisable();
		StopCoroutine(_SeekingCoroutine);
	}

	override public void Update() {
		if (_TargetObject == null) {
			FindTarget();
		}
		base.Update();
	}

	override protected void WayComplete() {
		base.WayComplete();
		if (_TargetObject != null && Vector3.Distance(transform.position, _TargetWorkingPoint.Position) < Road.NearestPointDistance) {
			List<PossibleStatesTransitionDescription> transitions = new List<PossibleStatesTransitionDescription>();
			foreach (PossibleStatesTransitionDescription target in PossibleTargets) {
				if (target.IsLevelObjectAffected(_TargetObject)) {
					transitions.Add(new PossibleStatesTransitionDescription(target, _TargetObject));
					break;
				}
			}

			if (_NextState is DamagingState) {
				(_NextState as DamagingState).SetTarget(_TargetObject);
			}
			GotoNextState();
		} else {
			_TargetObject = null;
			_TargetWorkingPoint = null;
			_SeekingCoroutine = StartCoroutine(StartSeeking());
		}
	}

	private IEnumerator StartSeeking() {
		yield return new WaitForEndOfFrame();
		if (isActiveAndEnabled && !_LevelObject.IsPathBlocked() && _TargetObject == null) {
			while (!RunToRandomPoint()) {
				RunToRandomPoint();
			}
		}
		foreach (PossibleStatesTransitionDescription stateTransition in PossibleTargets) {
			stateTransition.CheckForCache();
		}
	}

	private void FindTarget() {
		foreach (PossibleStatesTransitionDescription target in PossibleTargets) {
			List<LevelObject> levelObjects = Level.Instance.GetLevelObjectsOfType(target.GetAffectedObjectType());
			List<LevelObject> levelObjectsCopy = new List<LevelObject>(levelObjects);
			foreach (LevelObject levelObject in levelObjectsCopy) {
				if (!target.IsLevelObjectAffected(levelObject)) {
					levelObjects.Remove(levelObject);
				}
			}
			if (levelObjects.Count > target.MinimumNotAffectedCount) {
				foreach (LevelObject levelObject in levelObjects) {
					if (!levelObject.IsPathBlocked()) {
						WorkingPoint levelObjectPoint = levelObject.GetNearestWorkingPoint(LevelObject, GetNextState().GetStateName());
						if (levelObjectPoint != null) {
							float distance = Level.Instance.Road.GetDistance(transform.position, levelObject.GetNearestWorkingPoint(LevelObject, _NextState.StateName));
							if (distance <= LineOfSightRadius) {
								_TargetWorkingPoint = levelObjectPoint;
								_TargetObject = levelObject;
								if (_NextState is DamagingState) {
									RunTo(_TargetObject, _NextState.StateName);
								} else {
									RunTo(levelObjectPoint);
								}
								return;
							}
						}
					}
				}
			}
		}
	}
}