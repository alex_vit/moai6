using UnityEngine;

[RequireComponent (typeof (LevelObjectDrops))]
public class StateWithDrops : LevelObjectState{
	protected LevelObjectDrops Drops;

	override public void Awake() {
		base.Awake();
		Drops = this.GetComponent<LevelObjectDrops> ();
		Drops.enabled = true;
	}

	override protected void PrepareNextPrefab(LevelObject nextLevelObject) {
		base.PrepareNextPrefab(nextLevelObject);
		if (!(nextLevelObject.CurrentState is StateWithDrops)) {
			Drops.enabled = false;
		}
	}
}