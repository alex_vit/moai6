﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent (typeof (LevelObjectTimer))]
public class StateProducingDrops : StateWithDrops {
	public int DropsID;
	[SerializeField]
	private bool DropsAreHidden;
	[SerializeField]
	private DropsType ProducingType;
	[SerializeField]
	private List<DropToProduce> DropsToProduce;
	[SerializeField]
	private float TimeToProduceDrops;

	private LevelObjectTimer Timer;

	public List<DropToProduce> GetDropsToProduce(bool includeHidden = false) {
		if (!includeHidden && DropsAreHidden && Level.Instance.GetActiveBonuses<ShowHiddenDropsBonus>(LevelObject).Count == 0) {
			return new List<DropToProduce>();
		}
		return DropsToProduce;
	}

	public void SetDropsToProduce(List<DropToProduce> newDrops) {
		DropsToProduce = newDrops;
	}

	public DropsType GetProducingType() {
		return ProducingType;
	}

	public void SetProducingType(DropsType newType) {
		ProducingType = newType;
	}

	public float GetTimeToProduceDrops() {
		return TimeToProduceDrops;
	}

	override public void Awake() {
		base.Awake();
		Drops.DropsID = DropsID;
		Timer = this.GetComponent<LevelObjectTimer> ();
		foreach (DropToProduce dropToProduce in DropsToProduce) {
			dropToProduce.SetOwner(LevelObject);
			PrefabsCache.Instance.CheckPrefabForCache(dropToProduce.DropPrefab, PrefabsCachePriority.Slowly);
		}
	}

	override public void OnEnable() {
		base.OnEnable();
		List<DropToProduce> resultDrops = new List<DropToProduce>(){};
		if (ProducingType == DropsType.AllInList) {
			foreach (DropToProduce dropToProduce in DropsToProduce) {
				resultDrops.Add(dropToProduce);
			}
		} else if (ProducingType == DropsType.OneFromList) {
			float maxValue = 0.0f;
			for (int i = 0; i < DropsToProduce.Count; i++) {
				maxValue += 1.0f;
			}
			float randomValue = Random.Range(0.0f, maxValue);
			float currentProbability = 0.0f;
			foreach (DropToProduce dropToProduce in DropsToProduce) {
				if ((randomValue < currentProbability + 1.0f) && randomValue >= currentProbability) {
					resultDrops.Add(dropToProduce);
					break;
				}
				currentProbability += 1.0f;
			}
		} else if (ProducingType == DropsType.Consistently) {
			if (DropsToProduce.Count > 0) {
				DropToProduce dropToProduce = DropsToProduce[0];
				if (dropToProduce.CountType == DropCountType.FiniteAtAll) {
					dropToProduce.TotalCount -= ProfileManager.GetCurrentProfile().GetDropGathered(DropsID, dropToProduce.Name);
				} else {
					dropToProduce.TotalCount -= dropToProduce.GetDropCount();
				}
				if (dropToProduce.TotalCount <= 0) {
					DropsToProduce.RemoveAt(0);
				}
				if (DropsToProduce.Count == 0) {
					_LevelObject.RemoveState(this);
				}
				else {
					resultDrops.Add(DropsToProduce[0]);
				}
			} else {
				_LevelObject.RemoveState(this);
				return;
			}
		}
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusActivated, OnBonus);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusDeactivated, OnBonus);
		Drops.SetDrops(resultDrops);
		
		_LevelObject.LevelObjectAnimator.SetFloat(AnimationForProcess.ANIMATION_PARAMETER_PROCESS_TIME, this.TimeToProduceDrops - this.AnimationLength);
		RefreshTimer();
	}

	private void OnBonus(GameEvent gameEvent) {
		if (gameEvent.Target is DropsSpeedUpBonus) {
			RefreshTimer();
		}
	}

	private void RefreshTimer() {
		float speedModifier = 1.0f;
		List<DropsSpeedUpBonus> speedUpDropsBonuses = Level.Instance.GetActiveBonuses<DropsSpeedUpBonus>(LevelObject);
		foreach (DropsSpeedUpBonus speedUpWorkersBonus in speedUpDropsBonuses) {
			speedModifier *= speedUpWorkersBonus.SpeedModifier;
		}
		_LevelObject.LevelObjectAnimator.speed = speedModifier;
		Timer.StartTimer (TimeToProduceDrops / speedModifier, Timer.PercentPassed, OnTimerComplete);
	}

	override public void OnDisable() {
		base.OnDisable();
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusActivated, OnBonus);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusDeactivated, OnBonus);
		Timer.StopTimer();
	}

	public void OnTimerComplete() {
		StartCoroutine(GenerateDrops());
	}

	private IEnumerator GenerateDrops() {
		for (int i = 0; i < 5; i ++) {
			yield return new WaitForEndOfFrame();
		}
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectDropsProduced, this));
		Drops.GenerateDrops();
		GotoNextState();
	}
}