﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LevelObject))]
[RequireComponent (typeof (Animator))]
public class LevelObjectState : MonoBehaviour {

	[SerializeField]
	protected float AnimationLength;

#if UNITY_EDITOR
	[HideInInspector]
	public bool Expanded;
#endif
	
	[SerializeField]
	protected bool _interactiveWhenBlocked;

	public bool InteractiveWhenBlocked {
		get { return _interactiveWhenBlocked; }
	}

	[HideInInspector]
	[SerializeField]
	protected string _StateName;
	protected LevelObject _LevelObject;
	[SerializeField]
	protected List<ExtendedStateDescription> _NextStateDescriptions;

	[SerializeField]
	private bool SingleTask;
	[SerializeField]
	private List<Task> _Tasks;

	[SerializeField]
	private bool ShowFlag;

	public string StateName {
		get {
			return _StateName;
		}
	}

	public LevelObject LevelObject {
		get {
			return _LevelObject;
		}
	}

	public List<Task> Tasks {
		get {
			return _Tasks;
		}
	}

	public bool IsSingleTask() {
		return SingleTask;
	}

	public virtual void Awake() {
		_LevelObject = this.gameObject.GetComponent<LevelObject>();
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectStateRemoved, OnStateRemoved);
		if (_NextStateDescriptions.Count > 0) {
			List<ExtendedStateDescription> nextStatesCopy = new List<ExtendedStateDescription>(_NextStateDescriptions);
			foreach (ExtendedStateDescription nextState in nextStatesCopy) {
				if (!nextState.IsOwnState() && !Level.Instance.IsPrefabAllowed(nextState.GetLevelObject())) {
					RemoveNextState(nextState);
				}
			}
		}
		foreach (Task task in Tasks) {
			task.SetOwner(LevelObject);
		}
	}

	public virtual void OnDestroy() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectStateRemoved, OnStateRemoved);
	}

	public virtual void OnEnable() {
		if (Level.Instance) {
			List<RestrictStateBonus> restrictStateBonuses = Level.Instance.GetActiveBonuses<RestrictStateBonus>(LevelObject);
			if (restrictStateBonuses.Count > 0) {
				GotoNextState(1);
			} else {
				if (_LevelObject.LevelObjectAnimator.HasState(0, AnimatorStringsHashesCache.GetStringHash(StateName))) {
					_LevelObject.LevelObjectAnimator.Play(AnimatorStringsHashesCache.GetStringHash(StateName));
				} else {
					AnimationClip[] clips = _LevelObject.LevelObjectAnimator.runtimeAnimatorController.animationClips;
					foreach (AnimationClip clip in clips) {
						if (clip.name.Contains(StateName)) {
							_LevelObject.LevelObjectAnimator.Play(AnimatorStringsHashesCache.GetStringHash(clip.name));
							break;
						}
					}
				}
				UpdateSingleTask();
				_LevelObject.ShowFlag(ShowFlag);
				GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectBlockChanged, PathBlockChanged);
			}
			CheckNextStatePrefabForCache();
		}
	}

	public virtual void OnDisable() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectBlockChanged, PathBlockChanged);
	}

	public virtual void OnCollidersMouseDown() {
		if (enabled) {
			Level.Instance.ContextMenu.HideMenu ();
			if (Tasks.Count > 0) {
				if (SingleTask && Tasks.Count > 0) {
					if (Tasks[0].IsTaskAvailable()) {
						Level.Instance.ContextMenu.HideTooltip();
						Level.Instance.TaskManager.AddTask(LevelObject, Tasks[0]);
					}
				} else {
					Level.Instance.ContextMenu.ShowMenuForState(this);
				}
			}
		}
	}

	public virtual void GotoNextState(int index = -1)
	{
		if (!isActiveAndEnabled) return;
		ExtendedStateDescription nextState = GetNextState(index);
		if (nextState != null) {
			LevelObject nextLevelObject = null;
			if (!nextState.IsOwnState()) {
				GameObject nextObject = PrefabsCache.Instance.GetCachedInstanceForPrefab(nextState.GetLevelObject(), transform);
				nextLevelObject = nextObject.GetComponent<LevelObject>();
				PrepareForDestroyBeforeNextPrefab(nextLevelObject);
				_LevelObject.CurrentStateName = "";
				PrefabsCache.Instance.FreeCachedInstance(gameObject);
			} else {
				nextLevelObject = LevelObject;
			}
			nextLevelObject.CurrentStateName = nextState.GetStateName();
			PrepareNextPrefab(nextLevelObject);
		}
	}

	public virtual ExtendedStateDescription GetNextState(int index = -1) {
		if (_NextStateDescriptions.Count > 0) {
			if (index < 0 || _NextStateDescriptions.Count <= index) {
				return _NextStateDescriptions[Random.Range(0, _NextStateDescriptions.Count)];
			}
			return _NextStateDescriptions[index];
		}
		return null;
	}

	protected virtual void PrepareForDestroyBeforeNextPrefab(LevelObject nextLevelObject) {
		List<IPassableBetweenObjects> passableComponents = new List<IPassableBetweenObjects>(GetComponents<IPassableBetweenObjects>());
		foreach (IPassableBetweenObjects passableComponent in passableComponents) {
			MonoBehaviour successorComponent = nextLevelObject.GetComponent(passableComponent.GetType()) as MonoBehaviour;
			passableComponent.PassToSuccessor(successorComponent);
		}
	}

	protected virtual void PrepareNextPrefab(LevelObject nextLevelObject) {}

	private void CheckNextStatePrefabForCache() {
		foreach (Task task in Tasks) {
			BaseStateDescription targetState = task.TargetStateDescription;
			string targetStateName = targetState.GetStateName();
			LevelObjectState levelObjectState = _LevelObject.GetStateComponentByName(targetStateName);
			if (_LevelObject.HasState(targetStateName)) {
				ExtendedStateDescription targetStateDescription = levelObjectState.GetNextState();
				if (targetStateDescription != null && !targetStateDescription.IsOwnState() && Level.Instance.IsPrefabAllowed(targetStateDescription.GetLevelObject())) {
					PrefabsCache.Instance.CheckPrefabForCache(targetStateDescription.GetLevelObject(), PrefabsCachePriority.Slowly);
				}
			}
		}
	}

	private void OnStateRemoved(GameEvent gameEvent) {
		LevelObjectState levelObjectState = gameEvent.Target as LevelObjectState;
		if (levelObjectState.LevelObject == LevelObject) {
			List<ExtendedStateDescription> nextStatesCopy = new List<ExtendedStateDescription>(_NextStateDescriptions);
			foreach (ExtendedStateDescription nextState in nextStatesCopy) {
				if (nextState.GetStateName() == levelObjectState.StateName) {
					RemoveNextState(nextState);
					break;
				}
			}
			List<Task> tasksCopy = new List<Task>(Tasks);
			foreach (Task task in tasksCopy) {
				if (task.TargetStateDescription.GetStateName() == levelObjectState.StateName) {
					Tasks.Remove(task);
				}
			}
		}
	}

	private void RemoveNextState(ExtendedStateDescription nextState) {
		_NextStateDescriptions.Remove(nextState);
		if (_NextStateDescriptions.Count == 0) {
			LevelObject.RemoveState(this);
		}
	}

	private void PathBlockChanged(GameEvent gameEvent) {
		if ((gameEvent.Target as LevelObject) == _LevelObject) {
			UpdateSingleTask();
		}
	}

	private void UpdateSingleTask() {
		if (!_LevelObject.IsPathBlocked() || _interactiveWhenBlocked) {
			if (SingleTask && _Tasks.Count > 0) {
				_LevelObject.ShowSingleTask(_Tasks[0]);
			} else {
				_LevelObject.HideSingleTask();
			}
		} else {
			_LevelObject.HideSingleTask();
		}
	}
}
