﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class PointRunningState : RunningState
{
    [SerializeField] 
    private List<Waypoint> _MotionPoints;
    
    private Waypoint _activeWaypoint;
    private int _activePointIndex;
    
    protected override void StartRunning()
    {
        OnRunningCompleteHandler += OnArrival;
        if(_activePointIndex < _MotionPoints.Count)
            RunTo(_MotionPoints[_activePointIndex++].GetParameters(), true);    
    }

    private void OnArrival()
    {
        GotoNextState();
    }
}
