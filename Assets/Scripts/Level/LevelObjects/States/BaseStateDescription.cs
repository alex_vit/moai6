using UnityEngine;

[System.Serializable]
public class BaseStateDescription {
	[SerializeField]
	private string _StateName;

	public string GetStateName() {
		return _StateName;
	}

	public void SetStateName(string value) {
		_StateName = value;
	}
}