using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PossibleStatesDescription {
	[SerializeField]
	protected GameObject _TargetGameObject;
	[SerializeField]
	protected List<string> PossibleStates;

	protected LevelObject _LevelObject;

#if UNITY_EDITOR
	public void SetTargetGameObject(GameObject gameObject) {
		_TargetGameObject = gameObject;
	}
#endif

	public string GetAffectedObjectType() {
		if (LevelObject != null) {
			return LevelObject.LevelObjectType;
		}
		return "";
	}

	public bool IsLevelObjectAffected(LevelObject levelObject, string targetState = null) {
		if (targetState == null) {
			targetState = levelObject.CurrentStateName;
		}
		return (levelObject.LevelObjectType == LevelObject.LevelObjectType && PossibleStates.Contains(targetState));
	}
/*use it for check same objects e.g buildings for same states*/
	public bool IsLevelObjectHasSameState(LevelObject levelObject, string targetState = null)
	{
		if (targetState == null)
			targetState = levelObject.CurrentStateName;
		return PossibleStates.Contains(targetState);
	}

	public LevelObject LevelObject
	{
		get {
			if (_LevelObject == null && _TargetGameObject != null) {
				_LevelObject = _TargetGameObject.GetComponent<LevelObject>();
			}
			return _LevelObject;
		}
	}
}