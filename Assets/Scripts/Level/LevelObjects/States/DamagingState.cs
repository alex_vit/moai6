using System.Collections;
using UnityEngine;

public class DamagingState : LevelObjectState {
	[SerializeField]
	private float _DamageValue;
	private LevelObject _Target;
	private LevelObjectHealth _TargetHealth;
	private Coroutine _DamagingCoroutine;

	public void SetTarget(LevelObject target) {
		_Target = target;
		CheckTarget();
	}

	override public void OnEnable() {
		base.OnEnable();
		CheckTarget();
	}

	override public void OnDisable() {
		base.OnDisable();
		if (_DamagingCoroutine != null) {
			StopCoroutine(_DamagingCoroutine);
		}
	}

	private void CheckTarget() {
		if (isActiveAndEnabled && _Target != null) {
			_TargetHealth = _Target.GetComponent<LevelObjectHealth>();
			_DamagingCoroutine = StartCoroutine(Damage());
		}
	}

	private IEnumerator Damage() {
		yield return new WaitForEndOfFrame();
		while (_TargetHealth.GetCurrentHealth() > float.Epsilon) {
			yield return new WaitForSeconds(0.5f);
			_TargetHealth.Damage(_DamageValue);
			yield return new WaitForSeconds(0.5f);
		}
		_Target.FreeWorkingPoints();
		GotoNextState();
	}
}