using UnityEngine;

[System.Serializable]
public class ExtendedStateDescription : BaseStateDescription {
	[SerializeField]
	private bool _OwnState;
	[SerializeField]
	private GameObject _LevelObject;

	public bool IsOwnState() {
		return _OwnState;
	}

	public GameObject GetLevelObject() {
		return _LevelObject;
	}
}