using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AdditionalLevelObjectElement {
	private static Dictionary<int, MonoBehaviour> ComponentsCache = new Dictionary<int, MonoBehaviour>();

	[SerializeField]
	private GameObject _ElementPrefab;
	[SerializeField]
	private Vector2 _ElementPosition;
	private Transform _LevelObjectTransform;
	private GameObject _ElementPrefabInstance;

	public void Init(Transform parentTransform) {
		if (_ElementPrefab != null && Level.Instance != null) {
			_LevelObjectTransform = parentTransform;
			PrefabsCache.Instance.CheckPrefabForCache(_ElementPrefab, PrefabsCachePriority.ASAP);
		}
	}

	public void ShowElementObject() {
		if (Level.Instance && _ElementPrefab != null && _ElementPrefabInstance == null) {
			_ElementPrefabInstance = PrefabsCache.Instance.GetCachedInstanceForPrefab(_ElementPrefab);
			_ElementPrefabInstance.transform.SetParent(_LevelObjectTransform, false);
			_ElementPrefabInstance.transform.localPosition = new Vector3(_ElementPosition.x, _ElementPosition.y, 0.0f);
		}
	}

	public void HideElementObject() {
		if (_ElementPrefabInstance != null) {
			PrefabsCache.Instance.FreeCachedInstance(_ElementPrefabInstance);
			_ElementPrefabInstance = null;
		}
	}

	public T GetComponent<T>() where T : UnityEngine.Component {
		if (_ElementPrefabInstance != null) {
			int instanceID = _ElementPrefabInstance.GetInstanceID();
			if (!ComponentsCache.ContainsKey(instanceID)) {
				ComponentsCache.Add(instanceID, _ElementPrefabInstance.GetComponent<T>() as MonoBehaviour);
			}
			return ComponentsCache[instanceID] as T;
		}
		return null;
	}
}