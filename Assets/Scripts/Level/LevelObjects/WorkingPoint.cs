using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorkingPoint : WaypointParameters {
	public AnimationDirections WorkerDirection;
	[System.NonSerialized]
	public bool Occupied;
	[System.NonSerialized]
	public bool Blocked;

	public WorkingPoint() {
		SpeedModifier = 1.0f;
	}

	public WorkingPoint(WorkingPoint workingPoint) {
		Position = workingPoint.Position;
		SpeedModifier = 1.0f;
		Occupied = workingPoint.Occupied;
		Blocked = workingPoint.Blocked;
		WorkerDirection = workingPoint.WorkerDirection;
		RestrictToSpecificObjectsStates = new List<PossibleStatesDescription>(workingPoint.RestrictToSpecificObjectsStates);
	}

	public WorkingPoint(Vector3 position, float speedModifier, bool occupied, bool blocked) {
		Position = position;
		SpeedModifier = 1.0f;
		Occupied = occupied;
		Blocked = blocked;
	}
}