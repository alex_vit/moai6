﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LevelObject))]
public class LevelObjectUnits : MonoBehaviour, IPassableBetweenObjects {

	[SerializeField]
	private GameObject UnitPrefab;
	[SerializeField]
	private int UnitCount;
	private List<GameObject> _Units;
	private LevelObject _LevelObject;

	private List<GameObject> Units {
		get {
			if (_Units == null) {
				_Units = new List<GameObject>();
			}
			return _Units;
		}
	}

	public void Awake() {
		PrefabsCache.Instance.CheckPrefabForCache(UnitPrefab, PrefabsCachePriority.Slowly);
	}

	public void OnEnable() {
		if (transform.parent == null || transform.parent.GetComponent<LevelObjectsContainer>() == null) {
			return;
		}
		int newUnitsCount = UnitCount - Units.Count;
		for (int i = 0; i < newUnitsCount; i++) {
			GameObject unit = PrefabsCache.Instance.GetCachedInstanceForPrefab(UnitPrefab);
			unit.transform.parent = this.transform.parent;
			unit.transform.position = GetLevelObject().GetWorkingPointInWorldSpace(0).Position;
			unit.GetComponent<LevelObject>().CurrentStateType = typeof(RandomPointRunningState);
		}
	}

	public LevelObject GetLevelObject() {
		if (_LevelObject == null) {
			_LevelObject = GetComponent<LevelObject>();
		}
		return _LevelObject;
	}

	public IResource GetUnitsResource() {
		return UnitPrefab.GetComponent<IResource>();
	}

	public int GetUnitsCount() {
		return UnitCount;
	}

	public int GetFreeSlotsCount() {
		return UnitCount - Units.Count;
	}

	public void PassToSuccessor(MonoBehaviour successor) {
		if (successor is LevelObjectUnits) {
			(successor as LevelObjectUnits).PassUnits(Units);	
		}
		Units.Clear();
	}

	public void PassUnits(List<GameObject> oldUnits) {
		Units.AddRange(oldUnits);
	}

	public void AddUnit(GameObject unit) {
		Units.Add(unit);
	}

	public void RemoveUnit(GameObject unit) {
		Units.Remove(unit);
	}

	public void OnDestroy() {
		for (int i = 0; i < Units.Count; i++) {
			PrefabsCache.Instance.FreeCachedInstance(Units[i]);
		}
	}
}
