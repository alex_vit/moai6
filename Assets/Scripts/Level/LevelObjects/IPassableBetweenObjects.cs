using UnityEngine;

public interface IPassableBetweenObjects {
	void PassToSuccessor(MonoBehaviour successor);
}