﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (LevelObject))]
public class LevelObjectDrops : MonoBehaviour {

	private const int DROP_COUNT_LIMIT = 50;

	public delegate void AllDropsGatheredDelegate();
	public event AllDropsGatheredDelegate OnAllDropsGathered;
	public int DropsID;

	private LevelObject LevelObject;
	private List<DropToProduce> DropsToProduce;
	private List<GameObject> DropsGameObjects;
	private int CurrentDropIndex;

	public void Awake() {
		LevelObject = this.gameObject.GetComponent<LevelObject>();
		DropsGameObjects = new List<GameObject>();
		DropsToProduce = new List<DropToProduce>();
	}

	public void GenerateDrops() {
		CurrentDropIndex = 1;
		foreach (DropToProduce dropToProduce in DropsToProduce){
			int dropCount = dropToProduce.GetDropCount();
			while (dropCount > 0) {
				if (dropCount <= DROP_COUNT_LIMIT) {
					AddDrops(dropToProduce.DropPrefab, dropCount);
				} else {
					AddDrops(dropToProduce.DropPrefab, DROP_COUNT_LIMIT);
				}
				dropCount -= DROP_COUNT_LIMIT;
			}
		}
	}

	public List<DropToProduce> GetDrops() {
		return new List<DropToProduce>(DropsToProduce);
	}

	public void SetDrops(List<DropToProduce> resultDrops) {
		DropsToProduce = resultDrops;
	}

	private void AddDrops(GameObject dropPrefab, int dropCount) {
		GameObject drop = PrefabsCache.Instance.GetCachedInstanceForPrefab(dropPrefab);
		drop.transform.SetParent(transform.parent, false);
		drop.transform.position = transform.position;
		Drop dropComponent = drop.GetComponentInChildren<Drop>();
		if (dropComponent) {
			dropComponent.enabled = true;
			dropComponent.DropCount = dropCount;
			dropComponent.SetIndexInRow(CurrentDropIndex);
			dropComponent.OnDropGathered += DropGathered;
		}
		DropsGameObjects.Add(drop);
		CurrentDropIndex++;
	}

	private void DropGathered(GameObject dropGameObject) {
		Drop dropComponent = dropGameObject.GetComponentInChildren<Drop>();
		if (dropComponent) {
			Level.Instance.AddDropGathered(DropsID, dropGameObject.name, dropComponent.DropCount);
			dropComponent.OnDropGathered -= DropGathered;
		}
		DropsGameObjects.Remove(dropGameObject);
		if (DropsGameObjects.Count == 0 && OnAllDropsGathered != null) {
			OnAllDropsGathered();
		}
	}
}
