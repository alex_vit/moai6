﻿using System.Collections;
using UnityEngine;

[RequireComponent (typeof (LevelObject))]
public class LevelObjectTimer : MonoBehaviour {
	public delegate void TimerForStateDelegate();
	[SerializeField]
	private AdditionalLevelObjectElement _ProgressBar;
	private float _TimePassed;
	private float _TimeTotal;
	private IEnumerator Corutine;
	private ProgressBar _ProgressBarComponent;

	public float TimePassed {
		get {
			return _TimePassed;
		}
	}

	public float PercentPassed {
		get {
			if (_TimeTotal == 0.0f) {
				return 0.0f;
			}
			return _TimePassed / _TimeTotal;
		}
	}

	public void Awake() {
		_TimePassed = 0.0f;
		_ProgressBar.Init(transform);
	}

	public void StartTimer(float seconds, float percentPassed, TimerForStateDelegate completeDelegate) {
		this.StopTimer ();
#if DEBUG
		if (Level.Instance.InstantAction) {
			seconds = 0.1f;
		}
#endif
		_TimeTotal = seconds;
		_TimePassed = _TimeTotal * percentPassed;
		Corutine = ProcessTask (seconds, completeDelegate);
		StartCoroutine (Corutine);
		if (seconds > 0.0f) {
			_ProgressBar.ShowElementObject();
			_ProgressBarComponent = _ProgressBar.GetComponent<ProgressBar>();
		}
	}

	public void StopTimer() {
		if (Corutine != null) {
			_TimePassed = 0.0f;
			StopCoroutine (Corutine);
			Corutine = null;
			_ProgressBar.HideElementObject();
		}
	}

	private IEnumerator ProcessTask(float seconds, TimerForStateDelegate completeDelegate) {
		if (seconds > 0.0f) {
			while (_TimePassed < _TimeTotal) {
				yield return new WaitForEndOfFrame();
				_TimePassed += Time.deltaTime;
				if (_ProgressBarComponent != null) {
					_ProgressBarComponent.Progress = _TimePassed / _TimeTotal;
				}
			}
		}
		StopTimer();
		completeDelegate ();
	}
}
