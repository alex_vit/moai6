﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObject : MonoBehaviour, IOnLevelMouseHandler {
	[SerializeField]
	private string _LevelObjectType;
	[SerializeField]
	private int _LevelObjectGrade;
	[SerializeField]
	private BaseStateDescription _CurrentStateDescription;
	[SerializeField]
	private bool UnblocksByAnyPoint;
	[SerializeField]
	private bool _OccupyPointsСonsistently;
	[SerializeField]
	private List<WorkingPoint> _WorkingPoints;
	[SerializeField]
	private string TooltipHeader;
	[SerializeField]
	private GameCursor _Cursor;

	[SerializeField]
	public AdditionalLevelObjectElement _TaskMark;
	[SerializeField]
	public AdditionalLevelObjectElement _Dust;
	[SerializeField]
	public AdditionalLevelObjectElement _SingleTaskIcon;
	[SerializeField]
	public AdditionalLevelObjectElement _PathBlockedIcon;
	[SerializeField]
	public AdditionalLevelObjectElement _Selector;
	[SerializeField]
	public AdditionalLevelObjectElement _Flag;
	[SerializeField]
	public AdditionalLevelObjectElement _UnderwaterEffect;

	[SerializeField]
	public CompositeCollider2D _Collider;

	private List<HighlightEffect> Highlighters;
	private List<LevelObjectState> States;
	private List<CachedCollider> LevelObjectColliders;
	private List<LevelObjectSortablePart> _LevelObjectSortableParts;
	private bool _IsMouseOver;
	private bool _IsMouseDown;
	private Animator _LevelObjectAnimator;
	private List<FadeEffect> _FadeEffects;
	private bool CollidersEnabled;
	private bool PathBlocked;
	private bool _Hidden;
	private Coroutine EnableCoroutine;
	private Coroutine ColliderCoroutine;
	private bool signalShowed;

	#if UNITY_EDITOR
	public List<WorkingPoint> WorkingPoints {get {return _WorkingPoints;} set {_WorkingPoints = value;}}
	public void SetCurrentStateDescription(BaseStateDescription stateDescription) {
		_CurrentStateDescription = stateDescription;
	}
	public void SetCollider(CompositeCollider2D collider) {
		_Collider = collider;
	}
	[SerializeField]
	private bool _AdditionalElementsExpanded;
	#endif

	public bool Hidden {
		get {
			return _Hidden;
		}
		set {
			_Hidden = value;
			CheckPathBlockedIcon();
		}
	}

	public List<FadeEffect> FadeEffects {
		get {
			return _FadeEffects;
		}
	}

	public string LevelObjectType {
		get {
			return _LevelObjectType;
		}
	}

	public int LevelObjectGrade {
		get {
			return _LevelObjectGrade;
		}
	}
		
	public string CurrentStateName
	{
		get {
			return _CurrentStateDescription.GetStateName();
		}
		set {
			_CurrentStateDescription.SetStateName(value);
			if(States == null)
				return;
			for (int i = 0; i < States.Count; i++) {
				LevelObjectState state = States[i];
				state.enabled = false;
			}
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectStateChanged, this));
			for (int i = 0; i < States.Count; i++) {
				LevelObjectState state = States[i];
				if (state.StateName == _CurrentStateDescription.GetStateName()) {
					state.enabled = true;
					break;
				}
			}
			if (isActiveAndEnabled) {
				UpdateSelector();
				ColliderCoroutine = StartCoroutine(RefreshCollider());
			}
		}
	}

	public LevelObjectState CurrentState
	{
		get {
			return GetStateComponentByName(_CurrentStateDescription.GetStateName());
		}
	}

	public System.Type CurrentStateType
	{
		get {
			return CurrentState.GetType();
		}
		set {
			for (int i = 0; i < States.Count; i++) {
				LevelObjectState state = States[i];
				if (state.GetType() == value) {
					CurrentStateName = state.StateName;
					break;
				}
			}
		}
	}

	public Animator LevelObjectAnimator {
		get {
			return _LevelObjectAnimator;
		}
	}

	public List<DropToProduce> GetTaskDrops(Task task) {
		LevelObjectState state = GetStateComponentByName(task.TargetStateDescription.GetStateName());
		while (state != null) {
			if (state is StateProducingDrops) {
				return (state as StateProducingDrops).GetDropsToProduce();
			} else if (state is StateWaitingForWorkers) {
				state =  GetStateComponentByName(state.GetNextState().GetStateName());
			} else {
				state = null;
			}
		}
		return new List<DropToProduce>();
	}

	public void Awake() {
		_FadeEffects = new List<FadeEffect>(gameObject.GetComponentsInChildren<FadeEffect>());
		foreach (FadeEffect fadeEffect in _FadeEffects) {
			fadeEffect.enabled = false;
		}
		_LevelObjectAnimator = gameObject.GetComponent<Animator>();
        States = new List<LevelObjectState> (gameObject.GetComponents<LevelObjectState> ());
		Highlighters = new List<HighlightEffect>(gameObject.GetComponentsInChildren<HighlightEffect> ());
		LevelObjectColliders = new List<CachedCollider>(GetComponentsInChildren<CachedCollider>(true));
		_LevelObjectSortableParts = new List<LevelObjectSortablePart>(GetComponentsInChildren<LevelObjectSortablePart>());
		_IsMouseDown = false;
		_IsMouseOver = false;

		_TaskMark.Init(transform);
		_Dust.Init(transform);
		_SingleTaskIcon.Init(transform);
		_PathBlockedIcon.Init(transform);
		_Selector.Init(transform);
		_Flag.Init(transform);
		_UnderwaterEffect.Init(transform);
	}

	public void OnEnable() {
		if (Level.Instance) {
			GameEventDispatcher.Instance.AddEventListener(GameEventType.PathblockerRemoved, UpdatePathBlocked);
			GameEventDispatcher.Instance.AddEventListener(GameEventType.ResourceValueChanged, UpdateSelector);
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectEnable, this));
			EnableCoroutine = StartCoroutine(PerformEnable());
			if (Level.Instance.Underwater) {
				_UnderwaterEffect.ShowElementObject();
			}
			FreeWorkingPoints();
			ColliderCoroutine = StartCoroutine(RefreshCollider());
		}
	}

	private IEnumerator PerformEnable() {
		yield return new WaitForEndOfFrame();
		UpdatePathBlocked();
		if (CurrentState is StateProducingDrops && !Level.Instance.Started) {
			CurrentState.GotoNextState();
			GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelStarted, OnLevelStarted);
		}
		UpdateSelector();
	}

	public void OnDisable() {
		ShowFlag(false);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.PathblockerRemoved, UpdatePathBlocked);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ResourceValueChanged, UpdateSelector);
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectDisable, this));
		if (Level.Instance != null && Level.Instance.Underwater) {
			_UnderwaterEffect.HideElementObject();
		}
		if (EnableCoroutine != null) {
			StopCoroutine(EnableCoroutine);
		}
		if (ColliderCoroutine != null) {
			StopCoroutine(ColliderCoroutine);
		}
	}

	void OnDestroy()
	{
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelStarted, OnLevelStarted);
	}

	private void OnLevelStarted(GameEvent gameEvent) {
		if (!IsPathBlocked() && CurrentState is StateWaitingDropsGathering) {
			CurrentState.GotoNextState();
		}
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelStarted, OnLevelStarted);
	}

	public void GotoNextStateAfterAnimation() {
		CurrentState.GotoNextState();
	}

	public bool HasState(string stateName) {
		return GetStateComponentByName(stateName) != null;
	}

	public void RemoveState(LevelObjectState levelObjectState) {
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectStateRemoved, levelObjectState));
		States.Remove(levelObjectState);
	}

	public int GetOrder() {
		if (!Hidden && (!IsPathBlocked() || CurrentState.InteractiveWhenBlocked)) {
			if (_LevelObjectSortableParts.Count > 0) {
				return _LevelObjectSortableParts[0].Order;
			}
			return 1;
		}
		return -1;
	}

	public void MouseOver() {
		if (!_IsMouseOver) {
			GameCursorManager.Instance.ShowCursor(_Cursor);
			StartHighlight();
			_IsMouseOver = true;
		} else {
			if (!_IsMouseDown && Input.GetMouseButtonDown(0)) {
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectMousePressed, this));
				CurrentState.OnCollidersMouseDown();
				StopHighlight();
				_IsMouseOver = false;
			}
		}
		_IsMouseDown = Input.GetMouseButtonDown(0);
	}

	public void MouseOut() {
		if (_IsMouseOver) {
			GameCursorManager.Instance.ShowCursor(null);
			StopHighlight();
			_IsMouseOver = false;
		}
	}

	public void ShowSingleTask(Task task) {
		_SingleTaskIcon.ShowElementObject();
		SingleTaskIcon taskIcon = _SingleTaskIcon.GetComponent<SingleTaskIcon>();
		if (taskIcon != null) {
			taskIcon.SetStateTask(this, task);
		}
	}

	public void HideSingleTask() {
		_SingleTaskIcon.HideElementObject();
	}

	public void ShowFlag(bool show) {
		if (show) {
			_Flag.ShowElementObject();
		} else {
			_Flag.HideElementObject();
		}
	}

	public void ShowTaskMark(bool show) {
		if (show) {
			_TaskMark.ShowElementObject();
		} else {
			_TaskMark.HideElementObject();
		}
	}

	public void ShowDust(float timeForDust) {
		_Dust.ShowElementObject();
		if (_Dust.GetComponent<LevelObjectDust>() != null) {
			_Dust.GetComponent<LevelObjectDust>().ShowDust(timeForDust);
		}
	}

	public void HideDust() {
		_Dust.HideElementObject();
	}

	public bool IsPathBlocked() {
		return PathBlocked;
	}

	public void InheritWorkingPoints(LevelObject oldLevelObject) {
		int pointsCount = Mathf.Min(_WorkingPoints.Count, oldLevelObject._WorkingPoints.Count);
		for (int i = 0; i < pointsCount; i++) {
			_WorkingPoints[i].Occupied = oldLevelObject._WorkingPoints[i].Occupied;
		}
	}

	public void FreeWorkingPoints() {
		foreach (WorkingPoint workingPoint in _WorkingPoints) {
			workingPoint.Occupied = false;
		}
	}

	public WorkingPoint GetNearestWorkingPoint(LevelObject forLevelObject, string workerState) {
		return GetWorkingPointInWorldSpace(GetNearestWorkingPointIndex(forLevelObject, workerState));
	}

	public WorkingPoint GetNearestWorkingPointForOccupying(LevelObject forLevelObject, string workerState) {
		int minDistancePointIndex = GetNearestWorkingPointIndex(forLevelObject, workerState);
		if (minDistancePointIndex > -1) {
			_WorkingPoints[minDistancePointIndex].Occupied = true;
		}
		return  GetWorkingPointInWorldSpace(minDistancePointIndex);
	}

	public int GetNearestWorkingPointIndex(LevelObject forLevelObject, string workerState) {
		WaypointParameters waypointParameters = new WaypointParameters(forLevelObject.transform.position, 1.0f);
		float minDistance = float.MaxValue;
		int minDistancePointIndex = -1;
		if (!_OccupyPointsСonsistently) {
			for (int i = 0; i < _WorkingPoints.Count; i++) {
				WorkingPoint workingPoint = GetWorkingPointInWorldSpace(i);
				if (!workingPoint.Occupied && !workingPoint.Blocked) {
					if (Level.Instance.Road.HasPath(waypointParameters, workingPoint, true)) {
						float distance = Level.Instance.Road.GetDistance(waypointParameters, workingPoint);
						if (distance < minDistance) {
							minDistance = distance;
							minDistancePointIndex = i;
						}
					}
				}
			}
		} else {
			for (int i = 0; i < _WorkingPoints.Count; i++) {
				WorkingPoint workingPoint = GetWorkingPointInWorldSpace(i);
				if (!workingPoint.Occupied && !workingPoint.Blocked && workingPoint.IsLevelObjectInRestrictedList(forLevelObject, workerState)) {
					minDistancePointIndex = i;
					break;
				}
			}
			if (minDistancePointIndex < 0) {
				for (int i = 0; i < _WorkingPoints.Count; i++) {
					WorkingPoint workingPoint = GetWorkingPointInWorldSpace(i);
					if (!workingPoint.Occupied && !workingPoint.Blocked) {
						minDistancePointIndex = i;
						break;
					}
				}
			}
		}
		return minDistancePointIndex;
	}

	public WorkingPoint GetWorkingPointInWorldSpace(int index) {
		if (index >= 0 && index < _WorkingPoints.Count) {
			return GetWorkingPointInWorldSpace(_WorkingPoints[index]);
		}
		return null;
	}

	private WorkingPoint GetWorkingPointInWorldSpace(WorkingPoint workingPoint) {
		WorkingPoint result = new WorkingPoint(workingPoint);
		result.Position += transform.position;
		return result;
	}

	private void UpdateSelector(GameEvent gameEvent = null) {
		if(CurrentState == null || CurrentState.Tasks == null)
			return;
		if (CurrentState.Tasks.Count > 0 && CurrentState.Tasks[0].TooltipType == TooltipType.CloudTooltip && (!IsPathBlocked() || CurrentState.InteractiveWhenBlocked)) {
			if (CurrentState.Tasks[0].IsTaskAvailable() && !signalShowed) {
				Level.Instance.ContextMenu.ShowSignalForTask(CurrentState.Tasks[0], this);
				signalShowed = true;	
			} else if (!CurrentState.Tasks[0].IsTaskAvailable()) {
				signalShowed = false;
			}
		}
		
		if (_Selector.GetComponent<Selector>() != null) {
			bool hasAvailableTasks = false;
			if (CurrentState != null && CurrentState.Tasks != null) {
				foreach (Task task in CurrentState.Tasks) {
					if (task.IsTaskAvailable()) {
						hasAvailableTasks = true;
						break;
					}
				}
			}
			_Selector.GetComponent<Selector>().SetEnabled(hasAvailableTasks);
		}
	}

	private void StartHighlight () {
		if(CurrentState == null || CurrentState.Tasks == null)
			return;
		if (CurrentState.Tasks.Count > 0) {
			if (Highlighters.Count > 0) {
				foreach (HighlightEffect effect in Highlighters) {
					effect.StartEffect();
				}
			}
			_Selector.ShowElementObject();
			UpdateSelector();
		}

		if (CurrentState.IsSingleTask()) {
			if (_SingleTaskIcon.GetComponent<SingleTaskIcon>() != null) {
				_SingleTaskIcon.GetComponent<SingleTaskIcon>().Highlight();
			}
			if (CurrentState.Tasks.Count > 0) {
				Level.Instance.ContextMenu.ShowTooltipForTask(CurrentState.Tasks[0], this);
			}
		} else if (TooltipHeader != null && TooltipHeader != "") {
			List<DropToProduce> dropsToProduce = GetProducigDrops();
			LevelObjectUnits levelObjectUnits = GetComponent<LevelObjectUnits>();
			if (dropsToProduce != null) {
				List<IResource> resources = new List<IResource>();
				List<int> count = new List<int>();
				foreach (DropToProduce dropToProduce in dropsToProduce) {
					resources.Add(dropToProduce.DropPrefab.GetComponent<IResource>());
					count.Add(dropToProduce.GetDropCount());
				}
				Level.Instance.ContextMenu.ShowTooltipProduce(TooltipHeader, resources, count);
			} else if (levelObjectUnits != null && levelObjectUnits.isActiveAndEnabled) {
				List<IResource> resources = new List<IResource>(){levelObjectUnits.GetUnitsResource()};
				List<int> count = new List<int>(){levelObjectUnits.GetUnitsCount()};
				Level.Instance.ContextMenu.ShowTooltipProduce(TooltipHeader, resources, count);
			}
		}
	}

	private void StopHighlight () {
		if(CurrentState == null || CurrentState.Tasks == null)
			return;
		if (Highlighters.Count > 0) {
			foreach (HighlightEffect effect in Highlighters) {
				effect.CancelEffect();
			}
		}
		_Selector.HideElementObject();
		if (_SingleTaskIcon.GetComponent<SingleTaskIcon>() != null) {
			_SingleTaskIcon.GetComponent<SingleTaskIcon>().CancelHighlight();
		}
		Level.Instance.ContextMenu.HideTooltip();
	}

	private IEnumerator RefreshCollider() {
		yield return new WaitForEndOfFrame();
		for (int i = 0; i < LevelObjectColliders.Count; i++) {
			LevelObjectColliders[i].UpdatePaths();
		}
	}

	public List<DropToProduce> GetProducigDrops() {
		StateProducingDrops stateProducingDrops = (CurrentState as StateProducingDrops);
		if (stateProducingDrops == null && CurrentState is StateWaitingDropsGathering && CurrentState.GetNextState() != null) {
			stateProducingDrops = GetStateComponentByName(CurrentState.GetNextState().GetStateName()) as StateProducingDrops;
		}
		if (stateProducingDrops != null && stateProducingDrops.GetTimeToProduceDrops() > 0.1f) {
			return stateProducingDrops.GetDropsToProduce();
		}
		return null;
	}

    public void UpdatePathBlocked(GameEvent gameEvent = null) {
		bool wasBlocked = PathBlocked;
		foreach (WorkingPoint workingPoint in _WorkingPoints) {
			WorkingPoint newWorkingPoint = new WorkingPoint(workingPoint);
			newWorkingPoint.Position += transform.position;
			if(Level.Instance != null)
				workingPoint.Blocked = !Level.Instance.Road.HasPathFromStart(newWorkingPoint, UnblocksByAnyPoint);
		}

		PathBlocked = true && UnblocksByAnyPoint;
		foreach (WorkingPoint workingPoint in _WorkingPoints) {
			if (UnblocksByAnyPoint) {
				if (!workingPoint.Blocked) {
					PathBlocked = false;
					break;
				}
			} else {
				if (workingPoint.Blocked) {
					PathBlocked = true;
					break;
				}
			}
		}
		if (!PathBlocked && !UnblocksByAnyPoint && _WorkingPoints.Count > 1) {
			for (int i = 0; i < _WorkingPoints.Count; i++) {
				WorkingPoint newWorkingPoint = new WorkingPoint(_WorkingPoints[i]);
				newWorkingPoint.Position += transform.position;
				if (Level.Instance.Road.HasBlockerNear(newWorkingPoint)) {
					PathBlocked = true;
					break;
				}
			}
		}

		if (wasBlocked != PathBlocked) {
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectBlockChanged, this));
			if ((PathBlocked && (CurrentState as StateProducingDrops) != null) || (!PathBlocked && (CurrentState as StateWaitingDropsGathering) != null)) {
				CurrentState.GotoNextState();
			}
		}
		CheckPathBlockedIcon();
	}

	private void CheckPathBlockedIcon() {
		if (PathBlocked && !Hidden) {
			_PathBlockedIcon.ShowElementObject();
		} else {
			_PathBlockedIcon.HideElementObject();
		}
	}

	public LevelObjectState GetStateComponentByName(string name){
		foreach (LevelObjectState state in States) {
			if (state.StateName == name) {
				return state;
			}
		}
		return null;
	}

	public T GetStateComponentByType<T>() where T : LevelObjectState {
		for (int i = 0; i < States.Count; i++) {
			LevelObjectState state = States[i];
			if (state.GetType() == typeof(T)) {
				return state as T;
			}
		}
		return null;
	}
}
