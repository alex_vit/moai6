using UnityEngine;

public class LevelObjectPathBlocker : MonoBehaviour {
	[SerializeField]
	private Waypoint TargetWaypoint;

	public void OnEnable() {
		if (TargetWaypoint != null) {
			Level.Instance.Road.AddPathblocker(this, TargetWaypoint);
		} else {
			Level.Instance.Road.AddPathblocker(this, new WorkingPoint(transform.position, 1.0f, false, false));
		}
	}

	public void OnDisable() {
		if (Level.Instance != null) {
			Level.Instance.Road.RemovePathblocker(this);
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.PathblockerRemoved, this));
		}
	}
}