using UnityEngine;

public class LevelObjectAudio : MonoBehaviour {

	private AudioSource AudioSource;

	public void PlaySound(AudioData audioData) {
		AudioSource = AudioManager.Instance.PlaySound(audioData);
	}

	public void StopSound() {
		AudioManager.Instance.StopSound(AudioSource);
	}
}