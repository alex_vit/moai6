public interface IOnLevelMouseHandler {
	int GetOrder();
	void MouseOver();
	void MouseOut();
}