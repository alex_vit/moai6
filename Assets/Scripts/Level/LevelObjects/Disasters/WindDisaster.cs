using UnityEngine;

public class WindDisaster : LevelObjectDisaster {
	[SerializeField]
	private AdditionalLevelObjectElement _WindPrefab;

	override public void Awake() {
		base.Awake();
		_WindPrefab.Init(transform);
	}

	override public void OnEnable() {
		base.OnEnable();
		_WindPrefab.ShowElementObject();
	}

	override public void OnDisable() {
		base.OnDisable();
		_WindPrefab.HideElementObject();
	}
}