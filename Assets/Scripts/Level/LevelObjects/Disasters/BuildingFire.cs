using UnityEngine;

public class BuildingFire : MonoBehaviour{
	[SerializeField]
	private AdditionalLevelObjectElement _FirePrefab;

	public void Awake() {
		_FirePrefab.Init(transform);
	}

	public void OnEnable() {
		_FirePrefab.ShowElementObject();
	}

	public void OnDisable() {
		_FirePrefab.HideElementObject();
	}
}