using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObjectDisaster : MonoBehaviour {
	[SerializeField]
	private Color SpritesColor;
	private List<SpriteRenderer> SpriteRenderers;
	private Color CurrentSpritesColor;

	public virtual void Awake() {
		SpriteRenderers = new List<SpriteRenderer>(gameObject.GetComponentsInChildren<SpriteRenderer> ());
	}

	public virtual void OnEnable() {
	}

	public virtual void OnDisable() {
		foreach (SpriteRenderer renderer in SpriteRenderers) {
			if (renderer.sortingLayerName == "LevelObjects" || renderer.sortingLayerName == "Default") {
				renderer.color = Color.white;
			}
		}
	}

	public virtual void Update() {
		if (isActiveAndEnabled && CurrentSpritesColor != SpritesColor) {
			CurrentSpritesColor = SpritesColor;
			foreach (SpriteRenderer renderer in SpriteRenderers) {
				if (renderer.sortingLayerName == "LevelObjects" || renderer.sortingLayerName == "Default") {
					renderer.color = SpritesColor;
				}
			}
		}
	}
}