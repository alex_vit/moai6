using UnityEngine;

public class Selector : MonoBehaviour {
	[SerializeField]
	private SpriteRenderer Image;
	[SerializeField]
	private Color EnabledColor;
	[SerializeField]
	private Color DisabledColor;

	public void SetEnabled(bool enabled) {
		Image.color = enabled ? EnabledColor : DisabledColor;
	}
}