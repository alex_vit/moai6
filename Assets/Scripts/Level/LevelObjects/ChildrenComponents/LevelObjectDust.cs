using System.Collections.Generic;
using UnityEngine;

public class LevelObjectDust : MonoBehaviour{

	private float ProgressTime = 0.0f;
	private float CurrentProgress;

	private List<Animator> Animators;
	private List<SpriteRenderer> Sprites;

	public void Awake() {
		Animators = new List<Animator>(GetComponentsInChildren<Animator>());
		Sprites = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());
	}

	public void OnEnable() {
		foreach (Animator animator in Animators) {
			//TODO: Replace string
			animator.Play("Dust1", 0, Random.Range(0.0f, 1.0f));
		}
	}

	public void Update() {
		if (isActiveAndEnabled) {
			CurrentProgress += Time.deltaTime;
			foreach (SpriteRenderer sprite in Sprites) {
				Color newColor = sprite.color;
				newColor.a = 1.0f - CurrentProgress / ProgressTime;
				sprite.color = newColor;
			}
			if (CurrentProgress >= ProgressTime) {
				gameObject.SetActive(false);
			}
		}
	}

	public void ShowDust(float time) {
		ProgressTime = time;
		CurrentProgress = 0.0f;
		gameObject.SetActive(true);
	}

	public void HideDust() {
		ProgressTime = 0.0f;
		CurrentProgress = 0.0f;
		gameObject.SetActive(false);
	}
}