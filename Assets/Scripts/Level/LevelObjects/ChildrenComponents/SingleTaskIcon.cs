using UnityEngine;

public class SingleTaskIcon : MonoBehaviour{
	[SerializeField]
	private SpriteRenderer Icon;
	[SerializeField]
	private HighlightEffect Effect;

	private Task Task;

	public void SetStateTask(LevelObject levelObject, Task task) {
		Task = task;
		UpdateAvailability();
	}

	public void Highlight() {
		Effect.StartEffect();
	}

	public void CancelHighlight() {
		Effect.CancelEffect();
	}

	public void OnEnable() {
		GameEventDispatcher.Instance.AddEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
	}

	public void OnDisable() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
	}

	private void UpdateAvailability(GameEvent gameEvent = null) {
		bool taskAvailable = Task.IsTaskAvailable();
		Icon.sprite = taskAvailable ? Task.TaskIcon.EnabledIcon : Task.TaskIcon.DisabledIcon;
	}
}