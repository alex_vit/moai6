using System.Collections;
using UnityEngine;

public class FadeEffect : MonoBehaviour {
	private SpriteRenderer OriginalRenderer;
	private SpriteRenderer EffectRenderer;
	private Sprite TargetSprite;
	private float TransitionProgress;
	private Animator Animator;

	public void Awake() {
		Animator = GetComponentInParent<Animator>();
		OriginalRenderer = this.GetComponent<SpriteRenderer>();
		TransitionProgress = 1.0f;
		GameObject tempObject = new GameObject("FadeEffect");
		tempObject.transform.SetParent(transform, false);
		tempObject.transform.position = transform.position;
		tempObject.SetActive(false);
		EffectRenderer = tempObject.AddComponent<SpriteRenderer>();
		EffectRenderer.sortingLayerName = OriginalRenderer.sortingLayerName;
		EffectRenderer.sortingOrder = OriginalRenderer.sortingOrder;
	}

	public void OnEnable() {
		EffectRenderer.gameObject.SetActive(false);
		EffectRenderer.sprite = OriginalRenderer.sprite;
		TargetSprite = OriginalRenderer.sprite;
	}

	public void OnDisable() {
		OriginalRenderer.color = Color.white;
		EffectRenderer.gameObject.SetActive(false);
	}

	public void LateUpdate() {
		bool spriteChanged = false;
		if (TargetSprite != OriginalRenderer.sprite) {
			EffectRenderer.sprite = TargetSprite;
			TargetSprite = OriginalRenderer.sprite;
			EffectRenderer.gameObject.SetActive(true);
			TransitionProgress = 0.0f;
			spriteChanged = true;
		}
		if (TransitionProgress < 1.0f) {
			EffectRenderer.sortingOrder = OriginalRenderer.sortingOrder;
			Color newColor = OriginalRenderer.color;
			newColor.a = TransitionProgress;
			OriginalRenderer.color = newColor;
			newColor = EffectRenderer.color;
			newColor.a = 1.0f - TransitionProgress;
			EffectRenderer.color = newColor;
			if (!spriteChanged) {
				//TODO: Replace magic number
				if (Animator != null) {
					TransitionProgress += Time.deltaTime / 0.017f * Animator.GetCurrentAnimatorStateInfo(0).speedMultiplier;
				} else {
					TransitionProgress += Time.deltaTime / 1.0f;
				}
				if (TransitionProgress >= 1.0f) {
					EffectRenderer.gameObject.SetActive(false);
				}
			}
		}
	}
}