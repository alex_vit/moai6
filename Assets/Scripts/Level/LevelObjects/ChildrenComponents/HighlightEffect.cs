using System.Collections;
using UnityEngine;

public class HighlightEffect : MonoBehaviour {

	private float CurrentHighlightRate;
	private bool IsRunning;
	private Material Material;
	private SpriteRenderer Renderer;
	private Coroutine EffectCoroutine;

	public void Awake() {
		CurrentHighlightRate = 1.0f;
		Renderer = GetComponent<SpriteRenderer>();
		Material = Renderer.material;
	}

	public void OnEnable() {
		EffectCoroutine = StartCoroutine(UpdateEffect());
	}

	public void OnDisable() {
		StopCoroutine(EffectCoroutine);
	}

	public void StartEffect() {
		IsRunning = true;
	}

	public void CancelEffect() {
		IsRunning = false;
	}

	public IEnumerator UpdateEffect() {
		while (true) {
			yield return new WaitForEndOfFrame();
			if (IsRunning && CurrentHighlightRate < 1.25f) {
				CurrentHighlightRate += 0.02f;
				Material.SetFloat("_Highlight", CurrentHighlightRate);
			} else if (!IsRunning && CurrentHighlightRate > 1.0f) {
				CurrentHighlightRate -= 0.02f;
				Material.SetFloat("_Highlight", CurrentHighlightRate);
			}
		}
	}

}