using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelUnitsSpawnPoints : TriggeredBehaviour {
	[SerializeField]
	private LevelObjectsContainer _Container;
	[SerializeField]
	private List<GameObject> _PossibleUnitsPrefabs;
	[SerializeField]
	private float _TimeBetweenSpawn;
	[SerializeField]
	private int _MaximumUnitsCountAtSameTime;
	[SerializeField]
	private int _TotalUnitsCount;
	[SerializeField] 
	private bool IgnoreObstacles = false;
	[SerializeField]
	private List<Waypoint> _SpawnWaypoints;
	
	private Coroutine _SpawnCoroutine;
	private List<GameObject> _SpawnedUnits;
	private int _SpawnedUnitsCount;
	private List<Waypoint> _AvailableSpawnWaypoints;

	private bool isRunning = false;
	
	#region overrided
	public override void OnEnable() {
		base.OnEnable();
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelStarted, OnLevelStarted);
	}

	public override void OnDisable() {
		base.OnDisable();
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelStarted, OnLevelStarted);
		StopSpawnObjects();
	}
	//if spawn point has trigger and it's done, when units are don't spawn we began it
	protected override void TriggersComplete()
	{
		if(Triggers == null || Triggers.Count == 0)
			return;
		if (!isRunning)
		{
			BeginSpawnObjects();
		}
	}
	#endregion

	private void OnLevelStarted(GameEvent gameEvent)
	{
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelStarted, OnLevelStarted);
		if(IsComplete && !isRunning)
			BeginSpawnObjects();
	}

	private void BeginSpawnObjects()
	{
		_AvailableSpawnWaypoints = new List<Waypoint>();
		_SpawnedUnits = new List<GameObject>();
		_SpawnedUnitsCount = 0;
		for (int i = 0; i < _PossibleUnitsPrefabs.Count; i++) {
			PrefabsCache.Instance.CheckPrefabForCache(_PossibleUnitsPrefabs[i], PrefabsCachePriority.Slowly);
		}
		RefreshAvailableSpawnPoints();
		
		_SpawnCoroutine = StartCoroutine(PerformTransition());
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectReleased, OnLevelObjectReleased);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.PathblockerRemoved, RefreshAvailableSpawnPoints);
	}

	private void StopSpawnObjects()
	{
		if (_SpawnCoroutine != null)
		{
			isRunning = false;
			StopCoroutine(_SpawnCoroutine);
		}
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectReleased, OnLevelObjectReleased);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.PathblockerRemoved, RefreshAvailableSpawnPoints);
	}

	private void RefreshAvailableSpawnPoints(GameEvent gameEvent = null) {
		for (int i = 0; i < _SpawnWaypoints.Count; i++) {
			Waypoint spawnWaypoint = _SpawnWaypoints[i];
			if (!_AvailableSpawnWaypoints.Contains(spawnWaypoint) && Level.Instance.Road.HasPathFromStart(spawnWaypoint.GetParameters(), true, IgnoreObstacles)) {
				_AvailableSpawnWaypoints.Add(spawnWaypoint);
			}
		}
	}

	private void OnLevelObjectReleased(GameEvent gameEvent) {
		GameObject targetObject = (gameEvent.Target as LevelObject).gameObject;
		if (_SpawnedUnits.Contains(targetObject)) {
			_SpawnedUnits.Remove(targetObject);
		}
	}

	private IEnumerator PerformTransition()
	{
		if (!isRunning)
			isRunning = true;
		while (true) {
			if (_AvailableSpawnWaypoints.Count > 0 && _SpawnedUnits.Count < _MaximumUnitsCountAtSameTime && _SpawnedUnitsCount < _TotalUnitsCount) {
				Waypoint spawnWaypoint = _AvailableSpawnWaypoints[Random.Range(0, _AvailableSpawnWaypoints.Count)];
				float respawnTime = _TimeBetweenSpawn;
				List<RespawnSpeedBonus> bonuses = Level.Instance.GetActiveBonuses<RespawnSpeedBonus>();
				foreach (RespawnSpeedBonus respawnSpeedBonus in bonuses) {
					respawnTime *= respawnSpeedBonus.GetSpawnTimeModifier();
				}
				yield return new WaitForSeconds(respawnTime);
				GameObject unit = PrefabsCache.Instance.GetCachedInstanceForPrefab(_PossibleUnitsPrefabs[Random.Range(0, _PossibleUnitsPrefabs.Count)]);
				unit.transform.SetParent(_Container.transform, false);
				unit.transform.position = spawnWaypoint.transform.position;
				_SpawnedUnits.Add(unit);
				_SpawnedUnitsCount++;
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelObjectSpawned, unit.GetComponent<LevelObject>()));
				Debug.Log("dispatch event");
			} else {
				yield return new WaitForSeconds(1.0f);
			}
		}
	}
}