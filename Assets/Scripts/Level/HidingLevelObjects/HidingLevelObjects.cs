using System.Collections.Generic;
using UnityEngine;

public class HidingLevelObjects : MonoBehaviour {
	[SerializeField]
	private bool _HideSprites;
	[SerializeField]
	protected List<HidingArea> _Areas;
	[SerializeField]
	protected List<Transform> _AffectedContainers;
	[SerializeField]
	[RangeAttribute(0.0f, 1.0f)]
	private float SpritesAlpha;

	private LevelObject OwnLevelObject;
	private List<SpriteRenderer> OwnRenderers;

	private float CurrentSpritesAlpha;
	private List<SpriteRenderer> TargetSpriteRenderers;

	#if UNITY_EDITOR
	public List<HidingArea> Areas {
		get {
			return _Areas;
		}
	}
	#endif

	public virtual void Awake() {
		if (_AffectedContainers == null || _AffectedContainers.Count == 0) {
			_AffectedContainers = new List<Transform>() {transform.parent};
		}
		OwnLevelObject = GetComponent<LevelObject>();
		OwnRenderers = new List<SpriteRenderer>();
		List<SpriteRenderer> renderers = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());
		foreach (SpriteRenderer renderer in renderers) {
			if (renderer.sortingLayerName == "LevelObjects") {
				OwnRenderers.Add(renderer);
			}
		}
	}

	public virtual void OnEnable() {
		TargetSpriteRenderers = new List<SpriteRenderer>();
		SpritesAlpha = 0.0f;
		UpdateAffectedObjects(false);
		if (_HideSprites) {
			foreach (Transform container in _AffectedContainers) {
				for (int i = 0; i < container.childCount; i++) {
					Transform childContainer = container.GetChild(i);
					if (IsTransformInAreas(childContainer) && childContainer != transform) {
						TargetSpriteRenderers.AddRange(childContainer.GetComponentsInChildren<SpriteRenderer>(true));
					}
				}
			}
		}
		if (OwnLevelObject != null) {
			GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusActivated, CheckBonus);
			GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusDeactivated, CheckBonus);
			CheckBonus();
		}
	}

	public virtual void OnDisable() {
		UpdateAffectedObjects(true);
		if (_HideSprites) {
			Color newColor = Color.white;
			newColor.a = 1.0f;
			foreach (SpriteRenderer renderer in TargetSpriteRenderers) {
				renderer.color = newColor;
			}
		}
		if (OwnLevelObject != null) {
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusActivated, CheckBonus);
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusDeactivated, CheckBonus);
		}
	}

	public virtual void Update() {
		if (isActiveAndEnabled && _HideSprites && (SpritesAlpha - CurrentSpritesAlpha) != float.Epsilon) {
			Color newColor = Color.white;
			newColor.a = SpritesAlpha;
			foreach (SpriteRenderer renderer in TargetSpriteRenderers) {
				renderer.color = newColor;
			}
		}
	}

	private void UpdateAffectedObjects(bool show) {
		foreach (Transform container in _AffectedContainers) {
			if (container != null)
			{
				for (int i = 0; i < container.childCount; i++)
				{
					Transform childContainer = container.GetChild(i);
					if (IsTransformInAreas(childContainer) && childContainer != transform)
					{
						LevelObject targetLevelObject = childContainer.GetComponent<LevelObject>();
						if (targetLevelObject != null)
						{
							targetLevelObject.Hidden = !show;
						}
					}
				}
			}
		}
	}

	private void CheckBonus(GameEvent gameEvent = null) {
		if (_HideSprites) {
			if (Level.Instance.GetActiveBonuses<ShowHiddenDropsBonus>(OwnLevelObject).Count > 0) {
				SetRenderersColor(Color.Lerp(Color.clear, Color.white, 0.4f));
				SpritesAlpha = 1.0f;
			} else {
				SpritesAlpha = 0.0f;
				SetRenderersColor(Color.white);
			}
		}
	}

	private void SetRenderersColor(Color color) {
		foreach (SpriteRenderer renderer in OwnRenderers) {
			renderer.color = color;
		}
	}

	protected bool IsTransformInAreas(Transform target) {
		foreach (HidingArea hidingArea in _Areas) {
			if (Vector3.Distance(target.position, new Vector3(hidingArea.Position.x, hidingArea.Position.y, 0.0f) + transform.position) < hidingArea.Radius) {
				return true;
			}
		}
		return false;
	}
}