using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class Fog : HidingLevelObjects{

	[SerializeField]
	private Texture FogTexture;
	[SerializeField]
	private Color FogColor;
	private Texture2D MaskTexture;
	private bool NeedToRedraw;

	override public void Awake() {
		base.Awake();
		MaskTexture = new Texture2D(27, 15);
		for (int y = 0; y < MaskTexture.height; y++)
		{
			for (int x = 0; x < MaskTexture.width; x++)
			{
				Color color = Color.white;
				MaskTexture.SetPixel(x, y, color);
			}
		}
		MaskTexture.Apply();
		Renderer renderer = GetComponentInChildren<Renderer>();
		Material material =  new Material(renderer.sharedMaterial);
		material.SetTexture("_MaskTex", MaskTexture);
		material.SetTexture("_FogTex", FogTexture);
		material.SetColor("_Color", FogColor);
		renderer.sharedMaterial = material;
		renderer.sortingLayerName = "Fog";
	}

	override public void OnEnable() {
		foreach (HidingArea lightSource in _Areas) {
			lightSource.CurrentRadius = lightSource.Radius;
		}
		NeedToRedraw = true;
		ShowObjects(true);
		Update();
	}

	override public void OnDisable() {
		ShowObjects(false);
	}

	public void AddLight(HidingArea lightSource) {
		_Areas.Add(lightSource);
		lightSource.CurrentRadius = 0.0f;
		NeedToRedraw = true;
		ShowObjects(true);
	}

	public void RemoveLight(HidingArea lightSource) {
		_Areas.Remove(lightSource);
		NeedToRedraw = true;
		ShowObjects(true);
	}

	public void UpdateLights() {
		NeedToRedraw = true;
	}

	override public void Update()
	{
		foreach (HidingArea lightSource in _Areas) {
			if (lightSource.CurrentRadius < lightSource.Radius) {
				lightSource.CurrentRadius += 0.1f;
				NeedToRedraw = true;
			}
		}
		#if UNITY_EDITOR
		NeedToRedraw = true;
		#endif
		if (NeedToRedraw) {
			Color[] maskPixels = Enumerable.Repeat<Color>(Color.white, 27 * 15).ToArray();;
			foreach (HidingArea lightSource in _Areas) {
				Rect lightRect = lightSource.GetRect();
				lightRect.x += 13;
				lightRect.y += 7;
				for (int lightY = Mathf.RoundToInt(lightRect.yMin); lightY < lightRect.yMax; lightY++)
				{
					int y = Mathf.Clamp(lightY, 0, 14);
					float inCircleY = y - lightRect.center.y;
					for (int lightX = Mathf.RoundToInt(lightRect.xMin); lightX < lightRect.xMax; lightX++)
					{
						int x = Mathf.Clamp(lightX, 0, 26);
						float inCircleX = x - lightRect.center.x;
						float radius = Mathf.Sqrt(inCircleX * inCircleX + inCircleY * inCircleY);
						if (radius < lightSource.CurrentRadius) {
							float lerpValue = radius > (lightSource.CurrentRadius * lightSource.LightBorder) ? (radius - (lightSource.CurrentRadius * lightSource.LightBorder)) / (lightSource.CurrentRadius * lightSource.LightBorder) : 0.0f;
							maskPixels[x + 27 * y] = Color.Lerp(Color.black, maskPixels[x + 27 * y], lerpValue);
						}
					}
				}
			}
			MaskTexture.SetPixels(maskPixels);
			MaskTexture.Apply();
			NeedToRedraw = false;
		}
	}

	private void ShowObjects(bool onlyInAreas) {
		foreach (Transform container in _AffectedContainers) {
			if(container == null)
				continue;
			for (int i = 0; i < container.childCount; i++) {
				Transform childContainer = container.GetChild(i);
				LevelObject targetLevelObject = childContainer.GetComponent<LevelObject>();
				if (targetLevelObject != null) {
					if (!onlyInAreas || (onlyInAreas && IsTransformInAreas(childContainer))) {
						targetLevelObject.Hidden = false;
					} else {
						targetLevelObject.Hidden = true;
					}
				}
			}
		}
	}
}