﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* stub for monday build*/

public class MapObjectsHider : MonoBehaviour
{
	[SerializeField]
	private List<GameObject> HidableObjects;
	
	void OnEnable(){
		Hide();
	}

	void OnDisable()
	{
		Show();
	}

	private void Hide()
	{
		foreach (var go in HidableObjects)
		{
			go.SetActive(false);
		}
	}

	private void Show()
	{
		foreach (var go in HidableObjects)
		{
			go.SetActive(true);
		}
	}
}
