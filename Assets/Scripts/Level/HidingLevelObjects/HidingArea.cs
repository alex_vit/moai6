using UnityEngine;

[System.Serializable]
public class HidingArea {
	public Vector2 Position;
	[RangeAttribute(0.0f, 15.0f)]
	public float Radius;
	[RangeAttribute(0.0f, 1.0f)]
	public float LightBorder;
	private float _CurrentRadius;

	public float CurrentRadius {
		get {
			return _CurrentRadius;
		}
		set {
			_CurrentRadius = value;
		}
	}

	public Rect GetRect() {
		return new Rect(Position.x - Radius, Position.y - Radius, Radius * 2, Radius * 2);
	}
}