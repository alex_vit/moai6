using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Allowed Level Object Prefabs", fileName="AllowedLevelObjectPrefabs")]
public class AllowedLevelObjectPrefabs : ScriptableObject{
	[SerializeField]
	private List<GameObject> _AllowedPrefabs;

	public bool IsPrefabAllowed(GameObject buildingPrefab) {
		foreach (GameObject levelObject in _AllowedPrefabs) {
			if (levelObject == buildingPrefab) {
				return true;
			}
		}
		return false;
	}
}