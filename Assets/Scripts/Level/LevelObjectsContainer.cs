﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LevelObjectsContainer : MonoBehaviour {

	private Coroutine UpdateCoroutine;
	private List<LevelObjectSortablePart> SortableParts;

	public void Awake() {
		SortableParts = new List<LevelObjectSortablePart>();
		if (Application.isPlaying) {
			UpdateCoroutine = StartCoroutine(SortLevelObjects());
			GameEventDispatcher.Instance.AddEventListener(GameEventType.SortablePartEnable, OnSortablePartEnable);
			GameEventDispatcher.Instance.AddEventListener(GameEventType.SortablePartDisable, OnSortablePartDisable);
		}
	}

	public void OnEnable() {
		if (!Application.isPlaying) {
			RefreshSorting();
		}
	}

	public void OnDestroy() {
		if (UpdateCoroutine != null) {
			StopCoroutine(UpdateCoroutine);
		}
		if (Application.isPlaying) {
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.SortablePartEnable, OnSortablePartEnable);
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.SortablePartDisable, OnSortablePartDisable);
		}
	}

	private void OnSortablePartEnable(GameEvent gameEvent) {
		if (!SortableParts.Contains((gameEvent.Target as LevelObjectSortablePart))) {
			SortableParts.Add((gameEvent.Target as LevelObjectSortablePart));
		}
	}

	private void OnSortablePartDisable(GameEvent gameEvent) {
		if (SortableParts.Contains((gameEvent.Target as LevelObjectSortablePart))) {
			SortableParts.Remove((gameEvent.Target as LevelObjectSortablePart));
		}
	}

	private IEnumerator SortLevelObjects() {
        while (true)
        {
            foreach (LevelObjectSortablePart sortablePart in SortableParts)
            {
                if (sortablePart.transform.hasChanged)
                {
                    RefreshSorting();
                    break;
                }
            }
			for (int i = 0; i < 10; i++) {
				yield return new WaitForEndOfFrame();
			}
        }
	}

	private void RefreshSorting() {
		List<LevelObjectSortablePart> sortablePartsToSort = null;
		if (Application.isPlaying) {
			sortablePartsToSort = new List<LevelObjectSortablePart>(SortableParts);
		} else {
			sortablePartsToSort = new List<LevelObjectSortablePart>(GetComponentsInChildren<LevelObjectSortablePart>());
		}
		int order = 0;
		int partsOrder = 0;
		while (sortablePartsToSort.Count > 0) {
			LevelObjectSortablePart minPart = sortablePartsToSort[0];
			foreach (LevelObjectSortablePart levelObjectSortablePart in sortablePartsToSort) {
				if (levelObjectSortablePart.CompareTo(minPart) < 0) {
					minPart = levelObjectSortablePart;
				}
			}
			if (minPart.isActiveAndEnabled) {
				foreach (Renderer renderer in minPart.AffectedRenderers) {
					renderer.sortingOrder = order;
					order++;
				}
				minPart.Order = partsOrder;
				partsOrder++;
			}
			sortablePartsToSort.Remove(minPart);
		}
	}
}
