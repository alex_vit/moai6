using System.Collections.Generic;
using UnityEngine;

public class DropsSynchronization : MonoBehaviour {
	[SerializeField]
	private List<LevelObject> SynchronizedObjects;
	private List<StateProducingDrops> StatesProducingDrops;

	public void OnEnable() {
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectDropsProduced, OnDropsProduced);
		StatesProducingDrops = new List<StateProducingDrops>();
		for (int i = 0; i < SynchronizedObjects.Count; i++) {
			StatesProducingDrops.Add(SynchronizedObjects[i].GetStateComponentByType<StateProducingDrops>());
			if (i > 0) {
				StatesProducingDrops[i].SetDropsToProduce(StatesProducingDrops[0].GetDropsToProduce(true));
				StatesProducingDrops[i].SetProducingType(StatesProducingDrops[0].GetProducingType());
			}
		}
	}

	private void OnDisable()
	{
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectDropsProduced, OnDropsProduced);
	}

	private void OnDropsProduced(GameEvent gameEvent) {
		StateProducingDrops stateProducingDrops = gameEvent.Target as StateProducingDrops;
		if (StatesProducingDrops.Contains(stateProducingDrops)) {
			for (int i = 0; i < StatesProducingDrops.Count; i++) {
				StatesProducingDrops[i].SetDropsToProduce(stateProducingDrops.GetDropsToProduce(true));
			}
		}
	}
}