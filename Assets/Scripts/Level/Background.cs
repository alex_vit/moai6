﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Background : MonoBehaviour, IOnLevelMouseHandler {

	[SerializeField]
	private Color Color;
	[SerializeField]
	private Texture BackgroundTexture;
	[SerializeField]
	private Texture CausticsMaskTexture;
	[SerializeField]
	[Range(0.0f, 2.0f)]
	private float MaskScaleX;
	[SerializeField]
	[Range(0.0f, 2.0f)]
	private float MaskScaleY;
	private Material Material;
	private Color OldColor;

	private Coroutine UpdateColorCoroutine;

	public void Awake() {
		UpdateBackground();
	}

	public void OnEnable() {
		UpdateColorCoroutine = StartCoroutine(UpdateColor());
	}

	public void OnDisable() {
		if (UpdateColorCoroutine != null) {
			StopCoroutine(UpdateColorCoroutine);
			UpdateColorCoroutine = null;
		}
	}

	public void UpdateBackground() {
		MeshRenderer renderer = this.GetComponent<MeshRenderer>();
		Material = null;
		if (renderer) {
			Material =  new Material(renderer.sharedMaterial);
		}

		Image image = this.GetComponent<Image>();
		if (image) {
			Material = image.material;
		}
		if (Material) {
			Material.SetTexture("_MainTex", BackgroundTexture);
			if (CausticsMaskTexture == null) {
				var texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
				texture.SetPixel(0, 0, Color.clear);
				texture.Apply();
				Material.SetTexture("_MaskTex", texture);
			} else {
				Material.SetTexture("_MaskTex", CausticsMaskTexture);
			}
			Material.SetColor("_RendererColor", Color);
			Material.SetFloat("_MaskScaleX", MaskScaleX);
			Material.SetFloat("_MaskScaleY", MaskScaleY);
			OldColor = Color;
		}
		if (renderer) {
			renderer.sharedMaterial = Material;
		}
		if (image) {
			image.material = Material;
		}
	}

	public int GetOrder() {
		return 0;
	}

	public void MouseOver() {
		if (Input.GetMouseButtonDown(0)) {
			Level.Instance.ContextMenu.HideMenu();
		}
	}

	public void MouseOut() {
	}

	private IEnumerator UpdateColor() {
		while (true) {
			for (int i = 0; i < 2; i++) {
				yield return new WaitForEndOfFrame();
			}
			if (!Color.Equals(OldColor, Color)) {
				Material.SetColor("_RendererColor", Color);
				OldColor = Color;
			}
		}
	}
}
