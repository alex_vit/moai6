﻿using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;

public class HeroIcon : MonoBehaviour
{

	private void OnMouseEnter()
	{
		int _currLevelProgress = StorylineManager.Instance.GetCurrentLevelNumber() / 10;
		int _currLevelHeroType = StorylineManager.Instance.GetCurrentLevel().Herotype == HeroType.MALE ? 0 : 1;

		string nameTemplate = "hero_" + _currLevelHeroType + "_name";
		string rankTemplate = "hero_" + _currLevelHeroType + "_status_" + _currLevelProgress;
		
		UIHeroTooltip.Instance.Show("hero_tooltip",nameTemplate,"status_tooltip",rankTemplate);

	}

	private void OnMouseExit()
	{
		UIHeroTooltip.Instance.Hide();
	}
}
