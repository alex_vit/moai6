﻿using UnityEngine;

public class ProgressBar : MonoBehaviour {

    [SerializeField]
    private SpriteRenderer ProgressSprite;
    private float _Progress;
    private float CurrentX;
    private float InitialWidth;

    public float Progress
    {
        set
        {
            _Progress = value;
            CurrentX = ProgressSprite.size.x;
            ProgressSprite.size = new Vector2(InitialWidth * _Progress, ProgressSprite.size.y);
            ProgressSprite.transform.localPosition -= new Vector3((CurrentX - ProgressSprite.size.x) / 2, 0, 0);
        }
    }

    public void Awake()
    {
        InitialWidth = ProgressSprite.size.x;
    }

	public void OnEnable() {
		Progress = 0.0f;
	}
}
