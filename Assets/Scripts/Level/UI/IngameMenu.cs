using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameMenu : PopupWindow {

	override public void Show() {
		base.Show();
		Level.Instance.Paused = true;
	}

	override public void Close() {
		base.Close();
		Level.Instance.Paused = false;
	}

	public void Replay() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}