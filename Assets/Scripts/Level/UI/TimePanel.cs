using UnityEngine;
using UnityEngine.UI;

public class TimePanel : MonoBehaviour {
	[SerializeField]
	private RectTransform ProgressBar;

	private float BaseProgressHeight;

	public void Awake() {
		BaseProgressHeight = ProgressBar.rect.height;
	}

	public void Update() {
		if (ProgressBar.rect.height > 0) {
			ProgressBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, BaseProgressHeight - BaseProgressHeight * Level.Instance.CurrentLevelTime / Level.Instance.LevelTimeForGold);
		}
	}
}