using UnityEngine;

[CreateAssetMenu(fileName="TaskIcon", menuName="TimeManager/Task Icon")]
public class TaskIcon : ScriptableObject {
	public Sprite EnabledIcon;
	public Sprite DisabledIcon;
}