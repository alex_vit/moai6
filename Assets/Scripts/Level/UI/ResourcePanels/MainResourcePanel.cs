using System.Collections.Generic;
using UnityEngine;

public class MainResourcePanel : ResourcePanel {

	public bool ResourceIsInPanel(string resourceType) {
		return ResourceIndicators.ContainsKey(resourceType);
	}

	override protected void UpdateCounters(LevelResource resource) {
		if (ResourceIndicators.ContainsKey(resource.GetResourceType())) {
			ResourceIndicators[resource.GetResourceType()].UpdateCount(resource.ResourceCount);
		}
	}

	public void ShowIndicators(List<LevelResource> levelResources) {
		for (int i = 0; i < levelResources.Count; i++) {
			AddIndicator(levelResources[i]);
			AffectedDrops.Add(levelResources[i].GetResourceType());
		}
	}

	override public void AddDrop(Drop drop) {
		base.AddDrop(drop);
		FlyDropTo(drop, (ResourceIndicators[drop.ResourceType] as MonoBehaviour).transform.position);
	}
}