﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UI;
using UnityEngine;

public class ResourcePanel : MonoBehaviour {

	[SerializeField]
	protected GameObject IndicatorPrefab;
	protected Dictionary<string, IResourceIndicator> ResourceIndicators;
	protected List<string> AffectedDrops;
	protected Dictionary<string, string> DropsDescription;

	public void Awake() {
		PrefabsCache.Instance.CheckPrefabForCache(IndicatorPrefab, PrefabsCachePriority.Immidiate);
		AffectedDrops = new List<string>();
		DropsDescription = new Dictionary<string, string>();
		ResourceIndicators = new Dictionary<string, IResourceIndicator>();
		GameEventDispatcher.Instance.AddEventListener(GameEventType.ResourceValueChanged, OnResourceValueChanged);
	}

	private void OnDestroy()
	{
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ResourceValueChanged, OnResourceValueChanged);
	}

	public virtual void AddDrop(Drop drop) {
		drop.enabled = false;
		if (!AffectedDrops.Contains(drop.ResourceType)) {
			AffectedDrops.Add(drop.ResourceType);
		}
		if (drop is ResourceDrop)
		{
			Debug.Log("update descriptions");
			ResourceDrop rd = drop as ResourceDrop;
			Debug.Log(rd.Description);
			if(!DropsDescription.ContainsKey(rd.ResourceType))
				DropsDescription.Add(rd.ResourceType,rd.Description);
		}
	}

	private void OnResourceValueChanged(GameEvent gameEvent) {
		LevelResource resource = gameEvent.Target as LevelResource;
		if (AffectedDrops.Contains(resource.GetResourceType())) {
			UpdateCounters(resource);
		}
	}

	protected virtual void UpdateCounters(LevelResource resource) {
		if (resource.ResourceCount > 0) {
			if (!ResourceIndicators.ContainsKey(resource.GetResourceType())) {
				AddIndicator(resource);
			}
			ResourceIndicators[resource.GetResourceType()].UpdateCount(resource.ResourceCount);
		} else {
			if (ResourceIndicators.ContainsKey(resource.GetResourceType())) {
				PrefabsCache.Instance.FreeCachedInstance((ResourceIndicators[resource.GetResourceType()] as MonoBehaviour).gameObject);
				ResourceIndicators.Remove(resource.GetResourceType());
			}
		}
	}

	protected virtual void AddIndicator(LevelResource levelResource) {
		GameObject indicatorObject = PrefabsCache.Instance.GetCachedInstanceForPrefab(IndicatorPrefab);
		ResourceIndicator indicator = indicatorObject.GetComponent<ResourceIndicator>();
		indicator.SetLevelResource(levelResource);
		indicatorObject.transform.SetParent(transform, false);
		ResourceIndicators.Add(levelResource.GetResourceType(), indicator as IResourceIndicator);
	}

	protected void FlyDropTo(Drop drop, Vector3 destination) {
		StartCoroutine(PerformFlyDrop(drop, destination));
	}

	private IEnumerator PerformFlyDrop(Drop drop, Vector3 destination) {
		yield return new WaitForEndOfFrame();
		Vector3 dropPosition = new Vector3(drop.transform.position.x, drop.transform.position.y, 0.0f);
		Animation anim = drop.GetAnimation();

		AnimationClip flyClip = new AnimationClip();
		flyClip.legacy = true;
		flyClip.frameRate = 45.0f;
		Keyframe[] keys = new Keyframe[2];
		keys[0] = new Keyframe(0.0f, dropPosition.x);
		keys[0].outTangent = 0.5708f * Mathf.Abs(destination.x) / destination.x;
		keys[1] = new Keyframe(0.3f, destination.x);
		keys[1].inTangent = -0.5708f * Mathf.Abs(destination.x) / destination.x;
		flyClip.SetCurve("", typeof(Transform), "localPosition.x", new AnimationCurve(keys));
		keys = new Keyframe[2];
		keys[0] = new Keyframe(0.0f, dropPosition.y);
		keys[0].outTangent = 0.5708f;
		keys[1] = new Keyframe(0.3f, destination.y);
		flyClip.SetCurve("", typeof(Transform), "localPosition.y", new AnimationCurve(keys));
		anim.AddClip(flyClip, flyClip.name);
		anim.Play(flyClip.name);

		StartCoroutine(OnDropFlyComplete(drop));

	}

	protected virtual IEnumerator OnDropFlyComplete(Drop drop) {
	//TODO make constant
		yield return new WaitForSeconds(0.3f);
		Level.Instance.ModifyResourceCount(drop.ResourceType, drop.DropCount);
		PrefabsCache.Instance.FreeCachedInstance(drop.gameObject);
	}

	public string GetResourceDescription(string resourceType)
	{
		if(DropsDescription.ContainsKey(resourceType))
			return  DropsDescription[resourceType];
		return "";
	}
}