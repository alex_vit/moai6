using UnityEngine;

public class AdditionalResourcePanel : ResourcePanel{

	override public void AddDrop(Drop drop) {
		base.AddDrop(drop);
		FlyDropTo(drop, transform.position);
	}
}