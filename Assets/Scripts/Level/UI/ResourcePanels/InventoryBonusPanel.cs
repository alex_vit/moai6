using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryBonusPanel : MonoBehaviour {
	[SerializeField]
	private GameObject IndicatorPrefab;

	private List<BonusIndicator> _indicators;

	public void OnEnable() {
		_indicators = new List<BonusIndicator>();
		AvailableLevelInventoryBonuses inventoryBonuses = Level.Instance.GetInventoryBonuses();
		int bonusCount = 0;
		if (!inventoryBonuses) {
			gameObject.SetActive(false);
		} else {
			var index = 0;
			foreach (LevelBonus inventoryBonus in inventoryBonuses.Bonuses) {
				if (inventoryBonus.GetBonusType() != BonusType.HiddenPassiveBonus &&
				    (!inventoryBonus.GetCheckIfInventoryEquipped() || ProfileManager.GetCurrentProfile()
					     .IsItemEquipped(inventoryBonus.GetInventoryItem().GetItemType()))) {
					GameObject indicatorObject = PrefabsCache.Instance.GetCachedInstanceForPrefab(IndicatorPrefab);
					BonusIndicator indicator = indicatorObject.GetComponent<BonusIndicator>();
					indicator.SetBonus(inventoryBonus);
					indicator.InInventoryPanel = true;
					indicator.HideInPanel();
					_indicators.Add(indicator);
					indicatorObject.transform.SetParent(this.transform, false);
					(indicatorObject.transform as RectTransform).anchoredPosition = new Vector2(0.0f, -72.0f - index * 124.0f);
					index++;
					//indicatorObject.transform.localPosition = new Vector3(0.0f, 674.0f - i * 134.0f, 0.0f);
					bonusCount++;
				}
			}
			if (bonusCount == 0) {
				gameObject.SetActive(false);
			}
		}
	}

	public void ShowBonuses() {
		foreach (var indicator in _indicators) {
			indicator.ShowInPanel();
		}
	}
}