﻿using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResourceIndicator : MonoBehaviour, IResourceIndicator, IPointerEnterHandler, IPointerExitHandler {

	[SerializeField]
	private Image Icon;
	[SerializeField]
	private Text ValueText;

	private string _ResourceType;

	public void SetLevelResource(LevelResource levelResource)
	{
		_ResourceType = levelResource.GetResourceType();
		Icon.sprite = levelResource.GetResourceIcon();
		UpdateCount(levelResource.ResourceCount);
	}

	public void UpdateCount(int value) {
		ValueText.text = value.ToString();
	}
	
	public void OnPointerEnter(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = true;
		UITooltip.Instance.Show("",Level.Instance.LevelUi.MainResourcePanel.GetResourceDescription(_ResourceType));
		//UITooltip.Instance.Show("","tooltip_resource_moai3_mabdrake_root");
	}

	public void OnPointerExit(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = false;
		UITooltip.Instance.Hide();
	}
}
