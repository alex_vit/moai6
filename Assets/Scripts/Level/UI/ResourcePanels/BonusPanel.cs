using System.Collections.Generic;
using UnityEngine;

public class BonusPanel : ResourcePanel {
	override public void AddDrop(Drop drop) {
		base.AddDrop(drop);
		FlyDropTo(drop, transform.position);
	}

	override protected void AddIndicator(LevelResource levelResource) {
		GameObject indicatorObject = PrefabsCache.Instance.GetCachedInstanceForPrefab(IndicatorPrefab);
		BonusIndicator indicator = indicatorObject.GetComponent<BonusIndicator>();
		indicator.SetBonus(levelResource.GetBonus());
		indicator.SetResourceType(levelResource.GetResourceType());
		indicatorObject.transform.SetParent(transform, false);
		ResourceIndicators.Add(levelResource.GetResourceType(), indicator);
	}
}