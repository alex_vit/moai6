using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BonusIndicator : MonoBehaviour, IResourceIndicator, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
	public bool InInventoryPanel;
	[SerializeField]
	private Image InactiveIcon;
	[SerializeField]
	private Image ActiveIcon;
	[SerializeField]
	private Text Counter;
	private LevelBonus Bonus;
	private string BonusResourceType;
	private float TimePassed;
	private Button _Button;
	private Coroutine _Coroutine;
	private Coroutine _showHideCoroutine;

	public void ShowInPanel() {
		if (_showHideCoroutine != null) {
			StopCoroutine(_showHideCoroutine);
		}
		_showHideCoroutine = StartCoroutine(ShowCoroutine());
	}
	
	public void HideInPanel() {
		if (_showHideCoroutine != null) {
			StopCoroutine(_showHideCoroutine);
		}
		if (!Bonus.IsActive() || Bonus.GetBonusType() == BonusType.PassiveBonus) {
			_showHideCoroutine = StartCoroutine(HideCoroutine());	
		}
	}
	
	private IEnumerator ShowCoroutine() {
		var wait = new WaitForSeconds(0.03f);
		while ((transform as RectTransform).anchoredPosition.x < 0.0f) {
			(transform as RectTransform).anchoredPosition = new Vector2((transform as RectTransform).anchoredPosition.x + 20.0f, (transform as RectTransform).anchoredPosition.y);
			yield return wait;
		}
		yield return new WaitForSeconds(5.0f);
		HideInPanel();
	}

	private IEnumerator HideCoroutine() {
		var wait = new WaitForSeconds(0.03f);
		while ((transform as RectTransform).anchoredPosition.x > -124.0f) {
			(transform as RectTransform).anchoredPosition = new Vector2((transform as RectTransform).anchoredPosition.x - 20.0f, (transform as RectTransform).anchoredPosition.y);
			yield return wait;
		}
	}

	public void Awake() {
		_Button = GetComponent<Button>();
	}

	public void OnDestroy() {		
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusDeactivated, OnBonusDeactivated);
	}

	public void SetBonus(LevelBonus levelBonus) {
		Bonus = levelBonus;
		InactiveIcon.sprite = Bonus.GetInactiveSprite();
		ActiveIcon.sprite = Bonus.GetActiveSprite();
		ActiveIcon.fillAmount = 1.0f;
		TimePassed = 0.0f;
		Bonus.DisableBonus();
		if (Bonus.GetCosts().Count > 0) {
			UpdateAvailability();
			GameEventDispatcher.Instance.AddEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
		} else if (Bonus.GetDuration() <= 0.0f) {
			Bonus.EnableBonus();
		}
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusDeactivated, OnBonusDeactivated);
	}

	public void SetResourceType(string resourceType) {
		BonusResourceType = resourceType;
		UpdateCount(Level.Instance.GetResourceCount(resourceType));
	}

	public void UpdateCount(int value) {
		Counter.text = value.ToString();
	}

	public void OnPointerClick(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = false;
		if (!Bonus.IsActive() && _Button.interactable) {
			float costModifier = 1.0f;
			Bonus.EnableBonus();
			List<BonusCostModifierBonus> costBonuses = Level.Instance.GetActiveBonuses<BonusCostModifierBonus>(Bonus);
			foreach (BonusCostModifierBonus costModifierBonus in costBonuses) {
				costModifier *= costModifierBonus.GetCostModifier();
			}
			foreach (TaskCost cost in Bonus.GetCosts()) {
				Level.Instance.ModifyResourceCount(cost.GetResourceType(), Mathf.RoundToInt(-1 * cost.GetCount() * costModifier));
			}
			_Coroutine = StartCoroutine(BonusCoroutine());
		}
	}

	public void OnPointerEnter(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = true;
		UITooltip.Instance.Show(Bonus.TitleText, Bonus.InfoText);
	}

	public void OnPointerExit(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = false;
		UITooltip.Instance.Hide();
	}

	private IEnumerator BonusCoroutine() {
		yield return new WaitForSeconds (Bonus.GetDuration());
		Bonus.DisableBonus();
	}

	public void Update() {
		if (Bonus != null && Bonus.IsActive() && Bonus.GetDuration() > 0.0f) {
			TimePassed += Time.deltaTime;
			ActiveIcon.fillAmount = 1.0f - TimePassed / Bonus.GetDuration();
		}
		if (ActiveIcon.fillAmount <= 0.01f && Bonus != null && Bonus.IsActive())
		{
			Bonus.DisableBonus();
			UpdateAvailability();
		}
	}

	public void OnBonusDeactivated(GameEvent gameEvent) {
		if (gameEvent.Target != null && gameEvent.Target == Bonus) {
			if (_Coroutine != null) {
				StopCoroutine(_Coroutine);
				_Coroutine = null;
			}
			if (!string.IsNullOrEmpty(BonusResourceType)) {
				Level.Instance.ModifyResourceCount(BonusResourceType, -1);
			} else {
				UpdateAvailability();
			}
			TimePassed = 0.0f;
			ActiveIcon.fillAmount = 1.0f;
			if (InInventoryPanel) {
				HideInPanel();
			}
		}
	}

	private void UpdateAvailability(GameEvent gameEvent = null) {
		if (Bonus.GetBonusType() == BonusType.ActiveBonus) {
			bool enoughResources = true;
			foreach (TaskCost cost in Bonus.GetCosts()) {
				if (!cost.IsAvailable() && !Bonus.IsActive()) {
					enoughResources = false;
				}
			}
			if (InInventoryPanel) {
				enoughResources = enoughResources && Bonus.ActiveInPanel();
			}
			_Button.interactable = enoughResources;
			ActiveIcon.enabled = enoughResources || Bonus.IsActive();
		} else {
			ActiveIcon.enabled = false;
		}
	}
}