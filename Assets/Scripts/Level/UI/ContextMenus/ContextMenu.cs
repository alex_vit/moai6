﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextMenu : MonoBehaviour {
	[SerializeField]
	private GameObject ContextMenuPrefab;
	[SerializeField]
	private TooltipLevelObject LevelObjectTooltip;
	[SerializeField]
	private TooltipLevelObject LevelObjectInnerTooltip;
	[SerializeField]
	private List<CloudTooltip> CloudTooltips;

	private TooltipLevelObject ActiveTooltip;

	private List<GameObject> menuItems;
	private RectTransform menuRect;
	private RectTransform rootCanvasRect;

	public void Awake() {
		PrefabsCache.Instance.CheckPrefabForCache(ContextMenuPrefab, PrefabsCachePriority.Slowly);
	}

	public void ShowMenuForState(LevelObjectState state) {
		if (menuItems == null) {
			menuItems = new List<GameObject> (){ };
		}

		Clear ();

		transform.position = new Vector3(state.gameObject.transform.position.x, state.gameObject.transform.position.y, 0.0f);

		int activeTasksCount = state.Tasks.Count;
		foreach (Task task in state.Tasks) {
			if (!state.LevelObject.HasState(task.TargetStateDescription.GetStateName())) {
				activeTasksCount--;
			}
		}
		float angle = 15.0f * activeTasksCount - 15.0f - CorrectAngleAboutCorners();
		int tasksCount = 0;
		
		
		
		foreach (Task task in state.Tasks) {
			if (state.LevelObject.HasState(task.TargetStateDescription.GetStateName())) {
				GameObject menuItem = PrefabsCache.Instance.GetCachedInstanceForPrefab(ContextMenuPrefab);
				ContextMenuItem contextComponent = menuItem.GetComponent<ContextMenuItem> ();
				contextComponent.SetStateTask (state.LevelObject, task);
				menuItem.transform.SetParent(transform, false);
				contextComponent.GetIconTransform().Rotate (new Vector3(0, 0, -angle));
				menuItem.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);
				menuItem.transform.Rotate (new Vector3(0, 0, angle));
				menuItems.Add (menuItem);
				angle -= 30.0f;
				tasksCount++;
			}
		}

		if (tasksCount > 0) {
			gameObject.SetActive (true);
			
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.ContextMenuShowed, state.LevelObject));
		}
	}
	
	

	public void HideMenu() {
		LevelObjectTooltip.Hide();
		LevelObjectInnerTooltip.Hide();
		gameObject.SetActive (false);
	}

	public void ShowTooltipProduce(string header, List<IResource> resources, List<int> count) {
		LevelObjectTooltip.Show();
		LevelObjectTooltip.SetHeader(header);
		LevelObjectTooltip.SetProduce(resources, count);
	}

	public void ShowSignalForTask(Task task, LevelObject levelObject) {
		if (task.TooltipType == TooltipType.CloudTooltip) {
			List<DropToProduce> dropsToProduce = levelObject.GetTaskDrops(task);
			foreach (var cloud in CloudTooltips) {
				if (cloud.CurrentLevelObject == null) {
					cloud.CurrentLevelObject = levelObject;
					cloud.transform.position = new Vector3(levelObject.transform.position.x + 1.0f, levelObject.transform.position.y + 2.0f, 0.0f);
					cloud.ShowSignal();
					if (dropsToProduce.Count > 0)
					{
						if (dropsToProduce[0].CountType == DropCountType.FiniteAtAll || dropsToProduce[0].CountType == DropCountType.FiniteOnLevel)
						{
							cloud.SetProduce(new List<IResource>() { dropsToProduce[0].DropPrefab.GetComponent<IResource>() }, new List<int>() { dropsToProduce[0].TotalCount });
						}
						else
						{
	            
							cloud.SetCostsAndProduce(task.GetCurrentCosts(), dropsToProduce);
						}
					}
					else
					{
						cloud.SetCosts(task.GetCurrentCosts());
					}
					return;
				}
			}
		}
	}

	public void ShowTooltipForTask(Task task, LevelObject levelObject) {
        List<DropToProduce> dropsToProduce = levelObject.GetTaskDrops(task);
        switch (task.TooltipType)
        {
            case TooltipType.SimpleTooltip:
                ActiveTooltip = LevelObjectTooltip;
                break;
            case TooltipType.InnerTooltip:
                ActiveTooltip = LevelObjectInnerTooltip;
                break;
            case TooltipType.CloudTooltip:
	            foreach (var cloud in CloudTooltips) {
		            if (cloud.CurrentLevelObject == null) {
			            ActiveTooltip = cloud;
			            cloud.transform.position = new Vector3(levelObject.transform.position.x + 1.0f, levelObject.transform.position.y + 2.0f, 0.0f);       
		            }
	            }
                break;
        }
		ActiveTooltip.Show();
        if (dropsToProduce.Count > 0)
        {
            if (dropsToProduce[0].CountType == DropCountType.FiniteAtAll || dropsToProduce[0].CountType == DropCountType.FiniteOnLevel)
            {
                if (ActiveTooltip is TooltipWithHeader) (ActiveTooltip as TooltipWithHeader).SetHeader(task.TaskName);
                if (ActiveTooltip is TooltipLevelObject) (ActiveTooltip as TooltipLevelObject).SetProduce(new List<IResource>() { dropsToProduce[0].DropPrefab.GetComponent<IResource>() }, new List<int>() { dropsToProduce[0].TotalCount });
            }
            else
            {
	            
                if (ActiveTooltip is TooltipWithHeader) (ActiveTooltip as TooltipWithHeader).SetHeader(task.TaskName);
                ActiveTooltip.SetCostsAndProduce(task.GetCurrentCosts(), dropsToProduce);
            }
        }
        else
        {
            if (ActiveTooltip is TooltipWithHeader) (ActiveTooltip as TooltipWithHeader).SetHeader(task.TaskName);
            ActiveTooltip.SetCosts(task.GetCurrentCosts());
        }
		
		
	}

	public void HideTooltip() {
		LevelObjectTooltip.Hide();
		LevelObjectInnerTooltip.Hide();
		foreach (var cloud in CloudTooltips) {
			cloud.Hide();
		}
	}

	private void Clear() {
		foreach (GameObject item in menuItems) {
			GameObject.Destroy (item);
		}
	}

	private float CorrectAngleAboutCorners()
	{
		if (menuRect == null)
			menuRect = GetComponent<RectTransform>();
		if(rootCanvasRect == null)
			rootCanvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
		
		Vector2 _rootCanvasSize;
		_rootCanvasSize.x = rootCanvasRect.rect.width *
		                    rootCanvasRect.localScale.x * 100;
		_rootCanvasSize.y = rootCanvasRect.rect.height *
		                    rootCanvasRect.localScale.y * 100;

		float alignX = _rootCanvasSize.x / 2 - Mathf.Abs(menuRect.anchoredPosition.x) - menuRect.rect.width / 2  -70f;
		float alignY = _rootCanvasSize.y / 2 - Mathf.Abs(menuRect.anchoredPosition.y) - menuRect.rect.height / 2 - 70f;

		if (alignX >= 0f && alignY >= 0f)
			return 0;
		if (Mathf.Abs(alignX) >= Mathf.Abs(alignY))
		{
			if (menuRect.anchoredPosition.y > 0)
				return -180f;
			return 0f;
		}
		if (menuRect.anchoredPosition.x > 0)
			return -90f;
		return 90f;
	}

}
