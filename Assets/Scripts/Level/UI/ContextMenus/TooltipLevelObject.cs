using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TooltipLevelObject : TooltipWithHeader {

	[SerializeField]
	protected GameObject ResourceCostPrefab;
	[SerializeField]
	protected RectTransform CostContainer;
	[SerializeField]
	protected RectTransform ProduceContainer;
	[SerializeField]
	protected Image Arrow;
	[SerializeField]
	protected GridLayoutGroup CostGrid;
	[SerializeField]
	private Sprite GoalIcon;
	private List<GameObject> AddedObjects;

	override public void Awake() {
		base.Awake();
		AddedObjects = new List<GameObject>();
		PrefabsCache.Instance.CheckPrefabForCache(ResourceCostPrefab, PrefabsCachePriority.Slowly);
	}

	override public void Show() {
		Clear();
		base.Show();
	}

	public void SetCostsAndProduce(List<TaskCost> costs, List<DropToProduce> drops) {
		SetDrops(drops);
		SetCosts(costs);
		Arrow.gameObject.SetActive(true);
		ProduceContainer.gameObject.SetActive(true);
		if (CostGrid != null) {
			CostGrid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
		}
	}

	public void SetProduce(List<IResource> resources, List<int> count) {
		Arrow.gameObject.SetActive(false);
		ProduceContainer.gameObject.SetActive(false);
		if (CostGrid != null) {
			CostGrid.constraint = GridLayoutGroup.Constraint.FixedRowCount;
		}
		if (resources.Count > 0) {
			for (int i = 0; i < resources.Count; i++) {
				IResource cost = resources[i];
				AddResourceIcon(CostContainer, cost.ResourceIcon, TooltipResourceTextColor.NeutralColor, count[i].ToString());
			}
		}
	}

	public void SetCosts(List<TaskCost> costs) {
		Arrow.gameObject.SetActive(false);
		ProduceContainer.gameObject.SetActive(false);
		if (CostGrid != null) {
			CostGrid.constraint = GridLayoutGroup.Constraint.FixedRowCount;
		}
		if (costs.Count > 0) {
			var summaryCosts = new Dictionary<string, TaskCost>();
			for (int i = 0; i < costs.Count; i++) {
				if (!summaryCosts.ContainsKey(costs[i].GetResourceType())) {
					summaryCosts.Add(costs[i].GetResourceType(), costs[i]);
				}
				else {
					summaryCosts[costs[i].GetResourceType()].SetCount(summaryCosts[costs[i].GetResourceType()].GetCount() + costs[i].GetCount());
				}
			}
			foreach (var cost in summaryCosts.Values) {
				if (cost.GetMinimalCountToShowInTooltip() <= cost.GetCount()) {
					AddResourceIcon(CostContainer, cost.GetCostType() == TaskCostType.LevelGoalCost ? GoalIcon : cost.GetIcon(), cost.IsAvailable() ? TooltipResourceTextColor.EnoughColor : TooltipResourceTextColor.NotEnoughColor, cost.GetCount().ToString());
				}	
			}
		}
	}

	protected virtual void Clear() {
		if (AddedObjects != null) {
			while (AddedObjects.Count > 0)
			{
				PrefabsCache.Instance.FreeCachedInstance(AddedObjects[0]);
				AddedObjects.RemoveAt(0);
			}
		}
	}

	private void AddResourceIcon(Transform container, Sprite sprite, TooltipResourceTextColor color, string countText) {
		GameObject costObject = PrefabsCache.Instance.GetCachedInstanceForPrefab(ResourceCostPrefab);
		costObject.transform.SetParent(container, false);
		AddedObjects.Add(costObject);
		TooltipResourceIcon resourceCost = costObject.GetComponentInChildren<TooltipResourceIcon>();
		resourceCost.SetSprite(sprite);
		resourceCost.SetTextColor(color);
		resourceCost.SetText(countText);
	}

	private void SetDrops(List<DropToProduce> drops) {
		if (drops.Count > 0) {
			for (int i = 0; i < drops.Count; i++) {
				DropToProduce drop = drops[i];
				IResource dropResource = drop.DropPrefab.GetComponent<IResource>();
				Sprite icon;
				if (dropResource != null) {
					icon = dropResource.ResourceIcon;
				} else {
					icon = drop.DropPrefab.GetComponentInChildren<SpriteRenderer>().sprite;
				}
				AddResourceIcon(ProduceContainer, icon, TooltipResourceTextColor.NeutralColor, drop.GetDropCount().ToString());
			}
		}
	}
}