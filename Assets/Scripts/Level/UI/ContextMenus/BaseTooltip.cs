using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTooltip : MonoBehaviour {

	[SerializeField]
	protected bool FollowMouse;
	[SerializeField]
	protected bool StaticPosition;
	private bool Shown;
	private Coroutine UpdatePositionCoroutine;

	public virtual void Awake() {
	}

	public virtual void Show() {
		gameObject.SetActive(true);
		UpdatePosition();
		Shown = false;
		StartCoroutine(MarkAsShown());
	}

	public virtual void Hide() {
		if (Shown) {
			if (FollowMouse) {
				StopCoroutine(UpdatePositionCoroutine);
			}
			gameObject.SetActive(false);
		}
	}

	private IEnumerator PerformUpdatePosition() {
		while (true) {
			UpdatePosition();
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator MarkAsShown() {
		yield return new WaitForEndOfFrame();
		Shown = true;
		if (FollowMouse) {
			UpdatePositionCoroutine = StartCoroutine(PerformUpdatePosition());
		}
	}

	private void UpdatePosition() {
		if (StaticPosition) return; 
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePosition.z = 0.0f;
		transform.position = mousePosition + new Vector3(0.0f, 0.5f, 0.0f);
		RectTransform rectTransform = transform as RectTransform;
		RectTransform parentRectTransform = transform.parent.transform as RectTransform;
		Vector3 pos = rectTransform.localPosition;
		Vector3 minPosition = parentRectTransform.rect.min - rectTransform.rect.min;
		Vector3 maxPosition = parentRectTransform.rect.max - rectTransform.rect.max;
		pos.x = Mathf.Clamp(rectTransform.localPosition.x, minPosition.x, maxPosition.x);
		pos.y = Mathf.Clamp(rectTransform.localPosition.y, minPosition.y, maxPosition.y);
		rectTransform.localPosition = pos;
	}
}