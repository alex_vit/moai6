﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ContextMenuItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
	[SerializeField]
	private Image Icon;
	[SerializeField]
	private Button Button;

	private Task Task;
	private LevelObject LevelObject;

	public RectTransform GetIconTransform() {
		return Icon.rectTransform;
	}

	public void SetStateTask(LevelObject levelObject, Task task) {
		Task = task;
		LevelObject = levelObject;
		UpdateAvailability(null);
	}

	public void OnEnable() {
		GameEventDispatcher.Instance.AddEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
	}

	public void OnDisable() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
	}

	public void OnPointerClick(UnityEngine.EventSystems.PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = false;
		Level.Instance.ContextMenu.HideMenu();
		Level.Instance.TaskManager.AddTask(LevelObject, Task);
	}

	public void OnPointerEnter(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = true;
		Level.Instance.ContextMenu.ShowTooltipForTask(Task, LevelObject);
	}

	public void OnPointerExit(PointerEventData eventData) {
		Level.Instance.MouseIsOnUI = false;
		Level.Instance.ContextMenu.HideTooltip();
	}

	private void UpdateAvailability(GameEvent gameEvent) {
		bool taskAvailable = Task.IsTaskAvailable();
		Button.interactable = taskAvailable;
		Icon.sprite = taskAvailable ? Task.TaskIcon.EnabledIcon : Task.TaskIcon.DisabledIcon;
		Icon.SetNativeSize();
	}
}
