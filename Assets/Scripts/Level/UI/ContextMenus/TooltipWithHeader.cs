using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipWithHeader : BaseTooltip {
	[SerializeField]
	protected LocalizedText Header;

	public void SetHeader(string headerKey) {
		if (Header != null) Header.SetKey(headerKey);
	}
}