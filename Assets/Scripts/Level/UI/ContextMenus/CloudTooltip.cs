using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloudTooltip : TooltipLevelObject {
	[SerializeField]
	private Animator CloudAnimator;
	[SerializeField]
	private AudioData signalSound;

	private bool isSignal;
	public LevelObject CurrentLevelObject;

	public void ShowSignal() {
		Show();
		AudioManager.Instance.PlaySound(signalSound);
		isSignal = true;
		StartCoroutine(HideCoroutine());
	}

	public override void Show() {
		base.Show();
		isSignal = false;
		CloudAnimator.Play(1809480452, 0);
	}
	
	public override void Hide() {
		if (!isSignal) {
			CurrentLevelObject = null;
			base.Hide();	
		}
	}

	private IEnumerator HideCoroutine() {
		yield return new WaitForSeconds(5.0f);
		isSignal = false;
		Hide();
	}
}