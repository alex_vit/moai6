using UnityEngine;
using UnityEngine.UI;

public class TooltipResourceIcon : MonoBehaviour {
	[SerializeField]
	private Image _Image;
	[SerializeField]
	private Text _Text;
	[SerializeField]
	private Color _EnoughColor;
	[SerializeField]
	private Color _NotEnoughColor;
	[SerializeField]
	private Color _NeutralColor;

	public void SetTextColor(TooltipResourceTextColor textColor) {
		switch (textColor) {
			case TooltipResourceTextColor.EnoughColor :
				_Text.color = _EnoughColor;
				break;
			case TooltipResourceTextColor.NotEnoughColor :
				_Text.color = _NotEnoughColor;
				break;
			case TooltipResourceTextColor.NeutralColor :
				_Text.color = _NeutralColor;
				break;
		}
	}

	public void SetText(string value) {
		_Text.text = value;
	}

	public void SetSprite(Sprite sprite) {
		_Image.sprite = sprite;
	}

	public void OnEnable() {
		GameEventDispatcher.Instance.AddEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
	}

	public void OnDisable() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ResourceValueChanged, UpdateAvailability);
	}

	private void UpdateAvailability(GameEvent gameEvent) {
		//ShowCosts(CurrentCosts);
	}
}