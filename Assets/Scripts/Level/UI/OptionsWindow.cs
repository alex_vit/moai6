using UnityEngine;
using UnityEngine.UI;

public class OptionsWindow : PopupWindow {
	[SerializeField]
	private Slider MusicSlider;
	[SerializeField]
	private Slider SoundSlider;
	[SerializeField]
	private Toggle FullscreenToggle;
	[SerializeField]
	private Toggle CursorToggle;

	public void OnEnable() {
		MusicSlider.value = ProfileManager.GetCurrentProfile().MusicVolume;
		SoundSlider.value = ProfileManager.GetCurrentProfile().SoundVolume;
		FullscreenToggle.isOn = ProfileManager.GetCurrentProfile().Fullscreen;
		CursorToggle.isOn = !ProfileManager.GetCurrentProfile().SystemCursor;
	}

	public void OnMusicVolumeChange(float value) {
		AudioManager.Instance.SetMusicVolume(value);
	}

	public void OnSoundVolumeChange(float value) {
		AudioManager.Instance.SetSoundVolume(value);
	}

	public void OnOKClick() {
		ProfileManager.GetCurrentProfile().MusicVolume = MusicSlider.value;
		ProfileManager.GetCurrentProfile().SoundVolume = SoundSlider.value;
		ProfileManager.GetCurrentProfile().Fullscreen = FullscreenToggle.isOn;
		ProfileManager.GetCurrentProfile().SystemCursor = !CursorToggle.isOn;
		ProfileManager.Instance.Save();
		ScreensizeManager.CheckScreenSize();
		GameCursorManager.Instance.RefreshCursor();
		Close();
	}

	public void OnCancelClick() {
		AudioManager.Instance.SetMusicVolume(ProfileManager.GetCurrentProfile().MusicVolume);
		AudioManager.Instance.SetSoundVolume(ProfileManager.GetCurrentProfile().SoundVolume);
		Close();
	}
}