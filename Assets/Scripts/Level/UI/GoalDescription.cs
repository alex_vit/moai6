using UnityEngine;
using UnityEngine.UI;

public class GoalDescription : MonoBehaviour {
	[SerializeField]
	private Image Checkmark;
	[SerializeField]
	private Text DescriptionText;
	[SerializeField]
	private Text Line;
	[SerializeField]
	private Color IncompleteColor;
	[SerializeField]
	private Color CompleteColor;

	public void OnEnable() {
		Checkmark.gameObject.SetActive(false);
		DescriptionText.color = IncompleteColor;
		Line.text = "";
	}


	public void SetText(string text) {
		DescriptionText.text = text;
	}

	public void SetComplete() {
		int lineLength = Mathf.RoundToInt(DescriptionText.text.Length * 1.2f);
		for (int i = 0; i < lineLength; i++) {
			Line.text += "–";
		}
		DescriptionText.color = CompleteColor;
		Line.color = CompleteColor;
		Checkmark.gameObject.SetActive(true);
	}
	
	public void ResetComplete() {
		Line.text = "";
		DescriptionText.color = IncompleteColor;
		Line.color = IncompleteColor;
		Checkmark.gameObject.SetActive(false);
	}
}