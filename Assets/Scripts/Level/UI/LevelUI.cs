using UnityEngine;
using UnityEngine.UI;

public class LevelUI : MonoBehaviour{
	[SerializeField]
	private ContextMenu _ContextMenu;
	[SerializeField]
	private MainResourcePanel _MainResourcePanel;
	[SerializeField]
	private AdditionalResourcePanel _AdditionalResourcePanel;
	[SerializeField]
	private GoalPanel _GoalPanel;
	[SerializeField]
	private BonusPanel _BonusPanel;
	[SerializeField]
	private TimePanel _TimePanel;
	[SerializeField]
	private IngameMenu _IngameMenu;
	[SerializeField]
	private Statistics _StatisticsWindow;
	[SerializeField]
	private Text _StartText;

	[SerializeField]
	private Text _LevelLabel;
	[SerializeField]
	private Text _BonusLevelLabel;

	public ContextMenu ContextMenu { get {return _ContextMenu;} }
	public MainResourcePanel MainResourcePanel { get {return _MainResourcePanel;} }
	public AdditionalResourcePanel AdditionalResourcePanel { get {return _AdditionalResourcePanel;} }
	public GoalPanel GoalPanel { get {return _GoalPanel;} }
	public BonusPanel BonusPanel { get {return _BonusPanel;} }
	public TimePanel TimePanel { get {return _TimePanel;} }
	public IngameMenu IngameMenu { get {return _IngameMenu;} }
	public Statistics StatisticsWindow { get {return _StatisticsWindow;} }
	public Text StartText { get {return _StartText;} }

	private void Awake() {
		GetComponent<Canvas>().worldCamera = Camera.main;
	}

	public void OnEnable() {
		_LevelLabel.gameObject.SetActive(!StorylineManager.Instance.GetCurrentLevel().BonusLevel);
		_BonusLevelLabel.gameObject.SetActive(StorylineManager.Instance.GetCurrentLevel().BonusLevel);
		if (ProfileManager.GetCurrentProfile().GameMode == 1) {
			_TimePanel.gameObject.SetActive(false);
		}
	}
}