using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalPanel : MonoBehaviour {
	[SerializeField]
	private GameObject GoalTextPrefab;

	public void ShowGoals(List<LevelGoal> goals) {
		for (int i = 0; i < goals.Count; i++) {
			LevelGoal goal = goals[i];
			GameObject goalGameObject = GameObject.Instantiate(GoalTextPrefab);
			goalGameObject.transform.SetParent(transform, false);
			goal.GoalUI = goalGameObject.GetComponent<GoalDescription>();
		}
	}
}