using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Statistics : PopupWindow {
	[SerializeField]
	private Text ForCompleteValue;
	[SerializeField]
	private Text TimeValue;
	[SerializeField]
	private Text ResourceValue;
	[SerializeField]
	private Text TotalValue;
	[SerializeField]
	private Image Chest;
	[SerializeField]
	private Sprite ChestNormal;
	[SerializeField]
	private Sprite ChestPerfect;
	[SerializeField]
	private Text Perfect;
	[SerializeField]
	private Text Normal;

	[SerializeField] private StatisticsIngredient Ingredient;

	public void Show(int scoresForComplete, int scoresForTime, int scoresForResources, bool gold) {
		Show();
		Chest.sprite = gold ? ChestPerfect : ChestNormal;
		Chest.SetNativeSize();
		Perfect.gameObject.SetActive(gold);
		Normal.gameObject.SetActive(!gold);
		ForCompleteValue.text = scoresForComplete.ToString();
		TimeValue.text = scoresForTime.ToString();
		ResourceValue.text = scoresForResources.ToString();
		TotalValue.text = (scoresForComplete + scoresForTime + scoresForResources).ToString();
	}

	public void ShowGathered() {
		if (Level.Instance.GatheredInventoryItems.Count > 0) {
			Close();
			var gatheredByName = new Dictionary<string, List<InventoryItem>>();
			foreach (var gatheredInventoryItem in Level.Instance.GatheredInventoryItems) {
				if (!gatheredByName.ContainsKey(gatheredInventoryItem.name)) {
					gatheredByName.Add(gatheredInventoryItem.name, new List<InventoryItem>());
				}
				gatheredByName[gatheredInventoryItem.name].Add(gatheredInventoryItem);
			}
			var index = 0;
			foreach (var key in gatheredByName.Keys) {
				Ingredient.Items[index].gameObject.SetActive(true);
				Ingredient.Items[index]._Image.sprite = gatheredByName[key][0].GetActiveSprite();
				Ingredient.Items[index]._Text.text = LocalizationManager.Instance.GetString(gatheredByName[key][0].LocaleName) + " (+" + gatheredByName[key].Count + ")";
				index++;
			}
			Ingredient.Show();
		}
		else {
			Continue();
		}
	}

	public void Continue() {
		Level.Instance.CommitGatheredItems();
		SceneSwitcher.Instance.SwitchTo(SceneSwitcher.Instance.LastSceneName);
	}
}