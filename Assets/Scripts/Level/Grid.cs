﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Grid : MonoBehaviour {

	#if UNITY_EDITOR
	static public Vector3 CellDiffX = new Vector3(1.2f, -0.3f, 0.0f);
	static public Vector3 CellDiffY = new Vector3(-0.56f, -0.7f, 0.0f);

	static public Vector3 startPoint = new Vector3 (0.51f, 0.26f, 0.0f);
	static public Vector3 gridStart = startPoint - CellDiffX * 14 - CellDiffY * 5;

	static private List<float> XLinesB;
	static private List<float> YLinesB;

	static public Vector3 AlignPositionOnGrid(Vector3 point) {
		FillLinesB();
		float minDistance = float.MaxValue;
		Vector3 result = new Vector3(0.0f, 0.0f, 0.0f);
		for (int i = 0; i < XLinesB.Count; i +=2) {
			float xLinesB = XLinesB[i];
			for (int j = 1; j < YLinesB.Count; j +=2) {
				float yLinesB = YLinesB[j];
				Vector3 intersectionPoint = new Vector3(0.0f, 0.0f, 0.0f);
				intersectionPoint.x = (yLinesB - xLinesB) / 1.5f;
				intersectionPoint.y = 1.25f * intersectionPoint.x + xLinesB;
				float newMinDistance = Vector3.Distance(point, intersectionPoint);
				if (newMinDistance < minDistance) {
					minDistance = newMinDistance;
					result = intersectionPoint;
				}
			}
		}
		return result;
	}

	public void OnDrawGizmos()
	{
		DrawGrid ();
	}

	public void OnDrawGizmosSelected()
	{
		DrawGrid ();
	}

	static private void FillLinesB() {
		if (XLinesB == null) {
			XLinesB = new List<float>(){};
			YLinesB = new List<float>(){};

			for (int i = 0; i < 56; i++) {
				Vector3[] positions = new Vector3[2];
				positions [0] = gridStart + CellDiffX / 2 + CellDiffX / 2 * i - Mathf.Floor(i / 2) * CellDiffY;
				positions [1] = positions [0] + CellDiffY * 50;
				XLinesB.Add(GetLineParameterB(1.25f, positions [0], positions [1]));
			}

			for (int i = -16; i < 38; i++) {
				Vector3[] positions = new Vector3[2];
				positions [0] = gridStart + CellDiffY / 2 + CellDiffY / 2 * i + Mathf.Floor(i / 3) * CellDiffX;
				positions [1] = positions [0] + CellDiffX * 25;
				YLinesB.Add(GetLineParameterB(-0.25f, positions [0], positions [1]));
			}
		}
	}

	private void DrawGrid()
	{
		FillLinesB();

		Handles.color = Color.black;


		for (int i = 1; i < 56; i+=2) {
			Vector3[] positions = new Vector3[2];
			positions [0] = gridStart + CellDiffX / 2 + CellDiffX / 2 * i - Mathf.Floor(i / 2) * CellDiffY;
			positions [1] = positions [0] + CellDiffY * 50;
			LinePoints points = CutLines (1.25f, XLinesB[i], positions [0], positions [1]);
			positions [0] = points.point1;
			positions [1] = points.point2;
			if (i % 2 == 0) {
				Handles.DrawAAPolyLine( 1, positions );
			} else {
				Handles.DrawAAPolyLine( 2, positions );
			}
		}

		for (int i = -15, j = 0; i < 38; i+=2, j+=2) {
			Vector3[] positions = new Vector3[2];
			positions [0] = gridStart + CellDiffY / 2 + CellDiffY / 2 * i + Mathf.Floor(i / 6) * CellDiffX;
			positions [1] = positions [0] + CellDiffX * 25;
			LinePoints points = CutLines (-0.25f, YLinesB[j], positions [0], positions [1]);
			positions [0] = points.point1;
			positions [1] = points.point2;
			if (i % 2 == 0) {
				Handles.DrawAAPolyLine( 1, positions );
			} else {
				Handles.DrawAAPolyLine( 2, positions );
			}
		}
	}

	private LinePoints CutLines(float k, float b, Vector3 point1, Vector3 point2) {
		LinePoints result = new LinePoints ();
		result.point1 = CutLinePoint(point1, k, b);
		result.point2 = CutLinePoint(point2, k, b);
		return result;
	}

	private Vector3 CutLinePoint(Vector3 point, float k, float b) {
		Vector3 result = point;
		if (result.x < -13.68f) {
			result.x = -13.68f;
			result.y = k * result.x + b;
		}
		if (result.y > 7.7f) {
			result.y = 7.7f;
			result.x = (result.y - b) / k;
		}
		if (result.x > 13.68f) {
			result.x = 13.68f;
			result.y = k * result.x + b;
		}
		if (result.y < -7.7f) {
			result.y = -7.7f;
			result.x = (result.y - b) / k;
		}
		return result;
	}

	static private float GetLineParameterB(float k, Vector3 point1, Vector3 point2) {
		return point1.y - k * point1.x;
	}

	private class LinePoints {
		public Vector3 point1;
		public Vector3 point2;
	}
	#endif
}