using UnityEngine;

public class LevelUnlockedTrigger : Trigger  {
	[SerializeField]
	private StorylineLevel _TargetLevel;
	public StorylineLevel TargetLevel
	{
		get { return _TargetLevel; }
	}

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.LevelUnlocked);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.LevelUnlocked);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		if (_TargetLevel == gameEvent.Target) {
			OnUpdate();
			OnTrigger();
		}
	}
}