using UnityEngine;

public class LevelBonusActivationTrigger : Trigger {

	public LevelBonus LevelBonus;
	public int NeededCount;
	private int CurrentCount;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		CurrentCount = 0;
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.BonusActivated);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.BonusActivated);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		LevelBonus bonus = gameEvent.Target as LevelBonus;
		if (bonus != null && bonus.ToString() == LevelBonus.ToString()) {
			CurrentCount++;
			OnUpdate();
		}
		if (CurrentCount >= NeededCount) {
			OnTrigger();
		}
	}

	override public int GetCountToComplete() {
		return NeededCount;
	}

	override public int GetCurrentCount() {
		return CurrentCount;
	}
}