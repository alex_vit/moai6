using UnityEngine;

public class Trigger : ScriptableObject{

	public delegate void TriggerCompleteDelegate();
	public delegate void TriggerUpdateDelegate();

	protected bool _Complete;
	public bool Complete {
		get {
			return _Complete;
		}
	}
	private event TriggerCompleteDelegate CompleteDelegate;
	private event TriggerUpdateDelegate UpdateDelegate;

	public virtual void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		_Complete = false;
		CompleteDelegate += completeDelegate;
		UpdateDelegate += updateDelegate;
	}

	public virtual void StopListening() {
		CompleteDelegate = null;
		UpdateDelegate = null;
	}

	public virtual int GetCountToComplete() {
		return 1;
	}

	public virtual int GetCurrentCount() {
		return _Complete ? 1 : 0;
	}

	protected void StartListening(GameEventType eventType) {
		GameEventDispatcher.Instance.AddEventListener(eventType, OnEvent);
	}

	protected void StopListening(GameEventType eventType) {
		GameEventDispatcher.Instance.RemoveEventListener(eventType, OnEvent);
	}

	protected virtual void OnEvent(GameEvent gameEvent) {
	}

	protected virtual void OnTrigger() {
		_Complete = true;
		CallDelegate();
		StopListening();
	}

	protected void CallDelegate() {
		if(CompleteDelegate != null)
			CompleteDelegate();
	}

	protected void OnUpdate() {
		if(UpdateDelegate != null)
			UpdateDelegate();
	}
}