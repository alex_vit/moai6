using System.Collections.Generic;
using UnityEngine;

public class LevelObjectStateTrigger : Trigger  {

	public enum LevelObjectStateTriggerType {
		AnyObjectOfSameType,
		AnyObjectOfSameTypeAndGrade,
		SpecificObject,
		AnyObjectOfSameGrade
	}

	public PossibleStatesDescription PossibleStatesDescription;
	public LevelObjectStateTriggerType LevelObjectStateType;
	public bool OnExitFromStates;
	public int NeededCount;
	private int CurrentCount;
	private List<LevelObject> ObjectsWaitingForStateExiting;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		CurrentCount = 0;
		base.StartListening(updateDelegate, completeDelegate);
		ObjectsWaitingForStateExiting = new List<LevelObject>();
		StartListening(GameEventType.LevelObjectStateChanged);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.LevelObjectStateChanged);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		LevelObject levelObject = gameEvent.Target as LevelObject;
		
		if (levelObject != null && (PossibleStatesDescription.LevelObject.LevelObjectType == levelObject.LevelObjectType || LevelObjectStateType == LevelObjectStateTriggerType.AnyObjectOfSameGrade)) {
			if (!OnExitFromStates && (PossibleStatesDescription.IsLevelObjectAffected(levelObject) 
			                          || (LevelObjectStateType == LevelObjectStateTriggerType.AnyObjectOfSameGrade && PossibleStatesDescription.IsLevelObjectHasSameState(levelObject))) || 
			      (OnExitFromStates && !PossibleStatesDescription.IsLevelObjectAffected(levelObject) && ObjectsWaitingForStateExiting.Contains(levelObject))) {

				if (ObjectsWaitingForStateExiting.Contains(levelObject)) {
					ObjectsWaitingForStateExiting.Remove(levelObject);
				}
				bool conditionsAreMet = true;
				if ((LevelObjectStateType == LevelObjectStateTriggerType.AnyObjectOfSameTypeAndGrade || LevelObjectStateType == LevelObjectStateTriggerType.AnyObjectOfSameGrade) && PossibleStatesDescription.LevelObject.LevelObjectGrade != levelObject.LevelObjectGrade) {
					conditionsAreMet = false;
				}
				if (LevelObjectStateType == LevelObjectStateTriggerType.SpecificObject && levelObject != PossibleStatesDescription.LevelObject) {
					conditionsAreMet = false;
				}
				if (conditionsAreMet) {
					CurrentCount++;
					OnUpdate();
				}
			} else if (OnExitFromStates && PossibleStatesDescription.IsLevelObjectAffected(levelObject)) {
				ObjectsWaitingForStateExiting.Add(levelObject);
			}

		}
		if (CurrentCount >= NeededCount) {
			OnTrigger();
		}
	}

	override public int GetCountToComplete() {
		return NeededCount;
	}

	override public int GetCurrentCount() {
		return CurrentCount;
	}
}