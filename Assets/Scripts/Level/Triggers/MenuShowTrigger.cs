﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuShowTrigger : Trigger {
    [SerializeField]
    private LevelObject Target;
    
    override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
        base.StartListening(updateDelegate, completeDelegate);
        StartListening(GameEventType.ContextMenuShowed);
    }

    override public void StopListening() {
        base.StopListening();
        StopListening(GameEventType.ContextMenuShowed);
    }

    override protected void OnEvent(GameEvent gameEvent) {
        if (Target == gameEvent.Target as LevelObject) {
            Debug.Log("gameEvent.Target " + gameEvent.Target);
            OnUpdate();
            OnTrigger();
        }
    }
}
