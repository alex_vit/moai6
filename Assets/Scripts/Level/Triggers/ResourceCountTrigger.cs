using UnityEngine;

public class ResourceCountTrigger : Trigger  {

	public GameObject ResourcePrefab;
	public int NeededTotalCount;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.ResourceValueChanged);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		if (Level.Instance.GetResourceCount(ResourcePrefab) < NeededTotalCount) {
			_Complete = false;
		}
		OnUpdate();
		if (Level.Instance.GetResourceCount(ResourcePrefab) >= NeededTotalCount && !_Complete) {
			OnTrigger();
		}
	}

	override public int GetCountToComplete() {
		return NeededTotalCount;
	}

	override public int GetCurrentCount() {
		return Level.Instance.GetResourceCount(ResourcePrefab);
	}
	
	override protected void OnTrigger() {
		_Complete = true;
		CallDelegate();
	}
}