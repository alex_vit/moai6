﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawnTrigger : Trigger
{
	[SerializeField] private LevelObject Target;
	private Coroutine _delayProcess = null;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate)
	{
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.LevelObjectSpawned);
		Debug.Log("start listen");
	}

	override public void StopListening()
	{
		base.StopListening();
		StopListening(GameEventType.LevelObjectSpawned);
		if (_delayProcess != null)
		{
			Level.Instance.StopCoroutine(_delayProcess);
			_delayProcess = null;
		}
	}

	override protected void OnEvent(GameEvent gameEvent)
	{
		if (gameEvent.Target != null)
		{
			if (Target.LevelObjectType ==  (gameEvent.Target as LevelObject).LevelObjectType)
			{
				Debug.Log("compare succ");
				if(_delayProcess == null)
					_delayProcess = Level.Instance.StartCoroutine(Counter());
			}
		}
	}

	IEnumerator Counter()
	{	
		yield return new WaitForSeconds(3f);
		OnUpdate();
		OnTrigger();
	}
	
}
