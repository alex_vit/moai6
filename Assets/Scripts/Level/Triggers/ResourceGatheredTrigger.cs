using UnityEngine;

public class ResourceGatheredTrigger : Trigger {

	public GameObject ResourcePrefab;
	public int NeededGatheredCount;
	private int CurrentCount;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		base.StartListening(updateDelegate, completeDelegate);
		CurrentCount = 0;
		StartListening(GameEventType.ResourceGathered);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.ResourceGathered);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		GatheredDrop resource = gameEvent.Target as GatheredDrop;
		if (resource != null) {
			Drop drop = ResourcePrefab.GetComponentInChildren<Drop>();
			if (resource.ResourceType == drop.ResourceType) {
				CurrentCount += resource.GatheredCount;
				OnUpdate();
			}
		}
		if (CurrentCount >= NeededGatheredCount) {
			OnTrigger();
		}
	}

	override public int GetCountToComplete() {
		return NeededGatheredCount;
	}

	override public int GetCurrentCount() {
		return CurrentCount;
	}

}