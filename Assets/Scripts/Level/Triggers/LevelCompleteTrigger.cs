using UnityEngine;

public class LevelCompleteTrigger : Trigger  {
	[SerializeField]
	private StorylineLevel TargetLevel;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.LevelComplete);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.LevelComplete);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		if (TargetLevel == gameEvent.Target) {
			OnUpdate();
			OnTrigger();
		}
	}
}