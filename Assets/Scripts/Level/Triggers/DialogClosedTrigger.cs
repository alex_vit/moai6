using UnityEngine;

public class DialogClosedTrigger : Trigger  {

	public Dialog TargetDialog;

	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.DialogHide);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.DialogHide);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		Dialog dialog = gameEvent.Target as Dialog;
		if (dialog == TargetDialog) {
			Debug.Log("catch event");
			OnUpdate();
			OnTrigger();
		}
	}
}