using UnityEngine;

public class LevelObjectPathBlockTrigger : Trigger{

	[SerializeField]
	private bool Blocked;
	[SerializeField]
	private PossibleStatesDescription PossibleStatesDescription;


	override public void StartListening(TriggerUpdateDelegate updateDelegate, TriggerCompleteDelegate completeDelegate) {
		base.StartListening(updateDelegate, completeDelegate);
		StartListening(GameEventType.LevelObjectBlockChanged);
	}

	override public void StopListening() {
		base.StopListening();
		StopListening(GameEventType.LevelObjectBlockChanged);
	}

	override protected void OnEvent(GameEvent gameEvent) {
		LevelObject levelObject = gameEvent.Target as LevelObject;
		if (levelObject != null && PossibleStatesDescription.LevelObject == levelObject && PossibleStatesDescription.IsLevelObjectAffected(levelObject) && levelObject.IsPathBlocked() == Blocked) {
			OnTrigger();
		}
	}
}