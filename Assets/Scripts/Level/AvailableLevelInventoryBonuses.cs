using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/AvailableLevelInventoryBonuses", fileName="InventoryBonusesForLevel")]
public class AvailableLevelInventoryBonuses : ScriptableObject {
	public List<LevelBonus> Bonuses;
}