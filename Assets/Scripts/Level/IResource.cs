using UnityEngine;

public interface IResource {
	Sprite ResourceIcon { get;}
	int MinimalCountToShowInTooltip { get;}
	int ScoresModifier {get;}
}