using System.Collections.Generic;

public class TaskInQueue {
	public LevelObject LevelObject;
	public Task Task;
	public List<TaskInQueueWorker> Workers;
	public string PreviousStateName;
	public List<TaskCost> ActualCosts;

	public string GetWorkerState(TaskWorker taskWorker) {
		for (int i = 0; i < Workers.Count; i++) {
			TaskInQueueWorker taskInQueueWorker = Workers[i];
			if (taskInQueueWorker.Worker == taskWorker) {
				return taskInQueueWorker.WorkerInTaskState;
			}
		}
		return null;
	}

	public string GetWorkerAfterState(TaskWorker taskWorker) {
		for (int i = 0; i < Workers.Count; i++) {
			TaskInQueueWorker taskInQueueWorker = Workers[i];
			if (taskInQueueWorker.Worker == taskWorker) {
				return taskInQueueWorker.WorkerAfterTaskState;
			}
		}
		return null;
	}

	public int GetRemainWorkersCount() {
		int result = 0;
		for (int i = 0; i < Workers.Count; i++) {
			TaskInQueueWorker taskInQueueWorker = Workers[i];
			if (taskInQueueWorker.Worker == null) {
				result++;
			}
		}
		return result;
	}
}