﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager {

	private List<TaskWorker> TaskWorkers;
	private List<TaskInQueue> TasksQueue;

	public TaskManager() {
		TaskWorkers = new List<TaskWorker> () { };
		TasksQueue = new List<TaskInQueue> () {};
	}

	public bool ContainsWorker(TaskWorker worker) {
		return TaskWorkers.Contains(worker);
	}

	public void AddWorker(TaskWorker newWorker) {
		TaskWorkers.Add (newWorker);
		Level.Instance.StartCoroutine(CheckTasksToStart());
	}

	public void RemoveWorker(TaskWorker worker) {
		TaskWorkers.Remove(worker);
	}

	public void AddTask(LevelObject levelObject, Task task) {
		TaskInQueue taskInQueue = new TaskInQueue();
		taskInQueue.Workers = new List<TaskInQueueWorker>(){};
		taskInQueue.ActualCosts = task.GetCurrentCosts();
		for (int i = 0; i < taskInQueue.ActualCosts.Count; i++) {
			TaskCost cost = taskInQueue.ActualCosts[i];
			if (cost.GetCostType() == TaskCostType.WorkerCost) {
				for (int j = 0; j < cost.GetCount(); j++) {
					TaskInQueueWorker taskInQueueWorker = new TaskInQueueWorker();
					taskInQueueWorker.WorkerType = cost.GetResourceType();
					taskInQueueWorker.WorkerInTaskState = cost.TaskWorkerState;
					taskInQueueWorker.WorkerAfterTaskState = cost.TaskWorkerStateAfter;
					taskInQueue.Workers.Add(taskInQueueWorker);
				}
			}
			if (!cost.IsAvailable()) {
				return;
			}
		}
		taskInQueue.PreviousStateName = levelObject.CurrentStateName;
		taskInQueue.LevelObject = levelObject;
		taskInQueue.Task = task;
		levelObject.CurrentStateName = task.TargetStateDescription.GetStateName();
		if (levelObject.CurrentState is StateWaitingForWorkers) {
			(levelObject.CurrentState as StateWaitingForWorkers).SetWorkersTask(taskInQueue);
		}
		TasksQueue.Add(taskInQueue);
		UpdateLevelResources(taskInQueue, -1);
		Level.Instance.StartCoroutine(CheckTasksToStart());
	}

	public void CancelTask(TaskInQueue taskInQueue) {
		taskInQueue.LevelObject.CurrentStateName = taskInQueue.PreviousStateName;
		for (int i = 0; i < taskInQueue.Workers.Count; i++) {
			TaskInQueueWorker taskInQueueWorker = taskInQueue.Workers[i];
			if (taskInQueueWorker.Worker != null) {
				taskInQueueWorker.Worker.CancelTask();
			}
		}
		taskInQueue.LevelObject.FreeWorkingPoints();
		TasksQueue.Remove(taskInQueue);
		UpdateLevelResources(taskInQueue, 1);
		Level.Instance.StartCoroutine(CheckTasksToStart());
	}

	public void CompleteTask(TaskInQueue taskInQueue) {
		taskInQueue.LevelObject.FreeWorkingPoints();
		for (int i = 0; i < taskInQueue.Workers.Count; i++) {
			TaskInQueueWorker taskInQueueWorker = taskInQueue.Workers[i];
			if (taskInQueueWorker.Worker != null) {
				taskInQueueWorker.Worker.CompleteTask();
			}
		}
		TasksQueue.Remove(taskInQueue);
		Level.Instance.StartCoroutine(CheckTasksToStart());
	}

	private void UpdateLevelResources(TaskInQueue taskInQueue, int modifier) {
		for (int i = 0; i < taskInQueue.ActualCosts.Count; i++) {
			TaskCost cost = taskInQueue.ActualCosts[i];
			if (cost.GetCostType() == TaskCostType.ResourceCost) {
				Level.Instance.ModifyResourceCount(cost.GetResourceType(), cost.GetCount() * modifier);
			}
		}
	}

	private IEnumerator CheckTasksToStart() {
		yield return new WaitForEndOfFrame();
		for (int i = 0; i < TasksQueue.Count; i++) {
			TaskInQueue taskInQueue = TasksQueue[i];
			float minDistance = float.MaxValue;
			TaskWorker closestWorker = null;
			TaskInQueueWorker closestTaskInQueueWorker = null;
			for (int j = 0; j < TaskWorkers.Count; j++) {
				TaskWorker worker = TaskWorkers[j];
				if (!worker.LevelObject.IsPathBlocked() && worker.CurrentTask == null) {
					for (int n = 0; n < taskInQueue.Workers.Count; n++) {
						TaskInQueueWorker taskInQueueWorker = taskInQueue.Workers[n];
						if (taskInQueueWorker.Worker == null && taskInQueueWorker.WorkerType == worker.ResourceType) {
							WorkingPoint workingPoint = taskInQueue.LevelObject.GetNearestWorkingPoint(worker.LevelObject, taskInQueueWorker.WorkerInTaskState);
							if (workingPoint != null) {
								float distance = Level.Instance.Road.GetDistance(worker.transform.position, workingPoint);
								if (distance < minDistance) {
									minDistance = distance;
									closestWorker = worker;
									closestTaskInQueueWorker = taskInQueueWorker;
								}
								break;
							}
						}
					}
				}
			}
			if (closestWorker != null) {
				closestTaskInQueueWorker.Worker = closestWorker;
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelTaskStarted, taskInQueue.Task));
				closestWorker.StartTask(taskInQueue);
				Level.Instance.StartCoroutine(CheckTasksToStart());
			}
		}
	}
}