﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent (typeof (LevelObject))]
public class TaskWorker : MonoBehaviour, IResource {

	private TaskInQueue _CurrentTask;

	private LevelObject _LevelObject;
	private int currentTaskInSequenceIndex;
	[SerializeField]
	private string _WorkerType;
	[SerializeField]
	private Sprite _WorkerIcon;
	[SerializeField]
	private int _MinimalCountToShowInTooltip;
	[SerializeField]
	private int _ScoresModifier;
	[SerializeField]
	private GameObject _WorkerHomePrefab;
	private string _WorkerHomeLevelObjectType;
	private LevelObjectUnits _WorkerHome;
	private string PreviousStateName;

	public LevelObject LevelObject { get {return _LevelObject;} }
	public Sprite ResourceIcon { get {return _WorkerIcon;} }
	public string ResourceType { get {return _WorkerType;} }
	public int MinimalCountToShowInTooltip { get {return _MinimalCountToShowInTooltip;}}
	public int ScoresModifier {get {return _ScoresModifier;}}

	public TaskInQueue CurrentTask { get {return _CurrentTask;}}

	public void Awake() {
		_WorkerHomeLevelObjectType = _WorkerHomePrefab.GetComponent<LevelObject>().LevelObjectType;
	}

	public void OnEnable() {
		_LevelObject = this.gameObject.GetComponent<LevelObject> ();
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusActivated, OnBonus);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusDeactivated, OnBonus);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectBlockChanged, UpdateBlocked);
		FindNearestHome();
		UpdateBlocked();
		UpdateSpeed();
	}

	public void OnDisable() {
		if (Level.Instance) {
			if (Level.Instance.TaskManager.ContainsWorker(this)) {
				Level.Instance.ModifyResourceCount(_WorkerType, -1);
				Level.Instance.TaskManager.RemoveWorker (this);
			}
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusActivated, OnBonus);
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusDeactivated, OnBonus);
			GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectBlockChanged, UpdateBlocked);
			if (_WorkerHome != null) _WorkerHome.RemoveUnit(gameObject);
		}
	}

	public void StartTask(TaskInQueue newTask) {
		_CurrentTask = newTask;
		PreviousStateName = _LevelObject.CurrentStateName;
		_LevelObject.CurrentStateType = typeof(WorkerRunningState);
		(_LevelObject.CurrentState as WorkerRunningState).OnRunningCompleteHandler += OnRunningComplete;
		(_LevelObject.CurrentState as WorkerRunningState).RunTo(_CurrentTask.LevelObject, _CurrentTask.GetWorkerState(this));
	}

	public void CompleteTask() {
		LevelObject.CurrentStateName = CurrentTask.GetWorkerAfterState(this);
		_CurrentTask = null;
	}

	public void CancelTask() {
		LevelObject.CurrentStateName = PreviousStateName;
		_CurrentTask = null;
	}

	public void OnRunningComplete() {
		LevelObject.CurrentStateName = CurrentTask.GetWorkerState(this);
		if (_CurrentTask != null && _CurrentTask.LevelObject != null && _CurrentTask.LevelObject.CurrentState != null) {
			(_CurrentTask.LevelObject.CurrentState as StateWaitingForWorkers).OnWorkerRunningComplete(this);
		}
	}

	private void UpdateBlocked(GameEvent gameEvent = null) {
		LevelObject levelObject = gameEvent != null ? gameEvent.Target as LevelObject : null;
		if (levelObject == _LevelObject || gameEvent == null) {
			if (!_LevelObject.IsPathBlocked()) {
				if (!Level.Instance.TaskManager.ContainsWorker(this)) {
					Level.Instance.ModifyResourceCount(_WorkerType, 1);
					Level.Instance.TaskManager.AddWorker(this);
				}
			} else {
				if (Level.Instance.TaskManager.ContainsWorker(this)) {
					Level.Instance.ModifyResourceCount(_WorkerType, -1);
					Level.Instance.TaskManager.RemoveWorker(this);
				}
			}
		}
	}

	private void FindNearestHome() {
		List<LevelObject> possibleHomes = Level.Instance.GetLevelObjectsOfType(_WorkerHomeLevelObjectType);
		_WorkerHome = null;
		float closestHomeDistance = float.MaxValue;
		foreach (LevelObject possibleHome in possibleHomes) {
			float distance = Vector3.Distance(transform.position, possibleHome.transform.position);
			LevelObjectUnits levelObjectUnits = possibleHome.GetComponent<LevelObjectUnits>();
			if (distance < closestHomeDistance && levelObjectUnits.GetFreeSlotsCount() > 0) {
				closestHomeDistance = distance;
				_WorkerHome = levelObjectUnits;
			}
		}
		if (_WorkerHome != null) _WorkerHome.AddUnit(gameObject);
	}

	private void OnBonus(GameEvent gameEvent) {
		if (gameEvent.Target is SpeedUpWorkersBonus) {
			UpdateSpeed();
		}
	}

	private void UpdateSpeed() {
		float speedModifier = 1.0f;
		List<SpeedUpWorkersBonus> speedUpWorkersBonuses = Level.Instance.GetActiveBonuses<SpeedUpWorkersBonus>(LevelObject);
		foreach (SpeedUpWorkersBonus speedUpWorkersBonus in speedUpWorkersBonuses) {
			speedModifier *= speedUpWorkersBonus.SpeedModifier;
		}
		if(LevelObject == null || LevelObject.LevelObjectAnimator == null || LevelObject.LevelObjectAnimator.parameters == null)
			return;
		if(LevelObject.LevelObjectAnimator.parameters.Any(x => x.name == "SpeedMultiplier"))
			LevelObject.LevelObjectAnimator.SetFloat("SpeedMultiplier", speedModifier);
	}
}
