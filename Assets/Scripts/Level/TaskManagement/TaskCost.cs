using UnityEngine;

[System.Serializable]
public class TaskCost {
	[SerializeField]
	private TaskCostType CostType;

	[SerializeField]
	private GameObject ResourcePrefab;
	[SerializeField]
	private int ResourceCount;

	[SerializeField]
	private string _TaskWorkerState;
	[SerializeField]
	private string _TaskWorkerStateAfter;

	[SerializeField]
	private int GoalIndex;

	public TaskCost(TaskCost taskCost) {
		CostType = taskCost.CostType;
		ResourcePrefab = taskCost.ResourcePrefab;
		ResourceCount = taskCost.ResourceCount;
		_TaskWorkerState = taskCost._TaskWorkerState;
		_TaskWorkerStateAfter = taskCost._TaskWorkerStateAfter;
		GoalIndex = taskCost.GoalIndex;
	}

	public TaskCostType GetCostType() {
		return CostType;
	}

	public bool IsAvailable() {
		if (CostType == TaskCostType.LevelGoalCost) {
			return Level.Instance.IsGoalComplete(GoalIndex);
		}
		return ResourceCount <= Level.Instance.GetResourceCount(ResourcePrefab);
	}

	public Sprite GetIcon() {
		return (ResourcePrefab.GetComponentInChildren(typeof(IResource)) as IResource).ResourceIcon;
	}

	public int GetMinimalCountToShowInTooltip() {
		return ResourcePrefab != null ? (ResourcePrefab.GetComponentInChildren(typeof(IResource)) as IResource).MinimalCountToShowInTooltip : 0;
	}

	public void SetCount(int count) {
		ResourceCount = count;
	}

	public int GetCount() {
		return CostType == TaskCostType.LevelGoalCost ? (GoalIndex + 1) : ResourceCount;
	}

	public string GetResourceType() {
		return ResourcePrefab != null ? ResourcePrefab.name : "Goal";
	}

	public string TaskWorkerState {
		get {
			return _TaskWorkerState;
		}
	}

	public string TaskWorkerStateAfter {
		get {
			return _TaskWorkerStateAfter;
		}
	}
}