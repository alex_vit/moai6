﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Task {
	public TooltipType TooltipType;
	public TaskIcon TaskIcon;
	public string TaskName;
	public BaseStateDescription TargetStateDescription;
	[SerializeField]
	private List<TaskCost> TaskCosts;
	private LevelObject Owner;

	public List<TaskCost> GetCurrentCosts() {
		List<TaskCost> result = new List<TaskCost>();
		List<ReduceTaskCostBonus> bonuses = Level.Instance.GetActiveBonuses<ReduceTaskCostBonus>(Owner);
		for (int i = 0; i < TaskCosts.Count; i++) {
			TaskCost cost = TaskCosts[i];
			TaskCost resultCost = new TaskCost(cost);
			if (cost.GetCostType() == TaskCostType.ResourceCost) {
				int costModifier = 1;
				for (int j = 0; j < bonuses.Count; j++) {
					ReduceTaskCostBonus reduceTaskCostBonus = bonuses[j];
					costModifier *= reduceTaskCostBonus.CostModifier;
				}
				if (cost.GetCostType() == TaskCostType.ResourceCost) {
					resultCost.SetCount(cost.GetCount() / costModifier);
				}
			}
			result.Add(resultCost);
		}
		return result;
	}

	public void SetOwner(LevelObject owner) {
		Owner = owner;
	}

	public bool IsTaskAvailable() {
		for (int i = 0; i < TaskCosts.Count; i++) {
			TaskCost cost = TaskCosts[i];
			if (!cost.IsAvailable()) {
				return false;
			}
		}
		return true;
	}
}
