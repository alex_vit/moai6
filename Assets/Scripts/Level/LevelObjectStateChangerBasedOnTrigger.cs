﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelObjectStateChangerBasedOnTrigger : TriggeredBehaviour
{
    private LevelObject _levelObject;

    public override void Awake() {
        base.Awake();
        _levelObject = GetComponent<LevelObject>();
    }
    
    protected override void TriggersComplete()
    {
        base.TriggersComplete();
        if(Triggers == null || Triggers.Count == 0)
            return;
        if (_levelObject != null && _levelObject.CurrentState != null)
            _levelObject.CurrentState.GotoNextState();
    }
}
