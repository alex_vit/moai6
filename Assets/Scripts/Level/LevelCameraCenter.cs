﻿using UnityEngine;

[ExecuteInEditMode]
public class LevelCameraCenter : MonoBehaviour {

	[Range(-1.0f, 1.0f)]
	public float CameraOffset = 0.0f;

	public void MoveCamera() {
		if (Camera.main) {
			double screenHeight = Camera.main.orthographicSize * 2.0;
			double currentScreenWidth = screenHeight * Camera.main.aspect;
			double maximumScreenWidth = screenHeight * 16 / 9;
			float offset = (float) (maximumScreenWidth - currentScreenWidth) * CameraOffset / 2;
			Camera.main.transform.position = new Vector3(offset, Camera.main.transform.position.y, Camera.main.transform.position.z);
		}
	}

	public void Awake() {
		MoveCamera();
	}

	public void OnValidate()
	{
		MoveCamera();
	}
}
