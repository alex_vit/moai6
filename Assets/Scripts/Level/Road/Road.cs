﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Road : MonoBehaviour {
#if UNITY_EDITOR
	public List<RoadCollection> RoadCollections;
	public RoadSpritesCollection RoadSpritesCollection;
#endif

	[SerializeField]
	private Waypoint StartPoint;
	[SerializeField]
	private GameObject WaypointContainer;
	private List<Waypoint> _AllWaypoints;
	private Dictionary<Waypoint, List<LevelObjectPathBlocker>> Pathblockers;
	private Dictionary<Waypoint, List<List<Waypoint>>> CachedWays;

	public const float NearestPointDistance = 0.6f;

	public List<Waypoint> AllWaypoints {
		get {
			if (_AllWaypoints == null) {
				_AllWaypoints = new List<Waypoint>();
				List<List<Waypoint>> allPaths = GetAllPathsFromPoint(StartPoint);
				foreach (List<Waypoint> path in allPaths) {
					foreach (Waypoint waypoint in path) {
						if (!_AllWaypoints.Contains(waypoint)) {
							_AllWaypoints.Add(waypoint);
						}
					}
				}
			}
			return _AllWaypoints;
		}
	}

	public void Awake() {
		CachedWays = new Dictionary<Waypoint, List<List<Waypoint>>>();
		Pathblockers = new Dictionary<Waypoint, List<LevelObjectPathBlocker>>();
	}

	public Vector3 GetStartPointPosition() {
		return StartPoint.GetParameters().Position;
	}

	public void AddPathblocker(LevelObjectPathBlocker pathBlocker, Waypoint waypoint) {
		if (!Pathblockers.ContainsKey(waypoint)) {
			Pathblockers.Add(waypoint, new List<LevelObjectPathBlocker>());
		}
		Pathblockers[waypoint].Add(pathBlocker);
	}

	public void AddPathblocker(LevelObjectPathBlocker pathBlocker, WaypointParameters point) {
		Waypoint nearestPoint = GetNearestWaypoint(point, true);
		if (nearestPoint != null) {
			AddPathblocker(pathBlocker, nearestPoint);
		}
	}

	public void RemovePathblocker(LevelObjectPathBlocker pathBlocker) {
		foreach (List<LevelObjectPathBlocker> levelObjectPathBlockers in Pathblockers.Values) {
			if (levelObjectPathBlockers.Contains(pathBlocker)) {
				levelObjectPathBlockers.Remove(pathBlocker);
			}
		}
	}

	private bool IsWaypointBlocked(Waypoint waypoint) {
		if (waypoint != null && Pathblockers.ContainsKey(waypoint) && Pathblockers[waypoint].Count > 0) {
			return true;
		}
		return false;
	}

	public List<WaypointParameters> GetRandomPath(WaypointParameters startPosition, LevelObject restrictWayTo) {
		Waypoint randomPoint = null;
		while (randomPoint == null || IsWaypointBlocked(randomPoint)) {
			randomPoint = AllWaypoints[Random.Range(0, AllWaypoints.Count)];
		}
		return GetPath(startPosition, randomPoint.GetParameters(), false, restrictWayTo);
	}

	public Waypoint GetNearestWaypoint(WaypointParameters position, bool onlyNearestWaypoint) {
		Waypoint result = null;
		float minDistance = float.MaxValue;
		foreach (Waypoint wayPoint in AllWaypoints)
		{
			if( wayPoint.Neighbors == null ) {
				continue;
			}
			Vector3 waypointPosition = wayPoint.transform.position;
			if (position != null) {
				float distance = Vector3.Distance (waypointPosition, position.Position);
				if (distance < minDistance && (!onlyNearestWaypoint || distance <= NearestPointDistance)) {
					minDistance = distance;
					result = wayPoint;
				}
			}
		}
		return result;
	}

	public bool HasBlockerNear(WaypointParameters workingPointPosition) {
		if (IsWaypointBlocked(GetNearestWaypoint(workingPointPosition, true))) {
			return true;
		}
		return false;
	}

	public bool HasPathFromStart(WaypointParameters endPosition, bool onlyNearestWaypoint, bool ignoreBlocking = false) {
		if(StartPoint != null)
			return HasPath(StartPoint.GetParameters(), endPosition, onlyNearestWaypoint, ignoreBlocking);
		return false;
	}

	public bool HasPath(WaypointParameters startPosition, WaypointParameters endPosition, bool onlyNearestWaypoint, bool ignoreBlocking = false) {
		List<WaypointParameters> path = GetPath(startPosition, endPosition, onlyNearestWaypoint, null, ignoreBlocking);
		return path != null && path.Count > 0;
	}

	public float GetDistance(Vector3 startPoint, WaypointParameters endPoint) {
		return GetDistance(new WaypointParameters(startPoint, 1.0f), endPoint);
	}

	public float GetDistance(WaypointParameters startPoint, WaypointParameters endPoint) {
		List<WaypointParameters> path = GetPath(startPoint, endPoint, false);
		float result = 0.0f;
		if (path == null) {
			if (Vector3.Distance(startPoint.Position, endPoint.Position) > 0.6f) {
				result = float.MaxValue;
			}
		} else {
			for (int i = 0; i < path.Count - 1; i++) {
				result += Vector3.Distance(path[i].Position, path[i + 1].Position);
			}
		}
		return result;
	}

	public List<WaypointParameters> GetPath(WaypointParameters startPoint, WaypointParameters endPoint, bool onlyNearestWaypoint, LevelObject restrictWayTo = null, bool ignoreBlocking = false) {
		Waypoint startWaypoint = GetNearestWaypoint(startPoint, false);
		if (!CachedWays.ContainsKey(startWaypoint)) {
			CachedWays.Add(startWaypoint, GetAllPathsFromPoint (startWaypoint));
		}
		Waypoint endWaypoint = GetNearestWaypoint(endPoint, onlyNearestWaypoint);
		if (endWaypoint == null) {
			return null;
		}
		List<List<Waypoint>> allPathsFromStart = CachedWays[startWaypoint];
		List<Waypoint> minimalWay = null;
		foreach (List<Waypoint> originalWay in allPathsFromStart) {
			List<Waypoint> way = new List<Waypoint>(originalWay);
			way.Reverse ();
			int endPointIndex = way.IndexOf(endWaypoint);
			if (endPointIndex > -1) {
				way.RemoveRange (endPointIndex + 1, way.Count - (endPointIndex + 1));
				if (way.Count > 1) {
					if (GeometryUtils.IsPointInRectangle(startPoint.Position, way[0].GetParameters().Position, way[1].GetParameters().Position)) {
						way.RemoveAt(0);
					}
				}
				if (way.Count > 1) {
					if (GeometryUtils.IsPointInRectangle(endPoint.Position, way[way.Count - 1].GetParameters().Position, way[way.Count - 2].GetParameters().Position)) {
						way.RemoveAt(way.Count - 1);
					}
				}
				if (way.Count > 0) {
					if (GeometryUtils.IsPointInRectangle(endPoint.Position, startPoint.Position, way[0].GetParameters().Position)) {
						way.RemoveAt(0);
					}
				}

				bool wayBlocked = false;
				if (!ignoreBlocking)
				{
					foreach (Waypoint waypoint in way)
					{
						if (IsWaypointBlocked(waypoint) ||
						    (restrictWayTo != null && !waypoint.GetParameters().IsLevelObjectAllowed(restrictWayTo)))
						{
							wayBlocked = true;
							break;
						}
					}
				}
				if (!wayBlocked && (minimalWay == null || minimalWay.Count > way.Count)) {
					minimalWay = way;
				}
			}
		}
		List<WaypointParameters> result = null;
		if (minimalWay != null || Vector3.Distance(startPoint.Position, endPoint.Position) < 0.6f) {
			result = new List<WaypointParameters>();
			result.Add(startPoint);
			if (minimalWay != null) {
				foreach (Waypoint waypoint in minimalWay) {
					result.Add(waypoint.GetParameters());
				}
			}
			result.Add(endPoint);
		}
		return result;
	}

	private List<List<Waypoint>> GetAllPathsFromPoint(Waypoint point, List<Waypoint> visitedPoints = null) {
		List<List<Waypoint>> result = new List<List<Waypoint>> ();
		if (visitedPoints == null) {
			visitedPoints = new List<Waypoint> ();
		}
		visitedPoints.Add (point);
		foreach (Waypoint neighbor in point.Neighbors) {
			if (!visitedPoints.Contains(neighbor)) {
				List<List<Waypoint>> neighborPaths = GetAllPathsFromPoint (neighbor, new List<Waypoint>(visitedPoints));
				foreach (List<Waypoint> way in neighborPaths) {
					way.Add (point);
					result.Add (way);
				}
			}
		}
		if (result.Count == 0) {
			List<Waypoint> ownWay = new List<Waypoint> ();
			ownWay.Add (point);
			result.Add (ownWay);
		}
		return result;
	}

	#if UNITY_EDITOR
	public void OnDrawGizmos()
	{
		DrawRoad (Color.green);
	}
		
	public void OnDrawGizmosSelected()
	{
		DrawRoad (Color.blue);
	}

	private void DrawRoad(Color color)
	{
		Handles.color = color;

		GameObject waypointGameObject = GameObject.Find("WaypointContainer");
		if (waypointGameObject) {
			foreach (Transform child in waypointGameObject.transform) {
				Waypoint   t = child.GetComponent<Waypoint>();

				if ( t == null || t.Neighbors == null )
					return;
				Vector3 center = t.transform.position;
				Vector3[] positions = new Vector3[2];
				positions[0] = t.transform.position;
				for ( int i = 0; i < t.Neighbors.Count; i++ ) {
					if (t.Neighbors[i] != null && t.Neighbors[i].Neighbors != null && t.Neighbors[i].Neighbors.Contains(t)) {
						positions[1] = t.Neighbors[i].transform.position;
						Handles.DrawAAPolyLine(3, positions);
					}
				}
			}
		}
	}
	#endif
}
