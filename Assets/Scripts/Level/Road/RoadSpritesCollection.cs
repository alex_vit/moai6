using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Road/Road Sprites Collection", fileName="RoadSprites")]
public class RoadSpritesCollection : ScriptableObject{
	public List<RoadCollection> RoadCollections;
}