﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class Waypoint : MonoBehaviour {
	[SerializeField]
	private float _SpeedModifier = 1.0f;
	[SerializeField]
	private List<PossibleStatesDescription> RestrictToSpecificObjectsStates;
	[SerializeField]
	private int MultipleAnimationsIndex;
	public int RoadSprite = -1;
	public List<Waypoint> Neighbors;

	public WaypointParameters GetParameters() {
		return new WaypointParameters(transform.position, _SpeedModifier, MultipleAnimationsIndex, RestrictToSpecificObjectsStates);
	}

	#if UNITY_EDITOR
	public void OnDestroy ()
	{
		if (Neighbors.Count == 2) {
			Neighbors[0].Neighbors.Add(Neighbors[1]);
			Neighbors[1].Neighbors.Add(Neighbors[0]);
		}
		foreach(Waypoint neighbor in Neighbors)
		{
			neighbor.Neighbors.Remove(this);
		}
	}

	public void OnDrawGizmos()
	{
		DrawPoint ();
	}
		
	public void OnDrawGizmosSelected()
	{
		DrawPoint ();
	}

	private void DrawPoint()
	{
		Gizmos.DrawIcon(transform.position, "point.tif", true);
	}
	#endif
}
