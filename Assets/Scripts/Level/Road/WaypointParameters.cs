using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaypointParameters {
	public Vector3 Position;
	[SerializeField]
	protected float SpeedModifier;
	public int MultipleAnimationsIndex;
	[SerializeField]
	protected List<PossibleStatesDescription> RestrictToSpecificObjectsStates;

	public WaypointParameters() {
	}

	public WaypointParameters(Vector3 position, float speedModifier) {
		Position = position;
		SpeedModifier = speedModifier;
		MultipleAnimationsIndex = 0;
		RestrictToSpecificObjectsStates = new List<PossibleStatesDescription>();
	}

	public WaypointParameters(Vector3 position, float speedModifier, int multipleAnimationsIndex, List<PossibleStatesDescription> restrictToSpecificObjectsStates) {
		Position = position;
		SpeedModifier = speedModifier;
		MultipleAnimationsIndex = multipleAnimationsIndex;
		RestrictToSpecificObjectsStates = new List<PossibleStatesDescription>(restrictToSpecificObjectsStates);
	}

	public float GetSpeedModifier() {
		float result = SpeedModifier;
		List<WaypointSpeedLimitBonus> waypointSpeedLimitBonuses = Level.Instance.GetActiveBonuses<WaypointSpeedLimitBonus>();
		for (int i = 0; i < waypointSpeedLimitBonuses.Count; i++) {
			WaypointSpeedLimitBonus waypointSpeedLimitBonus = waypointSpeedLimitBonuses[i];
			result = waypointSpeedLimitBonus.GetLimitedSpeed(result);
		}
		return result;
	}

	public bool IsLevelObjectInRestrictedList(LevelObject levelObject, string targetState = null) {
		if (RestrictToSpecificObjectsStates != null && RestrictToSpecificObjectsStates.Count > 0) {
			for (int i = 0; i < RestrictToSpecificObjectsStates.Count; i++) {
				PossibleStatesDescription specificLevelObjectState = RestrictToSpecificObjectsStates[i];
				if (specificLevelObjectState.IsLevelObjectAffected(levelObject, targetState)) {
					return true;
				}
			}
		}
		return false;
	}

	public bool IsLevelObjectAllowed(LevelObject levelObject, string targetState = null) {
		if (RestrictToSpecificObjectsStates != null && RestrictToSpecificObjectsStates.Count > 0) {
			return IsLevelObjectInRestrictedList(levelObject, targetState);
		}
		return true;
	}
}