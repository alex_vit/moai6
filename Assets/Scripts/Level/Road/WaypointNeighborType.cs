[System.Flags]
public enum WaypointNeighborType {
	Left = 0x1,
	Up = 0x2,
	Right = 0x4,
	Down = 0x8
}