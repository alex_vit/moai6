using System.Collections.Generic;

[System.Serializable]
public class RoadCollection {
	public string Name;
	public List<RoadSprite> RoadSprites;
}