using UnityEngine;

[System.Serializable]
public class RoadSprite {
	public Sprite Sprite;
	[EnumFlagsAttribute]
	public WaypointNeighborType Types;
}