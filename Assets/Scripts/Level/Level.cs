using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour{
	static private Level _Instance;
	static public Level Instance { get {return _Instance;} }
	public ContextMenu ContextMenu { get {return _LevelUI.ContextMenu;} }
	private Road _Road;
	public Road Road { get {return _Road;} }

	[SerializeField]
	private bool _Underwater;
	public bool Underwater {get {return _Underwater;}}
	[SerializeField]
	private float _LevelTimeForGold;
	public float LevelTimeForGold
	{
		get
		{
			float result = _LevelTimeForGold;
			List<LevelTimeBonus> timeBonuses = GetActiveBonuses<LevelTimeBonus>();
			foreach (LevelTimeBonus timeBonus in timeBonuses) {
				result += timeBonus.AdditionalTime;
			}
			return result;
		}
	}
	[SerializeField]
	private List<LevelResource> LevelResources;
	[SerializeField]
	private AllowedLevelObjectPrefabs _AllowedLevelObjectPrefabs;
	[SerializeField]
	private AvailableLevelInventoryBonuses InventoryBonuses;
	[SerializeField]
	private LevelUI _LevelUI;
	[SerializeField]
	private Fog _Fog;

	[SerializeField] private DialogManager _DialogUI;
	
	private class GatheredLevelDrop {
		public int ID;
		public string Name;
		public int Count;

		public GatheredLevelDrop(int LevelObjectDropsId, string dropName, int dropCount) {
			ID = LevelObjectDropsId;
			Name = dropName;
			Count = dropCount;
		}
	}

	private List<InventoryItem> _gatheredInventoryItems;
	private List<GatheredLevelDrop> _gatheredDrops;

	public List<InventoryItem> GatheredInventoryItems {
		get { return _gatheredInventoryItems; }
	}


	private float _CurrentLevelTime;
	public float CurrentLevelTime { get {return _CurrentLevelTime;} }

	private TaskManager _TaskManager = new TaskManager();
	private List<LevelBonus> _ActiveBonuses;
	private AmbientSound _CurrentAmbient;

	private bool _Started;
	public bool Started { get {return _Started;} }
	private bool _Paused;
	public bool IsTutorial = false;
	private List<LevelGoal> _Goals;
	private List<LevelObject> _LevelObjects;
	private IOnLevelMouseHandler _ActiveMouseHandler;
	private bool _MouseIsOnUI;
	private Dictionary<Collider2D, IOnLevelMouseHandler> _OnLevelMouseHandlerByCollider;

	public bool MouseIsOnUI {
		get {
			return _MouseIsOnUI;
		}
		set {
			_MouseIsOnUI = value;
		}
	}

	public TaskManager TaskManager {
		get {
			return _TaskManager;
		}
	}

	public Fog Fog {
		get {
			return _Fog;
		}
	}
	public DialogManager DialogUI
	{
		get { return _DialogUI; }
	}

	public LevelUI LevelUi
	{
		get { return _LevelUI; }
	}


	public void OnEnable() {
		if (_Instance == null) {
			Init();
		} else if (_Instance != this) {
			Destroy (this);
		}
	}

	public void OnDestroy() {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelTaskStarted, LevelTaskStarted);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectEnable, LevelObjectAwake);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectDisable, LevelObjectDestroy);
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelTaskStarted, LevelTaskStarted);
		PrefabsCache.Instance.ClearCache();
		_Instance = null;
		Time.timeScale = 1.0f;
	}

	public void Update() {
		if (!_Paused) {
			if (_Started) {
				_CurrentLevelTime += Time.deltaTime;
			}
			if (!_MouseIsOnUI) {
				Vector2 origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
				RaycastHit2D[] hits = Physics2D.RaycastAll(origin, Vector2.zero, 0f);
				int maxColliderOrder = -1;
				IOnLevelMouseHandler newMouseHandler = null;
				for (int i = 0; i < hits.Length; i++) {
					RaycastHit2D hit = hits[i];
					if (!_OnLevelMouseHandlerByCollider.ContainsKey(hit.collider)) {
						_OnLevelMouseHandlerByCollider.Add(hit.collider, hit.collider.GetComponent<IOnLevelMouseHandler>());
					}
					IOnLevelMouseHandler mouseHandler = _OnLevelMouseHandlerByCollider[hit.collider];
					if (mouseHandler != null) {
						int currentObjectOrder = mouseHandler.GetOrder();
						if (currentObjectOrder > maxColliderOrder) {
							maxColliderOrder = currentObjectOrder;
							newMouseHandler = mouseHandler;
						}
					}
				}
				LevelObject obj = newMouseHandler as LevelObject;
				if(IsTutorial && obj != null && !DialogUI.ActiveDialog.AffectedObjects.Contains(obj))
					return;
				
				if (newMouseHandler != _ActiveMouseHandler) {
					if (_ActiveMouseHandler != null) {
						_ActiveMouseHandler.MouseOut();
					}
					_ActiveMouseHandler = newMouseHandler;
				}
				if (_ActiveMouseHandler != null) {
					_ActiveMouseHandler.MouseOver();
				}
			}
		}
	}

	public int GetResourceCount(string resourceType) {
		LevelResource resource = GetLevelResource(resourceType);
		if (resource != null && resource.GetResourceType() == resourceType) {
			return resource.ResourceCount;
		}
		return  0;
	}

	public int GetResourceCount(GameObject resourcePrefab) {
		string resourceType = resourcePrefab.name;
		return GetResourceCount(resourceType);
	}

	public AvailableLevelInventoryBonuses GetInventoryBonuses() {
		return InventoryBonuses;
	}

	public void ModifyResourceCount(string resourceType, int count) {
		LevelResource resource = GetLevelResource(resourceType);
		if (resource == null) {
			resource = new LevelResource();
			resource.SetResourceType(resourceType);
			resource.ResourceCount = 0;
			LevelResources.Add(resource);
		}
		resource.ResourceCount += count;
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.ResourceValueChanged, resource));
	}

	public int GetResourceProductionCount(string resourceType) {
		int result = 0;
		foreach (LevelObject levelObject in _LevelObjects) {
			List<DropToProduce> levelObjectDrops = levelObject.GetProducigDrops();
			if (levelObjectDrops != null) {
				foreach (DropToProduce dropToProduce in levelObjectDrops) {
					if (dropToProduce.Name == resourceType && dropToProduce.CountType != DropCountType.PercentOfProduction) {
						result += dropToProduce.GetDropCount();
					}
				}
			}
		}
		return result;
	}

	public bool IsPrefabAllowed(GameObject prefab) {
		return _AllowedLevelObjectPrefabs.IsPrefabAllowed(prefab);
	}

	public void AddDrop(Drop drop){
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.ResourceGathered, new GatheredDrop(drop.ResourceType, drop.DropCount)));
		if (GetLevelResource(drop.ResourceType) == null) {
			LevelResource newResource = new LevelResource();
			newResource.SetResourcePrefab(drop.gameObject);
			newResource.ResourceCount = 0;
			LevelResources.Add(newResource);
		}
		if (drop is InventoryDrop) {
			//TODO: Replace with const
			drop.GetAnimation().Play("Dissappear");
			StartCoroutine(AddInventoryItem(drop));
		}
		if (drop is BonusDrop) {
			_LevelUI.BonusPanel.AddDrop(drop);
		} else if (drop is ResourceDrop) {
			if (_LevelUI.MainResourcePanel.ResourceIsInPanel(drop.ResourceType)) {
				_LevelUI.MainResourcePanel.AddDrop(drop);
			} else {
				_LevelUI.AdditionalResourcePanel.AddDrop(drop);
			}
		}
	}

	public bool IsGoalComplete(int goalIndex) {
		return _Goals[goalIndex].IsComplete();
	}

	public void OnGoalComplete() {
		foreach (LevelGoal levelGoal in _Goals) {
			if (!levelGoal.IsComplete()) {
				return;
			}
		}
		CompleteLevel();
	}

	private void CompleteLevel() {
		bool goldTime = false;
		int scoresForComplete = 2000;
		int scoresForTime = 0;
		if (CurrentLevelTime < LevelTimeForGold || ProfileManager.GetCurrentProfile().GameMode == 1) {
			scoresForComplete = 10000;
			goldTime = true;
			scoresForTime = Mathf.RoundToInt((LevelTimeForGold - CurrentLevelTime) * 100.0f);

		}
		int scoresForResource = 0;
		foreach (LevelResource levelResource in LevelResources) {
			scoresForResource += levelResource.ResourceCount * levelResource.GetScoresModifier();
		}
		StorylineManager.Instance.CompleteCurrentLevel(goldTime, scoresForComplete + scoresForTime + scoresForResource);
		_LevelUI.StatisticsWindow.Show(scoresForComplete, scoresForTime, scoresForResource, goldTime);
		foreach (LevelBonus inventoryBonus in InventoryBonuses.Bonuses) {
			if (inventoryBonus.GetInventoryItem().IsConsumable()) {
				ProfileManager.GetCurrentProfile().ConsumeInventoryItem(inventoryBonus.GetInventoryItem().GetItemType(), 1);	
			}
		}
	}

	public void AddDropGathered(int LevelObjectDropsId, string dropName, int dropCount) {
		_gatheredDrops.Add(new GatheredLevelDrop(LevelObjectDropsId, dropName, dropCount));
	}

	private IEnumerator AddInventoryItem(Drop inventoryDrop) {
		//TODO make constant
		yield return new WaitForSeconds(0.3f);
		_gatheredInventoryItems.Add((inventoryDrop as InventoryDrop).InventoryItem);
	}

	public void CommitGatheredItems() {
		foreach (var inventoryItem in _gatheredInventoryItems) {
			ProfileManager.GetCurrentProfile().AddInventoryItem(inventoryItem.GetItemType(), inventoryItem.IsConsumable());	
		}
		foreach (var gatheredDrop in _gatheredDrops) {
			ProfileManager.GetCurrentProfile().AddDropGathered(gatheredDrop.ID, gatheredDrop.Name, gatheredDrop.Count);
		}
		ProfileManager.Instance.Save();
	}

	public void ActivateBonus(LevelBonus bonus) {
		_ActiveBonuses.Add(bonus);
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.BonusActivated, bonus));
	}

	public void DeactivateBonus(LevelBonus bonus) {
		_ActiveBonuses.Remove(bonus);
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.BonusDeactivated, bonus));
	}

	public List<T> GetActiveBonuses<T>() where T : LevelBonus {
		List<T> result = new List<T>();
		foreach (LevelBonus levelBonus in _ActiveBonuses) {
			if ((levelBonus as T) != null) {
				result.Add((T)levelBonus);
			}
		}
		return result;
	}

	public List<T> GetActiveBonuses<T>(LevelObject targetObject) where T : LevelBonus {
		List<T> result = new List<T>();
		foreach (LevelBonus levelBonus in _ActiveBonuses) {
			if ((levelBonus as T) != null && (!(levelBonus is ModificatorBonus) || ((levelBonus as ModificatorBonus).IsLevelObjectAffected(targetObject)))) {
				result.Add((T)levelBonus);
			}
		}
		return result;
	}

	public List<T> GetActiveBonuses<T>(LevelBonus targetBonus) where T : LevelBonus {
		List<T> result = new List<T>();
		foreach (LevelBonus levelBonus in _ActiveBonuses) {
			if (levelBonus is BonusModificatorBonus && (levelBonus as BonusModificatorBonus).IsBonusAffected(targetBonus)) {
				result.Add((T)levelBonus);
			}
		}
		return result;
	}

	public void PlayAmbient(AmbientSound newSound) {
		if (_CurrentAmbient != null) {
			_CurrentAmbient.Stop();
		}
		_CurrentAmbient = newSound;
		newSound.Play();
	}

	public bool Paused {
		get {
			return _Paused;
		}
		set {
			_Paused = value;
			#if DEBUG
			Time.timeScale = _Paused ? 0.0f : (CheatSpeed ? 2.0f : 1.0f);
			#else
			Time.timeScale = _Paused ? 0.0f : 1.0f;
			#endif
		}
	}

	public List<LevelObject> GetLevelObjects() {
		List<LevelObject> result = new List<LevelObject>();
		for (int i = 0; i < _LevelObjects.Count; i++) {
			LevelObject levelObject = _LevelObjects[i];
			if (levelObject.isActiveAndEnabled) {
				result.Add(levelObject);
			}
		}
		return result;
	}

	public List<LevelObject> GetLevelObjectsOfType(string type) {
		List<LevelObject> result = new List<LevelObject>();
		for (int i = 0; i < _LevelObjects.Count; i++) {
			LevelObject levelObject = _LevelObjects[i];
			if (levelObject.isActiveAndEnabled && levelObject.LevelObjectType == type) {
				result.Add(levelObject);
			}
		}
		return result;
	}

	public LevelObject GetLevelObjectOfGameObject(GameObject gameObject) {
		for (int i = 0; i < _LevelObjects.Count; i++) {
			LevelObject levelObject = _LevelObjects[i];
			if (levelObject.gameObject == gameObject) {
				return levelObject;
			}
		}
		return null;
	}

	private LevelResource GetLevelResource(string resourceType) {
		for (int i = 0; i < LevelResources.Count; i++) {
			LevelResource resource = LevelResources[i];
			if (resource.GetResourceType() == resourceType) {
				return resource;
			}
		}
		return null;
	}

	private void Init() {
		_Instance = this;
		_Started = false;
		Paused = false;
		_CurrentLevelTime = 0.0f;
		_OnLevelMouseHandlerByCollider = new Dictionary<Collider2D, IOnLevelMouseHandler>();
		_Road = gameObject.GetComponent<Road>();
		_ActiveBonuses = new List<LevelBonus>();
		_Goals = new List<LevelGoal>(this.gameObject.GetComponents<LevelGoal>());
		
		_gatheredInventoryItems = new List<InventoryItem>();
		_gatheredDrops = new List<GatheredLevelDrop>();

		if (InventoryBonuses != null) {
			foreach (LevelBonus inventoryBonus in InventoryBonuses.Bonuses) {
				if (inventoryBonus.GetBonusType() != BonusType.ActiveBonus && ProfileManager.GetCurrentProfile().IsItemEquipped(inventoryBonus.GetInventoryItem().GetItemType())) {
					ActivateBonus(inventoryBonus);
				}
			}
		}

		_LevelObjects = new List<LevelObject>();

		if (_Fog != null) {
			_Fog.gameObject.SetActive(true);
		}

		_LevelUI.gameObject.SetActive(true);
		_LevelUI.GoalPanel.ShowGoals(_Goals);
		_LevelUI.MainResourcePanel.ShowIndicators(LevelResources);

		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelTaskStarted, LevelTaskStarted);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectEnable, LevelObjectAwake);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectDisable, LevelObjectDestroy);
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelTaskStarted, LevelTaskStarted);
	}

#if DEBUG
	private bool CheatSpeed;
	[HideInInspector]
	public bool InstantAction;

	public void OnGUI() {
		GUILayout.BeginHorizontal();
		GUILayout.Space(256);
		GUILayout.BeginHorizontal();
		if (CheatSpeed) {
			if (GUILayout.Button("Normal speed")) {
				CheatSpeed = false;
			}
		} else {
			if (GUILayout.Button("Double speed")) {
				CheatSpeed = true;
			}
		}
		if (InstantAction) {
			if (GUILayout.Button("Normal actions ")) {
				InstantAction = false;
			}
		} else {
			if (GUILayout.Button("Instant actions")) {
				InstantAction = true;
			}
		}
		Time.timeScale = _Paused ? 0.0f : (CheatSpeed ? 2.0f : 1.0f);
		if (GUILayout.Button("Add 100 of each resource")) {
			for (int i = 0; i < LevelResources.Count; i++) {
				LevelResource resource = LevelResources[i];
				ModifyResourceCount(resource.GetResourceType(), 100);
			}
		}
		if (GUILayout.Button("Complete level")) {
			CompleteLevel();
		}
		GUILayout.EndHorizontal();
		GUILayout.EndHorizontal();
	}
#endif

	private void LevelObjectAwake(GameEvent gameEvent) {
		LevelObject levelObject = gameEvent.Target as LevelObject;
		if (!_LevelObjects.Contains(levelObject)) {
			_LevelObjects.Add(levelObject);
		}
	}

	private void LevelObjectDestroy(GameEvent gameEvent) {
		LevelObject levelObject = gameEvent.Target as LevelObject;
		if (_LevelObjects.Contains(levelObject)) {
			_LevelObjects.Remove(levelObject);
		}
	}

	private void LevelTaskStarted(GameEvent gameEvent) {
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelTaskStarted, LevelTaskStarted);
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LevelStarted, true));
		_Started = true;
		_LevelUI.StartText.gameObject.SetActive(false);
	}
}