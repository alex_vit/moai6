using System.Collections.Generic;
using UnityEngine;

public class OnLevelBonuses : MonoBehaviour {
	[SerializeField]
	private List<LevelBonus> Bonuses;

	public void OnEnable() {
		foreach (LevelBonus levelBonus in Bonuses) {
			if(levelBonus != null)
				levelBonus.EnableBonus();
		}
	}

	public void OnDisable() {
		foreach (LevelBonus levelBonus in Bonuses) {
			if(levelBonus != null)
				levelBonus.DisableBonus();
		}
	}
}