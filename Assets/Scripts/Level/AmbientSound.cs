using UnityEngine;

public class AmbientSound : MonoBehaviour {
	[SerializeField]
	private AudioSource Audio;

	public void Play() {
		Audio.Play();
	}

	public void Stop() {
		Audio.Stop();
	}

	public void OnMouseDown() {
		Level.Instance.PlayAmbient(this);
	}

}