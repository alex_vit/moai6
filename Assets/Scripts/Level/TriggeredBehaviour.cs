using System.Collections.Generic;
using UnityEngine;

public class TriggeredBehaviour : MonoBehaviour {

	public List<Trigger> Triggers;
	protected bool Complete;

	public bool IsComplete
	{
		get { return Complete; }
		private set { }
	}

	public virtual void Awake() {
		Complete = false;
	}

	public virtual void OnEnable() {
		if (Triggers != null) {
			foreach (Trigger trigger in Triggers) {
				trigger.StartListening(OnTriggerUpdate, OnTriggerComplete);
			}
		}
		OnTriggerComplete();
	}

	public virtual void OnDisable() {
		if (Triggers != null) {
			foreach (Trigger trigger in Triggers) {
				trigger.StopListening();
			}
		}
	}

	protected virtual void OnTriggerComplete() {
		foreach (Trigger trigger in Triggers) {
			if (!trigger.Complete) {
				return;
			}
		}
		if (!Complete) {
			Complete = true;
			TriggersComplete();
		}
	}

	public void ForceComplete()
	{
		if (!Complete) {
			Complete = true;
			TriggersComplete();
		}
	}

	protected virtual void OnTriggerUpdate() {
	}

	protected virtual void TriggersComplete() {
	}
}