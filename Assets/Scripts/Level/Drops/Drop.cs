﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour, IOnLevelMouseHandler {

	public delegate void DropGatheredDelegate(GameObject dropGameObject);
	public event DropGatheredDelegate OnDropGathered;

	[System.NonSerialized]
	public int DropCount;
	[SerializeField]
	private string _ResourceType;
	private PolygonCollider2D Collider;
	private Animation DropAnimation;

	private Coroutine AutoCoroutine;

	public string ResourceType { get {return _ResourceType;}}

	public virtual void Awake() {
		Collider = this.gameObject.GetComponentInChildren<PolygonCollider2D>();
		DropAnimation = GetComponent<Animation>();
		//GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusActivated, OnBonus);
	}

	public void OnEnable() {
		StartCoroutine(EnableCollider());
		if (Level.Instance.GetActiveBonuses<AutoDropBonus>().Count > 0) {
			StartCoroutine(AutoBonusTimeout());
		} else {
			AutoCoroutine = StartCoroutine(DropTimeout());
		}
		GameEventDispatcher.Instance.AddEventListener(GameEventType.BonusActivated, OnBonus);
	}

	public void OnDisable() {
		if (AutoCoroutine != null) {
			StopCoroutine(AutoCoroutine);
		}
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.BonusActivated, OnBonus);
	}

	public void SetIndexInRow(int index) {
		index = index - Mathf.FloorToInt(index / DropAnimation.GetClipCount()) * DropAnimation.GetClipCount();
		DropAnimation.Play(index.ToString());
	}

	public int GetOrder() {
		return int.MaxValue;
	}

	public void MouseOver() {
		StopCoroutine(AutoCoroutine);
		GatherDrop();
	}

	public void MouseOut() {
	}

	public Animation GetAnimation() {
		return DropAnimation;
	}

	private void OnBonus(GameEvent gameEvent) {
		if (this.isActiveAndEnabled && gameEvent.Target is AutoDropBonus) {
			StopCoroutine(AutoCoroutine);
			GatherDrop();
		}
	}
	
	private IEnumerator EnableCollider() {
		yield return new WaitForSeconds(0.1f);
		Collider.enabled = true;
	}

	private IEnumerator AutoBonusTimeout() {
		yield return new WaitForSeconds(0.5f);
		GatherDrop();
	}

	private IEnumerator DropTimeout() {
		yield return new WaitForSeconds(30.0f);
		GatherDrop();
	}

	private void GatherDrop() {
		Level.Instance.AddDrop(this);
		if (OnDropGathered != null) {
			OnDropGathered(gameObject);
		}
		Collider.enabled = false;
		this.enabled = false;
	}
}
