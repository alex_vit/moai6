public enum DropCountType {
	Infinite = 0,
	PercentOfProduction = 1,
	FiniteOnLevel = 2,
	FiniteAtAll = 3
}