using UnityEngine;

public class InventoryDrop : Drop {
	[SerializeField]
	private InventoryItem _InventoryItem;

	public InventoryItem InventoryItem {get {return _InventoryItem;}}
}