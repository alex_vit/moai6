using UnityEngine;

public class ResourceDrop : Drop, IResource {
	[SerializeField]
	private Sprite _ResourceIcon;
	[SerializeField]
	private int _ScoresModifier;
	[SerializeField]
	private int _MinimalCountToShowInTooltip;

	[SerializeField] private string _Description;

	public Sprite ResourceIcon { get {return _ResourceIcon;}}
	public int ScoresModifier { get {return _ScoresModifier;}}
	public int MinimalCountToShowInTooltip { get {return _MinimalCountToShowInTooltip;}}
	public string Description
	{
		get { return _Description; }
	}
}