using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DropToProduce {
	public GameObject DropPrefab;
	public DropCountType CountType;
	[SerializeField]
	private int DropCount;
	public int TotalCount;

	private LevelObject Owner;

	public string Name {get {return DropPrefab.name;}}

	public void SetOwner(LevelObject owner) {
		Owner = owner;
	}

	public int GetDropCount() {
		int result = DropCount;
		if (CountType == DropCountType.PercentOfProduction) {
			result = Level.Instance.GetResourceProductionCount(Name) * DropCount / 100;
		}
		List<DropsIncreaseCountBonus> dropsIncreaseCountBonuses = Level.Instance.GetActiveBonuses<DropsIncreaseCountBonus>(Owner);
		foreach (DropsIncreaseCountBonus dropsIncreaseCountBonus in dropsIncreaseCountBonuses) {
			result = Mathf.RoundToInt(result * dropsIncreaseCountBonus.DropCountModifier);
		}
		return result;
	}
}