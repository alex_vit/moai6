[System.Serializable]
public class GatheredDrop {
	public string ResourceType;
	public int GatheredCount;

	public GatheredDrop(string resourceType, int gatheredCount) {
		ResourceType = resourceType;
		GatheredCount = gatheredCount;
	}
}