using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBonus : ScriptableObject {
	[SerializeField] private string _titleText;
	[SerializeField] private string _infoText;
	
	private bool _Active;
	[SerializeField]
	private BonusType _Type;
	[SerializeField]
	private float Duration;
	[SerializeField]
	private Sprite ActiveSprite;
	[SerializeField]
	private Sprite InactiveSprite;
	[SerializeField]
	private List<TaskCost> Costs;
	[SerializeField]
	private InventoryItem _InventoryItem;
	[SerializeField]
	private bool _CheckIfInventoryEquipped;

	public string TitleText {
		get { return _titleText; }
	}

	public string InfoText {
		get { return _infoText; }
	}

	public BonusType GetBonusType() {
		return _Type;
	}

	public InventoryItem GetInventoryItem() {
		return _InventoryItem;
	}

	public bool GetCheckIfInventoryEquipped() {
		return _CheckIfInventoryEquipped;
	}

	public bool IsActive() {
		return _Active;
	}

	public virtual bool ActiveInPanel() {
		return true;
	}

	public virtual void OnEnable() {
		_Active = false;
	}

	public float GetDuration() {
		float timeModofier = 1.0f;
		foreach (IncreaseBonusDurationBonus increaseBonusTimeBonus in Level.Instance.GetActiveBonuses<IncreaseBonusDurationBonus>(this)) {
			timeModofier *= increaseBonusTimeBonus.GetIncreaseModifier();
		}
		return Duration * timeModofier;
	}

	public List<TaskCost> GetCosts() {
		return Costs;
	}

	public Sprite GetActiveSprite() {
		return ActiveSprite;
	}

	public Sprite GetInactiveSprite() {
		return InactiveSprite;
	}

	public virtual void EnableBonus() {
		if (!_Active)
		{
			_Active = true;
			Level.Instance.ActivateBonus(this);
		}
	}

	public virtual void DisableBonus() {
		if (Level.Instance != null && _Active) {
			_Active = false;
			Level.Instance.DeactivateBonus(this);
		}
	}
}