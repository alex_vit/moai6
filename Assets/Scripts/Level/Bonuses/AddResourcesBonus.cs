using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Resources Bonus", fileName="ResourcesBonus")]
public class AddResourcesBonus : LevelBonus {
	[SerializeField]
	private List<LevelResource> Resources;

	override public void EnableBonus() {
		base.EnableBonus();
		foreach (LevelResource levelResource in Resources) {
			Level.Instance.ModifyResourceCount(levelResource.GetResourceType(), levelResource.ResourceCount);
		}
	}
}

