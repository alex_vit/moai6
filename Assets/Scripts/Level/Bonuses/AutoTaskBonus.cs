using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Auto Task Bonus", fileName="AutoTaskBonus")]
public class AutoTaskBonus : ModificatorBonus {
	[SerializeField]
	private float TimeDelay;

	override public void EnableBonus() {
		base.EnableBonus();
		foreach (LevelObject levelObject in Level.Instance.GetLevelObjects()) {
			if (IsLevelObjectAffected(levelObject))
			{
				Level.Instance.StartCoroutine(PerformTask(levelObject));
			}
		}
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectStateChanged, OnLevelObjectStateChanged);
	}

	override public void DisableBonus() {
		base.DisableBonus();
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectStateChanged, OnLevelObjectStateChanged);
	}
	
	public override bool ActiveInPanel() {
		return FindObjectOfType<OnLevelBonuses>() != null;
	}

	private void OnLevelObjectStateChanged(GameEvent gameEvent) {
		LevelObject targetLevelObject = gameEvent.Target as LevelObject;
		if (IsLevelObjectAffected(targetLevelObject)) {
			Level.Instance.StartCoroutine(PerformTask(targetLevelObject));
		}
	}

	private IEnumerator PerformTask(LevelObject targetLevelObject) {
		string CurrentLevelObjectsStateName = targetLevelObject.CurrentStateName;

		while (IsActive())
		{
			yield return new WaitForSeconds(TimeDelay);
			if (CurrentLevelObjectsStateName == targetLevelObject.CurrentStateName)
			{
				Level.Instance.TaskManager.AddTask(targetLevelObject, targetLevelObject.CurrentState.Tasks[0]);
			}
		}
	}
}