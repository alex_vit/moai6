using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Waypoint Speed Limit Bonus", fileName="WaypointSpeedLimitBonus")]
public class WaypointSpeedLimitBonus : LevelBonus {
	[SerializeField]
	private float MinimumSpeedModifier;
	[SerializeField]
	private float MaximumSpeedModifier;

	public float GetLimitedSpeed(float currentSpeedModifier) {
		return Mathf.Clamp(currentSpeedModifier, MinimumSpeedModifier, MaximumSpeedModifier);
	}
}