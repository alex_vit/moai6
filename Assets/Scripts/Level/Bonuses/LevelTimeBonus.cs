using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Level Time Bonus", fileName="LevelTimeBonus")]
public class LevelTimeBonus : LevelBonus {
	public float AdditionalTime;
}