using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Bonus State Transition Bonus", fileName="BonusStateTransitionBonus")]
public class BonusStateTransitionBonus : LevelObjectStateTransitionBonus{

	private class TransitedObject {
		public LevelObject TargetLevelObject;
		public string OriginalStateName;
		public string TargetStateName;

		public TransitedObject(LevelObject targetLevelObject, string originalStateName, string targetStateName) {
			TargetLevelObject = targetLevelObject;
			OriginalStateName = originalStateName;
			TargetStateName = targetStateName;
		}
	}

	[SerializeField]
	private List<TransitedObject> TransitedObjects;

	override public void EnableBonus() {
		TransitedObjects = new List<TransitedObject>();
		base.EnableBonus();
	}

	override public void DisableBonus() {
		base.DisableBonus();
		if (TransitedObjects != null)
		{
			foreach (TransitedObject transitedObject in TransitedObjects)
			{
				if (transitedObject != null && transitedObject.TargetLevelObject.CurrentStateName == transitedObject.TargetStateName)
				{
					transitedObject.TargetLevelObject.CurrentStateName = transitedObject.OriginalStateName;
				}
			}
			TransitedObjects.Clear();
		}
	}

	override protected void OnLevelObjectStateChanged(GameEvent gameEvent) {
		base.OnLevelObjectStateChanged(gameEvent);
		LevelObject targetLevelObject = gameEvent.Target as LevelObject;
		foreach (TransitedObject transitedObject in TransitedObjects) {
			Debug.Log(targetLevelObject);
			if (transitedObject.TargetLevelObject == targetLevelObject) {
				Debug.Log(transitedObject.TargetLevelObject);
				if (transitedObject.TargetLevelObject.CurrentStateName != transitedObject.TargetStateName) {
					TransitedObjects.Remove(transitedObject);
					DisableBonus();
					return;
				}	
			}
		}
	}

	override protected void TransitObject(LevelObject targetLevelObject, string stateName) {
		TransitedObjects.Add(new TransitedObject(targetLevelObject, targetLevelObject.CurrentStateName, stateName));
		base.TransitObject(targetLevelObject, stateName);
	}

}