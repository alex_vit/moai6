using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Object Transition Bonus", fileName="ObjectTransitionBonus")]
public class LevelObjectStateTransitionBonus : LevelBonus {
	[SerializeField] private bool _allowBlocked;
	[SerializeField]
	private float TimeToWaitBetweenTransitions;
	[SerializeField]
	private List<PossibleStatesTransitionDescription> PossibleTransitions;
	private Coroutine TransitionCoroutine;
	private bool ReadyToTransit;

	override public void EnableBonus() {
		base.EnableBonus();
		Level.Instance.StartCoroutine(CheckNextStatePrefabForCache());
		GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelObjectStateChanged, OnLevelObjectStateChanged);
		if (TimeToWaitBetweenTransitions <= float.Epsilon && TimeToWaitBetweenTransitions >= 0.0f) {
			foreach (LevelObject levelObject in Level.Instance.GetLevelObjects()) {
				CheckObject(levelObject);
			}
		} else {
			ReadyToTransit = false;
			StartCoroutine();
		}
	}

	override public void DisableBonus() {
		base.DisableBonus();
		if (TimeToWaitBetweenTransitions < 0.0f) {
			foreach (LevelObject levelObject in Level.Instance.GetLevelObjects()) {
				foreach (PossibleStatesTransitionDescription stateTransition in PossibleTransitions) {
					if (stateTransition.IsLevelObjectAffected(levelObject)) {
						levelObject.CurrentStateName = stateTransition.TargetState;
						return;
					}
				}
			}
		} else {
			if (TransitionCoroutine != null && Level.Instance != null) {
				Level.Instance.StopCoroutine(TransitionCoroutine);
			}
		}
		GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelObjectStateChanged, OnLevelObjectStateChanged);
	}
	
	public override bool ActiveInPanel() {
		foreach (var levelObject in Level.Instance.GetLevelObjects()) {
			foreach (PossibleStatesTransitionDescription stateTransition in PossibleTransitions) {
				if (stateTransition.IsLevelObjectAffected(levelObject)) {
					return true;
				}
			}
		}
		return false;
	}

	private IEnumerator CheckNextStatePrefabForCache() {
		yield return new WaitForEndOfFrame();
		foreach (PossibleStatesTransitionDescription stateTransition in PossibleTransitions) {
			stateTransition.CheckForCache();
		}
	}

	protected virtual void OnLevelObjectStateChanged(GameEvent gameEvent) {
		CheckObject(gameEvent.Target as LevelObject);
	}

	protected void CheckObject(LevelObject targetLevelObject) {
		if ((!targetLevelObject.IsPathBlocked() || _allowBlocked) && (ReadyToTransit || TimeToWaitBetweenTransitions <= float.Epsilon && TimeToWaitBetweenTransitions >= 0.0f)) {
			foreach (PossibleStatesTransitionDescription stateTransition in PossibleTransitions) {
				if (stateTransition.IsLevelObjectAffected(targetLevelObject)) {
					var levelObjects = Level.Instance.GetLevelObjectsOfType(stateTransition.GetAffectedObjectType());
					var remainObjectsCount = 0;
					foreach (var levelObject in levelObjects) {
						if (!levelObject.IsPathBlocked() || _allowBlocked) {
							remainObjectsCount++;
						}
					}
					if (remainObjectsCount > stateTransition.MinimumNotAffectedCount) {
						if (stateTransition.IsLevelObjectAffected(targetLevelObject)) {
							TransitObject(targetLevelObject, stateTransition.TargetState);
							ReadyToTransit = false;
							StartCoroutine();
						}
					} else {
						ReadyToTransit = false;
						StartCoroutine();
					}
					break;
				}
			}
		}
	}

	protected virtual void TransitObject(LevelObject targetLevelObject, string stateName) {
		targetLevelObject.CurrentStateName = stateName;
	}

	private void StartCoroutine() {
		if (TimeToWaitBetweenTransitions > 0.0f) {
			TransitionCoroutine = Level.Instance.StartCoroutine(PerformTransition());
		}
	}

	private IEnumerator PerformTransition() {
		yield return new WaitForSeconds(TimeToWaitBetweenTransitions);
		ReadyToTransit = true;
	}
}