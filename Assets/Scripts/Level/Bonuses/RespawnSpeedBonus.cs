using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Respawn Speed Bonus", fileName="RespawnSpeedBonus")]
public class RespawnSpeedBonus : LevelBonus {
	[SerializeField]
	private float SpawnTimeModifier;

	public float GetSpawnTimeModifier() {
		return SpawnTimeModifier;
	}
}