using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Show Hidden Drops", fileName="ShowHiddenDrops")]
public class ShowHiddenDropsBonus : ModificatorBonus {
}