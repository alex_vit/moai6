using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Drops Increase", fileName="DropsIncrease")]
public class DropsIncreaseCountBonus : ModificatorBonus{
	public float DropCountModifier;
}