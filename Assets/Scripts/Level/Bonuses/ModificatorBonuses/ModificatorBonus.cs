using System.Collections.Generic;
using UnityEngine;

public class ModificatorBonus : LevelBonus {
	[SerializeField]
	private List<PossibleStatesDescription> AffectedPossibleStates;

	public virtual bool IsLevelObjectAffected(LevelObject levelObject) {
		foreach (PossibleStatesDescription possibleStatesDescription in AffectedPossibleStates) {
			if (possibleStatesDescription.IsLevelObjectAffected(levelObject)) {
				return true;
			}
		}
		return false;
	}
}