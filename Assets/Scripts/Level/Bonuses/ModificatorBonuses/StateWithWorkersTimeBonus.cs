using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/State With Workers Time Bonus", fileName="StateWithWorkersTimeBonus")]
public class StateWithWorkersTimeBonus : ModificatorBonus{
	public float TimeModifier;
}