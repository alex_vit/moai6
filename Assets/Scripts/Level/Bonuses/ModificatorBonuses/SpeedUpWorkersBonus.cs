using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/SpeedUp", fileName="SpeedUpBonus")]
public class SpeedUpWorkersBonus : ModificatorBonus{
	public float SpeedModifier;
	
	public override bool ActiveInPanel() {
		return FindObjectOfType<LevelUnitsSpawnPoints>() != null;
	}
}