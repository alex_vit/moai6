using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/SpeeUp Drops", fileName="SpeedUpDropsBonus")]
public class DropsSpeedUpBonus : ModificatorBonus{
	public float SpeedModifier;
}