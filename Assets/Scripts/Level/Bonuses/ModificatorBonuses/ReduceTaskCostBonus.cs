using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Reduce Task Cost", fileName="ReduceTaskCostBonus")]
public class ReduceTaskCostBonus : ModificatorBonus{
	public int CostModifier;
}