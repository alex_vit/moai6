using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Restrict State Bonus", fileName="RestrictStateBonus")]
public class RestrictStateBonus : ModificatorBonus {
}