using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Bonus Cost Modifier Bonus", fileName="BonusCostModifierBonus")]
public class BonusCostModifierBonus : BonusModificatorBonus {
	[SerializeField]
	private float CostModifier;

	public float GetCostModifier() {
		return CostModifier;
	}
}