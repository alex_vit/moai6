using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Increase Transition Bonus Time Bonus", fileName="IncreaseTransitionBonusTimeBonus")]
public class IncreaseTransitionBonusTimeBonus : BonusModificatorBonus {
	[SerializeField]
	private float IncreaseModifier;

	public float GetIncreaseModifier() {
		return IncreaseModifier;
	}
}