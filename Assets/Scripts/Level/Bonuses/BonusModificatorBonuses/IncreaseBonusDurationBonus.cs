using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/Bonus/Increase Bonus Duration Bonus", fileName="IncreaseBonusDurationBonus")]
public class IncreaseBonusDurationBonus : BonusModificatorBonus {
	[SerializeField]
	private float IncreaseModifier;

	public float GetIncreaseModifier() {
		return IncreaseModifier;
	}
}