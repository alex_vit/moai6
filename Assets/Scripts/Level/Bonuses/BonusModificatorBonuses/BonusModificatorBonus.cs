using System.Collections.Generic;
using UnityEngine;

public class BonusModificatorBonus : LevelBonus {
	[SerializeField]
	private List<LevelBonus> AffectedBonuses;

	public virtual bool IsBonusAffected(LevelBonus levelBonus) {
		foreach (LevelBonus affectedBonus in AffectedBonuses) {
			if (affectedBonus.name == levelBonus.name) {
				return true;
			}
		}
		return false;
	}
}