public enum BonusType {
	HiddenPassiveBonus,
	PassiveBonus,
	ActiveBonus
}