using UnityEngine;

public class MainMenu : MonoBehaviour{
	[SerializeField]
	private OptionsWindow OptionsWindow;
	[SerializeField]
	private EditPlayerWindow EditPlayerWindow;
	[SerializeField]
	private PlayersWindow PlayersWindow;
	[SerializeField]
	private GameModeWindow GameModeWindow;
	[SerializeField]
	private LocalizedText PlayerNameText;

	public void Awake() {
		if (ProfileManager.GetCurrentProfile().PlayerName == null) {
			EditPlayerWindow.Show(true);
		}
		PlayersWindow.OnWindowClosed += OnWindowClosed;
		EditPlayerWindow.OnWindowClosed += OnWindowClosed;
	}

	public void OnExitClick() {
		PlayersWindow.Show();
	}

	public void OnPlayerClick() {
		PlayersWindow.Show();
	}

	private void OnWindowClosed(PopupWindow window) {
		if (window is EditPlayerWindow || window is PlayersWindow) {
			if (window is EditPlayerWindow && ProfileManager.GetCurrentProfile().GameMode == -1) {
				GameModeWindow.Show();
			}
			PlayerNameText.OnEnable();
		}
	}
}