public class DeletePlayerWindow : PopupWindow {
	public void OnOkClick() {
		ProfileManager.DeleteCurrentProfile();
		Close();
	}
}