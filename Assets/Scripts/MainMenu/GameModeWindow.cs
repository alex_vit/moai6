using UnityEngine;
using UnityEngine.UI;

public class GameModeWindow : PopupWindow {
	[SerializeField]
	private Toggle CasualToggle;
	[SerializeField]
	private Toggle RelaxToggle;

	public void OnEnable() {
		CasualToggle.isOn = ProfileManager.GetCurrentProfile().GameMode == 0 ||
		                    ProfileManager.GetCurrentProfile().GameMode == -1;
		RelaxToggle.isOn = ProfileManager.GetCurrentProfile().GameMode == 1;
	}

	public void OnOnClick() {
		ProfileManager.GetCurrentProfile().GameMode = CasualToggle.isOn ? 0 : 1;
		ProfileManager.Instance.Save();
		Close();
	}
}