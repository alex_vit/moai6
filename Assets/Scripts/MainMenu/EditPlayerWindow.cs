using UnityEngine;
using UnityEngine.UI;

public class EditPlayerWindow : PopupWindow {
	[SerializeField]
	private Text Header;
	[SerializeField]
	private Button OkButton;
	[SerializeField]
	private Button CancelButton;
	[SerializeField]
	private InputField PlayerName;
	private bool NewPlayer;

	public void OnEnable() {
		if (ProfileManager.GetProfilesNames().Count == 1 && (ProfileManager.GetCurrentProfile().PlayerName == null || ProfileManager.GetCurrentProfile().PlayerName == "")) {
			CancelButton.interactable = false;
		} else {
			CancelButton.interactable = true;
		}
		OkButton.interactable = false;
		if (ProfileManager.GetCurrentProfile().PlayerName != null) {
			PlayerName.text = ProfileManager.GetCurrentProfile().PlayerName;
		} else {
			PlayerName.text = "";
		}
	}

	public void OnInputChange() {
		OkButton.interactable = PlayerName.text != "";
	}

	public void OnCancel() {
		if (NewPlayer && ProfileManager.GetCurrentProfile().PlayerName == null) {
			ProfileManager.DeleteCurrentProfile();
		}
		Close();
	}

	public void OnOk() {
		ProfileManager.GetCurrentProfile().PlayerName = PlayerName.text;
		ProfileManager.Instance.Save();
		Close();
	}

	public void Show(bool newPlayer) {
		Show();
		NewPlayer = newPlayer;
		Header.gameObject.SetActive(newPlayer);
	}
}