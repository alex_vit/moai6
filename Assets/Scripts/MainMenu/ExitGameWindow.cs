using UnityEngine;

public class ExitGameWindow : PopupWindow {
	public void Exit() {
		Application.Quit();
	}
}