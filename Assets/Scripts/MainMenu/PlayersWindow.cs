using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersWindow : PopupWindow {
	[SerializeField]
	private EditPlayerWindow EditPlayerWindow;
	[SerializeField]
	private DeletePlayerWindow DeletePlayerWindow;
	[SerializeField]
	private Button NewPlayerButton;
	[SerializeField]
	private Button DeletePlayerButton;
	[SerializeField]
	private List<Text> PlayerItems;

	override public void Awake() {
		base.Awake();
		EditPlayerWindow.OnWindowClosed += WindowClosed;
		DeletePlayerWindow.OnWindowClosed += WindowClosed;
	}

	public void OnEnable() {
		UpdatePlayersList();
	}

	override public void Close() {
		base.Close();
		ProfileManager.Instance.Save();
	}

	public void OnNewPlayer() {
		ProfileManager.AddNewProfile();
		EditPlayerWindow.Show(true);
	}

	public void OnEditPlayer() {
		EditPlayerWindow.Show(false);
	}

	public void OnDeletePlayer() {
		DeletePlayerWindow.Show();
	}

	public void OnToggleChange() {
		for (int i = 0; i < PlayerItems.Count; i++) {
			if (PlayerItems[i].isActiveAndEnabled && PlayerItems[i].GetComponent<Toggle>().isOn) {
				ProfileManager.SetActiveProfileIndex(i);
				break;
			}
		}
		UpdatePlayersList();
	}

	private void WindowClosed(PopupWindow window) {
		if (window is EditPlayerWindow || window is DeletePlayerWindow) {
			UpdatePlayersList();
		}
	}

	private void UpdatePlayersList() {
		List<string> playerNames = ProfileManager.GetProfilesNames();
		NewPlayerButton.interactable = playerNames.Count < PlayerItems.Count;
		DeletePlayerButton.interactable = playerNames.Count > 1;
		for (int i = 0; i < playerNames.Count; i++) {
			Text item = PlayerItems[i];
			item.gameObject.SetActive(true);
			item.text = playerNames[i];
			if (i == ProfileManager.GetActiveProfileIndex()) {
				item.GetComponent<Toggle>().Select();
				item.color = item.GetComponent<Toggle>().colors.highlightedColor;
			} else {
				item.color = item.GetComponent<Toggle>().colors.normalColor;
			}
		}
		for (int i = playerNames.Count; i < PlayerItems.Count; i++) {
			PlayerItems[i].gameObject.SetActive(false);
		}
	}
}