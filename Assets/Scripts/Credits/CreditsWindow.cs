﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.UI;

public class CreditsWindow : MonoBehaviour
{
	public float delay;
	
	[SerializeField] 
	private Image tint;
	[SerializeField] 
	private GameObject[] slides;

	private int currSlideIndex;

	private WaitForSeconds waitForDelay;
	private WaitForFixedUpdate waitForSlideUpdate;

	private void Awake()
	{
		currSlideIndex = 0;
		
		waitForDelay = new WaitForSeconds(delay);
		waitForSlideUpdate = new WaitForFixedUpdate();
		
		StopAllCoroutines();
		StartCoroutine(ShowSlides());
	}

	/*private void OnEnable()
	{
		StopAllCoroutines();
		StartCoroutine(ShowSlides());Cauca
	}*/
	private void OnMouseUp()
	{
		Debug.Log("click");
	}


	private void ChangeSlideImmediately()
	{
		StopAllCoroutines();
		ChangeSlideInAnimation();
		StartCoroutine(ShowSlides());
	}
	
	private void ChangeSlideInAnimation()
	{
		currSlideIndex = currSlideIndex + 1 > slides.Length - 1 ? 0 : ++currSlideIndex;
		
		foreach (var slide in slides)
			slide.SetActive(false);
		slides[currSlideIndex].SetActive(true);
	}

	#region engine

	IEnumerator ShowSlides()
	{
		while (true)
		{
			yield return waitForDelay;
			StartCoroutine(ChangeSlide());	
		}
	}

	IEnumerator ChangeSlide()
	{
		bool _faded = tint.color.a == 0f;
		float _limit = .8f;
		Color _currColor = tint.color;
		while (true)
		{
			if (tint.color.a > _limit)
			{
				_faded = tint.color.a < _limit;
				ChangeSlideInAnimation();
			}

			if (_faded)
				_currColor = new Color(tint.color.r, tint.color.g, tint.color.b, tint.color.a + Time.fixedDeltaTime);
			if (!_faded)
				_currColor = new Color(tint.color.r, tint.color.g, tint.color.b, tint.color.a - Time.fixedDeltaTime);
			tint.color = _currColor;
			
			if (tint.color.a <= 0f)
			{
				_currColor = new Color(tint.color.r, tint.color.g, tint.color.b, 0f);
				tint.color = _currColor;
				_faded = tint.color.a == 0f;
				break;
			}
			yield return waitForSlideUpdate;
		}
	}
	#endregion
}
