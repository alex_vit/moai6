using System.Runtime.Serialization.Formatters;
using UnityEngine;

public class ScreensizeManager {
	public static void CheckScreenSize() {
		if (ProfileManager.GetCurrentProfile().Fullscreen) {
			Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, ProfileManager.GetCurrentProfile().Fullscreen);
		} else {
			float aspectRatio = (float) Screen.currentResolution.width / Screen.currentResolution.height;
			aspectRatio = Mathf.Max(4.0f / 3.0f, aspectRatio);
			Screen.SetResolution((int)(768f * aspectRatio) , 768, ProfileManager.GetCurrentProfile().Fullscreen);
		}
	}
}