using UnityEngine;

public class SceneMusic : MonoBehaviour {
	[SerializeField]
	[Range(0.0f, 1.0f)]
	private float Volume = 1;
	[SerializeField]
	private AudioClip Music;

	public void OnEnable() {
		AudioManager.Instance.PlayMusic(Music, Volume);
	}
}