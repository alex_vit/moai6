using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSwitcher : GenericSingleton<SceneSwitcher>
{
	private string LastScene = "";
	public string LastSceneName
	{
		get { return LastScene; }
	}

	private GameObject Preloader;

	public void SwitchTo(string sceneName, Image progressbar = null) {
		if (!sceneName.Equals(""))
		{
			LastScene = SceneManager.GetActiveScene().name;
			StartCoroutine(LoadNewScene(sceneName, progressbar));
		}
	}

	private IEnumerator LoadNewScene(string sceneName, Image progressbar)
	{
		if (progressbar != null) {
			progressbar.fillAmount = 0.0f;
			yield return new WaitForSeconds(0.1f);
		}
		AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
		if (progressbar != null) {
			async.allowSceneActivation = false;
			while (!async.isDone && async.progress < 0.9f) {
				progressbar.fillAmount = async.progress * 0.5f;
				yield return null;
			}
			async.allowSceneActivation = true;
			while (progressbar.fillAmount < 1.0f) {
				progressbar.fillAmount += 0.05f;
				yield return new WaitForSeconds(0.1f);
				if (progressbar == null) {
					break;
				}
			}
		} else {
			yield return async;
		}
	}

}