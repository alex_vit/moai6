using UnityEngine;

[CreateAssetMenu(menuName = "TimeManager/AudioData", fileName="AudioData")]
public class AudioData : ScriptableObject {
	public AudioClip Sound;
	public float Volume;
	public float StartDelay;
	public float LoopDelay;
}