using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : GenericSingleton<AudioManager> {

	private Dictionary<AudioSource, float> VolumesBySounds;
	private List<AudioSource> AudioSources;
	private AudioSource MusicAudioSource;
	private float MusicVolume;

	private float ProfileMusicVolume;
	private float ProfileSoundVolume;

	override public void Init() {
		base.Init();
		ProfileMusicVolume = ProfileManager.GetCurrentProfile().MusicVolume;
		ProfileSoundVolume = ProfileManager.GetCurrentProfile().SoundVolume;
		VolumesBySounds = new Dictionary<AudioSource, float>();
		AudioSources = new List<AudioSource>();
	}

	public void SetSoundVolume(float volume) {
		ProfileSoundVolume = volume;
		foreach (AudioSource source in VolumesBySounds.Keys) {
			source.volume = VolumesBySounds[source] * ProfileSoundVolume;
		}
	}

	public void SetMusicVolume(float volume) {
		ProfileMusicVolume = volume;
		MusicAudioSource.volume = ProfileMusicVolume * MusicVolume;
	}

	public void PlayMusic(AudioClip music, float volume = 1.0f) {
		if (MusicAudioSource == null) {
			MusicAudioSource = gameObject.AddComponent<AudioSource> ();
			MusicAudioSource.loop = true;
		}
		MusicVolume = volume;
		MusicAudioSource.volume = ProfileMusicVolume * volume;
		MusicAudioSource.clip = music;
		MusicAudioSource.Play();
	}

	public AudioSource PlaySound(AudioData audioData) {
		var alreadyPlays = false;
		foreach (var audioSource in AudioSources) {
			if (audioSource.clip != audioData.Sound) continue;
			alreadyPlays = true;
			break;
		}
		return !alreadyPlays ? PlaySound(audioData.Sound, audioData.Volume, audioData.StartDelay, audioData.LoopDelay) : null;
	}

	public AudioSource PlaySound(AudioClip sound, float volume = 1.0f, float startDelay = -1.0f, float loopDelay = -1.0f) {
		AudioSource source = GetAudioSource();
		VolumesBySounds[source] = volume;
		source.volume = ProfileSoundVolume * volume;
		source.clip = sound;
		StartCoroutine(SoundCoroutine(source, startDelay, loopDelay));
		return source;
	}

	public void StopSound(AudioSource source) {
		if (source != null) {
			source.clip = null;
		}
	}

	private AudioSource GetAudioSource() {
		foreach (AudioSource audioSource in AudioSources) {
			if (audioSource.clip == null) {
				return audioSource;
			}
		}
		AudioSource source = gameObject.AddComponent<AudioSource> ();
		AudioSources.Add(source);
		VolumesBySounds.Add(source, 1.0f);
		return source;
	}

	private IEnumerator SoundCoroutine(AudioSource source, float startDelay, float loopDelay)
	{
		yield return new WaitForSeconds(startDelay);
		if (source.clip != null) {
			source.Play();
			yield return new WaitForSeconds(source.clip.length);
			if (loopDelay < 0.0f) {
				StopSound(source);
			} else {
				yield return new WaitForSeconds(loopDelay);
				startDelay = 0.0f;
				StartCoroutine(SoundCoroutine(source, startDelay, loopDelay));
			}
		}
		yield return null;
	}
}