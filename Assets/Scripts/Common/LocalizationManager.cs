using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class LocalizationManager : GenericSingleton<LocalizationManager> {
	public static readonly string LOCALIZATION_PATH = "Localization";

	private class Locale {
		public SystemLanguage Language;
		public Dictionary<string, string> Strings;
		public Dictionary<string, Sprite> Images;

		public Locale(SystemLanguage language, Dictionary<string, string> strings) {
			Language = language;
			Strings = strings;
			Images = new Dictionary<string, Sprite>();
		}
	}

	private Locale CurrentLocale;

	override public void Init() {
		if (!Initialized) {
			base.Init();
			LoadLocale();
		}
	}


	#if UNITY_EDITOR
	public void OnEnable()
	{
		LoadLocale();
	}
	#endif

	private void LoadLocale() {
		SystemLanguage systemLanguage = Application.systemLanguage;
		string localizationPath = System.IO.Path.Combine(Application.streamingAssetsPath, LocalizationManager.LOCALIZATION_PATH);
		string localeDir;
		SystemLanguage dirLanguage;
		if (System.IO.Directory.Exists(System.IO.Path.Combine(localizationPath, systemLanguage.ToString()))) {
			localeDir = System.IO.Path.Combine(localizationPath, systemLanguage.ToString());
			dirLanguage = systemLanguage;
		} else {
			if (System.IO.Directory.Exists(System.IO.Path.Combine(localizationPath, SystemLanguage.English.ToString()))) {
				localeDir = System.IO.Path.Combine(localizationPath, SystemLanguage.English.ToString());
				dirLanguage = SystemLanguage.English;
			} else {
				localeDir = System.IO.Path.Combine(localizationPath, "Default");
				dirLanguage = SystemLanguage.Unknown;
			}
		}
		string localePath = System.IO.Path.Combine(localeDir, "locale.csv");
		if (System.IO.File.Exists(localePath)) {
			string[] lines = File.ReadAllLines(localePath);
			Dictionary<string, string> localeStrings = new Dictionary<string, string>(){};
			foreach (string line in lines) {
				string[] lineSplit = line.Split(';');
				if (lineSplit.Length > 1 && lineSplit[0] != "") {
					if (localeStrings.ContainsKey(lineSplit[0])) {
//Debug.LogError("Duplicate key found: " + lineSplit[0]);
					} else {
						localeStrings.Add(lineSplit[0], lineSplit[1]);
					}
				}
			}
			CurrentLocale = new Locale(dirLanguage, localeStrings);
			StartCoroutine(LoadImages(Directory.GetFiles(localeDir, "*.*").Where(file => file.ToLower().EndsWith("png") || file.ToLower().EndsWith("jpg") || file.ToLower().EndsWith("jpeg")).Select(filename => System.IO.Path.GetFileName(filename)).ToList()));
		}
	}

	#if UNITY_EDITOR
	public List<string> GetKeysList(string partOfKey) {
		if (CurrentLocale != null) {
			return new List<string>(CurrentLocale.Strings.Keys).Where(key => key.Contains(partOfKey)).Take(15).ToList<string>();
		}
		Debug.LogError("Locale not found");
		return null;
	}

	public List<string> GetImagesList(string partOfName) {
		if (CurrentLocale != null) {
			return new List<string>(CurrentLocale.Images.Keys).Where(name => name.Contains(partOfName)).Take(15).ToList<string>();
		}
		Debug.LogError("Locale not found");
		return null;
	}
	#endif

	private IEnumerator LoadImages(List<string> imageNames)
	{
		foreach (string imageName in imageNames) {
			if (!CurrentLocale.Images.ContainsKey(imageName)) {
				WWW localFile = new WWW("file://" + LocalizationManager.Instance.GetImagePath(imageName));
				yield return localFile;
				Texture texture = localFile.texture;
				Sprite sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
				if (!CurrentLocale.Images.ContainsKey(imageName)) {
					CurrentLocale.Images.Add(imageName, sprite);
				}
#if !UNITY_EDITOR
				GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.LoadedLocalizedImage, imageName));
#endif
			}
		}
	}

	public Sprite GetImage(string imageName) {
		if (CurrentLocale != null && CurrentLocale.Images.ContainsKey(imageName)) {
			return CurrentLocale.Images[imageName];
		}
		return null;
	}

	public string GetString(string key) {
		if (CurrentLocale != null && key != null && key != "" && CurrentLocale.Strings.ContainsKey(key)) {
			string result = CurrentLocale.Strings[key];
			if (result.Contains("%USER_NAME%")) {
				string playerName = ProfileManager.GetCurrentProfile().PlayerName;
				result = result.Replace("%USER_NAME%", playerName != null ? playerName : "");
			}
			if (result.Contains("%CURRENT_LEVEL_NUMBER%")) {
				int currentLevel = StorylineManager.Instance.GetCurrentLevelNumber();
				result = result.Replace("%CURRENT_LEVEL_NUMBER%", currentLevel.ToString());
			}
			if (result.Contains("%CURRENT_LEVEL_NAME%")) {
				result = result.Replace("%CURRENT_LEVEL_NAME%", GetString(StorylineManager.Instance.GetCurrentLevel().LevelName));
			}
			return result;
		}
		return "";
	}

	private string GetImagePath(string imageName) {
		return System.IO.Path.Combine(Application.streamingAssetsPath, LocalizationManager.LOCALIZATION_PATH + "/" + CurrentLocale.Language + "/" + imageName);
	}
}