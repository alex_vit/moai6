using System.Collections.Generic;
using UnityEngine;

public class GameEventDispatcher : GenericSingleton<GameEventDispatcher>{

	public delegate void GameEventDelegate(GameEvent levelEvent);

	private Dictionary<GameEventType, GameEventDelegate> Delegates;

	override public void Init() {
		base.Init();
		Delegates = new Dictionary<GameEventType, GameEventDelegate>(){};
	}

	public void AddEventListener(GameEventType eventType, GameEventDelegate eventDelegate) {
		if (Delegates != null)
		{
			if (!Delegates.ContainsKey(eventType))
			{
				Delegates.Add(eventType, null);

			}
			Delegates[eventType] += eventDelegate;
		}
	}

	public void RemoveEventListener(GameEventType eventType, GameEventDelegate eventDelegate) {
		if (Delegates != null && Delegates.ContainsKey(eventType)) {
			Delegates[eventType] -= eventDelegate;
		}
	}

	public void DispatchEvent(GameEvent gameEvent) {
		if (Delegates != null && Delegates.ContainsKey(gameEvent.Type) && Delegates[gameEvent.Type] != null) {
			Delegates[gameEvent.Type](gameEvent);
		}
	}
}