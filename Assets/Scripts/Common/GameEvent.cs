public class GameEvent {
	public GameEventType Type;
	public object Target;
	public GameEvent(GameEventType type, object target) {
		Type = type;
		Target = target;
	}
}