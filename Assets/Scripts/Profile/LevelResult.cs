[System.Serializable]
public class LevelResult {
	public bool GoldTime;
	public int Scores;
	public bool Unlocked;
	public bool UnlockProcessed;
	public bool CompleteProcessed;

	public LevelResult() {
		Scores = 0;
		GoldTime = false;
	}
}