using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerProfile {
	[System.Serializable]
	private class ProfileInventoryItem {
		public int Grade;
		public string HeroSlot;
		public int Count;
		public bool Consumable;
		public bool Equipped;

		public ProfileInventoryItem() {
			Grade = 1;
			Count = 0;
			HeroSlot = null;
			Equipped = false;
			Consumable = true;
		}
	}

	public string PlayerName;
	public bool Fullscreen;
	public bool SystemCursor;
	public int GameMode;
	public float SoundVolume;
	public float MusicVolume;
	private int _CurrentLevelIndex;
	private List<LevelResult> _LevelResults;
	private Dictionary<string, ProfileInventoryItem> InventoryItems;
	private Dictionary<int, List<GatheredDrop>> GatheredDrops;

	public PlayerProfile() {
		_CurrentLevelIndex = 0;
		SoundVolume = 1.0f;
		MusicVolume = 1.0f;
		Fullscreen = true;
		SystemCursor = false;
		GameMode = -1;
		InventoryItems = new Dictionary<string, ProfileInventoryItem>();
		GatheredDrops = new Dictionary<int, List<GatheredDrop>>();
	}

	public List<LevelResult> GetLevelResults() {
		return _LevelResults;
	}

	public void InitLevelResults(int levelsCount) {
		_LevelResults = new List<LevelResult>();
		for (int i = 0; i < levelsCount; i++) {
			LevelResult result = new LevelResult();
			_LevelResults.Add(result);
		}
	}

	public int GetCurrentLevel() {
		return _CurrentLevelIndex;
	}

	public void SetCurrentLevel(int index) {
		_CurrentLevelIndex = index;
	}

	public void AddDropGathered(int LevelObjectDropsId, string dropName, int dropCount) {
		if (!GatheredDrops.ContainsKey(LevelObjectDropsId)) {
			GatheredDrops.Add(LevelObjectDropsId, new List<GatheredDrop>());
		}
		var dropFound = false;
		foreach (GatheredDrop gatheredDrop in GatheredDrops[LevelObjectDropsId]) {
			if (gatheredDrop.ResourceType == dropName) {
				gatheredDrop.GatheredCount += dropCount;
				dropFound = true;
				break;
			}
		}
		if (!dropFound) {
			GatheredDrops[LevelObjectDropsId].Add(new GatheredDrop(dropName, dropCount));
		}
	}

	public int GetDropGathered(int LevelObjectDropsId, string dropName) {
		if (GatheredDrops.ContainsKey(LevelObjectDropsId)) {
			foreach (GatheredDrop gatheredDrop in GatheredDrops[LevelObjectDropsId]) {
				if (gatheredDrop.ResourceType == dropName) {
					return gatheredDrop.GatheredCount;
				}
			}
		}
		return 0;
	}

	public void AddInventoryItem(string inventoryItemType, bool consumable) {
		ProfileInventoryItem profileInventoryItem = null;
		if (InventoryItems.ContainsKey(inventoryItemType)) {
			profileInventoryItem = InventoryItems[inventoryItemType];
		} else {
			profileInventoryItem = new ProfileInventoryItem();
			profileInventoryItem.Consumable = consumable;
			InventoryItems.Add(inventoryItemType, profileInventoryItem);
		}
		profileInventoryItem.Count += 1;
	}

	public void ConsumeInventoryItem(string inventoryItemType, int count) {
		if (InventoryItems.ContainsKey(inventoryItemType)) {
			ProfileInventoryItem profileInventoryItem = InventoryItems[inventoryItemType];
			if (profileInventoryItem.Consumable) {
				profileInventoryItem.Count -= count;
				if (profileInventoryItem.Count <= 0) {
					InventoryItems.Remove(inventoryItemType);
				}
			}
		}
	}

	public int GetInventoryItemCount(string inventoryItemType) {
		if (!InventoryItems.ContainsKey(inventoryItemType)) {
			return 0;
		}
		return InventoryItems[inventoryItemType].Count;
	}

	public int GetInventoryItemGrade(string inventoryItemType) {
		if (!InventoryItems.ContainsKey(inventoryItemType)) {
			return 0;
		}
		return InventoryItems[inventoryItemType].Grade;
	}

	public void UpgradeInventoryItem(string inventoryItemType) {
		if (InventoryItems.ContainsKey(inventoryItemType)) {
			InventoryItems[inventoryItemType].Grade += 1;
		}
	}

	public string GetEquippedItemType(string slotId) {
		foreach (string itemType in InventoryItems.Keys) {
			if (InventoryItems[itemType].HeroSlot == slotId && InventoryItems[itemType].Equipped) {
				return itemType;
			}
		}
		return null;
	}

	public bool IsItemEquipped(string inventoryItemType) {
		if (!InventoryItems.ContainsKey(inventoryItemType)) {
			return false;
		}
		return InventoryItems[inventoryItemType].Equipped;
	}

	public void EquipItem(string inventoryItemType, string slotId) {
		if (InventoryItems.ContainsKey(inventoryItemType)) {
			InventoryItems[inventoryItemType].Equipped = true;
			InventoryItems[inventoryItemType].HeroSlot = slotId;
		}
	}

	public void UnequipItem(string inventoryItemType) {
		if (InventoryItems.ContainsKey(inventoryItemType)) {
			InventoryItems[inventoryItemType].Equipped = false;
			InventoryItems[inventoryItemType].HeroSlot = "";
		}
	}
}