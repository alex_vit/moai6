using System.Collections.Generic;

[System.Serializable]
public class ProfileCollection {
	private int ActiveProfileIndex;
	private List<PlayerProfile> Profiles;

	public ProfileCollection() {
		Profiles = new List<PlayerProfile>();
		AddNewProfile();
	}

	public void AddNewProfile() {
		ActiveProfileIndex = Profiles.Count;
		Profiles.Add(new PlayerProfile());
	}

	public void DeleteCurrentProfile() {
		if (Profiles.Count > 1) {
			Profiles.RemoveAt(ActiveProfileIndex);
			ActiveProfileIndex = ActiveProfileIndex - 1;
			if (ActiveProfileIndex < 0) {
				ActiveProfileIndex = 0;
			}
		}
	}

	public PlayerProfile GetCurrentProfile() {
		return Profiles[ActiveProfileIndex];
	}

	public List<string> GetProfilesNames() {
		List<string> result = new List<string>();
		foreach (PlayerProfile playerProfile in Profiles) {
			result.Add(playerProfile.PlayerName);
		}
		return result;
	}

	public void SetActiveProfileIndex(int index) {
		ActiveProfileIndex = index;
	}

	public int GetActiveProfileIndex() {
	return ActiveProfileIndex;
	}
}