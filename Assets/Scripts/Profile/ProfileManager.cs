using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ProfileManager : GenericSingleton<ProfileManager>{
	public sealed class VersionDeserializationBinder : SerializationBinder
	{
		public override Type BindToType(string assemblyName, string typeName)
		{
			if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName))
			{
				Type typeToDeserialize = null;
				assemblyName = Assembly.GetExecutingAssembly().FullName;
				typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));
				return typeToDeserialize;
			}

			return null;
		}
	}

	public static void AddNewProfile() {
		Instance.Profiles.AddNewProfile();
	}

	public static void SetActiveProfileIndex(int index) {
		Instance.Profiles.SetActiveProfileIndex(index);
	}

	public static int GetActiveProfileIndex() {
		return Instance.Profiles.GetActiveProfileIndex();
	}

	public static void DeleteCurrentProfile() {
		Instance.Profiles.DeleteCurrentProfile();
	}

	public static PlayerProfile GetCurrentProfile() {
		return Instance.Profiles.GetCurrentProfile();
	}

	public static List<string> GetProfilesNames() {
		return Instance.Profiles.GetProfilesNames();
	}

	private ProfileCollection Profiles;

	override public void Init() {
		base.Init();
		Load();
		GameCursorManager.Instance.RefreshCursor();
		Screen.fullScreen = GetCurrentProfile().Fullscreen;
	}

	public void Save()
	{
		try
		{
			string directoryPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), Path.Combine("Alawar", "Moai6"));
			string filePath = Path.Combine(directoryPath, "player.dat");
			Stream stream = File.Open(filePath, FileMode.Create);
			BinaryFormatter bformatter = new BinaryFormatter();
			bformatter.Binder = new VersionDeserializationBinder();
			bformatter.Serialize(stream, Profiles);
			stream.Close();
		}
		catch (UnauthorizedAccessException) //many more exception might happen, check documentation
		{}
	}

	private void Load()
	{
		string directoryPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), Path.Combine("Alawar", "Moai6"));
		string filePath = Path.Combine(directoryPath, "player.dat");
		if (File.Exists(filePath))
		{
			try
			{
				Stream stream = File.Open(filePath, FileMode.Open);
				try
				{
					BinaryFormatter bformatter = new BinaryFormatter();
					bformatter.Binder = new VersionDeserializationBinder();
					Profiles = bformatter.Deserialize(stream) as ProfileCollection;
					stream.Close();
				}
				catch (Exception)
				{
					stream.Close();
					File.Delete(filePath);
				}
			}
			catch (IOException)
			{
				//hanlde IOFileExceptions
			}
		}
		if (Profiles == null) {
			if (!Directory.Exists(directoryPath)) {
				Directory.CreateDirectory(directoryPath);
			}
			Profiles = new ProfileCollection();
			Save();
		}
	}
}