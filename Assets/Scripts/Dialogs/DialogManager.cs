﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    [SerializeField] private List<Dialog> TutorialList;
    
    private Dialog _activeDialog;
    public Dialog ActiveDialog
    {
        get { return _activeDialog; }
    }

    public bool IsActive = false;
    private Coroutine autoHideProcess;
    
    void Awake()
    {
        IsActive = false;
        LevelResult levelResult = StorylineManager.Instance.GetLevelResult(StorylineManager.Instance.GetCurrentLevel());
        if (levelResult.Scores <= 0)
            IsActive = true;
    }
    
    private void OnEnable()
    {
        GameEventDispatcher.Instance.AddEventListener(GameEventType.DialogShow, ShowDialog);
        GameEventDispatcher.Instance.AddEventListener(GameEventType.DialogHide, HideDialog);
    }

    private void OnDisable()
    {
        if (GameEventDispatcher.Instance == null) return;
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.DialogShow, ShowDialog);
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.DialogHide, HideDialog);
    }

    private void ShowDialog(GameEvent gameEvent)
    {
        if (gameEvent != null && gameEvent.Target != null && _activeDialog == null && IsActive)
        {
            _activeDialog = gameEvent.Target as Dialog;
            _activeDialog.Show();
            Level.Instance.IsTutorial = true;

            if (autoHideProcess == null)
                autoHideProcess = StartCoroutine(AutoHideDialog(_activeDialog));
        }
    }

    private void HideDialog(GameEvent gameEvent)
    {
        if (gameEvent != null && gameEvent.Target != null && IsActive)
        {
            if (_activeDialog == gameEvent.Target as Dialog && _activeDialog != null)
            {
                if (autoHideProcess != null)
                {
                    StopCoroutine(autoHideProcess);
                    autoHideProcess = null;
                }

                _activeDialog.Hide();
                _activeDialog = null;
                Level.Instance.IsTutorial = false;
            }
        }
    }

    private IEnumerator AutoHideDialog(Dialog dialog)
    {
        yield return new WaitForSeconds(40f);
        GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.DialogHide, dialog));
    }
}
