using System;
using JetBrains.Annotations;

public class DialogClose : TriggeredBehaviour
{
	private Dialog _dialog;

	public override void Awake()
	{
		_dialog = GetComponent<Dialog>();
	}

	protected override void OnTriggerComplete()
	{
		foreach (Trigger trigger in Triggers)
		{
			if (!trigger.Complete)
			{
				return;
			}
		}
		if (!Complete && (Level.Instance == null || Level.Instance.DialogUI.ActiveDialog == _dialog)) {
			Complete = true;
			TriggersComplete();
		}
	}

	public void Activate()
	{
		if (Triggers != null) {
			foreach (Trigger trigger in Triggers) {
				if(trigger != null)
					trigger.StartListening(OnTriggerUpdate, OnTriggerComplete);
			}
		}
	}

	public void Deactivate()
	{
		if (Triggers != null) {
			foreach (Trigger trigger in Triggers) {
				if(trigger != null)
					trigger.StopListening();
			}
		}
	}

	override protected void TriggersComplete() {
		base.TriggersComplete();
		if(_dialog != null)
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.DialogHide, _dialog));
	}

	public void Close() {
		if (_dialog != null && Triggers != null && Triggers.Count <= 0)
		{
			Deactivate();
			//_dialog.Hide();
			GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.DialogHide, _dialog));
		}
	}
}