﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class Dialog : TriggeredBehaviour {
	private enum EmotionType {
		Normal = 0,
		Evil = 1,
		Wondering = 2
	}
	[SerializeField]
	private EmotionType Emotion;
	[SerializeField]
	private RectTransform Container;
	[SerializeField] 
	private List<LevelObject> _AffectedObjects;
	
	public List<LevelObject> AffectedObjects
	{
		get { return _AffectedObjects; }
	}

	private Animator DialogAnimator;
	private DialogClose _DialogClose;
	
	public bool IsShowed = false;

	override public void Awake() {
		base.Awake();
		//Container.gameObject.SetActive(false);
		_DialogClose = GetComponent<DialogClose>();
	}

	override public void OnDisable()
	{
		base.OnDisable();
		//gameObject.SetActive(false);
	}

	override protected void TriggersComplete() {
		//base.TriggersComplete();
		Debug.Log("complete triggers");
		
		GameEventDispatcher.Instance.DispatchEvent(new GameEvent(GameEventType.DialogShow, this));
	}

	private void OnTypewriterComplete() {
		DialogAnimator.SetBool(AnimatorStringsHashesCache.GetStringHash("Speaking"), false);
	}

	public void Show()
	{	
		if (!IsShowed)
		{
			
			IsShowed = true;
			Container.gameObject.SetActive(true);
			
			DialogAnimator = gameObject.GetComponentInChildren<Animator>(true);
			DialogAnimator.SetInteger(AnimatorStringsHashesCache.GetStringHash("EmotionType"), (int) Emotion);
			gameObject.GetComponentInChildren<TypewriterText>(true).SetDelegate(OnTypewriterComplete);
			
			if(_DialogClose != null)
				_DialogClose.Activate();
			
		}
	}

	public void Hide()
	{
		if (IsShowed)
		{
			Container.gameObject.SetActive(false);
		}
	}
}


