﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapDialogManager : DialogManager
{
    [SerializeField]
    private List<Dialog> MapLevelDialogs;

    [SerializeField] private ComicsContainer Comics;

    private bool isLocked = false;

    private void LockDialogs(GameEvent gameEvent)
    {
        isLocked = true;
    }

    private void UnLockDialogs(GameEvent gameEvent)
    {
        isLocked = false;
    }

    private void OnEnable()
    {
        GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelUnlocked, SetActiveDialog);
        GameEventDispatcher.Instance.AddEventListener(GameEventType.LevelButtonClicked, SetActiveBonusDialog);
        GameEventDispatcher.Instance.AddEventListener(GameEventType.DialogShow, ShowMapDialog);
        GameEventDispatcher.Instance.AddEventListener(GameEventType.DialogHide, HideMapDialog);
        GameEventDispatcher.Instance.AddEventListener(GameEventType.ComicsBeginned, LockDialogs);
        GameEventDispatcher.Instance.AddEventListener(GameEventType.ComicsEnded, UnLockDialogs);
    }
    
    private void OnDisable()
    {
        if(GameEventDispatcher.Instance == null) return;
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelUnlocked, SetActiveDialog);
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.LevelButtonClicked, SetActiveBonusDialog);
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.DialogShow, ShowMapDialog);
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.DialogHide, HideMapDialog);
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ComicsBeginned, LockDialogs);
        GameEventDispatcher.Instance.RemoveEventListener(GameEventType.ComicsEnded, UnLockDialogs);
    }
    
    
    
    void SetActiveDialog(GameEvent gameEvent)
    {
        if (gameEvent != null && gameEvent.Target != null && !Comics.IsShowing)
        {
            StorylineLevel level = gameEvent.Target as StorylineLevel;
            if (!level.BonusLevel){
                Debug.Log("SetActiveDialog");
                (MapLevelDialogs[level.LevelId - 1]).Show();
            }
        }
    }

    void SetActiveBonusDialog(GameEvent gameEvent)
    {
        if (gameEvent != null)
        {
            if (StorylineManager.Instance.GetCurrentLevel().BonusLevel)
                (MapLevelDialogs[StorylineManager.Instance.GetCurrentLevel().LevelId - 1]).Show();
        }
    }

    void ShowMapDialog(GameEvent gameEvent)
    {
        if (gameEvent != null && gameEvent.Target != null && !Comics.IsShowing)
        {
            Dialog dlg = (gameEvent.Target as Dialog);
            bool isFirstDialog = dlg.Triggers.Any(x => (x is LevelUnlockedTrigger));
            if (!isFirstDialog)
            {
                Debug.Log("ShowMapDialog");
                dlg.Show();
            }
        }
    }

    void HideMapDialog(GameEvent gameEvent)
    {
        if (gameEvent != null && gameEvent.Target != null)
            (gameEvent.Target as Dialog).Hide();
    }

   
}
