﻿Shader "Effects/Caustics"
{
    Properties
    {
        [PerRendererData] _MainTex ("Main Texture", 2D) = "white" {}
        _CausticsTex ("Caustics Sheet", 2D) = "white" {}
        _Rows ("Caustics Sheet Rows", Float) = 1.0
        _Columns ("Caustics Sheet Columns", Float) = 1.0
        [PerRendererData] _TileIndex ("Caustics Tile Index", Int) = 0
        [PerRendererData] _MaskTex ("Caustics", 2D) = "white" {}
        [PerRendererData] _MaskScaleX ("Mask scale x", Range(0.0, 2.0)) = 1.0
        [PerRendererData] _MaskScaleY ("Mask scale y", Range(0.0, 2.0)) = 1.0
        [PerRendererData] _TilingRatioWidth ("Tiling Ration Width", Float) = 1.0
        [PerRendererData] _TilingRatioHeight ("Tiling Ration Height", Float) = 1.0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"

            sampler2D _CausticsTex;
            sampler2D _MaskTex;
            float4 _CausticsTex_ST;
            float _Rows;
            float _Columns;
            int _TileIndex;
            float _MaskScaleX;
            float _MaskScaleY;
            float _TilingRatioWidth;
            float _TilingRatioHeight;

            v2f vert (appdata_t v)
            {
                v2f o;
                UNITY_INITIALIZE_OUTPUT(v2f, o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = v.texcoord;
                return o;
            }

            half4 frag (v2f IN) : COLOR
            {
                half4 mt = tex2D(_MainTex, IN.texcoord);
                float2 mkt_texcoord = IN.texcoord;
                mkt_texcoord.x = mkt_texcoord.x * _MaskScaleX;
                mkt_texcoord.y = mkt_texcoord.y * _MaskScaleY;
                half4 mkt = tex2D(_MaskTex, mkt_texcoord);
                float tileWidth = 1.0 / (float)_Columns;
                float tileHeight = 1.0 / (float)_Rows;

                int tilePositionY = floor(_TileIndex / _Columns + 0.00001);
                int tilePositionX = _TileIndex - tilePositionY * _Columns;

                tilePositionY = tilePositionY + 1;

                IN.texcoord.x = frac(IN.texcoord.x * _TilingRatioWidth / 1.7);
                IN.texcoord.y = frac(IN.texcoord.y * _TilingRatioHeight / 1.7);
                IN.texcoord.y = IN.texcoord.y - floor(IN.texcoord.y / tileHeight) * tileHeight + (_Rows - tilePositionY) * tileHeight;
                IN.texcoord.x = IN.texcoord.x - floor(IN.texcoord.x / tileWidth) * tileWidth + tilePositionX * tileWidth;

                half4 ct = tex2D(_CausticsTex, IN.texcoord);

                return ((mt * (1 - ct.w) + ct * ct.w) * mkt.w + (1 - mkt.w) * mt) * _RendererColor;
                //return ct * 2;
            }
            ENDCG
        }
    }
}