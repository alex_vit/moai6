Shader "Effects/Fog"
{
    Properties
    {
       [PerRendererData] _MaskTex ("Mask Texture", 2D) = "white" {}
       [PerRendererData] _FogTex ("Fog Texture", 2D) = "white" {}
       [PerRendererData] _Color ("Tint", Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
        }

        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Offset -1, -1
            Fog { Mode Off }
            ColorMask RGB
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MaskTex;
            sampler2D _FogTex;
            fixed4 _Color;

            struct appdata_t
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = v.texcoord;
                return o;
            }

            half4 frag (v2f IN) : COLOR
            {
                half4 ct = tex2D(_FogTex, IN.texcoord);
                half4 mt = tex2D(_MaskTex, IN.texcoord);
                ct.w = ct.w * mt;

                return ct * _Color;
                //return ct * 2;
            }
            ENDCG
        }
    }
}