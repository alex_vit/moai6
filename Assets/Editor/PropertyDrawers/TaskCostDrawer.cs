using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(TaskCost))]
public class TaskCostDrawer : SmartPropertyDrawer{

	override protected void Draw(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		PropertyHeight = 18.0f;
		DrawProperties(new Rect(position.x, position.y + 18.0f, position.width, position.height), property, label, simulateDraw);
	}

	override protected void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		base.DrawProperties(position, property, label, simulateDraw);
		SerializedProperty costTypeProperty = DrawProperty(position, property, "CostType", out position, simulateDraw);
		int enumIndex = costTypeProperty.enumValueIndex;
		if (enumIndex == 0 || enumIndex == 1) {
			SerializedProperty prefabProperty = DrawProperty(position, property, "ResourcePrefab", out position, simulateDraw);
			DrawProperty(position, property, "ResourceCount", out position, simulateDraw);
			if (enumIndex == 1) {
				GameObject prefabObject = (prefabProperty.objectReferenceValue as GameObject);
				if (prefabObject != null) {
					List<LevelObjectState> possibleStates = new List<LevelObjectState> (prefabObject.GetComponents<LevelObjectState>());
					possibleStates.Insert(0, null);
					List<string> stateNames = new List<string> () {"None"};
					for (int i = 1; i < possibleStates.Count; i++) {
						stateNames.Add (possibleStates[i].StateName);
					}
					DrawPopup(position, property, "_TaskWorkerState", stateNames.ToArray(), out position, simulateDraw);
					DrawPopup(position, property, "_TaskWorkerStateAfter", stateNames.ToArray(), out position, simulateDraw);
				}
			}
		} else if (enumIndex == 2) {
			Level level = (Level)Object.FindObjectOfType(typeof (Level));
			int goalsCount = level.GetComponents<LevelGoal>().Length;
			string[] goalVariants = new string[goalsCount];
			for (int i = 0; i < goalsCount; i++) {
				goalVariants[i] = (i + 1).ToString();
			}
			DrawPopup(position, property, "GoalIndex", goalVariants, out position, simulateDraw);
		}
	}
}