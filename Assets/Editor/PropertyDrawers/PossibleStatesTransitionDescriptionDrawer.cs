using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(PossibleStatesTransitionDescription))]
public class PossibleStatesTransitionDescriptionDrawer : SmartPropertyDrawer {

	override protected void Draw(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		PropertyHeight = 18.0f;
		DrawProperties(new Rect(position.x, position.y + 18.0f, position.width, position.height), property, label, simulateDraw);
	}

	override protected void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		base.DrawProperties(position, property, label, simulateDraw);
		SerializedProperty levelObjectProperty = DrawProperty(position, property, "_TargetGameObject", out position, simulateDraw);
		GameObject stateOwnerObject = (levelObjectProperty.objectReferenceValue as GameObject);
		if (stateOwnerObject != null) {
			List<LevelObjectState> possibleStates = new List<LevelObjectState> (stateOwnerObject.GetComponents<LevelObjectState> ());
			List<string> stateNames = new List<string> ();
			for (int i = 0; i < possibleStates.Count; i++) {
				stateNames.Add (possibleStates[i].StateName);
			}
			DrawArrayAsEnum(position, property, "PossibleStates", stateNames.ToArray(), out position, simulateDraw);
			DrawPopup(position, property, "_TargetState", stateNames.ToArray(), out position, simulateDraw);
			DrawProperty(position, property, "_MinimumNotAffectedCount", out position, simulateDraw);
		}
	}
}