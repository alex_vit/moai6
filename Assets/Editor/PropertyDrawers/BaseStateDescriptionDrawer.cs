using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(BaseStateDescription))]
public class BaseStateDescriptionDrawer : SmartPropertyDrawer {

	override protected void Draw(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		PropertyHeight = 0.0f;
		DrawProperties(position, property, label, simulateDraw);
	}

	override protected void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		base.DrawProperties(position, property, label, simulateDraw);
		GameObject stateOwnerObject = (property.serializedObject.targetObject as MonoBehaviour).gameObject;
		if (stateOwnerObject != null) {
			List<LevelObjectState> possibleStates = new List<LevelObjectState> (stateOwnerObject.GetComponents<LevelObjectState> ());
			possibleStates.Insert(0, null);
			List<string> stateNames = new List<string> () {"None"};
			for (int i = 1; i < possibleStates.Count; i++) {
				stateNames.Add (possibleStates[i].StateName);
			}
			DrawPopup(position, property, "_StateName", stateNames.ToArray(), out position, simulateDraw);
		}
	}
}