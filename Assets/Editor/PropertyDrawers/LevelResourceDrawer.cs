using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(LevelResource))]
public class LevelResourceDrawer : SmartPropertyDrawer{
	override protected void Draw(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		PropertyHeight = 18.0f;
		DrawProperties(new Rect(position.x, position.y + 18.0f, position.width, position.height), property, label, simulateDraw);
	}

	override protected void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		base.DrawProperties(position, property, label, simulateDraw);
		DrawProperty(position, property, "ResourcePrefab", out position, simulateDraw);
		DrawProperty(position, property, "ResourceCount", out position, simulateDraw);
	}
}