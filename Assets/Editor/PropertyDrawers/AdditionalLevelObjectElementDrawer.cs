using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(AdditionalLevelObjectElement))]
public class AdditionalLevelObjectElementDrawer : SmartPropertyDrawer {
	override protected void Draw(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		PropertyHeight = 18.0f;
		EditorGUI.LabelField(new Rect(position.x, position.y, position.width, 18.0f), property.displayName);
		DrawProperties(new Rect(position.x, position.y + 18.0f, position.width, position.height), property, label, simulateDraw);
	}

	override protected void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		base.DrawProperties(position, property, label, simulateDraw);
		DrawProperty(position, property, "_ElementPrefab", out position, simulateDraw);
		DrawProperty(position, property, "_ElementPosition", out position, simulateDraw);
	}
}