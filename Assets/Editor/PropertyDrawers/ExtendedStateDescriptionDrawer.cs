using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ExtendedStateDescription))]
public class ExtendedStateDescriptionDrawer : SmartPropertyDrawer {

	override protected void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		base.DrawProperties(position, property, label, simulateDraw);
		GameObject stateOwnerObject = null;
		SerializedProperty ownStateProperty = DrawProperty(position, property, "_OwnState", out position, simulateDraw);
		if (ownStateProperty.boolValue) {
			stateOwnerObject = (property.serializedObject.targetObject as MonoBehaviour).gameObject;
			property.FindPropertyRelative("_LevelObject").objectReferenceValue = null;
		} else {
			SerializedProperty levelObjectProperty = DrawProperty(position, property, "_LevelObject", out position, simulateDraw);
			stateOwnerObject = (levelObjectProperty.objectReferenceValue as GameObject);
		}
		if (stateOwnerObject != null) {
			List<LevelObjectState> possibleStates = new List<LevelObjectState> (stateOwnerObject.GetComponents<LevelObjectState> ());
			possibleStates.Insert(0, null);
			List<string> stateNames = new List<string> () {"None"};
			for (int i = 1; i < possibleStates.Count; i++) {
				stateNames.Add (possibleStates[i].StateName);
			}
			DrawPopup(position, property, "_StateName", stateNames.ToArray(), out position, simulateDraw);
		}
	}
}