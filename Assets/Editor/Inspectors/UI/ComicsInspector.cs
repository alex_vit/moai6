using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Comics), false)]
public class ComicsInspector : TriggeredBehaviourInspector {
	public override void OnInspectorGUI() {
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("Frames"), true);
		serializedObject.ApplyModifiedProperties();
		base.OnInspectorGUI();
	}
}