using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LocalizedText), true)]
public class LocalizedTextInspector : Editor{

	private bool TextChanged;

	override public void OnInspectorGUI() {
		serializedObject.Update();

		SerializedProperty stringKeyProperty = serializedObject.FindProperty("_StringKey");
		bool updateText = ShowLocalizedKeyInspector(stringKeyProperty, ref TextChanged);
		serializedObject.ApplyModifiedProperties();
		if (updateText) {
			(target as LocalizedText).OnEnable();
		}
	}

	public static bool ShowLocalizedKeyInspector(SerializedProperty stringKeyProperty, ref bool textChanged) {
		EditorGUILayout.BeginVertical();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(stringKeyProperty.displayName);
		string newKey = EditorGUILayout.TextField(stringKeyProperty.stringValue);
		if (newKey != stringKeyProperty.stringValue) {
			stringKeyProperty.stringValue = newKey;
			textChanged = true;
		}
		EditorGUILayout.EndHorizontal();

		bool updateText = false;
		if (textChanged) {
			List<string> variants = LocalizationManager.Instance.GetKeysList(newKey);
			int newSelected = EditorGUILayout.Popup(-1, variants.ToArray());
			//int newSelected = GUILayout.SelectionGrid(-1, variants.ToArray(), 1);
			if (newSelected > -1) {
				stringKeyProperty.stringValue = variants[newSelected];
				GUIUtility.keyboardControl = 0;
				updateText = true;
			}
		}
		EditorGUILayout.EndVertical();

		return updateText;

	}
}