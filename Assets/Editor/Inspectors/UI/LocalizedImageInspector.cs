using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LocalizedImage), true)]
public class LocalizedImageInspector : Editor{

	private bool Changed;

	override public void OnInspectorGUI() {
		serializedObject.Update();
		SerializedProperty imageNameProperty = serializedObject.FindProperty("_ImageName");
		ShowImageNameInspector(imageNameProperty, ref Changed);
		serializedObject.ApplyModifiedProperties();
	}

	public static void ShowImageNameInspector(SerializedProperty imageNameProperty, ref bool TextChanged) {

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(imageNameProperty.displayName);
		string newKey = EditorGUILayout.TextField(imageNameProperty.stringValue);
		if (newKey != imageNameProperty.stringValue) {
			imageNameProperty.stringValue = newKey;
			TextChanged = true;
		}
		EditorGUILayout.EndHorizontal();

		if (TextChanged) {
			List<string> variants = LocalizationManager.Instance.GetImagesList(newKey);
			int newSelected = GUILayout.SelectionGrid(-1, variants.ToArray(), 1);
			if (newSelected > -1) {
				imageNameProperty.stringValue = variants[newSelected];
				TextChanged = false;
				GUIUtility.keyboardControl = 0;
			}
		}

	}
}