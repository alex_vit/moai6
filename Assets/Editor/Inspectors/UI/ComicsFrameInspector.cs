using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ComicsFrame))]
public class ComicsFrameInspector : PropertyDrawer
{

	private bool TextChanged;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.PropertyField( position, property, label, true );
		SerializedProperty stringKeyProperty = property.FindPropertyRelative("_Text");

		string newKey = EditorGUI.TextField(new Rect(position.x + 15, position.y + 54, position.width - 33, 18), stringKeyProperty.displayName, stringKeyProperty.stringValue);
		if (newKey != stringKeyProperty.stringValue) {
			stringKeyProperty.stringValue = newKey;
			TextChanged = true;
		}

		if (TextChanged) {
			List<string> variants = LocalizationManager.Instance.GetKeysList(newKey);
			int newSelected = GUILayout.SelectionGrid(-1, variants.ToArray(), 1);
			if (newSelected > -1) {
				stringKeyProperty.stringValue = variants[newSelected];
				TextChanged = false;
				GUIUtility.keyboardControl = 0;
			}
		}
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		if (property.isExpanded) {
			return 72.0f;
		}
		return 18.0f;
	}
}