using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(StorylineLevel), true)]
public class StorylineLevelInspector : Editor{

	private bool TextChanged;

	override public void OnInspectorGUI() {
		base.OnInspectorGUI();
		serializedObject.Update();
		SerializedProperty stringKeyProperty = serializedObject.FindProperty("_LevelName");
		ShowLocalizedKeyInspector(stringKeyProperty, ref TextChanged);
		serializedObject.ApplyModifiedProperties();
	}

	public static void ShowLocalizedKeyInspector(SerializedProperty stringKeyProperty, ref bool TextChanged) {

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(stringKeyProperty.displayName);
		string newKey = EditorGUILayout.TextField(stringKeyProperty.stringValue);
		if (newKey != stringKeyProperty.stringValue) {
			stringKeyProperty.stringValue = newKey;
			TextChanged = true;
		}
		EditorGUILayout.EndHorizontal();

		if (TextChanged) {
			List<string> variants = LocalizationManager.Instance.GetKeysList(newKey);
			int newSelected = GUILayout.SelectionGrid(-1, variants.ToArray(), 1);
			if (newSelected > -1) {
				stringKeyProperty.stringValue = variants[newSelected];
				TextChanged = false;
				GUIUtility.keyboardControl = 0;
			}
		}
	}
}