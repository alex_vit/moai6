using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Dialog), false)]
public class DialogInspector : TriggeredBehaviourInspector {
	public override void OnInspectorGUI() {
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("Emotion"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("Container"));
		CreateListForObject(serializedObject.FindProperty("_AffectedObjects"));
		//EditorGUILayout.PropertyField(serializedObject.FindProperty("DialogAnimator"));
		serializedObject.ApplyModifiedProperties();
		base.OnInspectorGUI();
	}
	
	private void CreateListForObject(SerializedProperty listObj)
	{
		EditorGUILayout.PropertyField(listObj);
		EditorGUI.indentLevel += 1;
		if (listObj.isExpanded) {
			EditorGUILayout.PropertyField(listObj.FindPropertyRelative("Array.size"));
			for (int i = 0; i < listObj.arraySize; i++) {
				EditorGUILayout.PropertyField(listObj.GetArrayElementAtIndex(i));
			}
		}
		EditorGUI.indentLevel -= 1;
	}
}