using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TriggeredBehaviour), true)]
public class TriggeredBehaviourInspector : Editor {

	public override void OnInspectorGUI() {
		serializedObject.Update();
		SerializedProperty triggersProperty = serializedObject.FindProperty("Triggers");

		triggersProperty.isExpanded = EditorGUILayout.Foldout(triggersProperty.isExpanded, "Triggers");
		if (triggersProperty.isExpanded) {
			ShowTriggers(triggersProperty);
			if (GUILayout.Button (new GUIContent ("+", "Add trigger"))) {
				triggersProperty.arraySize++;
				SerializedProperty triggerProperty = triggersProperty.GetArrayElementAtIndex(triggersProperty.arraySize - 1);
				triggerProperty.objectReferenceValue = null;
			}
			if (GUILayout.Button (new GUIContent ("Add Selected Objects Triggers", "Add Selected Objects Triggers"))) {
				foreach (GameObject gameObject in Selection.gameObjects) {
					LevelObject levelObject = gameObject.GetComponent<LevelObject>();
					if (levelObject != null) {
						triggersProperty.arraySize++;
						SerializedProperty triggerProperty = triggersProperty.GetArrayElementAtIndex(triggersProperty.arraySize - 1);
						LevelObjectStateTrigger trigger = ScriptableObject.CreateInstance(typeof(LevelObjectStateTrigger)) as LevelObjectStateTrigger;
						trigger.LevelObjectStateType = LevelObjectStateTrigger.LevelObjectStateTriggerType.SpecificObject;
						PossibleStatesDescription possibleStatesDescription = new PossibleStatesDescription();
						possibleStatesDescription.SetTargetGameObject(gameObject);
						trigger.PossibleStatesDescription = possibleStatesDescription;
						trigger.NeededCount = 1;
						triggerProperty.objectReferenceValue = trigger;
					}
				}
			}
		}

		serializedObject.ApplyModifiedProperties();
	}

	private void ShowTriggers(SerializedProperty triggersProperty) {
		EditorGUI.indentLevel++;

		List<Trigger> availableTriggers = ReflectiveEnumerator.GetEnumerableOfType<Trigger>() as List<Trigger>;
		availableTriggers.Insert(0, null);
		List<string> triggerNames = new List<string>() {"None"};
		int selectedTriggerIndex = 0;
		Type currentTriggerType = null;

		for (int i = 1; i < availableTriggers.Count; i++) {
			Type triggerType = availableTriggers[i].GetType();
			triggerNames.Add(triggerType.ToString());
		}

		for (int i = 0; i < triggersProperty.arraySize; i++) {
			SerializedProperty triggerProperty = triggersProperty.GetArrayElementAtIndex(i);
			if (triggerProperty.objectReferenceValue != null) {
				currentTriggerType = triggerProperty.objectReferenceValue.GetType();
			}

			for (int j = 1; j < availableTriggers.Count; j++) {
				if (availableTriggers[j].GetType() == currentTriggerType) {
					selectedTriggerIndex = j;
					break;
				}
			}

			EditorGUILayout.BeginHorizontal();
			triggerProperty.isExpanded = EditorGUILayout.Foldout(triggerProperty.isExpanded, currentTriggerType != null ? currentTriggerType.Name : "Unassigned");
			bool deleted = false;
			if (GUILayout.Button (new GUIContent ("-", "Remove trigger"), GUILayout.Width(30))) {
				triggerProperty.objectReferenceValue = null;
				triggersProperty.DeleteArrayElementAtIndex (i);
				deleted = true;
			}
			EditorGUILayout.EndHorizontal();
			if (!deleted && triggerProperty.isExpanded) {
				EditorGUI.indentLevel++;
				EditorGUILayout.LabelField("Trigger Type:");
				int newSelectedTriggerIndex = EditorGUILayout.Popup(selectedTriggerIndex, triggerNames.ToArray());
				if (newSelectedTriggerIndex != selectedTriggerIndex) {
					if (newSelectedTriggerIndex == 0) {
						triggerProperty.objectReferenceValue = null;
					} else {
						currentTriggerType = availableTriggers[newSelectedTriggerIndex].GetType();
						triggerProperty.objectReferenceValue = ScriptableObject.CreateInstance(currentTriggerType);
					}
				}
				ShowTriggerEditor(triggerProperty.objectReferenceValue as Trigger);
				EditorGUI.indentLevel--;
			}
		}
		EditorGUI.indentLevel--;
	}

	private void ShowTriggerEditor(Trigger trigger) {
		Type editorType = typeof(Editor);
		Editor stateEditor = Editor.CreateEditor(trigger, editorType);
		if (stateEditor != null) {
			stateEditor.OnInspectorGUI();
		}
	}
}