﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

[CustomEditor(typeof(LevelObject))]
public class LevelObjectInspector : Editor {

	private bool StateWasChanged;
	private bool TooltipHeaderChanged;
	private static List<WorkingPoint> WorkingPoints;

	public void OnSceneGUI()
	{
		LevelObject levelObject = (LevelObject)target;
		float size = 0.1f;
		Handles.color = Color.blue;
		Handles.matrix = levelObject.transform.localToWorldMatrix;
		if (levelObject.WorkingPoints != null) {
			for (int i = 0; i < levelObject.WorkingPoints.Count; i++) {
				EditorGUI.BeginChangeCheck();
				levelObject.WorkingPoints[i].Position = Handles.FreeMoveHandle(levelObject.WorkingPoints[i].Position, Quaternion.identity, size, Vector3.zero, Handles.DotHandleCap);
				if (EditorGUI.EndChangeCheck())
				{
					Undo.RecordObject(levelObject, "Changed Working Point Position");
				}
			}
		}
	}

	private void CopyPoints()
	{
		WorkingPoints = (target as LevelObject).WorkingPoints;
	}

	private void PastePoints()
	{
		if (WorkingPoints != null) {
			(target as LevelObject).WorkingPoints = new List<WorkingPoint>(WorkingPoints);
			WorkingPoints = null;
		}
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_LevelObjectType"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_LevelObjectGrade"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_Cursor"));

		SerializedProperty elementsExpanded = serializedObject.FindProperty("_AdditionalElementsExpanded");

		elementsExpanded.boolValue = EditorGUILayout.Foldout(elementsExpanded.boolValue, "Additional Elements");

		if (elementsExpanded.boolValue) {
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_TaskMark"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Dust"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_SingleTaskIcon"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_PathBlockedIcon"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Selector"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_Flag"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("_UnderwaterEffect"), true);
		}

		EditorGUILayout.PropertyField(serializedObject.FindProperty("UnblocksByAnyPoint"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_OccupyPointsСonsistently"));
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button (new GUIContent ("Copy points", "Copy"))) {
			CopyPoints();
		}
		if (GUILayout.Button (new GUIContent ("Paste points", "Paste"))) {
			PastePoints();
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_WorkingPoints"), true);
		LocalizedTextInspector.ShowLocalizedKeyInspector(serializedObject.FindProperty("TooltipHeader"), ref TooltipHeaderChanged);
        LevelObjectState[] allStates = (target as LevelObject).GetComponents<LevelObjectState> ();
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_CurrentStateDescription"), true);
		if (EditorGUI.EndChangeCheck()) {
			StateWasChanged = true;
		}
		DrawStateInspectors(allStates);
		serializedObject.ApplyModifiedProperties ();
	}

	public static bool SampleState(LevelObject levelObject, string newStateName) {
		Animator animator = levelObject.gameObject.GetComponent<Animator> ();
		AnimationClip stateAnimationClip = null;
		if (animator != null) {
			string assetPath = AssetDatabase.GetAssetPath(animator.runtimeAnimatorController);
			AnimatorController controller = AssetDatabase.LoadAssetAtPath<AnimatorController>(assetPath);
			if (controller != null) {
				foreach (ChildAnimatorState childState in controller.layers[0].stateMachine.states) {
					if (childState.state.name == newStateName) {
						stateAnimationClip = childState.state.motion as AnimationClip;
						break;
					}
				}
				foreach (ChildAnimatorStateMachine childStateMachine in controller.layers[0].stateMachine.stateMachines) {
					foreach (ChildAnimatorState childState in childStateMachine.stateMachine.states) {
						if (childState.state.name.Contains(newStateName)) {
							stateAnimationClip = childState.state.motion as AnimationClip;
							break;
						}
					}
				}
			} else {
				AnimatorOverrideController overrideController = AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(assetPath);
				assetPath = AssetDatabase.GetAssetPath (overrideController.runtimeAnimatorController);
				controller = AssetDatabase.LoadAssetAtPath<AnimatorController>(assetPath);
				List<KeyValuePair<AnimationClip, AnimationClip>> overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>(){};
				foreach (ChildAnimatorState childState in controller.layers[0].stateMachine.states) {
					if (childState.state.name == newStateName && childState.state.motion != null) {
						overrideController.GetOverrides(overrides);
						foreach (KeyValuePair<AnimationClip, AnimationClip> pair in overrides) {
							if (pair.Key != null && pair.Key.name == childState.state.motion.name) {
								stateAnimationClip = pair.Value;
								break;
							}
						}
						break;
					}
				}

				foreach (ChildAnimatorStateMachine childStateMachine in controller.layers[0].stateMachine.stateMachines) {
					foreach (ChildAnimatorState childState in childStateMachine.stateMachine.states) {
						if (childState.state.name == newStateName && childState.state.motion != null) {
							overrideController.GetOverrides(overrides);
							foreach (KeyValuePair<AnimationClip, AnimationClip> pair in overrides) {
								if (pair.Key != null && pair.Key.name == childState.state.motion.name) {
									stateAnimationClip = pair.Value;
									break;
								}
							}
							break;
						}
					}
				}
			}
		}
		if (stateAnimationClip != null) {
			stateAnimationClip.SampleAnimation (levelObject.gameObject, 0);
			return true;
		}
		return false;
	}

	private void DrawStateInspectors(LevelObjectState[] allStates) {
		SerializedProperty currentStateNameProperty = serializedObject.FindProperty("_CurrentStateDescription");
		for (int i = 0; i < allStates.Length; i++) {
			bool sequenceExpanded = allStates[i].Expanded;
			EditorGUILayout.BeginHorizontal();
			sequenceExpanded = EditorGUILayout.Foldout(sequenceExpanded, allStates[i].StateName + " (" + allStates[i].GetType().ToString() + ")");
			allStates[i].Expanded = sequenceExpanded;
			bool deleted = false;
			if (GUILayout.Button (new GUIContent ("-", "Remove state"), GUILayout.Width(30))) {
				DestroyImmediate(allStates[i], true);
				deleted = true;
			}
			EditorGUILayout.EndHorizontal();

			Type editorType;
			if (allStates[i] is StateWithWorkers) {
				editorType = typeof(StateWithWorkersInspector);
			} else if (allStates[i] is StateProducingDrops) {
				editorType = typeof(StateProductingDropsInspector);
			} else if (allStates[i] is StateSeekengEnemy) {
				editorType = typeof(StateSeekingEnemyInspector);
			} else if (allStates[i] is RandomPointRunningState) {
				editorType = typeof(RandomPointRunningStateInspector);
			} else if (allStates[i] is PointRunningState){
				editorType = typeof(PointRunningStateInspector);
			} else if (allStates[i] is RunningState){
				editorType = typeof(MasterRunningStateInspector);
			} else if (allStates[i] is WorkerRunningState){
				editorType = typeof(WorkerRunningStateInspector);
			} else if (allStates[i] is StateWithTransitionBasedOnCounter){
				editorType = typeof(StateWithTransitionBasedOnCounterInspector);
			} else if (allStates[i] is DamagingState){
				editorType = typeof(DamagingStateInspector);
			} else{
				editorType = typeof(LevelObjectStateInspector);
			}

			Editor stateEditor = Editor.CreateEditor(allStates[i], editorType);
			if (StateWasChanged) {
				if (currentStateNameProperty.FindPropertyRelative("_StateName").stringValue == allStates[i].StateName) {
					stateEditor.serializedObject.Update();
					stateEditor.serializedObject.FindProperty("m_Enabled").boolValue = true;
					stateEditor.serializedObject.ApplyModifiedProperties();
					SampleState((target as LevelObject), allStates[i].StateName);
				} else {
					stateEditor.serializedObject.Update();
					stateEditor.serializedObject.FindProperty("m_Enabled").boolValue = false;
					stateEditor.serializedObject.ApplyModifiedProperties();
				}
			}

			if (!deleted && sequenceExpanded) {

				EditorGUI.indentLevel++;
				if (stateEditor != null) {
					stateEditor.OnInspectorGUI ();
				}
				EditorGUI.indentLevel--;
			}
		}
		StateWasChanged = false;
	}
}
