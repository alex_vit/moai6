using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelObjectSortablePart))]
public class LevelObjectSortablePartInspector : Editor
{
	static private List<Vector3> Corners;

	private void CopyCorners()
	{
		Corners = (target as LevelObjectSortablePart).Corners;
	}

	private void PasteCorners()
	{
		if (Corners != null) {
			(target as LevelObjectSortablePart).Corners = Corners;
			Corners = null;
		}
	}


	public void OnSceneGUI()
	{
		LevelObjectSortablePart levelObjectSortablePart = (LevelObjectSortablePart)target;

		float size = 0.08f;
		Vector3 snap = Vector3.one * 0.1f;

		Handles.matrix = levelObjectSortablePart.transform.localToWorldMatrix;

		if (levelObjectSortablePart.Corners == null) {
			levelObjectSortablePart.Corners = new List<Vector3>(){};
		}

		if (levelObjectSortablePart.Corners.Count == 0) {
			levelObjectSortablePart.Corners.Add(Vector3.zero);
			levelObjectSortablePart.Corners.Add(Vector3.zero);
			levelObjectSortablePart.Corners.Add(Vector3.zero);
			levelObjectSortablePart.Corners.Add(Vector3.zero);
		}

		for (int i = 0; i < 4; i++) {
			levelObjectSortablePart.Corners[i] = Handles.FreeMoveHandle(levelObjectSortablePart.Corners[i], Quaternion.identity, size, snap, Handles.DotHandleCap);
		}

		List<Vector3> sortedList = GetSortedPoints(levelObjectSortablePart.Corners);
		Handles.DrawLine(sortedList[0], sortedList[2]);
		Handles.DrawLine(sortedList[2], sortedList[3]);
		Handles.DrawLine(sortedList[3], sortedList[1]);
		Handles.DrawLine(sortedList[1], sortedList[0]);
	}

	public void OnDisable() {
		LevelObjectSortablePart levelObjectSortablePart = (LevelObjectSortablePart)target;
		levelObjectSortablePart.Corners = GetSortedPoints(levelObjectSortablePart.Corners);
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty("_Corners"));
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button (new GUIContent ("Copy corners", "Copy"))) {
			CopyCorners();
		}
		if (GUILayout.Button (new GUIContent ("Paste corners", "Paste"))) {
			PasteCorners();
		}
		EditorGUILayout.EndHorizontal();
		serializedObject.ApplyModifiedProperties ();
	}

	private List<Vector3> GetSortedPoints(List<Vector3> points) {
		List<Vector3> result = new List<Vector3>(points);
		foreach (Vector3 point in points) {
			if (point.x < result[0].x) {
				result[0] = point;
			}
			if (point.y > result[1].y) {
				result[1] = point;
			}
			if (point.y < result[2].y) {
				result[2] = point;
			}
			if (point.x > result[3].x) {
				result[3] = point;
			}
		}
		return result;
	}
}