using UnityEditor;
using UnityEngine;

public class StateProductingDropsInspector : LevelObjectStateInspector {

	public override void OnInspectorGUI() {
		base.OnInspectorGUI ();
		serializedObject.Update();
		if (serializedObject.FindProperty("DropsID").intValue == 0) {
			serializedObject.FindProperty("DropsID").intValue = target.GetInstanceID();	
		}
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("DropsID"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("DropsAreHidden"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("ProducingType"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("DropsToProduce"), true);

		SerializedProperty animationLengthProperty = serializedObject.FindProperty ("AnimationLength");
		SerializedProperty timeToProduceDropProperty = serializedObject.FindProperty ("TimeToProduceDrops");

		if (timeToProduceDropProperty.floatValue < animationLengthProperty.floatValue) {
			timeToProduceDropProperty.floatValue = animationLengthProperty.floatValue;
		}

		EditorGUILayout.PropertyField(animationLengthProperty);

		EditorGUILayout.PropertyField(timeToProduceDropProperty);

		serializedObject.ApplyModifiedProperties ();
	}

}