using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelObjectState), true)]
public class DummyLevelObjectStateInspector : Editor {

	public void Awake(){
		target.hideFlags = HideFlags.HideInInspector;
	}

	public override void OnInspectorGUI() {
	}
}