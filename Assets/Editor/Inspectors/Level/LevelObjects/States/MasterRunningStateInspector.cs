using UnityEditor;
using UnityEngine;

public class MasterRunningStateInspector : WorkerRunningStateInspector {

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("TargetPosition"));
		serializedObject.ApplyModifiedProperties ();
	}
}
