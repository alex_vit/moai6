using UnityEditor;

public class DamagingStateInspector : LevelObjectStateInspector {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI ();
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("_DamageValue"));
		serializedObject.ApplyModifiedProperties ();
	}
}