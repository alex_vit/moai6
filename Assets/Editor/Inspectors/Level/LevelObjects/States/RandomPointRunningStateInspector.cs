using UnityEditor;

public class RandomPointRunningStateInspector : MasterRunningStateInspector {
	public override void OnInspectorGUI() {
		serializedObject.Update();
		serializedObject.ApplyModifiedProperties ();
		base.OnInspectorGUI();
	}
}