using UnityEditor;

public class StateWithTransitionBasedOnCounterInspector : LevelObjectStateInspector {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("Counters"), true);
		serializedObject.ApplyModifiedProperties ();
	}
}