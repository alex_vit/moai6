using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StateWithWorkersInspector : LevelObjectStateInspector {

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("TimeToComplete"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("ShowDust"));
		serializedObject.ApplyModifiedProperties ();
	}
}