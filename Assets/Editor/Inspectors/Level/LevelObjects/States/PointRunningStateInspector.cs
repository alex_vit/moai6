﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PointRunningStateInspector : MasterRunningStateInspector {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI(); 
        serializedObject.Update();
        CreateListForObject(serializedObject.FindProperty("_MotionPoints"));
        serializedObject.ApplyModifiedProperties();
        
    }

    private void CreateListForObject(SerializedProperty listObj)
    {
        EditorGUILayout.PropertyField(listObj);
        EditorGUI.indentLevel += 1;
        if (listObj.isExpanded) {
            EditorGUILayout.PropertyField(listObj.FindPropertyRelative("Array.size"));
            for (int i = 0; i < listObj.arraySize; i++) {
                EditorGUILayout.PropertyField(listObj.GetArrayElementAtIndex(i));
            }
        }
        EditorGUI.indentLevel -= 1;
    }
}
