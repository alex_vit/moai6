using UnityEditor;

public class StateSeekingEnemyInspector : WorkerRunningStateInspector {
	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("LineOfSightRadius"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("PossibleTargets"), true);
		serializedObject.ApplyModifiedProperties ();
	}
}