﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using System;

public class LevelObjectStateInspector : Editor {

	private static List<Task> Tasks;
	private static bool TaskNameChanged;

	public class AnimatorControllers {
		public AnimatorController OriginalController;
		public AnimatorOverrideController OverrideController;
	}

	public void Awake() {
		//target.hideFlags = HideFlags.None;
	}

	private void CopyTasks()
	{
		Tasks = (target as LevelObjectState).Tasks;
	}

	private void PasteTasks()
	{
		if (Tasks != null) {
			(target as LevelObjectState).Tasks.AddRange(Tasks);
			Tasks = null;
		}
	}



	public override void OnInspectorGUI() {
		serializedObject.Update();
		
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("_interactiveWhenBlocked"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("ShowFlag"));

		AnimatorControllers controllers = GetAnimatorControllers((target as LevelObjectState).gameObject.GetComponent<Animator> ());
		List<string> animations = GetAnimationsOfController(controllers.OriginalController);
		animations.Insert (0,"None");

		SerializedProperty stateNameProperty = serializedObject.FindProperty ("_StateName");

		int selectedAnimationIndex = animations.IndexOf (stateNameProperty.stringValue);
		if (selectedAnimationIndex < 0) {
			selectedAnimationIndex = 0;
		}

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Animation:");
		selectedAnimationIndex = EditorGUILayout.Popup (selectedAnimationIndex, animations.ToArray ());
		EditorGUILayout.EndHorizontal ();

		if (controllers.OriginalController != null) {
			SerializedProperty animationLengthProperty = serializedObject.FindProperty ("AnimationLength");
			float totalClipsTime = 0.0f;
			AnimatorState nextState = null;
			foreach (ChildAnimatorState childState in controllers.OriginalController.layers[0].stateMachine.states) {
				if (childState.state.name == animations[selectedAnimationIndex]) {
					nextState = childState.state;
					break;
				}
			}
			if (nextState == null) {
				foreach (ChildAnimatorStateMachine childStatemachine in controllers.OriginalController.layers[0].stateMachine.stateMachines) {
					foreach (ChildAnimatorState childState in childStatemachine.stateMachine.states) {
						if (childState.state.name == animations[selectedAnimationIndex]) {
							nextState = childState.state;
							break;
						}
					}
				}
			}
			while (nextState != null) {
				bool isComplexAnimation = false;
				foreach (StateMachineBehaviour behaviour in nextState.behaviours) {
					if (behaviour is RandomTransitionBehaviour) {
						nextState = null;
						break;
					} else if (behaviour is ComplexAnimation) {
						isComplexAnimation = true;
					}
				}
				if (!isComplexAnimation) {
					nextState = null;
				}
				if (nextState) {
					bool isProcess = false;
					foreach (StateMachineBehaviour behaviour in nextState.behaviours) {
						if (behaviour is AnimationForProcess) {
							isProcess = true;
							break;
						}
					}
					if (nextState.motion) {
						if (controllers.OverrideController) {
							totalClipsTime += controllers.OverrideController[nextState.motion.name].averageDuration / nextState.speed * (isProcess ? 0 : 1);
						} else {
							totalClipsTime += nextState.motion.averageDuration / nextState.speed * (isProcess ? 0 : 1);
						}
					}
					if (nextState.transitions.Length > 0) {
						nextState = nextState.transitions[0].destinationState;
					} else {
						nextState = null;
					}
				}
			}
			animationLengthProperty.floatValue = totalClipsTime;
		}

		stateNameProperty.stringValue = animations [selectedAnimationIndex];

		EditorGUILayout.PropertyField(serializedObject.FindProperty ("SingleTask"));

		SerializedProperty tasksProperty = serializedObject.FindProperty ("_Tasks");
		EditorGUILayout.BeginHorizontal();
		//EditorGUILayout.LabelField("Inspector", EditorStyles.boldLabel);
		tasksProperty.isExpanded = EditorGUILayout.Foldout(tasksProperty.isExpanded, "Tasks");
		if (GUILayout.Button (new GUIContent ("C", "Copy tasks"), GUILayout.Width(30))) {
			CopyTasks();
		}
		if (GUILayout.Button (new GUIContent ("P", "Paste tasks"), GUILayout.Width(30))) {
			PasteTasks();
		}
		if (GUILayout.Button (new GUIContent ("+", "Add task"), GUILayout.Width(30))) {
			tasksProperty.arraySize++;
		}
		EditorGUILayout.EndHorizontal();
		if (tasksProperty.isExpanded) {
			ShowTasks(tasksProperty);
		}

		EditorGUILayout.PropertyField(serializedObject.FindProperty ("_NextStateDescriptions"), true);

		serializedObject.ApplyModifiedProperties ();
	}

	static public void SelectState(SerializedProperty stateProperty, GameObject target, Rect position = default(Rect), string label = "") {
		if (target != null) {
			List<LevelObjectState> states = new List<LevelObjectState> (target.GetComponents<LevelObjectState> ());
			states.Insert(0, null);
			List<string> stateNames = new List<string> () {"None"};
			int selectedStateIndex = 0;

			for (int i = 1; i < states.Count; i++) {
				LevelObjectState state = states[i];
				if (state.StateName == stateProperty.stringValue) {
					selectedStateIndex = i;
				}
				stateNames.Add (state.StateName);
			}
			int newTargetStateIndex = -1;
			if (position != default(Rect)) {
				newTargetStateIndex = EditorGUI.Popup (position, label, selectedStateIndex, stateNames.ToArray());
			} else {
				newTargetStateIndex = EditorGUILayout.Popup (selectedStateIndex, stateNames.ToArray());
			}
			if (newTargetStateIndex != selectedStateIndex) {
				stateProperty.stringValue = stateNames[newTargetStateIndex];
			}
		}
	}

	private void ShowTasks(SerializedProperty tasksProperty) {
		EditorGUI.indentLevel++;
		for (int i = 0; i < tasksProperty.arraySize; i++) {
			SerializedProperty taskProperty = tasksProperty.GetArrayElementAtIndex (i);
			EditorGUILayout.BeginHorizontal();
			string taskName = taskProperty.FindPropertyRelative("TargetStateDescription._StateName").stringValue;
			if (taskName == null || taskName == "") {
				taskName = "Unassigned";
			}
			taskProperty.isExpanded = EditorGUILayout.Foldout(taskProperty.isExpanded, taskName);
			bool deleted = false;
			if (GUILayout.Button (new GUIContent ("-", "Remove task"), GUILayout.Width(30))) {
				tasksProperty.DeleteArrayElementAtIndex (i);
				deleted = true;
			}
			if (i > 0 && GUILayout.Button (new GUIContent ("↑", "Move task up"), GUILayout.Width(30))) {
				tasksProperty.MoveArrayElement(i, i - 1);
			}
			if (i < tasksProperty.arraySize - 1 && GUILayout.Button (new GUIContent ("↓", "Move task down"), GUILayout.Width(30))) {
				tasksProperty.MoveArrayElement(i, i + 1);
			}
			EditorGUILayout.EndHorizontal();
			if (!deleted && taskProperty.isExpanded) {
				EditorGUI.indentLevel++;
				LocalizedTextInspector.ShowLocalizedKeyInspector(taskProperty.FindPropertyRelative("TaskName"), ref TaskNameChanged);
				EditorGUILayout.PropertyField(taskProperty.FindPropertyRelative("TaskIcon"));
				EditorGUILayout.PropertyField(taskProperty.FindPropertyRelative("TooltipType"));
				EditorGUILayout.PropertyField(taskProperty.FindPropertyRelative("TargetStateDescription"), true);
				EditorGUILayout.PropertyField(taskProperty.FindPropertyRelative("TaskCosts"), true);
				EditorGUI.indentLevel--;
			}
		}
		EditorGUI.indentLevel--;
	}

	private void ShowTaskCosts(Task task, SerializedProperty taskCostsProperty) {
		EditorGUI.indentLevel++;

		List<TaskCost> availableTaskCosts = ReflectiveEnumerator.GetEnumerableOfType<TaskCost>() as List<TaskCost>;
		availableTaskCosts.Insert(0, null);
		List<string> taskCostsNames = new List<string>() {"None"};
		int selectedTaskCostIndex = 0;
		Type currentTaskCostType = null;

		for (int i = 1; i < availableTaskCosts.Count; i++) {
			Type triggerType = availableTaskCosts[i].GetType();
			taskCostsNames.Add(triggerType.ToString());
		}

		for (int i = 0; i < taskCostsProperty.arraySize; i++) {
			if (task.GetCurrentCosts()[i] != null) {
				currentTaskCostType = task.GetCurrentCosts()[i].GetType();
			}

			for (int j = 1; j < availableTaskCosts.Count; j++) {
				if (availableTaskCosts[j].GetType() == currentTaskCostType) {
					selectedTaskCostIndex = j;
					break;
				}
			}

			EditorGUILayout.BeginHorizontal();
			taskCostsProperty.isExpanded = EditorGUILayout.Foldout(taskCostsProperty.isExpanded, currentTaskCostType != null ? currentTaskCostType.Name : "Unassigned");
			bool deleted = false;
			if (GUILayout.Button (new GUIContent ("-", "Remove task cost"), GUILayout.Width(30))) {
				task.GetCurrentCosts()[i] = null;
				taskCostsProperty.DeleteArrayElementAtIndex (i);
				deleted = true;
			}
			EditorGUILayout.EndHorizontal();
			if (!deleted && taskCostsProperty.isExpanded) {
				EditorGUI.indentLevel++;
				EditorGUILayout.LabelField("Task Cost Type:");
				int newSelectedTaskCostIndex = EditorGUILayout.Popup(selectedTaskCostIndex, taskCostsNames.ToArray());
				if (newSelectedTaskCostIndex != selectedTaskCostIndex) {
					if (newSelectedTaskCostIndex == 0) {
						task.GetCurrentCosts()[i] = null;
					} else {
						currentTaskCostType = availableTaskCosts[newSelectedTaskCostIndex].GetType();
						task.GetCurrentCosts()[i] = (TaskCost)Activator.CreateInstance(currentTaskCostType);
					}
				}
				EditorGUILayout.PropertyField(taskCostsProperty.GetArrayElementAtIndex(i));
//ShowTriggerEditor(taskCostsProperty.objectReferenceValue as Trigger);
				EditorGUI.indentLevel--;
			}
		}
		EditorGUI.indentLevel--;
	}

	[MenuItem("TimeManager/Generate animations %#g")]
	static public void GenerateAnimations() {
		if (Selection.activeGameObject != null) {
			Animator animator = Selection.activeGameObject.GetComponent<Animator>();
			string assetPath = AssetDatabase.GetAssetPath(animator.runtimeAnimatorController);

			AnimatorController controller = AssetDatabase.LoadAssetAtPath<AnimatorController>(assetPath);
			if (controller != null) {
				foreach (ChildAnimatorState childState in controller.layers[0].stateMachine.states) {
					childState.state.motion = CreateAnimation(Selection.activeGameObject, Path.GetDirectoryName(assetPath), childState.state.name);
				}
				foreach (ChildAnimatorStateMachine childStatemachine in controller.layers[0].stateMachine.stateMachines) {
					foreach (ChildAnimatorState childState in childStatemachine.stateMachine.states) {
						childState.state.motion = CreateAnimation(Selection.activeGameObject, Path.GetDirectoryName(assetPath), childState.state.name);
					}
				}
			} else {
				AnimatorOverrideController overrideController = AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(assetPath);
				foreach (AnimationClip clip in overrideController.animationClips) {
					overrideController[clip.name] = CreateAnimation(Selection.activeGameObject, Path.GetDirectoryName(assetPath), clip.name);
				}
			}
		}
	}

	static AnimationClip CreateAnimation(GameObject gameObject, string path, string filename) {
		AnimationClip existingClip = (AnimationClip)AssetDatabase.LoadAssetAtPath(path + "/" + filename + ".anim", typeof(AnimationClip));
		if (existingClip) {
			return existingClip;
		}

		AnimationClip clip = new AnimationClip();
		clip.frameRate = 30.0f;
		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer sprite in sprites) {
			if (sprite.gameObject.activeSelf) {
				ObjectReferenceKeyframe keyframe = new ObjectReferenceKeyframe();
				keyframe.time = 0.0f;
				keyframe.value = null;
				AnimationUtility.SetObjectReferenceCurve(clip, EditorCurveBinding.PPtrCurve(sprite.gameObject.name, typeof(SpriteRenderer), "m_Sprite"), new ObjectReferenceKeyframe[]{keyframe});
			}
		}

		AssetDatabase.CreateAsset(clip, path + "/" + filename + ".anim");
		AssetDatabase.SaveAssets();

		return clip;
	}

	static public AnimatorControllers GetAnimatorControllers(Animator animator) {
		AnimatorControllers result = new AnimatorControllers();
		if (animator != null) {
			string assetPath = AssetDatabase.GetAssetPath(animator.runtimeAnimatorController);
			AnimatorController controller = AssetDatabase.LoadAssetAtPath<AnimatorController>(assetPath);
			if (controller != null) {
				result.OriginalController = controller;
				return result;
			}
			result.OverrideController = AssetDatabase.LoadAssetAtPath<AnimatorOverrideController>(assetPath);
			assetPath = AssetDatabase.GetAssetPath(result.OverrideController.runtimeAnimatorController);
			result.OriginalController = AssetDatabase.LoadAssetAtPath<AnimatorController>(assetPath);
		}
		return result;
	}

	static public List<string> GetAnimationsOfController(AnimatorController animatorController) {
		List<string> animations = new List<string> (){ };
		if (animatorController != null) {
			foreach (ChildAnimatorState childState in animatorController.layers[0].stateMachine.states) {
				animations.Add(childState.state.name);
			}
			foreach (ChildAnimatorStateMachine childStatemachine in animatorController.layers[0].stateMachine.stateMachines) {
				animations.Add(childStatemachine.stateMachine.name);
			}
		}
		return animations;
	}
}
