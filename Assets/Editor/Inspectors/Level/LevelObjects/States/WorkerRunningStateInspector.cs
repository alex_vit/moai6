using UnityEditor;
using UnityEngine;

public class WorkerRunningStateInspector : LevelObjectStateInspector {

	public override void OnInspectorGUI() {
		base.OnInspectorGUI();
		serializedObject.Update();
		EditorGUILayout.PropertyField(serializedObject.FindProperty ("BaseSpeed"));
		serializedObject.ApplyModifiedProperties ();
	}
}
