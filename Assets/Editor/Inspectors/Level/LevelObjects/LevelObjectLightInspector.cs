using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelObjectLight), true)]
public class LevelObjectLightInspector : Editor {
	public void OnSceneGUI()
	{
		LevelObjectLight light = (LevelObjectLight)target;
		Handles.color = Color.blue;
		Handles.CircleHandleCap(
				0,
				new Vector3(light.Light.Position.x, light.Light.Position.y, 0.0f),
				light.transform.rotation,
				light.Light.Radius,
				EventType.Repaint
		);
	}
}