﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Waypoint))]
public class WaypointInspector : Editor
{
	static private bool NeighborsDrawing;
	static private Waypoint LastNeighbor;

	protected virtual void OnSceneGUI()
	{
		Event eventCurrent = Event.current;
		if (!NeighborsDrawing) {
			if (eventCurrent.type == EventType.MouseUp)
			{
				((Waypoint)target).transform.position = Grid.AlignPositionOnGrid(((Waypoint)target).transform.position);
			}
		} else {
			Vector2 mousePos = eventCurrent.mousePosition;
			mousePos.y = Camera.current.pixelHeight - mousePos.y;
			Vector3 position = Grid.AlignPositionOnGrid(Camera.current.ScreenPointToRay(mousePos).origin);
			Handles.color = Color.green;
			Vector3[] positions = new Vector3[2];
			positions[0] = Selection.activeGameObject.transform.position;
			positions[1] = position;
			Handles.DrawAAPolyLine(3, positions);
			int controlID = GUIUtility.GetControlID (FocusType.Passive);
			switch (eventCurrent.GetTypeForControl (controlID)) {
				case EventType.MouseDown:
					GUIUtility.hotControl = controlID;
					eventCurrent.Use ();
					break;
				case EventType.MouseUp:
					AddNeighbor(position);
					GUIUtility.hotControl = 0;
					eventCurrent.Use();
					break;
				case EventType.MouseMove:
					GUIUtility.hotControl = controlID;
					eventCurrent.Use ();
					break;
				case EventType.KeyDown:
					StopDrawing();
					eventCurrent.Use ();
					break;
			}
		}
	}

	[MenuItem("TimeManager/Add waypoint between %#w")]
	static public void AddWaypoint() {
		if (Selection.gameObjects != null && Selection.gameObjects.Length == 2) {
			Waypoint waypoint1 = Selection.gameObjects[0].GetComponent<Waypoint>();
			Waypoint waypoint2 = Selection.gameObjects[1].GetComponent<Waypoint>();
			if (waypoint1 != null && waypoint2 != null) {
				if (waypoint1.Neighbors.Contains(waypoint2) && waypoint2.Neighbors.Contains(waypoint1)) {
					waypoint1.Neighbors.Remove(waypoint2);
					waypoint2.Neighbors.Remove(waypoint1);

					int maxIndex = 0;
					foreach (Transform child in waypoint1.transform.parent) {
						string[] nameSplit = child.name.Split('_');
						int curIndex = int.Parse(nameSplit[1]) + 1;
						maxIndex = Mathf.Max (maxIndex, curIndex);
					}

					Transform newPoint = (new GameObject()).transform;
					newPoint.position = Grid.AlignPositionOnGrid((waypoint1.transform.position + waypoint2.transform.position) / 2);
					Waypoint waypoint = newPoint.gameObject.AddComponent<Waypoint> ();
					if (waypoint1.RoadSprite == waypoint2.RoadSprite) {
						waypoint.RoadSprite = waypoint1.RoadSprite;
					}
					waypoint.Neighbors = new List<Waypoint>();
					newPoint.gameObject.name = "Waypoint_" + maxIndex;
					newPoint.SetParent (waypoint1.transform.parent);

					waypoint.Neighbors.Add(waypoint1);
					waypoint.Neighbors.Add(waypoint2);

					waypoint1.Neighbors.Add(waypoint);
					waypoint2.Neighbors.Add(waypoint);
				}
			}
		}
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();

		EditorGUILayout.PropertyField(serializedObject.FindProperty("_SpeedModifier"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("MultipleAnimationsIndex"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("RestrictToSpecificObjectsStates"), true);


		Road road = GameObject.FindObjectOfType<Road>();
		if (road != null) {
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("Road sprite:");

			SerializedProperty roadSprite = serializedObject.FindProperty ("RoadSprite");
			List<string> roadSprites = new List<string> () {};
			roadSprites.Add ("None");

			for (int i = 0; i < road.RoadSpritesCollection.RoadCollections.Count; i++) {
				roadSprites.Add (road.RoadSpritesCollection.RoadCollections[i].Name);
			}
			int newTargetStateIndex = EditorGUILayout.Popup (roadSprite.intValue + 1, roadSprites.ToArray()) - 1;
			if (newTargetStateIndex != roadSprite.intValue) {
				roadSprite.intValue = newTargetStateIndex;
			}
			EditorGUILayout.EndHorizontal();
		}
		if (NeighborsDrawing) {
			if (GUILayout.Button (new GUIContent ("Stop drawing", "Stop drawing"))) {
				StopDrawing();
			}
		} else {
			if (GUILayout.Button (new GUIContent ("Draw road from here", "Draw road from here"))) {
				NeighborsDrawing = true;
			}
		}

		if (LastNeighbor != null) {
			AddPoint(serializedObject.FindProperty("Neighbors"), LastNeighbor);
			LastNeighbor = null;
		}

		EditorGUILayout.PropertyField(serializedObject.FindProperty("Neighbors"), true);

		serializedObject.ApplyModifiedProperties ();
	}

	private void AddNeighbor(Vector3 position) {
		Transform currentDrawingPoint = Selection.activeGameObject.transform;
		int maxIndex = 0;
		foreach (Transform child in currentDrawingPoint.parent) {
			string[] nameSplit = child.name.Split('_');
			int curIndex = int.Parse(nameSplit[1]) + 1;
			maxIndex = Mathf.Max (maxIndex, curIndex);
		}
		Transform newPoint = (new GameObject()).transform;
		newPoint.position = position;
		Waypoint waypoint = newPoint.gameObject.AddComponent<Waypoint> ();
		waypoint.RoadSprite = ((Waypoint)target).RoadSprite;
		newPoint.gameObject.name = "Waypoint_" + maxIndex;
		newPoint.SetParent (currentDrawingPoint.parent);

		serializedObject.Update();

		AddPoint(serializedObject.FindProperty("Neighbors"), waypoint);
		LastNeighbor = (Waypoint)target;
		serializedObject.ApplyModifiedProperties();
		Selection.activeGameObject = newPoint.gameObject;
	}

	private void AddPoint(SerializedProperty neighborsArray, Waypoint waypoint) {
		neighborsArray.arraySize++;
		SerializedProperty newElement = neighborsArray.GetArrayElementAtIndex(neighborsArray.arraySize - 1);
		newElement.objectReferenceValue = waypoint;
	}

	private void StopDrawing() {
		NeighborsDrawing = false;
	}
}

