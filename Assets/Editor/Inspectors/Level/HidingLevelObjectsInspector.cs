using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(HidingLevelObjects), true)]
public class HidingLevelObjectsInspector : Editor {
	public void OnSceneGUI()
	{
		HidingLevelObjects hidingLevelObjects = (HidingLevelObjects)target;
		Handles.color = Color.blue;
		Handles.matrix = hidingLevelObjects.transform.localToWorldMatrix;
		foreach (HidingArea hidingArea in hidingLevelObjects.Areas) {
			Handles.CircleHandleCap(
					0,
					new Vector3(hidingArea.Position.x, hidingArea.Position.y, 0.0f),
					hidingLevelObjects.transform.rotation,
					hidingArea.Radius,
					EventType.Repaint
			);
		}
	}
}