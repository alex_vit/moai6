using UnityEditor;

[CustomEditor(typeof(Background), false)]
public class BackgroundInspector :Editor {

	public void Awake() {
		(this.target as Background).UpdateBackground();
	}

	public override void OnInspectorGUI() {
		EditorGUI.BeginChangeCheck();
		this.DrawDefaultInspector();
		if (EditorGUI.EndChangeCheck())
		{
			(this.target as Background).UpdateBackground();
		}
	}
}