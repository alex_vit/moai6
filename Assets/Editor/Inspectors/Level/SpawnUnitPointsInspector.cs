﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelUnitsSpawnPoints), true)]
public class SpawnUnitPointsInspector : TriggeredBehaviourInspector {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        serializedObject.Update();
        
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_Container"));
        CreateListForObject(serializedObject.FindProperty("_PossibleUnitsPrefabs"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_TimeBetweenSpawn"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_MaximumUnitsCountAtSameTime"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_TotalUnitsCount"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("IgnoreObstacles"));
        CreateListForObject(serializedObject.FindProperty("_SpawnWaypoints"));
        
        
        serializedObject.ApplyModifiedProperties();
    }

    private void CreateListForObject(SerializedProperty listObj)
    {
        EditorGUILayout.PropertyField(listObj);
        EditorGUI.indentLevel += 1;
        if (listObj.isExpanded) {
            EditorGUILayout.PropertyField(listObj.FindPropertyRelative("Array.size"));
            for (int i = 0; i < listObj.arraySize; i++) {
                EditorGUILayout.PropertyField(listObj.GetArrayElementAtIndex(i));
            }
        }
        EditorGUI.indentLevel -= 1;
    }
}
