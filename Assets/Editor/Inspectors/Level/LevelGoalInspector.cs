using UnityEditor;

[CustomEditor(typeof(LevelGoal), false)]
public class LevelGoalInspector : TriggeredBehaviourInspector {

	private bool DescriptionChanged;
	private bool GoalCounterChanged;

	override public void OnInspectorGUI() {
		serializedObject.Update();

		LocalizedTextInspector.ShowLocalizedKeyInspector(serializedObject.FindProperty("_GoalDescription"), ref DescriptionChanged);
		LocalizedTextInspector.ShowLocalizedKeyInspector(serializedObject.FindProperty("_GoalCounter"), ref GoalCounterChanged);

		serializedObject.ApplyModifiedProperties();

		base.OnInspectorGUI();
	}
}