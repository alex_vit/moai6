using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TriggeredAnimation), true)]
public class TriggeredAnimationInspector : TriggeredBehaviourInspector {
	public override void OnInspectorGUI() {
		serializedObject.Update();
		SerializedProperty animatorProperty = serializedObject.FindProperty ("_TargetAnimator");
		EditorGUILayout.PropertyField(animatorProperty);

		LevelObjectStateInspector.AnimatorControllers controllers = LevelObjectStateInspector.GetAnimatorControllers(animatorProperty.objectReferenceValue as Animator);
		List<string> animations = LevelObjectStateInspector.GetAnimationsOfController(controllers.OriginalController);
		animations.Insert (0,"None");

		SerializedProperty animationNameProperty = serializedObject.FindProperty ("_AnimationName");

		int selectedAnimationIndex = animations.IndexOf (animationNameProperty.stringValue);
		if (selectedAnimationIndex < 0) {
			selectedAnimationIndex = 0;
		}

		EditorGUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField ("Animation:");
		selectedAnimationIndex = EditorGUILayout.Popup (selectedAnimationIndex, animations.ToArray ());
		EditorGUILayout.EndHorizontal ();

		animationNameProperty.stringValue = animations [selectedAnimationIndex];

		serializedObject.ApplyModifiedProperties ();

		base.OnInspectorGUI();
	}
}