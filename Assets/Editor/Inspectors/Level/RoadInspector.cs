﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Road))]
public class RoadInspector : Editor
{
	private Transform WaypointContainer;
	private Transform RoadContainer;

	public override void OnInspectorGUI() {
		if (WaypointContainer == null) {
			GameObject waypointGameObject = GameObject.Find("WaypointContainer");
			GameObject roadGameObject = GameObject.Find("RoadContainer");
			if (waypointGameObject == null) {
				waypointGameObject = new GameObject();
				waypointGameObject.name = "WaypointContainer";
			}
			if (roadGameObject == null) {
				roadGameObject = new GameObject();
				roadGameObject.name = "RoadContainer";
			}
			WaypointContainer = waypointGameObject.transform;
			RoadContainer = roadGameObject.transform;
		}

		serializedObject.Update();

		SerializedProperty waypointContainerProperty = serializedObject.FindProperty("WaypointContainer");
		if (waypointContainerProperty.objectReferenceValue == null) {
			waypointContainerProperty.objectReferenceValue = WaypointContainer;
		}
		EditorGUILayout.PropertyField(serializedObject.FindProperty("RoadCollections"), true);
		EditorGUILayout.PropertyField(serializedObject.FindProperty("RoadSpritesCollection"), true);
		SerializedProperty startPointProp = serializedObject.FindProperty("StartPoint");

		EditorGUILayout.PropertyField(startPointProp);
		if (startPointProp.objectReferenceValue == null) {
			if (GUILayout.Button (new GUIContent ("Add start waypoint", "Add start waypoint"))) {
				Transform newPoint = (new GameObject()).transform;
				newPoint.position = Grid.AlignPositionOnGrid(newPoint.position);
				Waypoint waypoint = newPoint.gameObject.AddComponent<Waypoint> ();
				newPoint.gameObject.name = "WayPoint_0";
				newPoint.SetParent (WaypointContainer);
				startPointProp.objectReferenceValue = waypoint;
			}
		}

		if (GUILayout.Button (new GUIContent ("Draw road", "Draw road"))) {
			while(RoadContainer.childCount > 0)
			{
				GameObject.DestroyImmediate(RoadContainer.GetChild(0).gameObject);
			}

			List<Waypoint> VisitedWaypoints = new List<Waypoint>() {};

			foreach (Waypoint waypoint in WaypointContainer.gameObject.GetComponentsInChildren<Waypoint>()) {
				if (waypoint.RoadSprite >= 0) {
					WaypointNeighborType waypointType = 0;
					foreach (Waypoint neighbor in waypoint.Neighbors) {
						if (neighbor.RoadSprite == waypoint.RoadSprite) {
							WaypointNeighborType neighborType = GetNeighborType(waypoint, neighbor);
							waypointType = waypointType | neighborType;
							if (!VisitedWaypoints.Contains(neighbor)) {
								DrawRoadToNeighbor(neighborType, waypoint, neighbor);
							}
						}
					}
					VisitedWaypoints.Add(waypoint);
					CreateRoadSprite(waypoint.RoadSprite, waypointType, waypoint.transform.position);
				}
			}
		}

		serializedObject.ApplyModifiedProperties();
	}

	private WaypointNeighborType GetNeighborType(Waypoint waypoint, Waypoint neighbour) {
		if (waypoint.transform.position.y < neighbour.transform.position.y) {
			if (waypoint.transform.position.x > neighbour.transform.position.x) {
				return WaypointNeighborType.Left;
			} else {
				return WaypointNeighborType.Up;
			}
		} else {
			if (waypoint.transform.position.x > neighbour.transform.position.x) {
				return WaypointNeighborType.Down;
			} else {
				return WaypointNeighborType.Right;
			}
		}
	}

	private void DrawRoadToNeighbor(WaypointNeighborType type, Waypoint waypoint, Waypoint neighbour) {
		Vector3 direction = new Vector3(0.0f, 0.0f, 0.0f);
		if (type == WaypointNeighborType.Right || type == WaypointNeighborType.Left) {
			direction = (type == WaypointNeighborType.Right ? 1 : -1) * Grid.CellDiffX;
			type = WaypointNeighborType.Right | WaypointNeighborType.Left;
		} else {
			direction = (type == WaypointNeighborType.Up ? -1 : 1) * Grid.CellDiffY;
			type = WaypointNeighborType.Up | WaypointNeighborType.Down;
		}
		int cellsCount = Mathf.FloorToInt(Mathf.Abs((waypoint.transform.position.x - neighbour.transform.position.x) / direction.x) + 0.01f);
		for (int i = 1; i < cellsCount; i++) {
			CreateRoadSprite(waypoint.RoadSprite, type, waypoint.transform.position + i * direction);
		}
	}

	private void CreateRoadSprite(int spriteIndex, WaypointNeighborType type, Vector3 position) {
		Road roadManager = (Road)target;
		Transform newPoint = (new GameObject()).transform;
		newPoint.SetParent (RoadContainer);
		SpriteRenderer sprite = newPoint.gameObject.AddComponent<SpriteRenderer> ();

		WaypointNeighborType allTypes = WaypointNeighborType.Up | WaypointNeighborType.Down | WaypointNeighborType.Left | WaypointNeighborType.Right;
		List<Sprite> sprites = new List<Sprite>(){};
		foreach (RoadSprite roadSprite in roadManager.RoadSpritesCollection.RoadCollections[spriteIndex].RoadSprites) {
			if (roadSprite.Types == type || (allTypes == type && (int)roadSprite.Types == -1)) {
				sprites.Add(roadSprite.Sprite);
			}
		}
		sprite.sprite = sprites[UnityEngine.Random.Range(0, sprites.Count)];
		newPoint.transform.position = new Vector3(position.x + 0.3f, position.y - 0.5f, 0.0f);
		newPoint.gameObject.name = "Road";
	}
}