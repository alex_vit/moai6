using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CollidersCaching : Editor {
	[MenuItem ("TimeManager/Cache all colliders")]
	public static void Migrate() {
		AssetDatabase.Refresh();
		string[] prefabs = AssetDatabase.FindAssets("t:prefab");
		int cachedObjects = 0;
		foreach (string prefabGUID in prefabs) {
			string assetPath = AssetDatabase.GUIDToAssetPath(prefabGUID);
			GameObject gameObject = (GameObject)AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject));
			LevelObject levelObject = gameObject.GetComponent<LevelObject>();
			if (levelObject != null) {
				GameObject instance = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
				levelObject = instance.GetComponent<LevelObject>();
				GameObject trueInstance = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
				LevelObject trueLevelObject = trueInstance.GetComponent<LevelObject>();
				List<CachedCollider> levelObjectColliders = new List<CachedCollider>(instance.GetComponentsInChildren<CachedCollider>(true));
				List<CachedCollider> trueLevelObjectColliders = new List<CachedCollider>(trueInstance.GetComponentsInChildren<CachedCollider>(true));
				CompositeCollider2D objectCollider = trueInstance.GetComponent<CompositeCollider2D>();
				if (objectCollider == null) {
					objectCollider = trueInstance.AddComponent<CompositeCollider2D>();
					objectCollider.geometryType = CompositeCollider2D.GeometryType.Polygons;
					objectCollider.vertexDistance = 0.1f;
					trueInstance.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
				}
				trueLevelObject.SetCollider(objectCollider);
				if (levelObjectColliders.Count > 0) {
					LevelObjectState currentState = null;
					List<LevelObjectState> states = new List<LevelObjectState>(levelObject.GetComponents<LevelObjectState>());
					foreach (CachedCollider levelObjectCollider in trueLevelObjectColliders) {
						levelObjectCollider.GetPathsBySprite().Clear();
					}
					foreach (LevelObjectState levelObjectState in states) {
						bool stateHasAnimation = LevelObjectInspector.SampleState(levelObject, levelObjectState.StateName);
						if (stateHasAnimation) {
							foreach (CachedCollider levelObjectCollider in levelObjectColliders) {
								CachedCollider trueLevelObjectCollider = null;
								foreach (CachedCollider colliderInTrue in trueLevelObjectColliders) {
									if (colliderInTrue.name == levelObjectCollider.name) {
										trueLevelObjectCollider = colliderInTrue;
										break;
									}
								}
								PolygonCollider2D trueLevelObjectCollider2d = trueLevelObjectCollider.GetComponent<PolygonCollider2D>();
								if (trueLevelObjectCollider2d == null) {
									trueLevelObjectCollider2d = trueLevelObjectCollider.gameObject.AddComponent<PolygonCollider2D>();
								}
								trueLevelObjectCollider2d.pathCount = 0;
								trueLevelObjectCollider2d.usedByComposite = true;
								trueLevelObjectCollider.SetCollider(trueLevelObjectCollider2d);

								if (trueLevelObjectCollider.GetPathsBySprite() == null) {
									trueLevelObjectCollider.SetPathsBySprite(new ColliderPathsBySprite());
								}
								trueLevelObjectCollider.SetSpriteRenderer(trueLevelObjectCollider.GetComponent<SpriteRenderer>());
								PolygonCollider2D tempCollider = levelObjectCollider.gameObject.AddComponent<PolygonCollider2D>();
								ColliderPath colliderPath = new ColliderPath();
								for (int i = 0; i < tempCollider.pathCount; i++) {
									Vector2[] points = tempCollider.GetPath(i);
									for (int j = 0; j <points.Length; j++) {
										Vector2 point = points[j];
										points[j] = new Vector2(point.x, point.y);
									}
									ColliderPathPoints colliderPathPoints = new ColliderPathPoints(points);
									colliderPath.AddPathPoints(colliderPathPoints);
								}
								if (levelObjectCollider.GetComponent<SpriteRenderer>() != null) {
									if (levelObjectCollider.GetComponent<SpriteRenderer>().sprite != null) {
										trueLevelObjectCollider.GetPathsBySprite().SetPathForSprite(levelObjectCollider.GetComponent<SpriteRenderer>().sprite.name, colliderPath);
									}
								} else {
									DestroyImmediate(trueLevelObjectCollider);
								}
								DestroyImmediate(tempCollider);
							}
						}
						if (levelObjectState.StateName == levelObject.CurrentStateName) {
							currentState = levelObjectState;
						}
					}
					PrefabUtility.ReplacePrefab(trueInstance, gameObject);
				} else if (objectCollider != null) {
					DestroyImmediate(objectCollider);
					PrefabUtility.ReplacePrefab(trueInstance, gameObject);
				}
				DestroyImmediate(trueInstance);
				DestroyImmediate(instance);
				cachedObjects++;
			}
		}
		Debug.Log("Total cached objects: " + cachedObjects);
		AssetDatabase.SaveAssets();
	}
}