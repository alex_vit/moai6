﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SelectedObjectsCollidersCaching : Editor
{
	[MenuItem("TimeManager/Cache selected colliders")]
	private static void ModifySelectedObjects()
	{
		GameObject[] _selectedObjects = Selection.gameObjects;

		if (_selectedObjects != null && _selectedObjects.Length > 0)
		{
			foreach (var go in _selectedObjects)
				CacheSelectedColliders(go);
		}
	}

	private static void CacheSelectedColliders(GameObject go)
	{
		GameObject _gameObject = (GameObject) PrefabUtility.GetPrefabParent(go);

		if (_gameObject == null)
			Debug.LogError("Object needs prefab for this operation");
		else
		{
			LevelObject _levelObject = _gameObject.GetComponent<LevelObject>();
			if (_levelObject != null)
			{
				GameObject instance = PrefabUtility.InstantiatePrefab(_gameObject) as GameObject;
				_levelObject = instance.GetComponent<LevelObject>();
				GameObject trueInstance = PrefabUtility.InstantiatePrefab(_gameObject) as GameObject;
				LevelObject trueLevelObject = trueInstance.GetComponent<LevelObject>();
				List<CachedCollider> levelObjectColliders = new List<CachedCollider>(instance.GetComponentsInChildren<CachedCollider>(true));
				List<CachedCollider> trueLevelObjectColliders = new List<CachedCollider>(trueInstance.GetComponentsInChildren<CachedCollider>(true));
				CompositeCollider2D objectCollider = trueInstance.GetComponent<CompositeCollider2D>();
				if (objectCollider == null) {
					objectCollider = trueInstance.AddComponent<CompositeCollider2D>();
					objectCollider.geometryType = CompositeCollider2D.GeometryType.Polygons;
					objectCollider.vertexDistance = 0.1f;
					trueInstance.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
				}
				trueLevelObject.SetCollider(objectCollider);
				if (levelObjectColliders.Count > 0) {
					LevelObjectState currentState = null;
					List<LevelObjectState> states = new List<LevelObjectState>(_levelObject.GetComponents<LevelObjectState>());
					foreach (CachedCollider levelObjectCollider in trueLevelObjectColliders) {
						levelObjectCollider.GetPathsBySprite().Clear();
					}
					foreach (LevelObjectState levelObjectState in states) {
						bool stateHasAnimation = LevelObjectInspector.SampleState(_levelObject, levelObjectState.StateName);
						if (stateHasAnimation) {
							foreach (CachedCollider levelObjectCollider in levelObjectColliders) {
								CachedCollider trueLevelObjectCollider = null;
								foreach (CachedCollider colliderInTrue in trueLevelObjectColliders) {
									if (colliderInTrue.name == levelObjectCollider.name) {
										trueLevelObjectCollider = colliderInTrue;
										break;
									}
								}
								PolygonCollider2D trueLevelObjectCollider2d = trueLevelObjectCollider.GetComponent<PolygonCollider2D>();
								if (trueLevelObjectCollider2d == null) {
									trueLevelObjectCollider2d = trueLevelObjectCollider.gameObject.AddComponent<PolygonCollider2D>();
								}
								trueLevelObjectCollider2d.pathCount = 0;
								trueLevelObjectCollider2d.usedByComposite = true;
								trueLevelObjectCollider.SetCollider(trueLevelObjectCollider2d);

								if (trueLevelObjectCollider.GetPathsBySprite() == null) {
									trueLevelObjectCollider.SetPathsBySprite(new ColliderPathsBySprite());
								}
								trueLevelObjectCollider.SetSpriteRenderer(trueLevelObjectCollider.GetComponent<SpriteRenderer>());
								PolygonCollider2D tempCollider = levelObjectCollider.gameObject.AddComponent<PolygonCollider2D>();
								ColliderPath colliderPath = new ColliderPath();
								for (int i = 0; i < tempCollider.pathCount; i++) {
									Vector2[] points = tempCollider.GetPath(i);
									for (int j = 0; j <points.Length; j++) {
										Vector2 point = points[j];
										points[j] = new Vector2(point.x, point.y);
									}
									ColliderPathPoints colliderPathPoints = new ColliderPathPoints(points);
									colliderPath.AddPathPoints(colliderPathPoints);
								}
								if (levelObjectCollider.GetComponent<SpriteRenderer>() != null) {
									if (levelObjectCollider.GetComponent<SpriteRenderer>().sprite != null) {
										trueLevelObjectCollider.GetPathsBySprite().SetPathForSprite(levelObjectCollider.GetComponent<SpriteRenderer>().sprite.name, colliderPath);
									}
								} else {
									DestroyImmediate(trueLevelObjectCollider);
								}
								DestroyImmediate(tempCollider);
							}
						}
						if (levelObjectState.StateName == _levelObject.CurrentStateName) {
							currentState = levelObjectState;
						}
					}
					PrefabUtility.ReplacePrefab(trueInstance, _gameObject);
				} else if (objectCollider != null) {
					DestroyImmediate(objectCollider);
					PrefabUtility.ReplacePrefab(trueInstance, _gameObject);
				}
				DestroyImmediate(trueInstance);
				DestroyImmediate(instance);
			}
		}
	}
}
