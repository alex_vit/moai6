using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Migrations :Editor {
	/*[MenuItem ("TimeManager/Migrations/Migrate current state name")]
	public static void MigrateCurrentStateName() {
		AssetDatabase.Refresh();
		string[] prefabs = AssetDatabase.FindAssets("t:prefab");
		int migratedCount = 0;
		foreach (string prefabGUID in prefabs) {
			GameObject gameObject = (GameObject)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(prefabGUID), typeof(GameObject));
			LevelObject levelObject = gameObject.GetComponent<LevelObject>();
			if (levelObject != null) {
				GameObject instance = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
				levelObject = instance.GetComponent<LevelObject>();
				BaseStateDescription stateDescription = new BaseStateDescription();
				stateDescription.SetStateName(levelObject.CurrentStateName);
				levelObject.SetCurrentStateDescription(stateDescription);
				PrefabUtility.ReplacePrefab(instance, gameObject);
				DestroyImmediate(instance);
				migratedCount++;
			}
		}
		Debug.Log("Total migrated: " + migratedCount);
		AssetDatabase.SaveAssets();
	}

	[MenuItem ("TimeManager/Migrations/Migrate next states")]
	public static void MigrateNextStates() {
		AssetDatabase.Refresh();
		string[] prefabs = AssetDatabase.FindAssets("t:prefab");
		int migratedCount = 0;
		foreach (string prefabGUID in prefabs) {
			GameObject gameObject = (GameObject)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(prefabGUID), typeof(GameObject));
			LevelObject levelObject = gameObject.GetComponent<LevelObject>();
			if (levelObject != null) {
				GameObject instance = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
				LevelObjectState[] states = instance.GetComponents<LevelObjectState>();
				foreach (LevelObjectState levelObjectState in states) {
					List<ExtendedStateDescription> stateDescriptions = new List<ExtendedStateDescription>();
					List<LevelObjectState.NextState> nextStates = levelObjectState.GetNextStates;
					foreach (LevelObjectState.NextState nextState in nextStates) {
						ExtendedStateDescription stateDescription = new ExtendedStateDescription();
						stateDescription.SetOwnState(nextState.NextPrefab == null);
						if (!stateDescription.IsOwnState()) {
							stateDescription.SetLevelObject(nextState.NextPrefab);
						}
						stateDescription.SetStateName(nextState.NextStateName);
						stateDescriptions.Add(stateDescription);
					}
					levelObjectState.NextStateDescriptions = stateDescriptions;
				}
				PrefabUtility.ReplacePrefab(instance, gameObject);
				DestroyImmediate(instance);
				migratedCount++;
			}
		}
		Debug.Log("Total migrated: " + migratedCount);
		AssetDatabase.SaveAssets();
	}*/

	/*[MenuItem ("TimeManager/Migrations/Migrate tasks target states")]
	public static void MigrateNextStates() {
		AssetDatabase.Refresh();
		string[] prefabs = AssetDatabase.FindAssets("t:prefab");
		int migratedCount = 0;
		foreach (string prefabGUID in prefabs) {
			GameObject gameObject = (GameObject)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(prefabGUID), typeof(GameObject));
			LevelObject levelObject = gameObject.GetComponent<LevelObject>();
			if (levelObject != null) {
				GameObject instance = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
				LevelObjectState[] states = instance.GetComponents<LevelObjectState>();
				foreach (LevelObjectState levelObjectState in states) {
					foreach (Task task in levelObjectState.Tasks) {
						BaseStateDescription stateDescription = new BaseStateDescription();
						stateDescription.SetStateName(task.TargetStateName);
						task.TargetStateDescription = stateDescription;
					}
				}
				PrefabUtility.ReplacePrefab(instance, gameObject);
				DestroyImmediate(instance);
				migratedCount++;
			}
		}
		Debug.Log("Total migrated: " + migratedCount);
		AssetDatabase.SaveAssets();
	}*/

	[MenuItem ("TimeManager/Migrations/Build additional elements")]
	public static void Migrate() {
		AssetDatabase.Refresh();
		string[] prefabs = AssetDatabase.FindAssets("t:prefab");
		int migratedCount = 0;
		foreach (string prefabGUID in prefabs) {
			string assetPath = AssetDatabase.GUIDToAssetPath(prefabGUID);
			GameObject gameObject = (GameObject)AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject));
			LevelObject levelObject = gameObject.GetComponent<LevelObject>();
			if (levelObject != null) {
				if (assetPath.Contains("Trees")) {
					GameObject instance = PrefabUtility.InstantiatePrefab(gameObject) as GameObject;
					levelObject = instance.GetComponent<LevelObject>();
					//levelObject._Selector = new AdditionalLevelObjectElement();
					//levelObject._Selector._ElementPrefab = Selection.gameObjects[0];

					PrefabUtility.ReplacePrefab(instance, gameObject);
					DestroyImmediate(instance);
					migratedCount++;
				}
			}
		}
		Debug.Log("Total migrated: " + migratedCount);
		AssetDatabase.SaveAssets();
	}
}