using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEngine;

public static class ReflectiveEnumerator
{
	static ReflectiveEnumerator() { }

	public static IEnumerable<T> GetEnumerableOfType<T>(params object[] constructorArgs) where T : class
	{
		List<T> objects = new List<T>();
		foreach (Type type in
		Assembly.GetAssembly(typeof(T)).GetTypes().Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T))))
		{
			if (type.IsSubclassOf(typeof(ScriptableObject))) {
				objects.Add(ScriptableObject.CreateInstance(type) as T);
			} else {
				objects.Add(Activator.CreateInstance(type) as T);
			}
		}
		return objects;
	}
}