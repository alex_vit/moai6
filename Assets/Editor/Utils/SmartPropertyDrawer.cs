using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SmartPropertyDrawer : PropertyDrawer {
	protected float PropertyHeight;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		Draw(position, property, label, false);
	}

	protected virtual void DrawProperties(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
	}

	protected SerializedProperty DrawProperty(Rect position, SerializedProperty propertyOwner, string propertyName, out Rect newPosition, bool simulateDraw) {
		PropertyHeight += 18.0f;
		SerializedProperty property = propertyOwner.FindPropertyRelative(propertyName);
		if (!simulateDraw) {
			EditorGUI.PropertyField(position, property, new GUIContent(property.displayName), true);
		}
		newPosition = new Rect(position.x, position.y + 18, position.width, position.height);
		return property;
	}

	protected void DrawPopup(Rect position, SerializedProperty propertyOwner, string propertyName, string[] variants, out Rect newPosition, bool simulateDraw) {
		PropertyHeight += 18.0f;
		SerializedProperty property = propertyOwner.FindPropertyRelative(propertyName);
		if (!simulateDraw) {
			int newIndex = -1;
			int selectedIndex = -1;
			if (property.propertyType == SerializedPropertyType.String) {
				for (int i = 0; i < variants.Length; i++) {
					if (property.stringValue == variants[i]) {
						selectedIndex = i;
						break;
					}
				}
			} else if (property.propertyType == SerializedPropertyType.Integer) {
				selectedIndex = property.intValue;
			}

			newIndex = EditorGUI.Popup(new Rect(position.x, position.y, position.width, 18.0f), property.displayName, selectedIndex, variants);
			if (newIndex != selectedIndex) {
				if (property.propertyType == SerializedPropertyType.String) {
					property.stringValue = variants[newIndex];
				} else if (property.propertyType == SerializedPropertyType.Integer) {
					property.intValue = newIndex;
				}
			}
		}
		newPosition = new Rect(position.x, position.y + 18.0f, position.width, position.height);
	}

	protected void DrawArrayAsEnum(Rect position, SerializedProperty propertyOwner, string propertyName, string[] variants, out Rect newPosition, bool simulateDraw) {
		PropertyHeight += 18.0f;
		SerializedProperty property = propertyOwner.FindPropertyRelative(propertyName);
		if (!simulateDraw) {
			int newMask = 0;
			int selectedMask = 0;
			for (int i = 0; i < variants.Length; i++) {
				for (int j = 0; j < property.arraySize; j++) {
					if (property.GetArrayElementAtIndex(j).stringValue == variants[i]) {
						selectedMask |= Mathf.RoundToInt(Mathf.Pow(2, i));
						break;
					}
				}
			}
			newMask = EditorGUI.MaskField(new Rect(position.x, position.y, position.width, 18.0f), property.displayName, selectedMask, variants);
			if (newMask != selectedMask) {
				property.arraySize = 0;
				for (int i = 0; i < variants.Length; i++) {
					if ((newMask & Mathf.RoundToInt(Mathf.Pow(2, i))) != 0) {
						property.InsertArrayElementAtIndex(0);
						property.GetArrayElementAtIndex(0).stringValue = variants[i];
					}
				}
			}
		}
		newPosition = new Rect(position.x, position.y + 18, position.width, position.height);
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		SimulateDraw(property, label);
		return PropertyHeight;
	}

	protected virtual void Draw(Rect position, SerializedProperty property, GUIContent label, bool simulateDraw) {
		PropertyHeight = 18.0f;
		Rect newPosition = new Rect(position);
		if (!simulateDraw) {
			property.isExpanded = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, 18.0f), property.isExpanded, property.displayName);
			newPosition = new Rect(position.x, position.y + 18, position.width, position.height);
		}
		if (property.isExpanded) {
			EditorGUI.indentLevel++;
			DrawProperties(newPosition, property, label, simulateDraw);
			EditorGUI.indentLevel--;
		}
	}

	private void SimulateDraw(SerializedProperty property, GUIContent label) {
		Draw(new Rect(0.0f, 0.0f, 0.0f, 0.0f), property, label, true);
	}
}